<div id="page-wrapper" class="gray-bg dashbard-1">
    <?php
    $url = $this->uri->segment_array();
    $segment = $this->uri->segment(3);
    breadcrumbs(array(uri_string() => 'Mapped Retailers'));
    ?>
    <div class="row border-bottom">
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <?php print_flash_message(); ?>
                <div class="ibox float-e-margins">

                    <div class="ibox-title" style="margin-bottom: 20px;">
                        <?php echo form_open($this->uri->uri_string(), array('method' => 'get')); ?>
                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group formWidht">
                                    <label>Keyword</label>
                                    <input type="text" placeholder="Name/Store Name" name="name" id="exampleInputEmail2" class="form-control formWidht" value="<?php echo isset($_GET['name']) ? $_GET['name'] : ''; ?>">
                                </div>                                
                            </div>

                            <div class="col-md-3">
                                <div class="form-group formWidht">
                                    <label>State</label>
                                    <?php
                                    $state_id = isset($_GET['state_id']) ? $_GET['state_id'] : '';
                                    state_dropdown($state_id, 'form-control');
                                    ?>                                     
                                </div>
                            </div>

                            

                            <div class="col-md-2 filter-div">
                                <input type="submit" name="filter" id="" class="btn btn-success" value="Filter">
                                <?php
                                if (isset($_GET['filter'])):
                                    ?>                                
                                    <a href="<?php echo base_url('admin/users/mapped_retailer'); ?>" class="btn btn-warning">Reset</a>
                                <?php endif; ?>
                            </div>

                        </div>
                        <?php echo form_close(); ?>
                    </div>

                    <div class="ibox-title">
                        <h2>View Mapped Retailers</h2>
						<span class="pull-right">
                            <a data-toggle="tooltip" title="Download Excel" href="<?php
                            $type = 'mapped';
                            echo base_url('admin/users/export_users?type=' . $type . '&') . http_build_query($_GET);
                            ?>" class="">
                                <img src="<?php echo base_url('assets/images/exldwn.ico'); ?>" height="40" width="40">
                            </a>
                        </span>
                    </div>
                    <div class="ibox-content borderNone">
                        <div class="table-responsive">
                            <?php if (!empty($result)): ?>
                                <?php echo form_open($this->uri->uri_string(), array('class' => "form-horizontal", 'id' => 'cat-listing', 'method' => 'get')); ?>
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><input type="checkbox"  class="i-checks check-all" name="check_all"></th>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Role</th>
											<th>Store Name</th>
											<th>Licence</th>
                                            <th>State</th>
                                            <th>Mapped On</th>
											<th>Status</th>
                                            <th>Address</th>
                                           
                                        </tr>
                                    </thead>                                
                                    <tbody>
                                        <?php  foreach ($result as $val): ?>

                                            <tr <?php if ($val->status == 8) { ?>style='background-color:#ff9494;'<?php } ?>>
                                                <td class="text-center">
                                                    <input type="checkbox"  class="i-checks child" name="users_id[]" value="<?php echo $val->id; ?>">
                                                </td>
                                                <td><?php echo $val->id; ?></td>
                                                <td><?php echo ucfirst($val->display_name); ?></td>
                                                <td><?php echo ucfirst($val->role); ?></td>
												<td><?php echo ucfirst($val->store_name); ?></td>
												<td><?php echo $val->licence; ?></td>
												<td ><?php echo $val->state; ?></td>
                                                <td ><?php echo site_date($val->created_on); ?></td>
												<td ><?php if($val->status == '9'):
												               echo '<span class="label label-success">Active</span>'; 
													       elseif($val->status == '10'):
														      echo '<span class="label label-warning">Inactive</span>';
														   endif;
													   ?>
											     </td>
                                                
                                                <td><a href="javascript:void(0)" class="user_address_modal" data-id="<?php echo $val->id; ?>" data-type="retailer"><span class="btn btn-default" title="User Address">View Details</span></a></td>
                                            </tr>

                                        <?php endforeach; ?>
                                    </tbody>                                                    
                                </table>                                
                                <button name="multi_activate" type="submit" class="viewActivateButton" value="multi_activate_action">ACTIVATE</button> 
                                <button name="multi_deactivate" type="submit" class="viewDeactivateButton" value="multi_deactivate_action">DEACTIVATE</button> 
                                <?php
                                echo form_close();
                            else:
                                ?>
                                <tr><td colspan="12">No Record Found.</td></tr>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <div class="dataTables_info tableData" id="editable_info" role="status" aria-live="polite"></div>
            </div>
            <?php echo $this->pagination->create_links(); ?>                                                        
        </div>
    </div>
</div>

<div class="modal fade" id="popupModal"  role="dialog" style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content"></div>
    </div>
</div>




</div>