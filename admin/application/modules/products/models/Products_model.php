<?php

class Products_model extends CI_Model {

    protected $projectTable = "social_users";

    function __construct() {
        parent::__construct();
    }

    public function product_validation_rules() {
        $data = $this->input->get();
        $validate_array = array();

//Product General Validation
        if (isset($data['title'])) {

            $validate_array = array_merge($validate_array, $this->product_general_details());
        }

//Product Type Validation
        if (isset($data['type'])) {
            $validate_array = array_merge($validate_array, $this->product_type($data));
        }

        return $validate_array;
    }

    public function product_general_details() {

        $arr = array(
            array(
                'field' => 'title',
                'label' => 'title',
                'rules' => 'required|min_length[3]|max_length[255]'
            ),
            array(
                'field' => 'description',
                'label' => 'description',
                'rules' => 'required|min_length[3]|max_length[255]'
            ),
//            array(
            //                'field' => 'quantity',
//                'label' => 'quantity',
//                'rules' => 'required|numeric'
//            ),
            array(
                'field' => 'category_id',
                'label' => 'category',
                'rules' => 'required'
            ),
            array(
                'field' => 'retailer_id',
                'label' => 'Retailer',
                'rules' => 'required'
            ),
            array(
                'field' => 'status',
                'label' => 'status',
                'rules' => 'required'
            )
        );

        return $arr;
    }

    public function product_type($data) {

//prd($data);

        if ($data['type'] == 1) {
            $arr[] = array(
                'field' => 'weight',
                'label' => 'Quantity',
                'rules' => 'required|numeric'
            );

            $arr[] = array(
                'field' => 'sub_category_id',
                'label' => 'sub category',
                'rules' => 'required'
            );

            $arr[] = array(
                'field' => 'price',
                'label' => 'price',
                'rules' => 'required|numeric'
            );
        } else {
            $arr[] = array(
                'field' => 'full_price',
                'label' => 'Full Plate Price',
                'rules' => 'required|numeric'
            );

            $arr[] = array(
                'field' => 'half_price',
                'label' => 'Full Plate Price',
                'rules' => 'numeric'
            );
        }

        return $arr;
    }

    public function get_form_validation_rules() {
//die("dfdffd");

        $form_validation = array(
            array(
                'field' => 'title',
                'label' => 'title',
                'rules' => 'required|min_length[3]|max_length[255]'
            ),
            array(
                'field' => 'description',
                'label' => 'description',
                'rules' => 'required|min_length[3]|max_length[255]'
            ),
            array(
                'field' => 'quantity',
                'label' => 'quantity',
                'rules' => 'required|numeric'
            ),
            array(
                'field' => 'category_id',
                'label' => 'category',
                'rules' => 'required'
            ),
            array(
                'field' => 'type',
                'label' => 'product type',
                'rules' => 'required'
            ),
            array(
                'field' => 'status',
                'label' => 'status',
                'rules' => 'required'
            )
        );

        if (isset($_POST['type']) && $_POST['type'] == 1) {

        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules($form_validation);
        return ($this->form_validation->run() === false) ? false : true;
    }

    public function custom_form_validation_rules($data) {
        $arr = array();
        if (!empty($data)) {
            foreach ($data['product_name'] as $key => $val) {

                $arr[] = array(
                    'field' => 'product_name[' . $key . ']',
                    'label' => 'product name',
                    'rules' => 'required',
                );
            }
            foreach ($data['description'] as $key => $val) {
                $arr[] = array(
                    'field' => 'description[' . $key . ']',
                    'label' => 'description',
                    'rules' => 'required',
                );
            }
            foreach ($data['product_type'] as $key => $val) {
                $arr[] = array(
                    'field' => 'product_type[' . $key . ']',
                    'label' => 'product type',
                    'rules' => array('required', 'min_length[1]', array('product_type', array($this, 'number_check'))),
                    'errors' => array(
                        'product_type' => 'Sorry! can contain only two number i.e 1 or 2',
                    ),
                );

                if ($val == 1) {
                    foreach ($data['weight'] as $key => $val) {

                        $arr[] = array(
                            'field' => 'weight[' . $key . ']',
                            'label' => 'weight',
                            'rules' => 'required|numeric',
                        );
                    }
                } else {
                    foreach ($data['weight'] as $key => $val) {

                        $arr[] = array(
                            'field' => 'weight[' . $key . ']',
                            'label' => 'weight',
                            'rules' => 'required',
                        );
                    }
                }
            }
            foreach ($data['price'] as $key => $val) {
                $arr[] = array(
                    'field' => 'price[' . $key . ']',
                    'label' => 'price',
                    'rules' => 'required',
                );
            }
            foreach ($data['special_price'] as $key => $val) {
                $arr[] = array(
                    'field' => 'special_price[' . $key . ']',
                    'label' => 'special price',
                    'rules' => '',
                );
            }
            foreach ($data['quantity'] as $key => $val) {
                $arr[] = array(
                    'field' => 'quantity[' . $key . ']',
                    'label' => 'quantity',
                    'rules' => '',
                );
            }

            foreach ($data['in_store'] as $key => $val) {

                $arr[] = array(
                    'field' => 'in_store[' . $key . ']',
                    'label' => 'in store',
                    'rules' => array('required', 'min_length[1]', array('in_store', array($this, 'number_check'))),
                    'errors' => array(
                        'in_store' => 'Sorry! can contain only two number i.e 1 or 2',
                    ),
                );
            }
            foreach ($data['is_cod'] as $key => $val) {

                $arr[] = array(
                    'field' => 'is_cod[' . $key . ']',
                    'label' => 'is cod',
                    'rules' => array('required', 'min_length[1]', array('is_cod', array($this, 'number_check'))),
                    'errors' => array(
                        'is_cod' => 'Sorry! can contain only two number i.e 1 or 2',
                    ),
                );
            }

            foreach ($data['category'] as $key => $val) {

                $arr[] = array(
                    'field' => 'category[' . $key . ']',
                    'label' => 'category',
                    'rules' => array('required', 'min_length[1]', array('category', array($this, 'cat_check'))),
                    'errors' => array(
                        'category' => 'Sorry! category not match in database',
                    ),
                );
            }
            foreach ($data['sub_category'] as $key => $val) {

                $arr[] = array(
                    'field' => 'sub_category[' . $key . ']',
                    'label' => 'sub category',
                    'rules' => array('min_length[1]', array('sub_category', array($this, 'sub_cat_check'))),
                    'errors' => array(
                        'sub_category' => 'Sorry! sub category not match in database',
                    ),
                );
            }
            foreach ($data['retailer_id'] as $key => $val) {

                $arr[] = array(
                    'field' => 'retailer_id[' . $key . ']',
                    'label' => 'Retailer',
                    'rules' => array('required', 'min_length[1]', array('retailer_id', array($this, 'retailer_name_check'))),
                    'errors' => array(
                        'retailer_name' => 'Sorry! Retailer does not match in database',
                    ),
                );
            }
        }
        $this->load->library('form_validation');
        $data1 = $this->form_validation->set_rules($arr);
        return ($this->form_validation->run() === false) ? false : true;
    }

    public function cat_check($str) {
        if (trim($str)) {
            $array = array('title' => $str, 'is_deleted' => '0', 'status' => '1', 'parent_id' => '0');
            $res = $this->db->select('id')->where($array)->get("category")->row();
            return ($res) ? $res->id : false;
        }
        //  return FALSE;
    }

    public function sub_cat_check($str) {

        if (trim($str)) {
            $str = trim($str);
            $array = array('title' => $str, 'is_deleted' => '0', 'status' => '1');
            $res = $this->db->select('id')->where($array)->get("category")->row();
            return ($res) ? $res->id : false;
        }
    }

    public function sub_cat_check_upload($str,$cat_id) {

        if (trim($str)) {
            $str = trim($str);
            $array = array('title' => $str, 'is_deleted' => '0','parent_id'=>$cat_id, 'status' => '1');
            $res = $this->db->select('id')->where($array)->get("category")->row();
            return ($res) ? $res->id : false;
        }
    }

    public function retailer_name_check($str) {
        if (trim($str)) {
            $str = trim($str);
            $status = array('1','9');
            $array = array('id' => $str, 'deleted' => '0');
            $res = $this->db->select('id')->where($array)->where_in('status',$status)->get("users")->row();
            return ($res) ? $res->id : false;
        }
    }

    public function number_check($num) {
        //echo $str;die;
        if ($num <= 2) {
            return TRUE;
        }
        return FALSE;
    }

    public function product_details($filter = null, $limit = null, $offset = null, $count = true) {
        ///filter start
        if (!empty($filter['title'])) {
            $title = trim($filter['title']);
            $where = "( product.title LIKE '%$title%'  ESCAPE '!'  "
                    . "OR product.description LIKE '%$title%'  ESCAPE '!')";
            $this->db->where($where);
        }

        if (!empty($filter['type'])) {
            $this->db->where("product.type", $filter['type']);
        }

        if (!empty($filter['category'])) {
            $this->db->where("product.category_id", $filter['category']);
        }

        if (isset($filter['status']) and $filter['status'] != '') {
            $this->db->where("product.status", $filter['status']);
        }

        if (!empty($filter['retailer_id'])) {
            $retailer_id = $filter['retailer_id'];
            $this->db->where("product.retailer_id", $retailer_id);
        }
        //filter end
            
        if($_SESSION['role_id']=='2'||$_SESSION['role_id']=='4')
        {
            $this->db->where("product.retailer_id", $_SESSION['id']);
        }


        if ($limit) {
            $this->db->limit($limit, $offset);
        }
        $this->db->where("product.status!=", '2');
        //LOC added on 2 april 2018
        $this->db->where(array('u.status!='=>'0','u.deleted!='=>'1'));
        //End here
        $this->db->select('product.*,c1.title as category_name,c2.title as sub_category_name');
        $this->db->join('category c1', 'product.category_id = c1.id');
        $this->db->join('category c2', 'product.sub_category_id = c2.id');
        $this->db->join('users u', 'product.retailer_id = u.id');
        $res = $this->db->order_by('product.modified_on', 'desc')->get('product');
        if ($count) {
            return $res->num_rows();
        } else {

            return $res->result();
        }
    }

    public function product_detail_by_id($id) {
        $res = $this->db->where("id", $id)->get('product')->row();
        return $res ? $res : false;
    }

    public function update_products($data, $id) {
        $return = $this->db->where('id', $id)->update('product', $data);
        return $return;
    }

    public function add_products($data) {

        $this->db->insert('product', $data);
        $res = $this->db->insert_id();
        return ($res) ? $res : false;
    }

    //find all products detail using category and subcategory id for api
    public function all_product_detailed_info($category_id, $subcategory_id) {
        if ($category_id && $subcategory_id) {
            $where = array('category_id' => $category_id, 'sub_category_id' => $subcategory_id, 'status' => '1');
        } else {
            $where = array('category_id' => $category_id, 'status' => '1');
        }

        $products = $this->db->select('*')->where($where)->get('product_detail')->result();

        return $products ? $products : false;
    }

    // soft delete product
    public function delete_product($id) {
        $return = $this->db->where('id', $id)->update('product', array('deleted' => '1'));
        return $return;
    }

    public function get_retailers_price_log($filter = null, $limit = false, $offset = false, $count = true) {

        if (isset($_GET['filter'])) {

            if (!empty($filter['retailer_id'])) {
                $this->db->where('trans_product_price.retailer_id', $filter['retailer_id']);
            }

            if (!empty($filter['product_id'])) {
                $this->db->where('product_id', $filter['product_id']);
            }

            if ($limit) {
                $this->db->limit($limit, $offset);
            }

            $this->db->select('trans_product_price.*,users.display_name,product.title');
            $this->db->join('product', 'product.id=trans_product_price.product_id', 'left');
            $this->db->join('users', 'users.id=trans_product_price.retailer_id', 'left');
            $this->db->order_by('trans_product_price.updated_at', 'DESC');

            $res = $this->db->get('trans_product_price');
        } else {
            $res = $this->get_max_retailer_log($limit, $offset);
        }

        if ($count) {

            return $res ? $res->num_rows() : false;
        } else {

            return $res ? $res->result() : false;
        }
    }

    public function get_max_retailer_log($limit = false, $offset = false) {

        $this->db->select('retailer_id AS id, COUNT(`retailer_id`) AS count');
        $this->db->where('retailer_id is NOT NULL', NULL, FALSE);
        $this->db->group_by('retailer_id');
        $this->db->order_by('count', 'DESC');
        $this->db->limit(1);

        $result = $this->db->get('trans_product_price')->row();

        if ($result) {

            $max_id = $result->id;

            //echo $this->db->last_query();exit;

            if ($limit) {

                $this->db->limit($limit, $offset);
            }

            $this->db->select('trans_product_price.*,users.display_name,product.title');
            $this->db->join('product', 'product.id=trans_product_price.product_id', 'left');
            $this->db->join('users', 'users.id=trans_product_price.retailer_id', 'left');
            $this->db->where('trans_product_price.retailer_id', $max_id);
            $this->db->order_by('trans_product_price.updated_at', 'DESC');

            $res = $this->db->get('trans_product_price');
        } else {
            $res = false;
        }

        return $res;
    }

}
