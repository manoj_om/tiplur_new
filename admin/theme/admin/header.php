<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="robots" content="noindex,nofollow" />
        <title>Tiplur</title>
        <link href="<?php echo base_url() ?>theme/admin/css/bootstrap.min.css?v=<?php echo strval(microtime(true))?>" rel="stylesheet">
        <link href="<?php echo base_url() ?>theme/admin/font-awesome/css/font-awesome.css?v=<?php echo strval(microtime(true))?>" rel="stylesheet">


<!-- <link href="<?php echo base_url() ?>theme/admin/css/animate.css" rel="stylesheet"> -->
        <link href="<?php echo base_url() ?>theme/admin/css/style.css?v=<?php echo strval(microtime(true))?>" rel="stylesheet">

        <!-- PAGE LEVEL STYLES -->
        <?php if (isset($css) AND count($css) > 0): ?>
            <?php foreach ($css as $css): ?>
                <link rel="stylesheet" type="text/css" href="<?php echo base_url($css); ?>?v=<?php echo strval(microtime(true))?>">
            <?php endforeach; ?>
        <?php endif; ?>
        <!-- END PAGE LEVEL  STYLES -->



        <!-- PAGE LEVEL  Header Javascript -->
        <?php if (isset($js_header) AND count($js_header) > 0): ?>
            <?php foreach ($js_header as $js_header): ?>
                <script type="text/javascript" src="<?php echo base_url($js_header); ?>?v=<?php echo strval(microtime(true))?>"></script>
            <?php endforeach; ?>
        <?php endif; ?>
        <!-- END PAGE LEVEL  Header Javascript -->


        <script>
            var base_url = "<?php echo base_url(); ?>";
        </script>
  </head>

    <body>
<?php $promotions = $this->uri->segment(2);
 $role_id = $this->session->userdata('role_id');//echo "<pre>"; print_r($this->session->userdata()); ?>

        <div id="wrapper">

            <nav class="navbar navbar-static-top topNavBar" role="navigation" style="margin-bottom: 0">

                <div class="navbar-header">
                    <!-- <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="javascript:void(0);"><i class="fa fa-bars"></i> </a>    -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#side-menu" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <?php if($promotions == 'promotions'):?>
                      <a href="<?php echo base_url('admin/promotions/dashboard'); ?>" class="navbar-brand">
                          <img style="width: 50px;float: left;" src="<?php echo base_url('theme/admin/img/assets/sm-logo.png'); ?>" class="img-responsive center-block" />
                      </a>
                  <?php else: ?>
                    <a href="<?php echo base_url('admin/dashboard'); ?>" class="navbar-brand">
                        <img style="width: 50px;float: left;" src="<?php echo base_url('theme/admin/img/assets/sm-logo.png'); ?>" class="img-responsive center-block" />
                    </a>
                  <?php endif; ?>
                  </div>


                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav" id="side-menu">
                        <?php
                        $uri = ($this->uri->segment(2)) ? $this->uri->segment(2) : '';
                        $sub_url = ($this->uri->segment(3)) ? $this->uri->segment(3) : '';
                        $type = isset($_GET['type']) ? $_GET['type'] : '';
                        ?>

                        <!-- <?php //if ($role_id == '4'): ?>
                            <li class="<?php echo (!empty($uri) && $uri == 'dashboard') ? 'active' : (!empty($uri) && $uri == 'banner') ? 'active':''; ?>" >
                                <a class="subLi" href="<?php echo base_url('/admin/promotions/dashboard') ?>"><i class="fa fa-home "></i> <span class="nav-label">Home</span></a>
                            </li>

                            <li class="<?php echo (!empty($uri) && $uri == 'promotions') ? 'active' : ''; ?>">
                                <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" ><i class="fa fa-certificate"></i> <span class="nav-label">Promotions</span> <span class="fa arrow"></span></a>
                                <ul class="dropdown-menu ">

                                     <li><a href="<?php echo base_url('admin/promotions'); ?>">Manage Promotions</a></li>
                                     <li><a href="<?php echo base_url('admin/promotions/participate_user'); ?>">Participated Customer</a></li>
                                    <li class=""><a href="<?php echo base_url('admin/promotions/upload'); ?>">Import Promotions </a></li>
                                </ul>
                            </li>
                       <?php// endif; ?> -->


                        
                          <li class="<?php echo (!empty($uri) && $uri == 'dashboard') ? 'active' : (!empty($uri) && $uri == 'banner') ? 'active':''; ?>" >
                              <a class="subLi" href="<?php echo base_url('/admin/dashboard') ?>"><i class="fa fa-home "></i> <span class="nav-label">Home</span></a>
                          </li>
                        <?php if($_SESSION['role_id']=='1'):?>
                        <li class="<?php echo (!empty($uri) && $uri == 'states') ? 'active' : ''; ?>">
                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" ><i class="fa fa-map-marker "></i> <span class="nav-label">States</span> <span class="fa arrow"></span></a>
                            <ul class="dropdown-menu">
                                <li class=""><a href="<?php echo base_url('admin/states'); ?>">Manage States</a></li>
								                <li class=""><a href="<?php echo base_url('admin/states/product-limit-list'); ?>">Manage Products Limit</a></li>
                                <li><a href="<?php echo base_url('admin/states/view_holidays'); ?>">View State Holidays</a></li>
                                <li><a href="<?php echo base_url('admin/states/view_terms'); ?>">View State T&C</a></li>
								                <li><a href="<?php echo base_url('admin/banner'); ?>">Mobile Banner Images</a></li>
                            </ul>
                        </li>
                        <?php endif;?>

                        <li class="<?php echo (!empty($uri) && $uri == 'products') ? 'active' : ''; ?>">
                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" ><i class="fa fa-cubes "></i> <span class="nav-label">Products</span> <span class="fa arrow"></span></a>
                            <ul class="dropdown-menu ">
                                <li class=""><a href="<?php echo base_url('admin/products'); ?>">Manage Products</a></li>
                                <?php if($_SESSION['role_id']=='1'):?>
                               <li><a href="<?php echo base_url('admin/products/create'); ?>">Add Products</a></li>
                                <li><a href="<?php echo base_url('admin/products/upload'); ?>">Products Upload</a></li>
                                <li><a href="<?php echo base_url('admin/products/price_log'); ?>">Price Activity Log</a></li>
                                <?php endif;?>
                            </ul>
                        </li>
                        <?php if($_SESSION['role_id']=='1'):?>
                        <li class="<?php echo (!empty($uri) && $uri == 'promotions') ? 'active' : ''; ?>">
                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" ><i class="fa fa-certificate"></i> <span class="nav-label">Promotions</span> <span class="fa arrow"></span></a>
                            <ul class="dropdown-menu ">
                                <li> <a class="subLi" href="<?php echo base_url('/admin/promotions/dashboard') ?>"><i class="fa fa-home "></i> <span class="nav-label">Promotion Dashboard</span></a></li>
                                 <li><a href="<?php echo base_url('admin/promotions'); ?>">Manage Promotions</a></li>
                                 <li><a href="<?php echo base_url('admin/promotions/participate_user'); ?>">Participated Customer</a></li>
                                <li class=""><a href="<?php echo base_url('admin/promotions/upload'); ?>">Import Promotions </a></li>
                            </ul>
                        </li>

                    <li class="<?php echo (!empty($uri) && $uri == 'category') ? 'active' : ''; ?>">
                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" ><i class="fa fa-glass "></i> <span class="nav-label">Category</span> <span class="fa arrow"></span></a>
                            <ul class="dropdown-menu ">
                                <li class=""><a href="<?php echo base_url('admin/category'); ?>">Manage category</a></li>
                                <li><a href="<?php echo base_url('admin/category/create'); ?>">Add category</a></li>
                                <li><a href="<?php echo base_url('admin/category/create?type=' . encrypt(1)); ?>">Add sub category</a></li>
                            </ul>
                        </li>

                        <li class="<?php echo (!empty($uri) && $uri == 'users') ? 'active' : ''; ?>">
                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" ><i class="fa fa-truck"></i> <span class="nav-label">Retailers</span> <span class="fa arrow"></span></a>
                            <ul class="dropdown-menu ">
                            <li class=""><a href="<?php echo base_url('admin/users/pending_request'); ?>">Pending  Retailers</a></li>
                                <li class=""><a href="<?php echo base_url('admin/users'); ?>">Registered Retailers</a></li>
                                <li><a href="<?php echo base_url('admin/users/mapped_retailer'); ?>">Mapped Retailers</a></li>
								                <li><a href="<?php echo base_url('admin/users/retailer-shops'); ?>">Retailers Shops</a></li>
                            </ul>
                        </li>

                        <li class="<?php echo (!empty($uri) && $sub_url == 'customers') ? 'active' : ''; ?>">
                            <a class="subLi" href="<?php echo base_url('admin/users/customers'); ?>"><i class="fa fa-users "></i> <span class="nav-label">Customers</span> </span></a>
                        </li>
                        <?php endif; ?>
						<li class="<?php echo (!empty($uri) && $uri == 'orders') ? 'active' : ''; ?>">
                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" ><i class="fa fa-shopping-cart "></i> <span class="nav-label">Orders</span> <span class="fa arrow"></span></a>
                            <ul class="dropdown-menu ">
                                <li class=""><a href="<?php echo base_url('admin/orders'); ?>">Manage Orders</a></li>
                                <?php if($_SESSION['role_id']=='1'):?>
                                <li><a href="<?php echo base_url('admin/orders/abandoned_cart'); ?>">Abandoned Cart</a></li>
                                <?php endif; ?>
                            </ul>
                        </li>
                        <?php if($_SESSION['role_id']=='1'):?>
                        <li class="<?php echo (!empty($uri) && $uri == 'notification') ? 'active' : ''; ?>">
                            <a class="subLi" href="<?php echo base_url('admin/notification/send_notifications'); ?>"><i class="fa fa-bell"></i> <span class="nav-label">Notification</span></span></a>
                        </li>
                        <?php endif?>
                    </ul>

                    <ul class="nav navbar-top-links navbar-right navRightPanel">
                        <li class="dropdown notification-outer">
                            <?php $count = 0; //unseen_notification_count(current_user()['id'])  ?>
                            <!-- onclick="update_notification()" -->
                            <a class="dropdown-toggle count-info" data-toggle="dropdown"  href="javascript:void(0);">
                                <i class="fa fa-bell" style="visibility: hidden;"></i>  <span class="label label-primary" ><?php //echo $count > 0 ? $count : '';  ?></span>
                            </a>
                            <ul class="dropdown-menu dropdown-alerts notification-outer " >
                                <?php $records = 0; //unseen_notification_records(current_user()['id']);
                                ?>
                                <?php if ($records): ?>
                                    <?php foreach ($records as $record): ?>
                                        <li>
                                            <a href="javascript:void(0);">
                                                <div>
                                                    <div class="user-img pull-left">
                                                        <strong><?php echo ucfirst(($record->notified_by_name) ? $record->notified_by_name : '--'); ?></strong>
                                                    </div>
                                                    <br />
                                                    <span><a href="#"><?php echo $record->message; ?></a></span>
                                                    <span class="pull-right text-muted small"><?php echo time_ago($record->created_on); ?></span>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="divider"></li>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <li>There is no any new notification, for check old notifications please click on below button</li>
                                <?php endif; ?>
                                <li class="divider"></li>
                                <li>
                                    <div class="text-center link-block">
                                        <a href="<?php echo base_url("admin/notification"); ?>">
                                            <strong>See All Notification</strong>
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                      
                        <li>
                            <a data-toggle="dropdown" class="dropdown-toggle userName" href="javascript:void(0);">
                                <span class="clear">
                                    <span class="block "><i class="fa fa-user fa-lg"></i>
                                        <?php echo ucfirst((current_user()['display_name'])); ?><b class="caret"></b>
                                    </span>
                                </span>
                            </a>
                            <ul class="dropdown-menu  m-t-xs">
                                <li><a href="<?php echo base_url('/admin/users/edit'); ?>">Profile</a></li>
                                <?php if($_SESSION['role_id']=='2'||$_SESSION['role_id']=='4'):?>
                                <li><a href="<?php echo base_url('/admin/users/payment_mode'); ?>">Change Payment Mode</a></li>
                                <?php endif;?>
                                <li><a href="<?php echo base_url(); ?>admin/users/resetpassword">Change Password</a></li>
                                <li class="divider"></li>
                                <li><a href="<?php echo base_url(); ?>logout">Logout</a></li>
                            </ul>
                        </li>


                        <!-- <li>
                            <a class="right-sidebar-toggle">
                                <i class="fa fa-tasks"></i>
                            </a>
                        </li> -->
                    </ul>
                </div>
            </nav>


            <!-- <nav class="navbar-default full-width" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#side-menu" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Brand</a>
        </div>
        <div class="collapse navbar-collapse">

        </div>
    </nav>


     <div class="navbar-brand">
                        <img src="img/assets/indexlogo.svg" class="img-responsive" />
                    </div>
            -->
