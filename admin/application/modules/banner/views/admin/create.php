<div id="page-wrapper" class="gray-bg dashbard-1">
    <?php breadcrumbs(array('admin/banner/create'=>'Add Banner Images'));  ?>
    <div class="row border-bottom"></div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
		<?php print_flash_message(); ?>
                        <div class="col-lg-12">
               <?php echo form_open_multipart($this->uri->uri_string(), 'class="form-horizontal"', 'autocomplete="off"'); ?>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h2 style="display: inline-block;">Upload Banner Image</h2>                               
                    </div>
                    <div class="ibox-content contentBorder">
                        <div class="row">
                            
							<div class="col-lg-6 col-md-4 col-sm-4 AddProdctInputCont">
								<div class="form-group formWidht">
										<?php $select = !empty($_POST['type'])?trim($_POST['type']):''; ?>
										<label>Type <span style="color: red">*</span></label>
										<select class="form-control m-b addContDrop" required name="type" readonly>
											<option value="mobile" <?php echo ($select =='mobile')?'selected':''; ?> >Mobile App</option>
										</select>
								</div>
								<span class='error vlError'><?php echo form_error('type'); ?></span>
                            </div>
						</div>
						<div class="row">	
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="form-group formWidht">
                                    <label>Banner Image <span style="color: red">*</span> </label>
                                    <input name="h_image" disabled="" title="Browse Image" id="old_user_pic" value="" class="form-control valid old_user_pic InputBox" readonly="" aria-invalid="false" type="text" required>
                                    <input name="image" value="" type="hidden" required>
                                    <label class="input-group-btn crateAddImageButtonuser editPagebtn">
                                        <span class="btn btn-primary">
                                            BROWSE <input name="image" id="user_pic"  class="user_pic" accept="image/gif, image/jpg, image/jpeg, image/png" type="file" >
                                        </span>								
                                    </label>	
                                </div> 
								<span class='error vlError file-error'><?php echo form_error('image'); ?></span>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12  text-center" style="margin-top:80px;">
                                <input type="submit" name="save" value="SAVE" class="btn btn-primary block full-width m-b updateProductBtn">
                            </div>  
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>