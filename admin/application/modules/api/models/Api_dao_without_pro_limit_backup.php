<?php

/**
 * Description : This Dao class is responsible of all the database related operation for API
 */
class Api_dao extends CI_Model {
	public function __construct() {
		parent::__construct ();
		include_once './application/objects/Response.php';
		date_default_timezone_set ( 'Asia/Calcutta' );
		$this->load->helper ( 'date' );
		
		// echo date('Y-m-d H:i:s'); die();
	}
	
	
	
	public function is_email_exist($post) {
		$response = new Response ();
		try {
			
			$query = $this->db->where(array('email'=>$post['email']))->get('users');
			//echo $this->db->last_query();
			
			if ($query->num_rows () > 0)
			{
				
				//send mail
				$mail = $this->send_mail($post['email']);
				if($mail){
					$response->setStatus ( 1 );
					$response->setMsg ( "Mail sent" );
					$response->setObjArray ( NULL );
				}else{
					$response->setStatus ( 0 );
					$response->setMsg ( "Error in sending mail" );
					$response->setObjArray ( NULL );
				}
				
			} else
			{
				$response->setStatus ( 0 );
				$response->setMsg ( "User does not exist" );
				$response->setObjArray ( NULL );
			}
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	
	//***************  check user exists **************\\
	
	public function isUserExist($post) 
	{
		$response = new Response ();
		try {  
			
			$query = $this->db->or_where(array('mobile'=>$post['mobile'],'email'=>$post['email']))->get('users');
			//echo $this->db->last_query();
			
			if ($query->num_rows () > 0) 
			{
				$response->setStatus ( 0 );
				$response->setMsg ( "User Already Exist" );
				$response->setObjArray ( $query->row() );
			} else 
			{
				$result  = $this->create_new_user($post); 
				if($result)
				{  
					$response->setStatus ( 1 );
					$response->setMsg ( "New User" );
					$response->setObjArray ( $result );
				}else{
					
					$response->setStatus ( 0 );
					$response->setMsg ( "something went wrong" );
					$response->setObjArray ( '' );
					
				}
			}
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	
	//************** End here ****************** \\
	
	public function create_new_user($post)
	{      
		$this->db->trans_start();
		$password = $post['password'];
		$insert_data = array();  
		
		$insert_data['email'] = $post['email'];
		$insert_data['mobile'] = $post['mobile'];
		$insert_data['lat'] = $post['latitude'];
		$insert_data['lng'] = $post['longitude'];
		$insert_data['display_name'] = $post['username'];
		$insert_data['dob'] = date('y-m-d',strtotime($post['dob']));
		$insert_data['state_id'] = $post['state_id'];
		$insert_data['status'] = '1';
		$insert_data['created_on'] = date('Y-m-d H:i:s');
		$insert_data['password'] = md5($password);
	
		if($post['is_retailer']){
			$insert_data['role_id'] = 2;
		}else{
			$insert_data['role_id'] = 3;
		}
		
		//upload Id Proof
		if (isset($_FILES ['doc_file'])) 
		{  
	      $path = './assets/uploads/document_file/'; 
		  $file = $this->upload_image('doc_file',$_FILES ['doc_file']['tmp_name'],$path);
		  $insert_data['doc_file'] = $file ['upload_data'] ['file_name'];
		}
		
		$insert_result  = $this->db->insert('users',$insert_data);
		$last_id  = $this->db->insert_id(); 
		if($last_id)
		{
			$user_info = user_data($last_id);
			$address = array('user_id' => $last_id, 'name' => $post['username'], 'mobile' => $post['mobile'], 'address' => $post['address'], 'city' => $post['city'], 'state' => $post['state'], 'pincode' => $post['pincode'], 'is_default' => 1, 'created_on' => date('Y-m-d H:i:s'));
			$insert_address = $this->add_address($address);
			if($insert_address){
				$address['id'] = $insert_address;
				$user_info->address = $address;
				//for retailer
				if($post['is_retailer']){
					
					$store = array();
					$store['store_name'] = $post['store_name'];
					$store['location_id'] = $insert_address;
					$store['licence'] = $post['licence'];
					$store['contact_person'] = $post['cp_name'];
					$store['contact_person_mobile'] = $post['cp_mobile'];
					$store['is_provide_snacks'] = $post['is_provide_snacks'];
					$store['min_order'] = $post['min_order'];
					$store['opening_time'] = $post['start_time'];
					$store['closing_time'] = $post['end_time'];
					$store['retailer_id'] = $last_id;
					$store['creation_date'] = date('Y-m-d H:i:s');
					$store['store_id'] = $this->add_store($store);
					$user_info->store = $store;
					
				}
			}
		}
		
		return $user_info? $user_info: false;
	}
	
	
	
	
	//**  Update Device information  **********\\
	
	public function updateDeviceData($userId, $deviceType, $deviceId, $fcmRegId) 
	{
		$result = $this->db->where(array('deviceId' => $deviceId, 'user_id' => $userId))->get('device_info');
        if ($result->num_rows() > 0) {
            // update
        	$update_data = array('fcmId' => $fcmRegId, 'deviceType' => $deviceType, 'is_logged_in' => '1', 'modificationDate' => date('Y-m-d H:i:s'));
            $this->db->where(array('deviceId' => $deviceId, 'user_id' => $userId));
            $result = $this->db->update('device_info', $update_data);
            $this->db->trans_complete();
            return $result ? true : false;
        } else {
            // insert
        	$insert_data = array('fcmId' => $fcmRegId, 'deviceType' => $deviceType, 'is_logged_in' => '1', 'user_id' => $userId, 'deviceId' => $deviceId, 'creation_date' => date('Y-m-d H:i:s'));
            $result = $this->db->insert('device_info', $insert_data);
            $this->db->trans_complete();
            return $result ? true : false;
        }
	}
	//************** End here ****************** \\
	
	//update profile
	public function update_profile($data) 
	{
		
		$response = new response ();
		$updated_data = array ();
		$user_id = $data ['id'];
		$db_data = array();
		$db_data['display_name'] = $data['username'];
		$db_data['dob'] = isset($data['dob'])?date('Y-m-d',strtotime($data['dob'])):NULL;
		$db_data['modified_on'] = date('Y-m-d H:i:s');
		
		if(isset( $_FILES ['image'] )) 
	    {
			$path = './assets/images/profile_image/'; 
		    $file = $this->upload_image('image',$_FILES ['doc_file']['tmp_name'],$path);
		    $db_data['image'] = $file ['upload_data'] ['file_name'];
		}
		
		
		// check exist user
		$check = $this->db->select ( 'id, image' )->where ( 'id', $user_id )->get ( 'users' )->first_row ();
		
		if ($check->id) 
		{
			// update user details
			$res = $this->db->where ( 'id', $user_id )->update ('users', $db_data);
			if ($res) 
			{
				
				$updated_user =  user_data($user_id);
				//return data
				$updated_data ['id'] = $updated_user->id;
				$updated_data ['email'] = $updated_user->email;
				$updated_data ['mobile'] = $updated_user->mobile;
				$updated_data ['display_name'] = $updated_user->display_name;
				$updated_data ['dob'] = $updated_user->dob;
				$updated_data ['address'] =  $this->get_address($updated_user->id, 1);
				$updated_data ['image'] = ($updated_user->image) ? base_url ( 'assets/images/profile_image/' . $updated_user->image ) :'';
				$updated_data ['doc_file'] = ($updated_user->doc_file) ? base_url ( 'assets/uploads/document_file/' . $updated_user->doc_file ) :'';
				
				if($data['is_retailer']){
				
					$store = array();
					
					$store['id'] = $data['store_id'];
					$store['store_name'] = $data['store_name'];
					$store['licence'] = $data['licence'];
					$store['contact_person'] = $data['cp_name'];
					$store['contact_person_mobile'] = $data['cp_mobile'];
					$store['is_provide_snacks'] = $data['is_provide_snacks'];
					$store['min_order'] = $data['min_order'];
					$store['modification_date'] = date('Y-m-d H:i:s');
					
					$update_store = $this->update_store($store);
					$updated_data ['store'] = $update_store;
					
				}
				
				$response->setStatus ( 1 );
				$response->setMsg ( "User Details updated successfully." );
				$response->setObjArray ( $updated_data );
			} else {
				$response->setStatus ( 0 );
				$response->setMsg ( "somethins going wrong" );
				$response->setObjArray ( $updated_data );
			}
		} else {
			$response->setStatus ( 0 );
			$response->setMsg ( "User does not exist. " );
		}
		return $response;
		
	}
	
	public function upload_image($name,$tmp_name,$path) 
	{
		$config ['upload_path'] = $path; 
		$config ['allowed_types'] = '*';
		$config ['file_name'] = md5 ( uniqid ( rand (), true ) );
		$this->load->library ( 'upload', $config );
		$this->upload->initialize ( $config );
		
		$sizeImage = getimagesize ( $tmp_name );
		$width = $sizeImage [0];
		$height = $sizeImage [1];
		if (! $this->upload->do_upload ($name) ) {
			
			echo $this->upload->display_errors ();
		} 
         else {
			
			$data = array (
					'upload_data' => $this->upload->data () 
			);
			// print_r($data); die();
			return $data;
		}
	}
	
	public function find_reatilers($data)
	{
		$radius = '50';	
		$retailers  = 	$this->db->query('SELECT ti_users.*, ti_store.min_order, ( 6371 * acos( cos( radians('.$data['latitude'].') ) * cos( radians( `lat` ) ) * cos( radians( `lng` ) - radians('.$data['longitude'].') ) + sin( radians('.$data['latitude'].') ) * sin( radians( `lat` ) ) ) ) AS distance FROM `ti_users` INNER JOIN `ti_store` ON ti_users.id=ti_store.retailer_id WHERE ti_users.status=1 AND ti_users.state_id='.$data['state_id'].' AND ti_users.role_id='.ROLE_RETAILER.' HAVING distance <= '.$radius.' ORDER BY distance ASC LIMIT 3')->result();
		
		if($retailers)
		{
			foreach($retailers as $res)
			{
				
				$return['id'] = $res->id;
				$return['username'] = $res->display_name;
				$return['email'] = $res->email;
				$return['mobile'] = $res->mobile;
				$addrss = $this->get_address($return['id'], 1);
				$return['address'] = $addrss?$addrss:array();
				$return['distance'] = $res->distance;
				$return['latitude'] = $res->lat;
				$return['longitude'] = $res->lng;
				$return['min_order'] = $res->min_order;
				//return store details (Retailer module)
				$user = array("user_id"=>$return['id']);
				$store_data = $this->get_store($user);
				
				if($store_data){
					$return['store_id'] = $store_data->id;
					$return['store_name'] = $store_data->store_name;
					$return['opening_time'] = $store_data->opening_time;
					$return['closing_time'] = $store_data->closing_time;
					$return['is_provide_snacks'] = $store_data->is_provide_snacks;
				}else{
					$return['store_id'] = 0;
					$return['store_name'] = "";
					$return['is_provide_snacks'] = "";
				}
				$x['retailer_id'] = $return['id'];
				$rating = $this->retailer_rating($x);
				
				if(sizeof($rating)>0){
					$return['rating'] = $rating['rating'];
				}else{
					$return['rating'] = 0;
				}
				
				$result[] = $return;
			}
			
			return $result ? $result :false;
		}else { return false; }
		
	}
	
	
	public function get_product_listing()
	{
		 $retailer_id = $this->input->post('retailer_id');	 
		 $type = $this->input->post('type');
		 
		 if($type==1){ //liquor
		 	$subcategory_id = $this->input->post('subcategory_id'); 
		 	//$where = array('sub_category_id'=>$subcategory_id, 'retailer_id'=>$retailer_id, 'type' => $type, 'status'=>'1', 'deleted'=>'0');
		 	$where = "pr.sub_category_id=".$this->db->escape($subcategory_id)." AND pr.retailer_id=".$this->db->escape($retailer_id)." AND pr.type=".$this->db->escape($type). " AND pr.status='1' AND pr.deleted='0'";
		 	$query = "SELECT ol.max_limit, pr.id, pr.category_id, cat.title category_name, pr.sub_category_id, (SELECT subcat.title FROM ti_category subcat WHERE pr.sub_category_id=subcat.id) subcategory_name, pr.retailer_id, user.display_name retailer_name, pr.title, pr.quantity, pr.weight, pr.description, pr.type, pr.price, pr.special_price, pr.food_category
					FROM ti_product pr INNER JOIN ti_category cat ON pr.category_id=cat.id INNER JOIN ti_users user ON pr.retailer_id=user.id INNER JOIN ti_order_limit ol ON ol.category_id = pr.category_id WHERE ". $where. " ORDER BY pr.title ASC";
		 	
		 
		 }else{ //snacks
		 	//$where = array('retailer_id'=>$retailer_id, 'type' => $type, 'status'=>'1', 'deleted'=>'0');
		 	$where = "pr.retailer_id=".$this->db->escape($retailer_id)." AND pr.type=".$this->db->escape($type). " AND pr.status='1' AND pr.deleted='0'";
		 	$query = "SELECT pr.id, pr.category_id, cat.title category_name, pr.sub_category_id, (SELECT subcat.title FROM ti_category subcat WHERE pr.sub_category_id=subcat.id) subcategory_name, pr.retailer_id, user.display_name retailer_name, pr.title, pr.quantity, pr.weight, pr.description, pr.type, pr.price, pr.special_price, pr.food_category
					FROM ti_product pr INNER JOIN ti_category cat ON pr.category_id=cat.id INNER JOIN ti_users user ON pr.retailer_id=user.id WHERE ". $where. " ORDER BY pr.title ASC";
		 	
		 
		 }
		 
		 
//		 $query = $this->db->select('id, category_id, category_name, sub_category_id, subcategory_name, retailer_id, retailer_name, title, quantity, weight, description, type, price, special_price, food_category')->where($where)->get('product_detail')->result(); 
		// echo $this->db->last_query($query);die;
		 //get selected quantity
		 
		 /* $query = "SELECT ol.max_limit, pr.id, pr.category_id, cat.title category_name, pr.sub_category_id, (SELECT subcat.title FROM ti_category subcat WHERE pr.sub_category_id=subcat.id) subcategory_name, pr.retailer_id, user.display_name retailer_name, pr.title, pr.quantity, pr.weight, pr.description, pr.type, pr.price, pr.special_price, pr.food_category
					FROM ti_product pr INNER JOIN ti_category cat ON pr.category_id=cat.id INNER JOIN ti_users user ON pr.retailer_id=user.id INNER JOIN ti_order_limit ol ON ol.category_id = pr.category_id WHERE ". $where. " ORDER BY pr.title ASC"; */
		 $res = $this->db->query($query);
		// echo $this->db->last_query($query);die;
		 if(sizeof($res->result())>0){
		 	foreach ($res->result() as $row){
		 		//$food = array();
		 		if($row->food_category==1){ //Liquor
		 			//$food[$row->food_category] = "Liquor";
		 			$row->food_category = "Liquor";
		 		}else if($row->food_category==2){ //veg
		 			//$food[$row->food_category] = "Veg";
		 			$row->food_category = "Veg";
		 		}else if($row->food_category==3){ //non-veg
		 			//$food[$row->food_category] = "Non-veg";
		 			$row->food_category = "Non-veg";
		 		}
		 		if(!$this->input->post('is_retailer')){
		 			$quantity = $this->db->select('quantity, weight')->where(array('user_id'=>$this->input->post('user_id'), 'product_id'=>$row->id, 'retailer_id'=>$this->input->post('retailer_id')))->get('cart')->result();
		 			$row->quantity_added = sizeof($quantity)>0?$quantity[0]->quantity:0;
		 			if($type==2){ //snacks
		 				$row->weight_added = $quantity[0]->weight?$quantity[0]->weight:"";
		 			}
		 		}
		 		
		 	}
		 	
		 	if($type==2){ //snacks
		 		return $this->group_by_id($res->result());
		 	}else{
		 		return $res->result();
		 	}
		 }else{
		 	return false;
		 }
		 
		 
	}
	
	/*
	 * @author: Anjani Gupta
	 * @date: 24/10/2017
	 * @method: update_cart
	 * @desc: add/delete cart products
	 * @params: action, product_id, retailer_id, quantity, price
	 */
	
	public function update_cart($cart){
		$response = array();
		$product =	array();
		$product['product_id'] = $cart['product_id'];
		$product['retailer_id']= $cart['retailer_id'];
		$product['user_id']= $cart['user_id'];
		$product['quantity']= $cart['quantity'];
		$product['price']= $cart['price'];
		$product['weight']= $cart['weight'];
		//print_r(array($product));die;
		$check_order_limit = $this->check_order_limit(array($product));
		//print_r($check_order_limit);die;
		if($check_order_limit){
		
			$where = array('product_id'=>$cart['product_id'],'retailer_id'=>$cart['retailer_id'],'user_id'=>$cart['user_id']);
			if($cart['action']=='add'){ //insert into cart
				$query = $this->db->where($where)->get('cart');
				if(sizeof($query->result())>0){
					$data = array(
							'quantity' => $cart['quantity'],
							'price' => $cart['price']*$cart['quantity'],
							'weight' => $cart['weight']
					);
					$this->db->where($where);
					$result = $this->db->update('cart', $data);
				}else{
					$data = array(
							'product_id' => $cart['product_id'],
							'retailer_id' => $cart['retailer_id'],
							'user_id' => $cart['user_id'],
							'quantity' => $cart['quantity'],
							'price' => $cart['price']*$cart['quantity'],
							'weight' => $cart['weight'],
							'created_on' => date('Y-m-d H:i:s')
					);
					$result = $this->db->insert('cart', $data);
				}
			}else if($cart['action']=='minus'){ //remove from cart if quantity is zero
				if($cart['quantity']!=0){
					$data = array(
							'quantity' => $cart['quantity'],
							'price' => $cart['price']*$cart['quantity'],
							'weight' => $cart['weight']
					);
					$this->db->where($where);
					$result = $this->db->update('cart', $data);
				}else{
					$result = $this->db->delete('cart', $where);
					//remove form cart if only snacks available in cart
					$this->remove_cart_product($cart);
				}
			}else{ //remove product if action is delete
				$result = $this->db->delete('cart', $where);
				//remove form cart if only snacks available in cart
				$this->remove_cart_product($cart);
			}
			
			if($result){
				$response['status'] = true;
				$q1 = "SELECT COUNT(*) count_l FROM ti_cart INNER JOIN ti_product ON ti_cart.product_id = ti_product.id WHERE ti_product.type=1 AND ti_cart.user_id=".$this->db->escape($cart['user_id']);
				$q2 = "SELECT COUNT(*) count_s FROM ti_cart INNER JOIN ti_product ON ti_cart.product_id = ti_product.id WHERE ti_product.type=2 AND ti_cart.user_id=".$this->db->escape($cart['user_id']);
				$r1= $this->db->query($q1);
				$r2= $this->db->query($q2);
				$response['count'] = array('liquor'=>$r1->result()[0]->count_l, 'snacks'=>$r2->result()[0]->count_s);
			}else{
				$response['status'] = false;
				$response['count'] = 0;
				$response['msg'] = "Error while updating cart.";
			}
		
		}else{
			$response['status'] = false;
			$response['count'] = 0;
			$response['msg'] = "Sorry! Your order is exceeded the limit.";
		}
		
		return $response;
		 
	}
	
	/*
	 * @author: Anjani Gupta
	 * @date: 24/10/2017
	 * @method: get_cart
	 * @desc: get cart detail
	 * @params: user_id
	 */
	public function get_cart($cart){
		$cartData = array();
		$liquor = array();
		$snacks = array();
		$where = array('cart.user_id'=>$cart['user_id']);
		$this->db->select('cart.product_id, cart.retailer_id, cart.user_id, cart.quantity, cart.weight, cart.price total_price, product.title, product.type, product.price unit_price');
		$this->db->from('cart');
		$this->db->where($where);
		$this->db->join('product', 'cart.product_id = product.id');
		$query = $this->db->get();
		
		if(sizeof($query->result())>0){
			
			foreach ($query->result() as $row){
				if($row->type==1){ //liquor
					$row->weight = $row->weight;
					array_push($liquor, (array)$row);
				}else{ //snacks
					array_push($snacks, (array)$row);
				}
				$retailerId = $row->retailer_id;
			}
			$cartData['liquor'] = $liquor;
			$cartData['snacks'] = $snacks;
			$cartData['is_provide_snacks'] = $this->db->select('is_provide_snacks')->where(array('retailer_id'=>$retailerId))->get('store')->row()->is_provide_snacks;
		}
		return $cartData;
	}
	
	/*
	 * @author: Anjani Gupta
	 * @date: 24/10/2017
	 * @method: add_address
	 * @desc: add multiple addresses of user
	 * @params: user_id, address, state, city, pincode
	 */
	
	public function add_address($user){
	 	$query = $this->db->insert('users_address', $user);
	 	return $query ? $this->db->insert_id() : false;
	}
	
	/*
	 * @author: Anjani Gupta
	 * @date: 25/10/2017
	 * @method: address
	 * @desc: get list of user address
	 * @params: user_id
	 */
	public function get_address($user_id, $is_default=false){
		if($is_default){
			$query = $this->db->select('*')->where(array('user_id'=>$user_id, 'is_default'=>$is_default, 'status'=>STATUS_ACTIVE))->get('users_address')->result();
		}else{
			$query = $this->db->select('*')->where(array('user_id'=>$user_id, 'status'=>STATUS_ACTIVE))->get('users_address')->result();
		}
		return $query ? $query :false;
	}
	
	 /*
	 * @author: Anjani Gupta
	 * @date: 25/10/2017
	 * @method: terms
	 * @desc: get terms & conditions by state id
	 * @params: state_id
	 */
	public function get_terms($state_id, $is_retailer=NULL){
		if($is_retailer){
			$query = $this->db->select('*')->where(array('state_id'=>$state_id, 'is_retailer' => $is_retailer))->get('terms_n_conditions')->result();
		}else{
			$query = $this->db->select('*')->where(array('state_id'=>$state_id, 'is_retailer' => '0'))->get('terms_n_conditions')->result();
		}
		
		
		return $query ? $query :false;
	}
	
	
	/*
	 * @author: Anjani Gupta
	 * @date: 25/10/2017
	 * @method: empty_cart
	 * @desc: empty cart data
	 * @params: user_id, retailer_id
	 */
	
	public function empty_cart($data){
		$where = array('user_id'=>$data['user_id'], 'retailer_id'=>$data['retailer_id']);
		$result = $this->db->delete('cart', $where);
		return $result;
	}
	
	
	/*
	 * @author: Anjani Gupta
	 * @date: 25/10/2017
	 * @method: complete_order
	 * @desc: order request by user
	 * @params: user_id, retailer_id, address_id, array of product object (product_id, quantity, price)
	 */
	public function complete_order($data){
		
		$product = json_decode($data['product'],true);
	
		$user_id = $data['user_id'];
		$categoryIds = $this->get_category_ids($product);
		//print_r($categoryIds);die;
		$check_todays_order = $this->get_todays_order($user_id, $categoryIds);
		
		if($check_todays_order['status']){
			
			$check_order_limit = $this->check_order_limit($product, $check_todays_order['data']);
			
			$status = array();
			if($check_order_limit){
					
				$this->db->trans_start();
				$insert_data = array();
				$row = array();
				
				$row['user_id'] = $user_id;
				$row['order_id'] = $this->generate_order_number($row['user_id']);
				$row['retailer_id'] = $data['retailer_id'];
				$row['address'] = $data['address_id'];
				
				foreach ($product as $val){
					$row['product_id'] = $val['product_id'];
					$row['quantity'] = $val['quantity'];
					$row['price'] = $val['price'];
					$row['weight'] = $val['weight'];
					$row['creation_date'] = date('Y-m-d H:i:s');
					array_push($insert_data, $row);
				}
				
				$this->db->insert_batch('order_detail',$insert_data);
				$this->db->insert('trans_order_status',array("order_number"=>$row['order_id'], "created_date" => date('Y-m-d H:i:s')));
				$this->empty_cart($data);
				$this->db->trans_complete();
				if ($this->db->trans_status() === FALSE)
				{
					$status['status'] = "0";
					$status['msg'] = "Error while placing order.";
				}else{
					
					/*
					 * send notification to retailer to accept/decline order
					 * send text message to retailer to accept/decline order
					 */
					$getUser = user_data($row['user_id']);
					
					$row['title'] = "New Order Request";
					$row['message'] = "Order requested by ".$getUser->display_name.". Please confirm the order.";
					$row['notified_to'] = $data['retailer_id'];
					$row['requested_by'] = $data['user_id']; 
					$row['type'] = ORDER_REQUEST_NOTIFICATION;
					$row['reason'] = "";
					
					//-------1. PUSH NOTIFICATION----------//
					$notify = new Notification();
					$notify->send_notification($row);
					
					//-------2. TEXT MESSAGE NOTIFICATION----------//
					//Implement SMS gateway here
					
					$status['status'] = "1";
					$status['msg'] = $row['order_id'];
				
				}
			}else{
				$status['status'] = "-1";
				$status['msg'] = "Sorry! Your order is exceeded the limit.";
			}
		}else{
			$status['status'] = "-1";
			$status['msg'] = $check_todays_order['msg'];
		}
		return $status;
	}
	
	/*
	 * @author: Anjani Gupta
	 * @date: 26/10/2017
	 * @method: generate_order_number
	 * @desc: Generate order number
	 * @params: 
	 */
	private function generate_order_number($user_id){
		$prefix = "TIP";
		$random_number = time();
		$state = $this->get_state_prefix($user_id);
		$user_id = $user_id;
		$order_number = $prefix.$random_number.$state.$user_id;
		$this->db->insert('order',array('order_number'=>$order_number, 'user_id'=>$user_id, 'created_date' => date('Y-m-d H:i:s')));
		return $order_number;
	}
	
	private function get_state_prefix($user_id){
		$query = "SELECT st.name FROM ti_states st INNER JOIN ti_users user ON st.id=user.state_id WHERE user.id=".$this->db->escape($user_id);
		$result = $this->db->query($query);
		$state = $result->row()->name;
		$prefix = strtoupper(substr($state, 0, 3));
		return $prefix;
	}
	
	/*
	 * @author: Anjani Gupta
	 * @date: 25/10/2017
	 * @method: request_order
	 * @desc: order request by user
	 * @params: order_id(optional), retailer_id, user_id
	 */
	
	public function order_history($data){ 
		$orderArray = array();
		
		if($data['order_id']!='0'){
			$orders = $this->db->where(array('user_id'=>$data['user_id'],'order_number'=>$data['order_id']))->order_by('created_date','DESC')->get('order')->result();
		}else{
			$orders = $this->db->where('user_id',$data['user_id'])->order_by('created_date','DESC')->get('order')->result();
		}
		
		foreach ($orders as $row){
			$x=array();
			$query = "SELECT tu.display_name, od.retailer_id, od.weight, od.product_id, od.quantity, od.price, od.address, od.payment_mode, od.status, od.creation_date requested_date,
						p.title, p.type, ua.name receiver, ua.mobile receiver_mob, ua.address receiver_add, ua.city, ua.state, ua.pincode, ua.is_default FROM ti_order_detail od INNER JOIN ti_product p ON od.product_id=p.id
						INNER JOIN ti_users_address ua ON ua.id=od.address INNER JOIN ti_users tu ON tu.id=od.user_id WHERE od.order_id=".$this->db->escape($row->order_number);
			$result = $this->db->query($query)->result();
			
			$orderDetails = array();
			
			$orderDetails['retailer_id'] = $result[0]->retailer_id;
			$orderDetails['retailer_name'] = $result[0]->display_name;
			$x['user_id'] = $result[0]->retailer_id;
			$orderDetails['retailer_store'] = $this->get_store($x);
			$orderDetails['address'] = array('name'=>$result[0]->receiver, 'mobile'=>$result[0]->receiver_mob, 'address'=>$result[0]->receiver_add, 'city'=>$result[0]->city, 'state'=>$result[0]->state, 'pincode'=>$result[0]->pincode, 'is_default'=>$result[0]->is_default);
			$orderDetails['payment_mode'] = $result[0]->payment_mode;
			$orderDetails['status'] = $result[0]->status;
			$orderDetails['complete_status'] = $this->db->select('status, comments , delivered_at, created_date as status_date')->where('order_number', $row->order_number)->get('trans_order_status')->result();
			
			$orderDetails['requested_date'] = $result[0]->requested_date;
			
			if($orderDetails['status']!=ORDER_REQUESTED){
				$interval  = round(abs(strtotime(date('Y-m-d H:i:s')) - strtotime($result[0]->requested_date))/60);
				if($interval<=60){
					$orderDetails['time_remaining'] = 60-$interval." Minutes";
				}else{
					$orderDetails['time_remaining'] = 0;
				}
			}else{
				$orderDetails['time_remaining'] = 0;
			}
			
			$products = array();
			foreach ($result as $val){
				$product = array();
				$product['id'] = $val->product_id;
				$product['title'] = $val->title;
				$product['type'] = $val->type;
				$product['quantity'] = $val->quantity;
				$product['price'] = $val->price;
				$product['weight'] = $val->weight;
				array_push($products, $product);
			}
			
			$orderDetails['product'] = $products;
			$orderDetails['rating'] = $this->get_ratings($data);
			
			$orderArray[$row->order_number] = $orderDetails;
		}
		
		return $orderArray;
	}
	
	public function get_all_categories($cat_id=false){
		if($cat_id){
			$where = array('parent_id'=>0, 'is_liquor'=>1, 'id'=>$cat_id);
		}else{
			$where = array('parent_id'=>0, 'is_liquor'=>1);
		}
		
		$category = $this->db->select('id, title, description, icon')->where($where)->get('category')->result();
		foreach ($category as $row){
			$row->icon = base_url().$row->icon;
		}
		return $category;
	}
	
	/*
	 * @author: Anjani Gupta
	 * @date: 26/10/2017
	 * @method: update_rating
	 * @desc: mark rating
	 * @params: order_id, rating
	 */
	public function update_rating($data){
		$query = $this->db->where(array('order_number'=>$data['order_id']))->get('rating')->result();
		$retailer_id = $this->db->select('retailer_id')->where(array('order_id'=>$data['order_id']))->limit(1)->get('order_detail')->result();
		if(sizeof($query)>0){
			$update_data = array('rating' => $data['rating'], 'user_id' => $data['user_id'], 'retailer_id' => $retailer_id[0]->retailer_id);
			$result = $this->db->where(array('order_number' => $data['order_id']))->update('rating', $update_data);
		}else{
			$insert_data = array('order_number' => $data['order_id'], 'rating' => $data['rating'], 'user_id' => $data['user_id'], 'retailer_id' =>  $retailer_id[0]->retailer_id, 'creation_date' => date('Y-m-d H:i:s'));
			$result = $this->db->insert('rating', $insert_data);
		}
		return $result;
	}
	
	/*
	 * @author: Anjani Gupta
	 * @date: 27/10/2017
	 * @method: get_ratings
	 * @desc: fetch all product ratings
	 * @params: order_id(optional), user_id
	 */
	public function get_ratings($data){
		if($data['order_id']!=0){
			$where = "WHERE ti_order.user_id=".$this->db->escape($data['user_id']);
		}else{
			$where = "WHERE ti_order.user_id=".$this->db->escape($data['user_id'])." AND ti_order.order_number=".$this->db->escape($data['order_id']);
		}
		
		$query = "SELECT ti_order.order_number, ti_rating.rating,  ti_rating.creation_date FROM ti_order INNER JOIN ti_rating ON ti_rating.order_number=ti_order.order_number " . $where;
		
		$res = $this->db->query($query)->result();
		return $res;
	}
	
	private function add_store($data){
		$query = $this->db->insert('store', $data);
		return $query ? $this->db->insert_id() : NULL;
	}
	
	/*
	 * @author: Anjani Gupta
	 * @date: 28/10/2017
	 * @method: update_price
	 * @desc: Update product price
	 * @params: product_id, user_id, price
	 */
	public function update_price($data){
		$update_data = array('price' => $data['price'], 'modified_on' => now());
		$result = $this->db->where(array('retailer_id' => $data['user_id'], 'id' => $data['product_id']))->update('product', $update_data);
		$this->db->insert('trans_product_price', array('product_id' => $data['product_id'], 'retailer_id' => $data['user_id'], 'price' => $data['price'], 'updated_at' => date('Y-m-d H:i:s')));
		return $result;
	}
	
	/*
	 * @author: Anjani Gupta
	 * @date: 28/10/2017
	 * @method: remove_product
	 * @desc: Remove product
	 * @params: product_id, user_id
	 */
	public function remove_product($data){
		$update_data = array('status' => '2', 'modified_on' => now() );
		$result = $this->db->where(array('retailer_id' => $data['user_id'], 'id' => $data['product_id']))->update('product', $update_data);
		return $result;
	}
	
	/*
	 * @author: Anjani Gupta
	 * @date: 30/10/2017
	 * @method: get_store
	 * @desc: Get store detail of user
	 * @params: user_id
	 */
	public function get_store($data){
		$query = $this->db->select('id, store_name, opening_time, closing_time, licence, contact_person, contact_person_mobile, is_provide_snacks, status, creation_date')->where(array('retailer_id'=>$data['user_id']))->get('store')->result();
		return $query? $query[0]:"";
	}
	
	/*
	 * @author: Anjani Gupta
	 * @date: 28/10/2017
	 * @method: requested_orders
	 * @desc: list of requested orders
	 * @params: retailer_id
	 */
	public function requested_orders($data){
		$order = array();
		$orders = array();
		$time_limit = '5';
		
		if($data['order_id']!='0'){
		
			$where = " `od`.`order_number` = '".$data['order_id']."' AND `od`.`created_date` > date_sub( NOW(), INTERVAL ".$time_limit." MINUTE ) AND `odd`.`retailer_id`=".$data['retailer_id']. " AND `odd`.`status`=". ORDER_REQUESTED;
		}else{
			
			$where = " `od`.`created_date` > date_sub( NOW(), INTERVAL ".$time_limit." MINUTE ) AND `odd`.`retailer_id`=".$data['retailer_id']. " AND `odd`.`status`=". ORDER_REQUESTED;
		}
		
		$query = "SELECT `od`.`order_number`, `od`.`created_date`, `odd`.`status`, `odd`.`address`, `odd`.`user_id`, `odd`.`status`
					FROM `ti_order` od
					INNER JOIN `ti_order_detail` odd  on `od`.`order_number` = `odd`.`order_id` 
					WHERE ".$where." GROUP BY  `od`.`order_number`";
		$res = $this->db->query($query);
		
		
		if(sizeof($res->result())>0){
			foreach ($res->result() as $row){
				$order['order_number'] = $row->order_number;
				$order['status'] = $row->status;
				$order['requested_time'] = $row->created_date;
				$interval  = abs(strtotime(date('Y-m-d H:i:s')) - strtotime($row->created_date));
				$order['time_left'] = gmdate("i:s", ($time_limit*60)-round($interval / 60*60));
				$order['time_remaining'] = ($time_limit*60)-round($interval / 60*60);
				$order['requested_by'] = $this->db->select('display_name')->where(array('id'=>$row->user_id))->get('users')->result()[0]->display_name;
				$order['delivery_address'] = $this->db->select('name, mobile, address, city, state, pincode, is_default')->where(array('id'=>$row->address))->get('users_address')->result()[0];
				$pr = array();
				$pr['order_id'] = $order['order_number'];
				$pr['user_id'] = $row->user_id;
				$order['product'] = $this->order_history($pr)[$order['order_number']]['product'];
				array_push($orders, $order);
			}
		}
		
		
		return $orders;
	}
	
	/*
	 * @author: Anjani Gupta
	 * @date: 31/10/2017
	 * @method: respond_order_request
	 * @desc: confirm/decline order request
	 * @params: order_id
	 */
	public function respond_order_request($data){
				
		$this->db->trans_start();
		
		$update_data = array('status' => $data['status'], 'modification_date' => date('Y-m-d H:i:s'));
		$this->db->where(array('order_id' => $data['order_id']));
		$this->db->update('order_detail', $update_data);
		
		if($data['reason']){
			$comments = $data['reason'];
		}else{
			$comments = "";
		}
		
		$insert_data = array('order_number' => $data['order_id'], 'delivered_at' => $data['delivered_at'], 'comments' => $comments, 'status' => $data['status'], 'created_date' => date('Y-m-d H:i:s'));
		$this->db->insert('trans_order_status', $insert_data);
		
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return false;
		}else{
			/*
			 * send notification to retailer to accept/decline order
			 * send text message to retailer to accept/decline order
			 */
			$row = array();
			
			$order_data = $this->db->where( array('order_id' => $data['order_id']) )->group_by('order_id')->get('order_detail')->result();
			$retailer = user_data($order_data[0]->retailer_id);
			
			if($data['status']==ORDER_ACCEPTED){
				$row['message'] = "Your order ".$data['order_id']." is accepted by the retailer ". $retailer->display_name;
				$row['title'] = "Order Confirmation";
				$row['type'] = ORDER_CONFIRMATION_NOTIFICATION;
			}else if($data['status']==ORDER_DECLINED){
				$row['message'] = "Your order ".$data['order_id']." is declined by the retailer ". $retailer->display_name;
				$row['type'] = ORDER_DECLINED_NOTIFICATION;
				$row['title'] = "Order Declined";
			}else if($data['status']==ORDER_ONGOING){
				$row['message'] = "Your order ".$data['order_id']." is out for by the retailer ". $retailer->display_name;
				$row['type'] = ORDER_ONGOING_NOTIFICATION;
				$row['title'] = "Order Ongoing";
			}else if($data['status']==ORDER_DELIVERED){
				$row['message'] = "Your order ".$data['order_id']." is delivered by the retailer ". $retailer->display_name;
				$row['type'] = ORDER_DELIVERED_NOTIFICATION;
				$row['title'] = "Order Delivered";
			}else if($data['status']==ORDER_FAILED){
				$row['message'] = "Your order ".$data['order_id']." is failed by the retailer ". $retailer->display_name;
				$row['type'] = ORDER_FAILED_NOTIFICATION;
				$row['title'] = "Order Failed";
			}else {
				$row['message'] = "";
				$row['type'] = "";
				$row['title'] = "Order Confirmation";
			}
			
			$row['order_id'] = $data['order_id']; 
			$row['notified_to'] = $order_data[0]->user_id;
			$row['requested_by'] = $order_data[0]->retailer_id;
			$row['reason'] = $comments;
			
			
			//-------1. PUSH NOTIFICATION----------//
			$notify = new Notification();
			$notify->send_notification($row);
			
			//-------2. TEXT MESSAGE NOTIFICATION----------//
			//Implement SMS gateway here
			
			return true;
		}
		
	}
	/*
	 * @author: Anjani Gupta
	 * @date: 31/10/2017
	 * @method: check_order_status
	 * @desc: check order status
	 * @params: order_id
	 */
	public function check_order_status($data){
		$query = "SELECT status FROM `ti_order_detail` WHERE `order_id` = ".$this->db->escape($data['order_id'])." GROUP BY order_id";
		$res = $this->db->query($query);
		$status = $res->row()->status;
		return $status;
	}
	/*
	 * Description: Product grouping by category id
	 */
	private function group_by_id($data){
		
		/* $products = array();
		$product = array();
		$count = 0;
		$key = $data[0]->category_id;
		$keyName = $data[0]->category_name;
		foreach ($data as $row){
			$count++;
			if($key==$row->category_id){
				array_push($product, $row);
				$products[$keyName] = $product;
				if($count==sizeof($data)){
					$products[$keyName] = $product;
					$product = array();
					array_push($product, $row);
					$key = $row->category_id;
					$keyName = $row->category_name;
				}
			}else{
				$products[$keyName] = $product;
				$product = array();
				array_push($product, $row);
				$key = $row->category_id;
				$keyName = $row->category_name;
			}
		}
		
		return  $products; */
		
		foreach($data as $pr)
		{
			$categories[$pr->category_name][] = $pr;
		}
		return $categories;
	}
	/*
	 * @author: Anjani Gupta
	 * @date: 02/11/2017
	 * @method: get_retailer_orders
	 * @desc: Get retailer orders
	 * @params: retailer_id
	 */
	public function get_retailer_orders($retailer_id){
		$this->db->select('order_detail.modification_date, order_detail.user_id, order_detail.weight, order_detail.order_id, order_detail.status,  order_detail.creation_date,  users.display_name order_by, order_status.title status_text');
		$this->db->join('users', 'users.id = order_detail.user_id', 'inner');
		$this->db->join('order_status', 'order_status.id = order_detail.status', 'inner');
		$this->db->where(array('retailer_id'=>$retailer_id));
		$this->db->group_by('order_detail.order_id');
		$this->db->order_by('order_detail.status');
		$query = $this->db->get('order_detail')->result();
		
		
		$orders = array();
		$order = array();
		$count = 0;
		$key = $query[0]->status;
		$keyName = $query[0]->status_text;
		foreach ($query as $row){
			$count++;
			if($key==$row->status){
				array_push($order, $row);
				if($count==sizeof($query)){
					$orders[$keyName] = $order;
					$order= array();
					array_push($order, $row);
					$key = $row->status;
					$keyName = $row->status_text;
				}
			}else{
				$orders[$keyName] = $order;
				$order= array();
				array_push($order, $row);
				$key = $row->status;
				$keyName = $row->status_text;
				
				if($count==sizeof($query)){
					$orders[$keyName] = $order;
					$order= array();
					array_push($order, $row);
				}
				
			}
			
		} 
		
		
		return $orders;
		
	}
	
	/*
     * @author: Anjani Gupta
     * @date: 02/11/2017
     * @method: retailer_order_detail
     * @desc: Get retailer order detail
     * @params: retailer_id, order_id
     */
	public function retailer_order_detail($data){
		$query = "SELECT tu.display_name, od.user_id, od.product_id, od.weight, od.quantity, od.price, od.address, od.payment_mode, od.status, od.creation_date requested_date, od.order_id order_number, 
						p.title, p.type, ua.name receiver, ua.mobile receiver_mob, ua.address receiver_add, ua.city, ua.state, ua.pincode, ua.is_default FROM ti_order_detail od INNER JOIN ti_product p ON od.product_id=p.id
						INNER JOIN ti_users_address ua ON ua.id=od.address INNER JOIN ti_users tu ON tu.id=od.user_id WHERE od.order_id=".$this->db->escape($data['order_id'])." AND od.retailer_id=".$this->db->escape($data['retailer_id']);
		
		$result = $this->db->query($query)->result();
		
		$orderDetails = array();
		
		$orderDetails['user_id'] = $result[0]->user_id;
		$orderDetails['order_by'] = $result[0]->display_name;
		$orderDetails['address'] = array('name'=>$result[0]->receiver, 'mobile'=>$result[0]->receiver_mob, 'address'=>$result[0]->receiver_add, 'city'=>$result[0]->city, 'state'=>$result[0]->state, 'pincode'=>$result[0]->pincode, 'is_default'=>$result[0]->is_default);
		$orderDetails['payment_mode'] = $result[0]->payment_mode;
		$orderDetails['status'] = $result[0]->status;
		$orderDetails['complete_status'] = $this->db->select('status, delivered_at, created_date as status_date')->where('order_number', $result[0]->order_number)->get('trans_order_status')->result();
		$orderDetails['requested_date'] = $result[0]->requested_date;
		$products = array();
		foreach ($result as $row){
	
				$product = array();
				$product['id'] = $row->product_id;
				$product['title'] = $row->title;
				$product['type'] = $row->type;
				$product['quantity'] = $row->quantity;
				$product['price'] = $row->price;
				$product['weight'] = $row->weight;
				array_push($products, $product);
			
		}
		
		$orderDetails['product'] = $products;
		
		return $orderDetails;
	}
	
	private function group_by_key($array, $key) {
		$return = array();
		foreach($array as $val) {
			$return[$val[$key]][] = $val;
		}
		return $return;
	}
	
	//update user address
	public function update_address($address){
		$this->db->trans_start();
		
		$data=array();
		$data['name'] = $address['name'];
		$data['mobile'] = $address['mobile'];
		$data['address'] = $address['address'];
		$data['city'] = $address['city'];
		$data['state'] = $address['state'];
		$data['pincode'] = $address['pincode'];
		$data['is_default'] = $address['is_default'];
		$data['modified_on'] = date('Y-m-d H:i:s');
		
		
		
		if($address['is_retailer']){ //retailer
			//udapte user table
			$this->db->where (array('id' => $address['user_id']))->update ('users', array('lat'=>$address['latitude'], 'lng'=>$address['longitude']));
		}else{ //customer
			if($data['is_default']=='1'){
				//set is_default to zero for all address
				$this->db->where (array('user_id' => $address['user_id']))->update ('users_address', array('is_default'=>'0', 'modified_on'=>date('Y-m-d H:i:s')));
			}
		}
		
		$this->db->where (array('id' => $address['id']))->update ('users_address', $data);
		
		$this->db->select('users_address.*, users.lat latitude, users.lng longitude');
		$this->db->from('users_address');
		$this->db->join('users', 'users.id = users_address.user_id');
		$this->db->where (array('users_address.id' => $address['id']));
		$res = $this->db->get ()->result();
		
		
		
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return false;
		}else{
			return $res[0];
		}
	}
	
	//update retailer store
	private function update_store($store){
		
		$this->db->where (array('id' => $store['id']))->update ('store', $store);
		$res = $this->db->where (array('id' => $store['id']))->get ('store')->result();
		return $res[0];
		
	}
	
	//delete user address
	public function delete_address($address){
		$this->db->trans_start();
		$data=array();
		$data['is_default'] = '0';
		$data['status'] = STATUS_DELETE;
		$data['modified_on'] = date('Y-m-d H:i:s');
		
		$check_addres = $this->db->where (array('user_id' => $address['user_id']))->get ('users_address')->result();
		
		if(sizeof($check_addres)==1){
			$this->db->where (array('id' => $address['id']))->update ('users_address', $data);
		}else if(sizeof($check_addres)>1){ //set default address randomaly for multiple address
			$status = $this->db->where (array('id' => $address['id']))->update ('users_address', $data);
			if($status){
				
				$data=array();
				$data['is_default'] = '1';
				$data['modified_on'] = date('Y-m-d H:i:s');
				$query = "SELECT * FROM ti_users_address WHERE user_id=".$this->db->escape($address['user_id']). " AND id!=".$this->db->escape($address['id']). " AND STATUS=".STATUS_ACTIVE;
				$result = $this->db->query($query);
				
				$addressId = $result->result()[0]->id;
				
				$this->db->where (array('id' => $addressId))->update ('users_address', $data);
			}
		}
		
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return false;
		}else{
			return true;
		}
	}
	
	private function remove_cart_product($cart){
		$get_cart_product = "SELECT COUNT(*) count FROM ti_cart INNER JOIN ti_product ON ti_cart.product_id=ti_product.id WHERE ti_product.type=1 AND ti_cart.retailer_id=".$this->db->escape($cart['retailer_id'])." AND ti_cart.user_id=".$this->db->escape($cart['user_id']);
		$res = $this->db->query($get_cart_product);
		if($res->result()[0]->count=='0'){
			$data=array();
			$data['user_id'] = $cart['user_id'];
			$data['retailer_id'] = $cart['retailer_id'];
			return $this->empty_cart($data);
		}
	}
	/*
	 * @method: check order limit when user place any order
	 * 
	 */
	private function check_order_limit($product, $allowed_limits=false){
		$catIds = array();
		$cats = array();
		$status = true;
		foreach ($product as $val){
			
			$product_id = $val['product_id'];
			$quantity = $val['quantity'];
			$price = $val['price'];
			$weight = intval($val['weight']);
			$selected_limit = $quantity * $weight;
			
			$get_cat = $this->get_cat_by_product($product_id);
			$cat_id = $get_cat->category_id;
			
			if(sizeof($cats)>0){
				if(!in_array($cat_id, $cats)){
					array_push($cats, $cat_id);
					$catIds[$cat_id] = $selected_limit;
				}else{
					$catIds[$cat_id] = $catIds[$cat_id]+$selected_limit;
				}
			}else{
				
				array_push($cats, $cat_id);
				$catIds[$cat_id] = $selected_limit;
				
			}
			
			
		}
		
		
		
		if($allowed_limits){
			foreach ($catIds as $k=>$v){
				$cat_id = $k;
				$max_limit = $allowed_limits[$k];
				if(intval($max_limit)<intval($v)){
					$status = false;
					break;
				}
				
			}
		}else{
			foreach ($catIds as $k=>$v){
				$cat_id = $k;
				$query = $this->db->where(array('category_id'=>$cat_id))->get('order_limit')->result();
				
				if(sizeof($query)>0){
					
					$max_limit = $query[0]->max_limit;
					
					if(intval($max_limit)<intval($v)){
						
						$status = false;
						break;
					}
				}
				
			}
		}
		
		return $status;
	}
	
	private function get_cat_by_product($product_id){
		$query = $this->db->select('*')->where(array('id'=>$product_id, 'type' => '1'))->get('product')->result();
		return $query ? $query[0] : NULL;
	}
	
	
	/*
	 * @method: update_terms
	 * @date: 09-11-2017
	 * @params: state_id, is_accepted, user_id
	 */
	
	public function update_terms($terms){
		$data = array();
		$data['state_id'] = $terms['state_id'];
		$data['user_id	'] = $terms['user_id'];
		$data['is_accepted'] = $terms['is_accepted'];
		
		$query = $this->db->where(array('state_id'=>$terms['state_id'], 'user_id'=>$terms['user_id']))->get('is_terms_accepted')->result();
		
		if(sizeof($query)>0){
			$data['modification_date'] = date('Y-m-d H:i:s');
			$response = $this->db->where(array('state_id'=>$terms['state_id'], 'user_id'=>$terms['user_id']))->update('is_terms_accepted', $data);
		}else{
			$data['creation_date'] = date('Y-m-d H:i:s');
			$response = $this->db->insert('is_terms_accepted', $data);
		}
		
		return $response;
	}
	
	public function subcategory_listing($data){
		$res = array();
		$cat = $this->db->select('id category_id, title category_name')->where('id',$data['category_id'])->get('category')->result();
		$subcategory = $this->db->select('id, title name, type, description')->where('parent_id',$data['category_id'])->get('category')->result();
		if($subcategory){
			$res['category_id'] = $cat[0]->category_id;
			$res['category_name'] = $cat[0]->category_name;
			$res['subcategories'] = $subcategory;
		}
		return $res;
		
	}
	/*
	 * Check per day order limit for a single user
	 * 
	 */
	private function get_todays_order($user_id, $categoryIds){
		
		$response = array();
		$status = true;
		$catIds = array();
		$cats = array();
		$allowed_limits = array();
		$query = "SELECT user_id, product_id, SUM(weight * quantity) AS total FROM ti_order_detail WHERE status NOT IN (".ORDER_DECLINED.",". ORDER_FAILED.") AND user_id=".$this->db->escape($user_id)." AND date(`creation_date`) = CURDATE() GROUP BY product_id";
		$gettodaysorder= $this->db->query($query)->result();
		
		if(sizeof($gettodaysorder)>0){
			foreach ($gettodaysorder as $row){
			
				$product_id = $row->product_id;
				$get_cat = $this->get_cat_by_product($product_id);
				$cat_id = $get_cat->category_id;
				
				if(sizeof($cats)>0){
					if(!in_array($cat_id, $cats)){
						array_push($cats, $cat_id);
						$catIds[$cat_id] = $row->total;
					}else{
						$catIds[$cat_id] = $catIds[$cat_id]+$row->total;
					}
				}else{
					array_push($cats, $cat_id);
					$catIds[$cat_id] = $row->total;
				}
				
			}
			
			foreach ($catIds as $k=>$v){
				if(in_array($k, $categoryIds)){
					$selected_limit = $v;
					$cat_name = $this->get_all_categories($k)[0]->title;
					$query = $this->db->where(array('category_id'=>$k))->get('order_limit')->result();
					if(sizeof($query)>0){
						
						$max_limit = $query[0]->max_limit;
						$allowed_limits[$k] = $max_limit-$selected_limit;
						if(intval($max_limit-$selected_limit)<=0){
							
							$status = false;
							break;
							
						}
						
						
					}
					
				}
			}
			
		}
		
		if(!$status){
			$response['status'] = false;
			$response['msg'] = "Sorry! Your order limit is exceeded for the category ".$cat_name.". Please select product in other categories.";
			$response['data'] = array("id"=>$cat_id, "category"=>$cat_name);
		}else{
			$response['status'] = true;
			$response['msg'] = "Allowed to book your order.";
			$response['data'] = $allowed_limits;
		}
		
		return $response;
	}
	
	private function get_category_ids($product){
		$cat_ids = array();
		$product_ids = array();
		foreach ($product as $val){
			array_push($product_ids, $val['product_id']);
		}
		
		$ids = join("','", $product_ids);
		
		$query = "SELECT DISTINCT category_id FROM ti_product WHERE id IN ('$ids')";
		$res = $this->db->query($query)->result_array();
		
		
		foreach ($res as $row){
			$catids[] = $row['category_id'];
		}
		
		return $catids;
	}
	
	/*
	 * @method: holidays
	 * @date: 14-11-2017
	 * @params: state_id
	 */
	
	public function get_holidays($data){
		$query = $this->db->where('state_id',$data['state_id'])->get('state_holidays')->result();
		return $query?$query:NULL;
	}
	
	public function retailer_rating($data){
		$rating_data = array(); 
		$response = array();
		$users = 0;
		$query = "SELECT r.rating, COUNT(r.rating) rating_count, (SELECT count(DISTINCT user_id) FROM ti_rating rt WHERE rt.rating = r.rating AND rt.retailer_id=".$this->db->escape($data['retailer_id']).") users FROM ti_rating r WHERE r.retailer_id=".$this->db->escape($data['retailer_id'])." GROUP BY r.rating ORDER BY r.rating";
		$res = $this->db->query($query);
		$rd=array();
		if($res->num_rows()>0){
			
			 foreach ($res->result() as $row){
			 	$r = array();
			 	$rd[] =  $row->rating;
				$r[$row->rating] = $row->users;
				array_push($rating_data, $r);
				$users = $users + $row->users;
			}  
			$result = (array)$res->result();
			$myRatings = array(1,2,3,4,5);
			$diff= array_diff($myRatings,$rd);
			//print_r($diff);die;
			/* for($i=1;$i<count($res->result());$i++){
				$rd[$i]	= $res->result()[$i];
			}
			 */
			$m =array();
			$t=false;
			foreach ($diff as $d=>$val){
				$t=$val;
				$m[]=array('rating'=>$val,'rating_count'=>0,'users'=>0);
			}
			array_splice($result,1,0,$m);
			$response["rating"] = round(rating_calculation($rating_data),1);
			$response["users"] = $users;
			$response["rating_detail"] = $result;
		}
		return $response;
		
	}
	
	public function request_count($data){
		//$query = "SELECT COUNT(DISTINCT order_id) request_count FROM `ti_order_detail` WHERE `retailer_id`=".$this->db->escape($data['retailer_id'])." AND status=".ORDER_REQUESTED;
		//$res = $this->db->query($query);
		//return $res->row()->request_count;
		$r=array();
		$r['retailer_id'] = $data['retailer_id'];
		$r['order_id'] = '0';
		$order_count = sizeof($this->requested_orders($r));
		return $order_count;
	}
	
	public function checkorder_rating($user_id)
	{
		$query =  $this->db->query('SELECT orders.order_number from  ti_order orders INNER JOIN ti_order_detail od ON orders.order_number=od.order_id WHERE orders.user_id = '.$user_id.' AND od.status='.ORDER_DELIVERED.'  order by orders.created_date desc limit 1')->row();
		if($query)
		{
		  $query1 = $this->db->query('SELECT id ,order_number,rating from ti_rating WHERE order_number = "'.$query->order_number.'"')->row();
		  return (empty($query1)) ?	$query->order_number :false;
			
		}else{ return false; }
		
	}
	
	
	private function send_mail($email){
		
		$subject = "Tiplur | Forgot Password";
		$randcode = $this->generate_password(6);
		$msg = "Your new login password is ".$randcode;
		
		$result = smtp_mail($email, $msg, $subject);
		
		if($result){
			$update_data = array('password' => md5($randcode));
			$this->db->where(array('email' => $email));
			return  $this->db->update('users', $update_data);
		}else{
			return false;
		}
		
	}
	
	private function generate_password($length){
		$alphabets = range('A','Z');
		$numbers = range('0','9');
		$additional_characters = array('$','@');
		$final_array = array_merge($alphabets,$numbers,$additional_characters);
		
		$password = '';
		
		while($length--) {
			$key = array_rand($final_array);
			$password .= $final_array[$key];
		}
		
		return $password;
	}

}

?>