<div id="page-wrapper" class="gray-bg dashbard-1">
    <?php
    $url = $this->uri->segment_array(); //prd($result);
    $name = (end($url) == 'promotions') ? 'Promotion Management' : 'Rewarded Users';
    $segment = $this->uri->segment(3);
    breadcrumbs(array(uri_string() => $name));
    ?>
    <div class="row border-bottom">
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <?php print_flash_message(); ?>
                <div class="ibox float-e-margins">

                    <div class="ibox-title">
                        <h2><?php echo (end($url) == 'users') ? 'View Retailers' : 'Users Reward Points'; ?></h2>

                        <span class="pull-right">
                           <!--  <a data-toggle="tooltip" title="Download Excel" href="<?php
                            $type = !empty($segment) ? 'customer' : 'user';
                            echo base_url('admin/promotions/export_partiUsers?type=' . $type . '&') . http_build_query($_GET);
                            ?>" class="">
                                <img src="<?php echo base_url('assets/images/exldwn.ico'); ?>" height="40" width="40">
                            </a> -->
                        </span>
                    </div>
                    <div class="ibox-content borderNone">
                        <div class="table-responsive">
                            <?php if (!empty($result)): ?>
                                <?php echo form_open($this->uri->uri_string(), array('class' => "form-horizontal", 'id' => 'cat-listing', 'method' => 'get')); ?>
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Mobile</th>
                                            <th>Total Reward Points</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($result as $val): ?>

                                            <tr>
                                                <td ><?php echo $val->display_name; ?></td>
                                                <td ><?php echo $val->mobile; ?></td>
                                                <td ><span class="badge badge-success"><?php echo $val->total_rewards; ?></span></td>
                                            </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>

                               
                                <?php
                                echo form_close();
                            else:
                                ?>
                                <tr><td colspan="12">No Record Found.</td></tr>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <div class="dataTables_info tableData" id="editable_info" role="status" aria-live="polite"></div>
            </div>
            <?php echo $this->pagination->create_links(); ?>
        </div>
    </div>
</div>
