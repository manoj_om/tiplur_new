$('.datepicker').datetimepicker({
    format: 'YYYY-MM-DD'
});

$('.order-decline').click(function (e) {
    e.preventDefault();
    var id = $(this).attr('data-id');
    $('#order_id').val(id);
    $("#declineOrderModal").modal({backdrop: 'static',keyboard: false});
});


$('.permit-view').on('click', function () {

    var img = $(this).children('img').attr('src');

    $("#myModal .modal-body").children('img').attr('src', img);

    $("#myModal").modal('show');

});
