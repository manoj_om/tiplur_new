<div id="page-wrapper" class="gray-bg dashbard-1">
    <?php $url = $this->uri->segment_array();

    $name = (end($url) == 'promotions') ? 'Promotion Management' : 'Promotion Management';
    $segment = $this->uri->segment(3);
    breadcrumbs(array(uri_string() => $name));
    ?>
    <div class="row border-bottom">
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <?php print_flash_message(); ?>
                <div class="ibox float-e-margins">

                    <div class="ibox-title" style="margin-bottom: 20px;">
                        <?php echo form_open($this->uri->uri_string(), array('method' => 'get')); ?>
                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group formWidht">
                                    <label>Brand name</label>
                                    <input type="text" placeholder="Brand Name" name="brand_name" id="exampleInputEmail2" class="form-control formWidht" value="<?php echo isset($_GET['brand_name']) ? $_GET['brand_name'] : ''; ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group formWidht">
                                    <label>Brand Promotion City</label>
                                    <input type="text" placeholder="City" name="city" id="exampleInputEmail2" class="form-control formWidht" value="<?php echo isset($_GET['brand_name']) ? $_GET['brand_name'] : ''; ?>">
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group formWidht">
                                    <label>Pincode</label>
                                    <input type="text" placeholder="Pincode" name="pincode" id="exampleInputEmail2" class="form-control formWidht" value="<?php echo isset($_GET['brand_name']) ? $_GET['brand_name'] : ''; ?>">
                                </div>
                            </div>


                            <div class="col-md-2 filter-div">
                                <input type="submit" name="promo_filter" id="" class="btn btn-success" value="Filter">
                                <?php
                                if (isset($_GET['promo_filter'])):
                                    ?>
                                    <a href="<?php echo (!empty($segment) && $segment == 'promotions') ? base_url('admin/promotions') : base_url('admin/promotions/participate_user'); ?>" class="btn btn-warning">Reset</a>
                                <?php endif; ?>
                            </div>

                        </div>
                        <?php echo form_close(); ?>
                    </div>

                    <!--div class="ibox-title">
                        <h2><?php //echo (end($url) == 'users') ? 'View Retailers' : 'View Customers'; ?></h2>

                        <span class="pull-right">
                            <a data-toggle="tooltip" title="Download Excel" href="<?php
                            $type = !empty($segment) ? 'promotions' : 'promotions';
                            echo base_url('admin/users/export_users?type=' . $type . '&') . http_build_query($_GET);
                            ?>" class="">
                                <img src="<?php echo base_url('assets/images/exldwn.ico'); ?>" height="40" width="40">
                            </a>
                        </span>
                    </div-->
                    <div class="ibox-content borderNone">
                        <div class="table-responsive">
                            <?php if (!empty($result)): ?>
                                <?php echo form_open($this->uri->uri_string(), array('class' => "form-horizontal", 'id' => 'cat-listing', 'method' => 'get')); ?>
                                <table class="table table-striped" id="mytable">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><input type="checkbox"  class="i-checks check-all" name="check_all"></th>
                                            <th>Brand Name</th>
                                            <th>Promotion Title</th>
                                            <th>Promotion ID</th>
                                            <th>Users Count</th>
                                            <!-- <th>Promotion Users Count</th> -->
                                            <th>Promotion Image</th>
                                            <th>Promotion Banner</th>
                                            <th>Released date</th>
                                            <th>Expiry date</th>
                                            <th>City</th>
                                            <th>Pincode</th>
                                            <th>Gift/Rewards</th>
                                            <th>Promotion Status</th>
                                            <th>Action</th>
                                          </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($result as $val): //echo $val;?>

                                            <tr>
                                                <td class="text-center">
                                                    <input type="checkbox"  class="i-checks child" name="promotion_id[]" value="<?php echo $val->promotion_id; ?>">
                                                </td>
                        												<td><?php echo $val->brand_name;?>	</td>
                        												<!-- <td><a href="javascript:void(0)" class="link promoted_user"><?php echo $val->promotion_title;?></a>	</td> -->
                        												<td><a href="<?php echo base_url('admin/promotions/promoted_users' . '/' . $val->promotion_id); ?>" ><?php echo $val->promotion_title;?>	</a></td>
                        												<td><?php echo $val->promotion_id;?>	</td>
                        												<td><?php $num = promoted_users($val->promotion_id); ?><span class="badge label-danger"> <?php echo $num->total; ?>	</span></td>
                        												<!-- <td><?php //echo ($val->count) ? $val->count : '';?>	</td> -->
                        												<td><a href="javascript:void(0)" class="doc-view"><img src="<?php echo ($val->promotion_img) ? base_url("/assets/images/promotion_image/$val->promotion_img")  : base_url('/assets/uploads/document_file/no_doc.png');?>" class="img-rounded" width="100" height="50" alt="brand_logo" class="img-responsive center-block"/></a>	</td>
                        												<td><a href="javascript:void(0)" class="doc-view"><img src="<?php echo ($val->prmototion_banner_url) ? base_url("/assets/images/promotion_image/$val->prmototion_banner_url")  : base_url('/assets/uploads/document_file/no_doc.png');?>" class="img-rounded" width="100" height="50" alt="brand_logo" class="img-responsive center-block"/></a>	</td>
                        												<td><?php echo site_date($val->released_date);?>	</td>
                        												<td><?php echo site_date($val->expiry_date);?>	</td>
                        												<td><?php echo $val->city;?>	</td>
                        												<td><?php echo $val->pincode;?>	</td>
                        												<td><?php echo $val->gifts_rewards;?>	</td>
                        												<td class="text-center"><?php if((promotion_expired($val->expiry_date,$val->promotion_id) == 'false') && ($val->promo_status == '1')):?>
                                                  <span class="badge badge-danger">Inactive</span>
                                              <?php else: ?>
                                                <span class="badge badge-success">Active</span>
                                              <?php endif;?>
                                              </td>
                                              <td class="text-center" width="100px;"><a href="<?php echo base_url('admin/promotions/promoted_users' . '/' . $val->promotion_id); ?>" ><i class="fa fa-eye" aria-hidden="true"></i>&nbsp;&nbsp;<a href="<?php echo base_url('admin/promotions/edit' . '/' . $val->id); ?>" ><i class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;<a href="javascript:void(0);" class="delete" form="product-listing" promotion_id="<?php echo $val->promotion_id; ?>"  ><i class="fa fa-trash fa-lg" aria-hidden="true"  ></i></td>

                                              </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                              <button name="multi_activate" type="submit" class="viewActivateButton" value="multi_activate_action">ACTIVATE</button>
                                <button name="multi_deactivate" type="submit" class="viewDeactivateButton" value="multi_deactivate_action">DEACTIVATE</button>
                                <!-- <button name="multi_delete" type="submit" class="viewDeleteButton" value="multi_delete_action">Multi-Delete</button> -->
                                <?php
                                echo form_close();
                            else:
                                ?>
                                <tr><td colspan="12">No Record Found.</td></tr>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <div class="dataTables_info tableData" id="editable_info" role="status" aria-live="polite"></div>
            </div>
            <?php echo $this->pagination->create_links(); ?>
        </div>
    </div>
</div>

<div class="modal fade" id="popupModal"  role="dialog" style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content"></div>
    </div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Promotion Image</h4>
            </div>
            <div class="modal-body">
                <img src="" class="img-responsive center-block">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
      </div>
</div>


</div>
