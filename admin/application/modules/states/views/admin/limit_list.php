<div id="page-wrapper" class="gray-bg dashbard-1">
    <?php breadcrumbs(array('admin/states/product-limit-list' => 'Manage Products Limit')); ?>
    <div class="row border-bottom">
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12 " style="margin-top: 40px;">
                <?php print_flash_message(); ?>
                <div class="ibox float-e-margins">

                    <div class="ibox-title" style="margin-bottom: 20px;">
                        <?php echo form_open($this->uri->uri_string(), array('method' => 'get')); ?>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group formWidht">
                                    <label>State Name</label>
                                    <?php
										$state_id = isset($_GET['state_id']) ? $_GET['state_id'] : '';
										state_dropdown($state_id, 'form-control');
                                    ?> 
                                </div>
                            </div>
                            <div class="col-md-2 filter-div">
                                <input type="submit" name="filter" id="" class="btn btn-success" value="Filter">
                                <?php if (isset($_GET['filter'])): ?>
                                    <a href="<?php echo base_url('admin/states/product-limit-list'); ?>" class="btn btn-warning">Reset</a>
                                <?php endif; ?>
                            </div>

                        </div>
                        <?php echo form_close(); ?>
                    </div>

                    <div class="ibox-title">
                        <h2>View Products Limit </h2>
                        <div class="ibox-tools" style="display: inline-block; float: right;">  
                            <a href="<?php echo base_url('admin/states/product-limit-create'); ?>" class="btn btn-primary block full-width m-b createuser">ADD LIMIT</a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <?php echo form_open($this->uri->uri_string(), array('class' => "form-horizontal", 'id' => 'product-listing', 'method' => 'get')); ?>
                        <div class="ibox-content borderNone">

                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <td class="text-center"><input type="checkbox"  class="i-checks check-all" name="check_all"></td>
                                        <th>State Name</th>                                        
                                        <th>Limits</th>
                                        <th>Creation Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>                                    
                                    <?php
                                    if ($result): 
									    foreach ($result as $val):
										$quantity  = explode(',',$val->quantity); 
										$brandname = explode(',',$val->brandname);
                                        $weight    = explode(',',$val->weight); 										
                                    ?>
                                            <tr>
                                                <td class="text-center">
                                                    <input type="checkbox"  class="i-checks child" name="state_id[]" value="<?php echo $val->state_id; ?>">
                                                </td>
												<td><?php echo $val->state; ?></td>
                                                <td><ul>
												   <?php if(!empty($brandname)): foreach($brandname as $k => $b): ?>
												      <li><?php echo isset($b) ? $b:''; ?> 
													  (<?php echo (isset($quantity[$k]) && isset($weight[$k])) ? $quantity[$k].' bottles of '.$weight[$k].' ML':'';?> )</li>
												   <?php  endforeach; endif;?>
												</ul>
												</td>                                                
                                                <td><?php echo date('F m,Y',strtotime($val->creation_date)); ?>	</td>
                                                <td class="text-center">
                                                    <a  href="<?php echo base_url('admin/states/product-limit-edit' . '/' . $val->state_id); ?>" ><i class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i></a>&nbsp;&nbsp;
                                                </td>
                                            </tr> 
                                            <?php 
                                        endforeach;
                                    else:
                                        ?>
                                        <tr><td colspan="12">Result not found.</td></tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                            </form>

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <div class="dataTables_info tableData" id="editable_info" role="status" aria-live="polite"></div>
            </div>
            <?php echo $this->pagination->create_links(); ?>

        </div>
    </div>
</div>

</div>