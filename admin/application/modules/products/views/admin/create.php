<div id="page-wrapper" class="gray-bg dashbard-1">
    <?php breadcrumbs(array('admin/products' => 'Manage Product', 'admin/products/create' => 'Add Products')); ?>

    <div class="row border-bottom">
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <?php if ($this->session->flashdata('message')) { ?>
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <?php echo $this->session->flashdata('message'); ?>
                    </div>
                <?php } ?>
                <div class="ibox float-e-margins">

                    <div class="ibox-title">
                        <h1>Add Product</h1>
                        <div class="ibox-tools">
                            <span>Bulk Upload</span>
                            <a href="<?php echo base_url('admin/products/upload'); ?>" type="submit" class="btn btn-primary block full-width m-b addProductBtn">BROWSE</a>
                        </div>
                    </div>
                    <?php echo form_open_multipart($this->uri->uri_string(), 'class="form-horizontal"'); ?>
                        <div class="ibox-content contentBorder">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 AddProdctInputCont">
                                    <div class="form-group formWidht">
                                        <label>Product Type   <span style="color: red;">*</span></label><br>
                                        <label class="radio-inline">
                                            <input type="radio" class="product-type" value='1' <?php echo (isset($_POST['type']) && $_POST['type'] == 1) ? "checked" : ""; ?> name="type">Liquor
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" class="product-type" value='2' <?php echo (isset($_POST['type']) && $_POST['type'] == 2) ? "checked" : ""; ?> name="type" checked>Snacks
                                        </label>
                                        <span class='error vlError'><?php echo form_error('type'); ?></span>
                                    </div>

                                </div>

                                <div class="category-div">
                                    <div class="col-lg-4 col-md-4 col-sm-4 AddProdctInputCont">
                                        <div class="form-group formWidht">
                                            <label>Category <span>*</span></label>
                                            <select name="category_id" class="form-control m-b addContDrop m-b-0">
                                                <option value="">Please select</option>
                                                <?php product_cat('', 2); //snacks ?>
                                            </select>
                                            <span class='error vlError'><?php echo form_error('category_id'); ?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="sub-cat-div" style="display: none"></div>

                                <div class="col-lg-4 col-md-4 col-sm-4 AddProdctInputCont">
                                    <div class="form-group formWidht">
                                        <label>Brand Title <span style="color: red;">*</span></label>
                                        <input type="text" placeholder="Title" name="title" id="title" value="<?php echo set_value('title', ''); ?>" class="form-control formWidht" required>
                                        <span class='error vlError'><?php echo form_error('title'); ?></span>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-4 AddProdctInputCont price-box" style="display: none;">
                                    <div class="form-group formWidht">
                                        <label>Price <span style="color: red;">*</span></label>
                                        <input type="text" placeholder="product price" value="<?php echo set_value('price', ''); ?>" name="price" id="product-price" class="form-control formWidht">
                                        <span class='error vlError'><?php echo form_error('price'); ?></span>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 AddProdctInputCont">
                                    <div class="form-group formWidht">
                                        <label>Special price </label>
                                        <input type="text" placeholder="Special price" value="<?php echo set_value('special_price', ''); ?>" name="special_price" id="Special" class="form-control formWidht">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 AddProdctInputCont weight-box" style="display: none;">
                                    <div class="form-group formWidht">
                                        <label>Quantity (ml)<span style="color: red;">*</span></label>
                                        <input type="text" placeholder="Quantity in ml" name="weight" id="weight" value="<?php echo set_value('weight', ''); ?>" class="form-control formWidht">
                                        <span class='error vlError'><?php echo form_error('weight'); ?></span>
                                    </div>
                                </div>

                                <div class="col-lg-2 col-md-2 col-sm-2 AddProdctInputCont plate_type">
                                    <div class="form-group formWidht">
                                        <label>Full Plate Price<span style="color: red;">*</span></label>
                                        <input type="text" placeholder="Full Plate Price" name="full_price" value="<?php echo set_value('full_price', ''); ?>" class="form-control formWidht">
                                        <span class='error vlError'><?php echo form_error('full_price'); ?></span>
                                    </div>
                                </div>

                                <div class="col-lg-2 col-md-2 col-sm-2 AddProdctInputCont plate_type">
                                    <div class="form-group formWidht">
                                        <label>Half Plate Price</label>
                                        <input type="text" placeholder="Half Plate Price" name="half_price" value="<?php echo set_value('half_price', ''); ?>" class="form-control formWidht">
                                        <span class='error vlError'><?php echo form_error('half_price'); ?></span>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-4 AddProdctInputCont">
                                    <div class="form-group formWidht" style="padding-top: 14px"><br>
                                        <label>IS COD : -
                                            <input type="checkbox" value='1' <?php echo (isset($_POST['is_cod']) && $_POST['is_cod'] == 1) ? "checked" : ""; ?> name="is_cod">
                                        </label>
                                        <label style="margin-left:100px;">IN Store  : -
                                            <input type="checkbox" value='1' <?php echo (isset($_POST['in_store']) && $_POST['in_store'] == 1) ? "checked" : ""; ?> name="in_store">
                                        </label>
                                    </div>

                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-4 AddProdctInputCont">
                                    <div class="form-group formWidht">
                                        <label>Retailer<span style="color: red;">*</span></label>
                                        <select name="retailer_id" class="form-control m-b addContDrop m-b-0" required>
                                            <option value="">Select</option>
                                            <?php
                                            $retailer = retailers();
                                            foreach ($retailer as $val):
                                                ?>
                                                <option value="<?php echo $val->id; ?>" <?php echo ($val->id == @$_POST['retailer_id']) ? 'selected' : ''; ?>  >
												<?php if($val->status =='9')
											      { echo ucfirst($val->display_name).' (Mapped)';
												  }else{ echo ucfirst($val->display_name);  }
											    ?>
												</option>
                                            <?php endforeach; ?>
                                        </select>
                                        <span class='error vlError'><?php echo form_error('retailer_id'); ?></span>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-4 AddProdctInputCont">
                                    <div class="form-group formWidht">
                                        <label>Status<span style="color: red;">*</span></label>
                                        <select name="status" class="form-control m-b addContDrop m-b-0" required>
                                            <?php
                                            $status = status();
                                            foreach ($status as $k => $val):
                                                ?>
                                                <option value="<?php echo $k; ?>" <?php echo ($k == @$_POST['status']) ? 'selected' : ''; ?>  ><?php echo $val; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <span class='error vlError'><?php echo form_error('status'); ?></span>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-4 AddProdctInputCont food-type">
                                    <div class="form-group formWidht">
                                        <label>Food Type   <span style="color: red;">*</span></label>
                                        <select name="food_type" class="form-control m-b addContDrop m-b-0" required>
                                            <?php
                                            $food_type = food_type();
                                            foreach ($food_type as $k => $val):
                                                ?>
                                                <option value="<?php echo $k; ?>" <?php echo ($k == @$_POST['food_type']) ? 'selected' : ''; ?>  ><?php echo $val; ?></option>
                                            <?php endforeach; ?>
                                            <span class='error vlError'><?php echo form_error('food_type'); ?></span>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 AddProdctInputCont">
                                    <div class="form-group formWidht">
                                        <label>Description <span style="color: red;">*</span></label>
                                        <textarea type="description" name="description" placeholder="Description" id="description" class="form-control formWidht"><?php echo set_value('description', ''); ?></textarea>
                                        <span class='error vlError'><?php echo form_error('description'); ?></span>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4" >

                                <div class="form-group formWidht">
                                    <label>Product Image *</label>
                                    <input name="image" id="image" required="required" class="file dis-inline formWidht chooseBtnStyle" accept="xls" type="file">
                                    <span class='error vlError'><?php echo form_error('is_liquor'); ?></span>
                                </div>

                        </div>
                            </div>
                            <div class="ibox-content contentBorder">
                                <div class="col-lg-12 col-md-12 col-sm-12  text-right" style="">
                                    <input type="button" name="save" value="ADD PRODUCT" class="btn btn-primary block full-width m-b updateProductBtn addBtn"/>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
