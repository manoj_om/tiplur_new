<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tiplur</title>
    <link href="<?php echo base_url("theme/site/css/bootstrap.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("theme/site/font-awesome/css/font-awesome.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("theme/site/css/animate.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("theme/site/css/style.css"); ?>" rel="stylesheet">
</head>

<body class="gray-bg logingScreenBack" >
	<?php $this->load->view($view); ?>

    <!-- Mainly scripts -->
    <script src="<?php echo base_url("theme/site/js/jquery-2.1.1.js"); ?>">
	</script><script src="<?php echo base_url("theme/site/js/jquery.validate.min.js"); ?>"></script>
    <script src="<?php echo base_url("theme/site/js/bootstrap.min.js"); ?>"></script>
	<script> var base_url = "<?php echo base_url(); ?>"; </script>
</body>

</html>

<script>
 $(document).ready(function() {
		$("#FpModal").click(function () {
               $('#Forgot-Pass').modal({backdrop: 'static'});
   
        });
		
		
	$("#forgot_pass_form").validate(
	{
		rules: {
			email: {
				required: true,
				email: true
			},
		},
		messages: {
			email: "Please Enter Email-Id."

		},
		submitHandler: function (form)
		{
			var email = $('#forgot_pass_form input#email').val();
			var forgoturl = base_url + 'users/forgot_password';

			$.ajax({
				type: 'POST',
				url: forgoturl,
				data: "&email=" + email,
				success: (function (data) 
				{
					var obj = $.parseJSON(data);
					if (obj.status == 1) 
					{
						$('#fp_msg').html('Your Reset Password link has been sent to your Email-id.Please check your email-id.');
						$('#fp_msg_error').val();
						$('#forgot_pass_form input#email').val('');
						
					} else if (obj.status == 3) {
						$('#fp_msg_error').show();
						$('#fp_msg_error').html('Sorry,This email-id does not exist!');
						$('#fp_msg').val('');
						$('#forgot_pass_form input#email').val('');
						setTimeout(function () {
							$('#fp_msg_error').hide();
						}, 3000);

					} else if (obj.status == 2)
					{
						$('#fp_msg').val();
						$('#fp_msg_error').html('Whoops! This didn\'t work. Please contact us.');
						$('#password_forgot input#email').val('');
					}

				})
			});
		}

    });
		
		
 });		
</script>