<div id="page-wrapper" class="gray-bg dashbard-1">
    <div class="row wrapper border-bottom white-bg page-heading bradPading">
        <div class="col-lg-8">
            <!--ol class="breadcrumb">
                <li>
                    <a href="<?php base_url(); ?>dashboard">Home</a>
                </li>

            </ol-->
        </div>
        <div class="col-lg-4 text-right"><i class="fa fa-calendar" aria-hidden="true"></i><span class="dateMargin"><?php echo date('d F Y', strtotime(date('Y-m-d H:i:s'))); ?></span> </div>
    </div>
    <div class="row border-bottom">
    </div>
    <div class="row  border-bottom  dashboard-header">
        <div class="wrapper wrapper-content animated fadeInRight" style="padding-bottom: 0px;">
            <div class="row">
                <h2 class="alert alert-warning text-center text-success bold">Brand Promotion & Award Overview<small class="subHeadStyle"></small></h2>
                 <div class="col-lg-3">
                    <a href="<?php echo base_url();?>admin/promotions/participate_user">
                        <div class="widget style1 firstWidget">
                            <div class="row">
                                <div class="col-xs-4 text-center">
                                    <div class="customersImg"></div>
                                </div>
                                <div class="col-xs-8 textColor">
                                    <h2 class="font-bold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo ($participate_users_count) ? $participate_users_count : '0'; ?></h2>
                                    <span> PARTICIPATED USERS </span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3">
                    <a href="<?php echo base_url();?>admin/promotions">
                        <div class="widget style1 SecondtWidget">
                            <div class="row">
                                <div class="col-xs-4 text-center">
                                    <div class="orderImg"></div>
                                </div>
                                <div class="col-xs-8 textColor">
                                    <h2 class="font-bold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo ($promotion_total_count) ? $promotion_total_count : '0'; ?></h2>
                                    <span> TOTAL PROMOTIONS </span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3">
                    <a href="<?php echo base_url();?>admin/promotions">
                        <div class="widget style1 thirdWidget">
                            <div class="row">
                                <div class="col-xs-4 text-center">
                                    <div class="orderImg"></div>
                                </div>
                                <div class="col-xs-8 textColor">
                                    <h2 class="font-bold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo ($promotion_active_count) ? $promotion_active_count : '0'; ?></h2>
                                    <span> ACTIVE PROMOTIONS </span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3">
                    <a href="<?php echo base_url();?>admin/promotions">
                        <div class="widget style1 fourthWidget">
                            <div class="row">
                                <div class="col-xs-4 text-center">
                                    <div class="customersImg"></div>
                                </div>
                                <div class="col-xs-8 textColor">
                                    <h2 class="font-bold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo ($brands_count) ? $brands_count : '0'; ?></h2>
                                    <span> TOTAL BRANDS </span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            
                <div class="col-lg-3">
                    <a href="<?php echo base_url();?>admin/promotions/userwise_total_rewards">
                        <div class="widget style1 fourthWidget">
                            <div class="row">
                                <div class="col-xs-4 text-center">
                                    <div class="customersImg"></div>
                                </div>
                                <div class="col-xs-8 textColor">
                                    <h2 class="font-bold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo ($total_reward_points) ? $total_reward_points : '0'; ?></h2>
                                    <span>Total Reward Points </span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

            </div>



        </div>
    </div>
    <!-- <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Recent Orders</h5>
                    </div>
                    <div class="ibox-content">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Order No</th>
                                    <th>Customer</th>
                                    <th>Amount</th>
                                    <th>Date & Time</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($recent_orders):
								//print_r($recent_orders);die;
                                    foreach ($recent_orders as $recent):
                                        ?>
                                        <tr>
                                            <td><a href="<?php echo base_url('admin/orders/view_order?id=').$recent->order_id;?>"><?php echo $recent->order_id;?></a></td>
                                            <td><?php echo $recent->user;?></td>
                                            <td><?php echo intval($recent->total_amount);?></td>
                                            <td><?php echo site_date_time($recent->order_date); ?></td>
                                            <td><?php echo print_order_status($recent->status); ?></td>
                                        </tr>
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Pending Orders</h5>
                    </div>
                    <div class="ibox-content">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Order No</th>
                                    <th>Customer</th>
                                    <th>Amount</th>
                                    <th>Date & Time</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($process_order_list):
                                    foreach ($process_order_list as $process):
                                        ?>
                                        <tr>
                                            <td><a href="<?php echo base_url('admin/orders/view_order?id=').$process->order_id;?>"><?php echo $process->order_id;?></a></td>
                                            <td><?php echo $process->user;?></td>
                                            <td><?php echo intval($process->total_amount);?></td>
                                            <td><?php echo site_date_time($process->order_date); ?></td>
                                            <td><?php echo print_order_status($process->status); ?></td>
                                        </tr>
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
</div>
