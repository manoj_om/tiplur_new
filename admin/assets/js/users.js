

/* Add password*/
jQuery(document).on('click', '#change-password', function () {
    if ($(this).is(":checked")) {
        var type = $(this).val();
        ;
    }
    $('#form_add_password').html('');
    if (type == 1) {
        $html = $('#for-password').html();
        $('#form_add_password').append($html);
    }
});

/*
 Check All Feature
 */
$(".check-all").click(function (e) {
    e.stopPropagation();
    if ($(this).is(':checked')) {
        $('input.child').prop('checked', true);
    }
    else {
        $('input.child').removeAttr('checked');
    }
});

// var rotation = 0;
//     $("#rright").click(function() {
        
//         rotation = (rotation +90) % 360;
//         $(".pic-view").css({'transform': 'rotate('+rotation+'deg)'});
//         if(rotation != 0){
//             $(".rotate_image_save").show();
//             $(".pic-view").css({'width': '100%', 'height': '100%'});
            
//         }else{
//             $(".rotate_image_save").hide();
//             $(".pic-view").css({'width': '100%', 'height': '100%'});
//         }
//         $('#rotation').val(rotation);
//     });

jQuery('.delete').click(function (e) {
    e.stopPropagation();
    var user_id = $(this).attr('user_id');
    var answer = confirm("Are you sure you want to delete from the database?");
    if (answer) {
        $.ajax({
            type: "POST",
            url: base_url + 'admin/users/user_delete',
            data: "user_id=" + user_id,
            success: function (result) {
                var obj = jQuery.parseJSON(result);


                if (obj['status'] == true) {
                    window.location.href = base_url + 'admin/users';
                } else {
                    alert('Somthing wrong ');
                }
            }
        });
    }


});


$('.user_address_modal').on('click', function () {

    var user_id = $(this).attr('data-id');
    var user_type = $(this).attr('data-type');

    $.ajax({
        type: "GET",
        url: base_url + 'admin/users/user_address',
        data: {id: user_id, type: user_type},
        success: function (data)
        {
            $resl = jQuery.parseJSON(data);
            $("#popupModal .modal-content").html($resl.html);
            $("#popupModal").modal('show');

               /* time picker for store time edit*/
				$('.opening_time').datetimepicker({
				  format: 'LT'
				});
                $('.closing_time').datetimepicker(
				{
				   format: 'LT'
				});

				$(".opening_time").on("dp.change", function (e)
				{
				   $('.closing_time').data("DateTimePicker").minDate(e.date);
				});
			   /*end here*/
               /* update timings*/
				$('.retailer_time_edit').on('click', function () {
	                    $("#popupModal .EDIT-TIIME").show();
						$(".retailer_time_edit").hide();
						$(".retailer_time_update").show();
                });

				$( ".retailer_time_update").button().on( "click", function() {
					$opentime = $('#opening_time').val();
					$closetime = $('#closing_time').val();
					if(!$opentime || !$closetime)
					{
						$('#opening_time').focus();
						$('#closing_time').focus();
						return false;
					}else{
		             saveRetailertime();
					}
				});
              /*end here*/
        }
    });

});

function ShowTimes(IDS,flag) {
    var status = 'none';
    if (flag) { status = 'block'; }
    else{
        $("input[name='ad_pack_id1']:checkbox").prop('checked',false);
    }
    document.getElementById('timehide').style.display = status;
  }




function saveRetailertime()
{
    $postdata = $("#time-edit-form").serialize();
    console.log($postdata);
    $('.retailer_time_update').html('<i class="fa fa-spin fa-spinner" style="color:grey;"></i> LOADING....');
        $.ajax({
            type: "POST",
            url: base_url + 'admin/users/update_retailer_store_time',
            data: $postdata,
            success: function (data)
            {
                $resl = jQuery.parseJSON(data);
                if($resl.status == true){
                    $('#popupModal .EDIT-TIIME').hide();
                    $('#popupModal .modal-content').html($resl.html);
                    $('#popupModal .time_msg').show();
                    $('.retailer_time_update').hide();
                    $('.retailer_time_edit').hide();
                }else{
                    alert('something went wrong');
                }
            }
        });
}
var rotation = 0;
    $("#rright").click(function() {
       // alert(rotation);
        rotation = (rotation +90) % 360;
       // console.log(rotation);
        $(".pic-view").css({'transform': 'rotate('+rotation+'deg)'});
        if(rotation != 0){
            $(".rotate_image_save").show();
            $(".pic-view").css({'width': '100%', 'height': '100%'});

        }else{
            $(".rotate_image_save").hide();
            $(".pic-view").css({'width': '100%', 'height': '100%'});
        }
        $('#rotation').val(rotation);
    });
    $( ".rotate_image_save").on( "click", function() {
					$image_src = $("#myModal .modal-body").children('img').attr('src');
                    $rotation = $('#rotation').val();
                    console.log($image_src);
                    console.log($rotation);
					if(!$image_src || !$rotation)
					{ alert("hggh");
						return false;
					}else{
		             saveRotateImage($image_src,$rotation);
					}
				});
        function saveRotateImage($image_src,$rotation)
{
     $('.rotate_image_save').html('<i class="fa fa-spin fa-spinner" style="color:grey;"></i> LOADING....');
        $.ajax({
            type: "POST",
            url: base_url + 'admin/users/update_customer_rotate_image',
            data: {image_src:$image_src,rotation:$rotation},
            success: function (data)
            {
               console.log(data);
                 $resl = jQuery.parseJSON(data);
                 if($resl.status == true){
                     console.log($resl);
                    alert("sucessfuly save");
                    location.reload(true);
                }else{
                    alert('something went wrong');
                }
            }
        });
}

$('.doc-view').on('click', function () {

    var img = $(this).children('img').attr('src');
  console.log(img);
    $("#myModal .modal-body").children('img').attr('src', img);

    $("#myModal").modal('show');

});


$('.doc-reject').click(function (e) {
    e.preventDefault();
    var id = $(this).attr('data-id');
    $('#user_id').val(id);
    $("#docRejectModal").modal('show');
});
