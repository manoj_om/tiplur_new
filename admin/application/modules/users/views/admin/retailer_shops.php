<div id="page-wrapper" class="gray-bg dashbard-1">
    <?php breadcrumbs(array(uri_string() => 'Store Details')); ?>
    <div class="row border-bottom">
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <?php print_flash_message(); ?>
                <div class="ibox float-e-margins">

                    <div class="ibox-title" style="margin-bottom: 20px;">
                        <?php echo form_open($this->uri->uri_string(), array('method' => 'get')); ?>
                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group formWidht">
                                    <label>Keyword</label>
                                    <input type="text" placeholder="Store Name" name="name" id="exampleInputEmail2" class="form-control formWidht" value="<?php echo isset($_GET['name']) ? $_GET['name'] : ''; ?>">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group formWidht">
                                    <label>Retailer Type</label>
									<?php $type = array('1'=>'Registered','2'=>'Mapped'); ?>
									<select id="type" name="type" class="form-control formWidht">
									<option value="">Select</option>
										<?php foreach($type as $k=>$v): ?>
											  <option value="<?php echo $k; ?>" <?php echo (isset($_GET['type']) && $_GET['type'] == $k) ? 'selected=selected':''; ?> ><?php echo $v; ?></option>
										<?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group formWidht">
                                    <label>Store Status</label>
									<?php $status = array('1'=>'Active','2'=>'Inactive','3'=>'Closed'); ?>
                                    <select id="status" name="status" class="form-control formWidht">
									<option value="">Select</option>
										<?php foreach($status as $k=>$v): ?>
											  <option value="<?php echo $k; ?>" <?php echo (isset($_GET['status']) && $_GET['status'] == $k) ? 'selected=selected':''; ?> ><?php echo $v; ?></option>
										<?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-2 filter-div">
                                <input type="submit" name="filter" id="" class="btn btn-success" value="Filter">
                                <?php
                                if (isset($_GET['filter'])):
                                    ?>
                                    <a href="<?php echo base_url('admin/users/retailer-shops'); ?>" class="btn btn-warning">Reset</a>
                                <?php endif; ?>
                            </div>

                        </div>
                        <?php echo form_close(); ?>
                    </div>

                    <div class="ibox-title">
                        <h2>View Retailer Shops</h2>
                    </div>
                    <div class="ibox-content borderNone">
                        <div class="table-responsive">
                            <?php if (!empty($result)):  ?>
                                <?php echo form_open($this->uri->uri_string(), array('class' => "form-horizontal", 'id' => 'cat-listing', 'method' => 'get')); ?>
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><input type="checkbox"  class="i-checks check-all" name="check_all"></th>
                                            <th>Store ID</th>
                                            <th>Store Name</th>
											<th>Contact Person</th>
                                            <th>Retailer Name</th>
                                            <th>Retailer Type</th>
                                            <th>Store Status</th>
                                            <th>Store Creation Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($result as $val): ?>

                                            <tr <?php if($val->status == '3'){ echo "style='background-color:#ff9494;'";   } ?>>
                                                <td class="text-center">
												<?php //if($val->status != '3'): ?>
                                                    <input type="checkbox"  class="i-checks child" name="store_id[]" value="<?php echo $val->id; ?>">
												<?php //endif; ?>
                                                </td>
                                                <td><?php echo $val->id; ?></td>
                                                <td><?php echo $val->store_name; ?></td>
												<td><?php echo $val->contact_person; ?></td>
                                                <td><?php echo ($val->display_name) ? ucfirst($val->display_name) : ''; ?>
												</td>
                                                <td><?php echo ($val->user_status == '9' || $val->user_status == '10') ? 'Mapped' : 'Registered'; ?></td>
                                                <td><?php echo ($val->status == '1')?'<span class="label label-success">active</span>':($val->status == '3' ?'<span class="label label-danger">Closed</span>':'<span class="label label-warning">Inactive</span>'); ?>
												</td>
												<td ><?php echo date('d M Y', strtotime($val->creation_date)); ?></td>
                                            </tr>

                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <button name="multi_activate" type="submit" class="viewActivateButton" value="multi_activate_action">ACTIVATE</button>
                                <button name="multi_deactivate" type="submit" class="viewDeactivateButton" value="multi_deactivate_action">DEACTIVATE</button>
								<button name="multi_closed" type="submit" class="viewDeleteButton" value="multi_closed">CLOSE</button>
                                <?php
                                echo form_close();
                            else:
                                ?>
                                <tr><td colspan="12">No Record Found.</td></tr>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <div class="dataTables_info tableData" id="editable_info" role="status" aria-live="polite"></div>
            </div>
            <?php echo $this->pagination->create_links(); ?>
        </div>
    </div>
</div>
</div>
