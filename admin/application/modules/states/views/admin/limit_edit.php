<?php //echo "<pre>";print_r($result);echo "</pre>"; die();
$brand_name=isset($result->brand_name)? explode(',', $result->brand_name):'';
?>
<div id="page-wrapper" class="gray-bg dashbard-1">
    <!--Breadcrumbs -->	
    <?php breadcrumbs(array('admin/states/product-limit-edit/'.$this->uri->segment('4') => 'Edit Products Limit')); ?>
    <div class="row border-bottom">
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">  
            <?php print_flash_message(); ?>
            
            <div class="col-lg-12">                
                <?php echo form_open_multipart($this->uri->uri_string(), 'class="form-horizontal"'); ?>
                <div class="ibox float-e-margins">
                    <div class="ibox-title addCatH1">
                        <h1>Edit Product Limit</h1>                        
                        <div class="ibox-tools">
                        </div>
                    </div>

                    <div class="ibox-content contentBorder ">
                        <div class="row contMargin">
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form-group formWidht">
                                    <label>State Name <span style="color: red;">*</span></label>
                                    <?php  $state_name = isset($state_name) ? $state_name : '';?> 
									<input class="form-control formWidht" value="<?php echo $state_name; ?>"  readonly>
									<input type="hidden" name="state_id" value="<?php echo isset($state_id) ?$state_id :''; ?>"/>
                                </div>                                 
                            </div>
                            <?php if($result->max_limit_weight == 0){
                                $max_limit_weight = '';
                            }else{
                                $max_limit_weight = $result->max_limit_weight;
                            } ?>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form-group formWidht">
                                    <label>Max Weight </label>
                                    <input class="form-control formWidht" type="number" placeholder="max limit weight" value="<?php echo set_value('max_limit_weight', isset($result->max_limit_weight)?$max_limit_weight:''); ?>" name="max_limit_weight" maxlength="6" min="1">
                                    <span class='error vlError'><?php echo form_error('max_limit_weight'); ?></span>
                                </div>                                 
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form-group formWidht">
                                    <label>Brand Name</label>
                                    <select name="brand_name[]" id="brand_name" class="form-control" multiple>
                                        <option value="IMPORTED" <?php echo (in_array('IMPORTED',$brand_name) ) ? 'selected' : ''; ?> >IMPORTED</option>
                                        <option value="WINE" <?php echo (in_array('WINE', $brand_name) ) ? 'selected' : ''; ?> >WINE</option>
                                        <option value="DOMESTIC" <?php echo (in_array('DOMESTIC', $brand_name) ) ? 'selected' : ''; ?>>DOMESTIC</option>
                                        <option value="BEER" <?php echo (in_array('BEER', $brand_name) ) ? 'selected' : ''; ?>>BEER</option>
                                    </select>
                                    <span class='error vlError'><?php echo form_error('brand_name[]'); ?></span>
                                </div>                                 
                            </div>
                        </div>
						<?php if(!empty($limits)):  
						      $i = 1;
						      foreach($limits as $limit): 
						?>
                        <div class="row contMargin"> 
						<input type="hidden"  name="id<?php echo $i; ?>" value="<?php echo $limit->id; ?>"/>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form-group formWidht">
                                    <label style="">Brand Name&nbsp;</label>
                                    <input class="form-control formWidht" type="text"  value="<?php echo isset($limit->brandname) ? $limit->brandname:''; ?>" name="brandname<?php echo $i; ?>" readonly>
                                </div>                                 
                            </div>
                           
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form-group formWidht">
                                    <label>Quantity<span style="color: red;">*</span></label>
                                    <input class="form-control formWidht" type="text" placeholder="Number Of Bottles" value="<?php echo set_value('quantity'.$i, isset($limit->quantity) ? $limit->quantity:''); ?>" name="quantity<?php echo $i; ?>">
                                    <span class='error vlError'><?php echo form_error('quantity'.$i); ?></span>
                                </div>                                 
                            </div>
                      

                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form-group formWidht">
                                    <label>Weight (ml)<span style="color: red;">*</span> </label>
                                    <input type="text" placeholder="Weight in ml" value="<?php echo set_value('weight'.$i, isset($limit->weight) ? $limit->weight:''); ?>" name="weight<?php echo $i; ?>"  class="form-control formWidht ">
                                    <span class='error vlError'><?php echo form_error('weight'.$i); ?></span>
                                </div>
                            </div>
                        </div> 
                       <?php $i++; endforeach;  endif;?>						
                        <div class="ibox-content contentBorder">
                            <div class="col-lg-12 col-md-12 col-sm-12 text-right">
                                <input type="submit" class="btn btn-primary block full-width m-b updateProductBtn" name="save" value="UPDATE LIMIT"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>
</div>
