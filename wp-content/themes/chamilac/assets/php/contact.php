<?php


	/* ==========[ Write Your Email Here ]========= */
		$to = 'youremail@domain.com';
	/* ============================================ */



	function Rec($text) {
		$text = htmlspecialchars(trim($text), ENT_QUOTES);
		if (1 === get_magic_quotes_gpc()) {
			$text = stripslashes($text);
		}
		$text = nl2br($text);
		return $text;
	};

	function cleanMsg($msg) {
		$msg = str_replace("&#039;","'", $msg);
		$msg = str_replace("&#8217;","'", $msg);
		$msg = str_replace("&quot;",'"', $msg);
		$msg = str_replace('<br>','', $msg);
		$msg = str_replace('<br />','', $msg);
		$msg = str_replace("&lt;","<", $msg);
		$msg = str_replace("&gt;",">", $msg);
		$msg = str_replace("&amp;","&", $msg);
		return $msg;
	}

	/* Here we get all the information from the fields sent over by the form. */
	$name     = (isset($_POST['required']['name'])) ? Rec($_POST['required']['name']) : '';
	$email   = (isset($_POST['required']['email'])) ? Rec($_POST['required']['email']) : '';
	$phone   = (isset($_POST['required']['phone'])) ? Rec($_POST['required']['phone']) : '';
	$subject   = (isset($_POST['required']['subject'])) ? Rec($_POST['required']['subject']) : '';
	$message = (isset($_POST['required']['message'])) ? Rec($_POST['required']['message']) : '';
	$message = cleanMsg($message);



	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'From:'.$name.' <'.$email.'>' . "\r\n";
	$headers .= 'Reply-To:'.$email. "\r\n" ;
	$headers .= 'Content-Type: text/plain; charset="utf-8"; DelSp="Yes"; format=flowed '."\r\n";
	$headers .= 'Content-Disposition: inline'. "\r\n";
	$headers .= 'Content-Transfer-Encoding: 7bit'." \r\n";
	$headers .= 'X-Mailer:PHP/'.phpversion();

	$msg  = "Full Name : ".$name."\n";
	if (!empty($email)) {
		$msg .= "Email Address : ".$email."\n";
	}
	if (!empty($phone)) {
		$msg .= "Phone Number : ".$phone."\n";
	}
	$msg .= "Subject : ".$subject."\n";
	$msg .= "Message : ".$message;

	/* this method sends the mail */
	if (mail($to, $subject, $msg, $headers)) {
		echo "success"; /* success message */
	}


?>