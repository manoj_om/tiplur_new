<?php //prd($results);?>
<div id="page-wrapper" class="gray-bg dashbard-1">
    <?php breadcrumbs(array('admin/promotions' => 'Promotion Management', 'admin/promotions/edit/' . @$this->uri->segment(4) => 'Update Promotions')); ?>

    <div class="row border-bottom">
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12" style="margin-top: 40px;">
                <?php print_flash_message(); ?>
                <div class="ibox float-e-margins">
                    <?php echo form_open_multipart($this->uri->uri_string(), 'class="form-horizontal"'); ?>
                    <div class="ibox-title">
                        <h2 style="display: inline-block;">Update Promotions</h2>
                        <!-- <div class="ibox-tools" style="display: inline-block; float: right; top: -60px;">
                            <a class="btn btn-primary block full-width m-b catLogBtn" href="<?php echo base_url('admin/promotios/create'); ?>">ADD PROMOTIONS</a>
                        </div> -->
                    </div>

                    <div class="ibox-content contentBorder">
                        <div class="row">

                          <div class="col-lg-6 col-md-6 col-sm-6 AddProdctInputCont">
                                <div class="form-group formWidht">
                                    <label>Promotion ID <span>*</span></label>
                                    <input type="text" placeholder="Promotion ID" name="promotion_id" id="promotion_id" value="<?php echo set_value('promotion_id', trim($result->promotion_id)); ?>" class="form-control formWidht">
                                    <span class='error vlError'><?php echo form_error('promotion_id'); ?></span>
                                </div>
                            </div>
                          <div class="col-lg-6 col-md-6 col-sm-6 AddProdctInputCont">
                                <div class="form-group formWidht">
                                    <label>Promotion Title <span>*</span></label>
                                    <input type="text" placeholder="Title" name="promotion_title" id="promotion_title" value="<?php echo set_value('promotion_title', trim($result->promotion_title)); ?>" class="form-control formWidht">
                                    <span class='error vlError'><?php echo form_error('promotion_title'); ?></span>
                                </div>
                            </div>
                          <div class="col-lg-6 col-md-6 col-sm-6 AddProdctInputCont">
                                <div class="form-group formWidht">
                                    <label>Brand Name <span>*</span></label>
                                    <input type="text" placeholder="Title" name="brand_name" id="brand_name" value="<?php echo set_value('brand_name', trim($result->brand_name)); ?>" class="form-control formWidht">
                                    <span class='error vlError'><?php echo form_error('brand_name'); ?></span>
                                </div>
                          </div>

                          <div class="col-lg-6 col-md-6 col-sm-6 AddProdctInputCont">
                                <div class="form-group formWidht">
                                    <label>Brand Barcode_180ml<span>*</span></label>
                                    <input type="text" placeholder="Title" name="brand_barcode_180ml" id="brand_barcode_180ml" value="<?php echo set_value('brand_barcode_180ml', trim($result->brand_barcode_180ml)); ?>" class="form-control formWidht">
                                    <span class='error vlError'><?php echo form_error('brand_barcode_180ml'); ?></span>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-6 AddProdctInputCont">
                                <div class="form-group formWidht">
                                    <label>Brand Barcode_375ml<span>*</span></label>
                                    <input type="text" placeholder="Title" name="brand_barcode_375ml" id="brand_barcode_375ml" value="<?php echo set_value('brand_barcode_375ml', trim($result->brand_barcode_375ml)); ?>" class="form-control formWidht">
                                    <span class='error vlError'><?php echo form_error('brand_barcode_375ml'); ?></span>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-6 AddProdctInputCont">
                                <div class="form-group formWidht">
                                    <label>Brand Barcode 750ml<span>*</span></label>
                                    <input type="text" placeholder="Title" name="brand_barcode_750ml" id="brand_barcode_750ml" value="<?php echo set_value('brand_barcode_750ml', trim($result->brand_barcode_750ml)); ?>" class="form-control formWidht">
                                    <span class='error vlError'><?php echo form_error('brand_barcode_750ml'); ?></span>
                                </div>
                            </div>
                    


                            <div class="col-lg-12 col-md-12 col-sm-12  text-center" style="">
                                <button type="submit" name="save" class="btn btn-primary block full-width m-b updateProductBtn addBtn">UPDATE PROMOTION</button>
                            </div>
                        </div>
                      </div>
                    </div>
            </div>
        </div>
        </form>
    </div>
</div>
</div>
