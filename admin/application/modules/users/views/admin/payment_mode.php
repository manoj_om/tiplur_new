<div id="page-wrapper" class="gray-bg dashbard-1">
    <?php breadcrumbs(array('admin/users' => 'User Management', 'admin/users/payment_mode' => 'Update Payment Mode')); ?>
    <div class="row border-bottom">
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <?php print_flash_message(); ?>
            <div class="col-lg-12">
                <?php
                $type = (!empty($_GET['type'])) ? '/?type' . $_GET['type'] : '';
                echo form_open_multipart($this->uri->uri_string() . $type, 'class="form-horizontal"', 'autocomplete="off"');
                ?>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h2 style="display: inline-block;">Update Payment Mode & Estimated Day and Time</h2>
                    </div>
                    <div class="ibox-content contentBorder">
                        <div class="row">
                        <?php

          $payment_mode=explode(',',$store_detail->payment_mode);
          $overlap = array_intersect($payment_mode, array('4','6','7','8'));
          $counts  = array_count_values($overlap);
	       // $userdata = user_data($store_detail->retailer_id);
	?>
                        <div class="col-lg-12 col-md-12 col-sm-12 ">
                        <label>Payment Mode</label>
                            </br>
                            <input type="radio" name="check" <?php echo count($counts)>0 ?'checked':'';?> value="offline" onchange="ShowTimes('paymentModeHide',true)"> COD offline
                            <div id="paymentModeHide" <?php echo count($counts)>0?'style="display:block"':'style="display:none"';?>>
                                <input type="checkbox" name="ad_pack_id2[]" <?php echo (in_array('4',$payment_mode))?'checked':'';?> value="4" > CASH<br>
                                    <input type="checkbox" name="ad_pack_id2[]" <?php echo (in_array('6',$payment_mode))?'checked':'';?>  value="6" > UPI/Wallet<br>
                                    <input type="checkbox" name="ad_pack_id2[]" <?php echo (in_array('7',$payment_mode))?'checked':'';?>  value="7">  CARD-MASTER/VISA<br>
                                    <input type="checkbox" name="ad_pack_id2[]" <?php echo (in_array('8',$payment_mode))?'checked':'';?>  value="8"> CARD-AMEX<br>
                                </div>
                            <br>
                            <input type="radio" name="check" <?php echo (in_array('9',$payment_mode))?'checked':'';?> value="online" onchange="ShowTimes('paymentModeHide',false)"> Payment Gateway
                            <br>
                            <br>
                            </div>
                            <?php if($_SESSION['role_id']==4){ ?>
                            <div class="col-lg-12 col-md-12 col-sm-12 ">
                                <label>Estimated Date & Time</label>
</br>
<label>Estimated Day</label>
</br>
                            <select size="1" id="Rank" title="" name="estimated_day">
                            <option value="" <?php echo ($store_detail->estimated_day=='')?'selected':'';?>>Select Days</option>
                            <option value="0" <?php echo ($store_detail->estimated_day=='0')?'selected':'';?>>0 Day</option>
                            <option value="1"<?php echo ($store_detail->estimated_day=='1')?'selected':'';?>>1 Day</option>
                            <option value="2<?php echo ($store_detail->estimated_day=='2')?'selected':'';?>">2 Day</option>
                        </select>
</br>
</br>
                        <div class="estimated_time" <?php echo ($store_detail->estimated_day=='0')?'display:block':'display:none';?>>
                            <div class="airman">
                            <label>Estimated Time</label>
</br>
                                <select class="second-level-select" name="estimated_time">
                                    <option value="">Select Time</option>
                                    <option value="1"<?php echo ($store_detail->estimated_time=='1')?'selected':'';?>>1 Hour</option>
                                    <option value="2"<?php echo ($store_detail->estimated_time=='2')?'selected':'';?>>2 Hour</option>
                                    <option value="3"<?php echo ($store_detail->estimated_time=='3')?'selected':'';?>>3 Hour</option>
                                    <option value="4"<?php echo ($store_detail->estimated_time=='4')?'selected':'';?>>4 Hour</option>
                                    <option value="5"<?php echo ($store_detail->estimated_time=='5')?'selected':'';?>>5 Hour</option>
                                    <option value="6"<?php echo ($store_detail->estimated_time=='6')?'selected':'';?>>6 Hour</option>
                                    <option value="7"<?php echo ($store_detail->estimated_time=='7')?'selected':'';?>>7 Hour</option>
                                    <option value="8"<?php echo ($store_detail->estimated_time=='8')?'selected':'';?>>8 Hour</option>
                                    <option value="9"<?php echo ($store_detail->estimated_time=='9')?'selected':'';?>>9 Hour</option>
                                    <option value="10"<?php echo ($store_detail->estimated_time=='10')?'selected':'';?>>10 Hour</option>
                                </select>
                            </div>

                                </div>
                                 </div>
                               <?php } ?>
                            <input type="hidden" value="<?php echo $store_detail->id; ?>" name="store_id"/>

                            <div class="col-lg-12 col-md-12 col-sm-12  text-center">
                                <input type="submit" name="save" value="UPDATE PAYMENT MODE" class="btn btn-primary block full-width m-b updateProductBtn"/>
                            </div>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
