<div id="page-wrapper" class="gray-bg dashbard-1">
    <?php
    $url = $this->uri->segment_array();
    $name = (end($url) == 'promotions') ? 'Promotion Management' : ' Participated Customer Management';
    $segment = $this->uri->segment(3);
    breadcrumbs(array(uri_string() => $name));
    ?>
    <div class="row border-bottom">
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <?php print_flash_message(); ?>
                <div class="ibox float-e-margins">

                    <div class="ibox-title" style="margin-bottom: 20px;">
                        <?php echo form_open($this->uri->uri_string(), array('method' => 'get')); ?>
                        <div class="row">

                            <div class="col-md-3">
                                <div class="form-group formWidht">
                                    <label>Keyword</label>
                                    <input type="text" placeholder="Name/Promotion-ID/Mobile/City" name="name" id="exampleInputEmail2" class="form-control formWidht" value="<?php echo isset($_GET['name']) ? $_GET['name'] : ''; ?>">
                                </div>
                            </div>

                            <!--div class="col-md-2">
                                <div class="form-group formWidht">
                                    <label>From Date</label>
                                    <input type="date" placeholder="Start Date" name="fromDate" class="form-control formWidht" value="<?php echo isset($_GET['fromDate']) ? $_GET['fromDate'] : ''; ?>">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group formWidht">
                                    <label>To Date</label>
                                    <input type="date" placeholder="End Date" name="toDate" class="form-control formWidht" value="<?php echo isset($_GET['toDate']) ? $_GET['toDate'] : ''; ?>">
                                </div>
                            </div-->

                            <div class="col-md-2">
                                <div class="form-group formWidht">
                                    <label>Status</label>
                                    <?php
                                    $status = isset($_GET['status']) ? $_GET['status'] : '';
                                    status_dropdown($status);
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-2 filter-div">
                                <input type="submit" name="filter" id="" class="btn btn-success" value="Filter">
                                <?php
                                if (isset($_GET['filter'])):
                                    ?>
                                    <a href="<?php echo (!empty($segment) && $segment == 'participate_user') ? base_url('admin/promotions/participate_user') : base_url('admin/promotions'); ?>" class="btn btn-warning">Reset</a>
                                <?php endif; ?>
                            </div>

                        </div>
                        <?php echo form_close(); ?>
                    </div>

                    <div class="ibox-title">
                        <h2><?php echo (end($url) == 'users') ? 'View Retailers' : 'View Participated Customers'; ?></h2>

                        <span class="pull-right">
                            <a data-toggle="tooltip" title="Download Excel" href="<?php
                            $type = !empty($segment) ? 'customer' : 'user';
                            echo base_url('admin/promotions/export_partiUsers?type=' . $type . '&') . http_build_query($_GET);
                            ?>" class="">
                                <img src="<?php echo base_url('assets/images/exldwn.ico'); ?>" height="40" width="40">
                            </a>
                        </span>
                    </div>
                    <div class="ibox-content borderNone">
                        <div class="table-responsive">
                            <?php if (!empty($result)): ?>
                                <?php echo form_open($this->uri->uri_string(), array('class' => "form-horizontal", 'id' => 'cat-listing', 'method' => 'get')); ?>
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><input type="checkbox"  class="i-checks check-all" name="check_all"></th>
                                            <!--th>ID</th-->
                                            <th>Document</th>
                                            <th>Name</th>
                                            <!-- <th>Promotion Banner</th> -->
                                            <th>Promotion ID</th>
                                            <th>Registered On</th>
                                            <th>participated On</th>
                                            <th>Promotion Expired On</th>
                                            <th>Phone Number</th>
                                            <!--th>Email</th-->
                                            <?php if (end($url) != 'users') { ?><th>DOB</th><?php } ?>
                                            <th>City</th>
                                            <!-- <th><?php echo (end($url) == 'users') ? 'Store Details' : 'Address'; ?></th> -->
                                            <th>Users Status</th>
                                            <th>Winning Status</th>
                                            <!-- <?php //if (end($url) != 'users') { ?><th>Action</th><?php //} ?> -->
                                            <!-- <th></th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($result as $val):  //prd($val); ?>

                                            <tr <?php if ($val->status == 8) { ?>style='background-color:#ff9494;'<?php } ?>>
                                                <td class="text-center">
                                                    <input type="checkbox"  class="i-checks child" name="users_id[]" value="<?php echo $val->user_id; ?>">
                                                </td>
                                                <!--td><?php echo $val->id; ?></td-->
                                                <td><a href="javascript:void(0)" class="doc-view"><img width="60" height="60" src="<?php echo ($val->promotion_img) ? base_url('/assets/uploads/document_file/' . $val->doc_file) : base_url('/assets/uploads/document_file/no_doc.png'); ?>" class="img-responsive center-block"></a></td>
                                                <td><a href="<?php
                                                    $order_url = (end($url) == 'users') ? 'admin/orders/view_order?user_id=' . $val->user_id
                                                     . '&type=retailer' : 'admin/orders/view_order?user_id=' . $val->user_id . '&type=user';
                                                    echo base_url($order_url);
                                                    ?>"><?php echo ucfirst($val->display_name); ?></a></td>
                                                    <!-- <td><a href="javascript:void(0)" class="doc-view"><img src="<?php echo ($val->prmototion_banner_url) ? base_url("/assets/images/promotion_image/$val->prmototion_banner_url")  : base_url('/assets/uploads/document_file/no_image.jpg');?>" class="img-rounded" width="100" height="50" class="img-responsive center-block"/></a>	</td> -->
                            												<td><?php echo ($val->promotion_id) ? $val->promotion_id : '--'; ?></td>
                                                <td ><?php echo site_date($val->created_on); ?></td>
                                                <td><?php echo ($val->participated_on) ? site_date_time($val->participated_on) : '--'; ?></td>
                                                <td><?php echo ($val->expiry_date) ? site_date($val->expiry_date) : '--'; ?></td>
                                                <td><?php echo ($val->mobile) ? $val->mobile : '--'; ?></td>
                                                <!--td><?php echo ($val->email) ? mailto($val->email) : '--'; ?></td-->
                                                <?php if (end($url) != 'users') { ?><td><?php echo site_date($val->dob); ?></td><?php } ?>
                                                <td ><?php echo $val->city; ?></td>
                                                <!-- <td><a href="javascript:void(0)" class="user_address_modal" data-id="<?php echo $val->id; ?>" data-type="<?php echo (end($url) == 'users') ? 'retailer' : 'user'; ?>"><span class="btn btn-default" title="User Address">View Details</span></a></td> -->
                                                <td><?php echo print_status($val->status); ?></td>
                                                <td><span class="label badge-warning"><?php echo ($val->winner_user) ? $val->winner_user : 'In Process';?></span></td>
                                                <!-- <?php //if (end($url) != 'users') { ?><td><?php //if ($val->status != 8) { ?><button type="button" class="btn btn-default doc-reject" data-id="<?php echo $val->id; ?>">Reject Document</button><?php //} ?></td><?php// } ?> -->
        <!--                                                <td class="text-center">
                                                <?php
                                                	//$type = (!empty($this->uri->segment(3))) ? $this->uri->segment(3) : '';
                                                	//if ($type != 'customers'):
                                                ?>
                                                        <a href="<?php //echo base_url('admin/users/edit/' . $val->id); ?>" ><i class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;<a href="javascript:void(0);" class="delete" form="cat-listing" user_id="<?php echo $val->id; ?>"  ><i class="fa fa-trash fa-lg" aria-hidden="true"  ></i>
                                                <?php //endif;     ?>
                                                </td>-->
                                            </tr>

                                        <?php endforeach; ?>
                                    </tbody>
                                </table>

                                <button name="multi_activate" type="submit" class="viewActivateButton" value="multi_activate_action">ACTIVATE</button>
                                <button name="multi_deactivate" type="submit" class="viewDeactivateButton" value="multi_deactivate_action">DEACTIVATE</button>
                                <?php
                                echo form_close();
                            else:
                                ?>
                                <tr><td colspan="12">No Record Found.</td></tr>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <div class="dataTables_info tableData" id="editable_info" role="status" aria-live="polite"></div>
            </div>
            <?php echo $this->pagination->create_links(); ?>
        </div>
    </div>
</div>

<div class="modal fade" id="popupModal"  role="dialog" style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content"></div>
    </div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">User Document</h4>
            </div>
            <div class="modal-body">
                <img src="" class="img-responsive center-block">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<!-- Modal -->
<div id="docRejectModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <form action="<?php echo base_url(); ?>admin/promotions/reject_doc" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Reject User Document</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to reject document uploaded by user?</p>
                    <input type="hidden" id="user_id" name="user_id" value=""/>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>
    </div>
</div>

</div>
