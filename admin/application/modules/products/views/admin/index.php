<div id="page-wrapper" class="gray-bg dashbard-1">
    <?php breadcrumbs(array('admin/products' => 'Manage Products')); ?>
    <div class="row border-bottom">
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12 " >
                <?php print_flash_message(); ?>
                <div class="ibox float-e-margins">

                    <div class="ibox-title" style="margin-bottom: 20px;">
                        <?php echo form_open($this->uri->uri_string(), array('method' => 'get')); ?>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group formWidht">
                                    <label>Product Name</label>
                                    <input type="text" class="form-control" name="title" placeholder="Product Name" value="<?php echo isset($_GET['title']) ? $_GET['title'] : '' ?>">
                                </div>
                            </div>
                            <?php if($_SESSION['role_id']=='1'): ?>
                            <div class="col-md-2">
                                <div class="form-group formWidht">
                                    <label>Retailer</label>
                                    <select id="retailer_id" name="retailer_id" class="form-control">
                                        <option value="">Please select</option>
                                        <?php
                                        $retailer = retailers();
                                        if (!empty($retailer)):
                                            foreach ($retailer as $val):
                                                ?>
                                                <option value="<?php echo $val->id; ?>" <?php echo ($val->id == @$_GET['retailer_id']) ? 'selected' : ''; ?>  >
												<?php if($val->status =='9') 
											      { echo ucfirst($val->display_name).' (Mapped)';
												  }else{ echo ucfirst($val->display_name);  } 
											    ?>
												</option>
                                                <?php
                                            endforeach;
                                        endif;
                                        ?>
                                    </select>
                                </div>                               
                            </div>
                                    <?php endif; ?>
                            <div class="col-md-2">
                                <div class="form-group formWidht">
                                    <label>Category</label>
                                    <select name="category" class="form-control">
                                        <option value="">Please select</option>
                                        <?php
                                        $category = isset($_GET['category']) ? $_GET['category'] : null;
                                        product_cat($category);
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group formWidht">
                                    <label>Product Type</label>
                                    <?php
                                    $selected = (isset($_GET['type']) && $_GET['type']) ? $_GET['type'] : '';
                                    $type = product_type();
                                    echo form_dropdown('type', $type, $selected, 'class="form-control"');
                                    ?>
                                </div>
                            </div>

                            <div class="col-md-1">
                                <div class="form-group formWidht">
                                    <label>Status</label>
                                    <?php
                                    $state_id = isset($_GET['status']) ? $_GET['status'] : '';
                                    status_dropdown($state_id, 'form-control');
                                    ?>
                                </div>
                            </div>

                            <div class="col-md-3 filter-div">
                                <input type="submit" name="filter" id="" class="btn btn-success" value="Filter">
                                <?php if (isset($_GET['filter'])): ?>
                                    <a href="<?php echo base_url('admin/products'); ?>" class="btn btn-warning">Reset</a>
                                <?php endif; ?>
                                <?php if (isset($_GET['retailer_id']) && !empty($_GET['retailer_id'])): ?>
                                    <a href="javascript:void(0)" class="btn btn-danger retailer-pro-delete" data-id="<?php echo $_GET['retailer_id']; ?>">Delete All</a>
                                <?php endif; ?>
                            </div>

                        </div>
                        <?php echo form_close(); ?>
                    </div>

                    <div class="ibox-title">
                        <h2>View Products </h2>
                        <a data-toggle="tooltip" title="Download Excel" href="<?php echo base_url('admin/products/export_orders?') . http_build_query($_GET); ?>" class="">
                            <img src="<?php echo base_url('assets/images/exldwn.ico'); ?>" height="40" width="40">
                        </a>
                        <?php if($_SESSION['role_id']=='1'): ?>
                        <div class="ibox-tools" style="display: inline-block; float: right;">  
                            <a href="<?php echo base_url('admin/products/create'); ?>" class="btn btn-primary block full-width m-b createuser">ADD PRODUCT</a>
                        </div>
                        <?php endif; ?>
                    </div>
                    <div class="table-responsive">
                        <?php echo form_open($this->uri->uri_string(), array('class' => "form-horizontal", 'id' => 'product-listing', 'method' => 'get')); ?>
                        <div class="ibox-content borderNone">

                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <td class="text-center"><input type="checkbox"  class="i-checks check-all" name="check_all"></td>
                                        <th>Product Name</th>
                                        <th>Image</th>
                                        <th>Retailer Name</th>
                                        <th>Category</th>
                                        <th>Quantity</th>
                                        <th>Product Type</th>
                                        <th>Price</th>
                                        <?php if($_SESSION['role_id']=='2'||$_SESSION['role_id']=='4'): ?>
                                        <th>Stock Available</th>
                                        <?php endif; ?>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>                                   
                                    <?php
                                    if ($result):
                                        foreach ($result as $val):
                                            $retailer = user_data($val->retailer_id);
                                            ?>
                                            <tr>
                                                <td class="text-center">
                                                    <input type="checkbox"  class="i-checks child" name="product_id[]" value="<?php echo $val->id; ?>">
                                                </td>
                                                <td><?php echo ucfirst(read_more($val->title)); ?></td>
                                                <td>
                                                    <img class="img-rounded" width="100" height="50"src="<?php echo ($val->image) ? base_url('/assets/images/product_image/'.$val->image) : base_url('/assets/images/profile_image/default-user.jpg'); ?>">
                                                </td>
                                                <td><?php echo $retailer ? ucfirst($retailer->display_name) : ''; ?></td>
                                                <td><?php $cat_link = '<a target="_blank" href="' . base_url('admin/products?category=') . $val->category_id . '&filter=Filter">' . $val->category_name . '</a>';
                                            ?>
                                                    <?php echo ($val->category_name) ? $cat_link : ''; ?>
                                                </td>
                                                <td><?php echo ($val->weight) ? $val->weight : ''; ?></td>
                                                <td><?php echo ($val->type) ? $type[$val->type] : 'NA'; ?></td>
                                                <td>&#8377;  <?php echo $val->price; ?></td>
                                                <?php if($_SESSION['role_id']=='2'||$_SESSION['role_id']=='4'): ?>
                                                <td>
                                                <div class="toggle-btn <?php echo ($val->in_store=='1')?'active':'';?>">
                                                <input type="checkbox"  <?php echo ($val->in_store=='1')?'checked':'';?> class="cb-value" product_id="<?php echo $val->id; ?>"/>
                                                <span class="round-btn"></span>
                                                </div> 
                                                </td>
                                                <?php endif; ?>
                                                <td><?php echo print_status($val->status); ?>	</td>
                                                <td class="text-center"><a href="<?php echo base_url('admin/products/edit' . '/' . $val->id); ?>" ><i class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;<a href="javascript:void(0);" class="delete" form="product-listing" product_id="<?php echo $val->id; ?>"  ><i class="fa fa-trash fa-lg" aria-hidden="true"  ></i></td>
                                                            </tr> 
                                                            <?php
                                                        endforeach;
                                                    else:
                                                        ?>
                                                        <tr><td colspan="12">Result not found.</td></tr>
                                                    <?php endif; ?>
                                                    </tbody>
                                                    </table>
                                                    </form>
                                                    <button name="multi_product" form="product-listing" type="submit" class="viewDeleteButton delete-btn" id="delete-btn" value="multi_action">DELETE</button> 
                                                    </div>

                                                    </div>
                                                    </div>
                                                    </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-5">
                                                            <div class="dataTables_info tableData" id="editable_info" role="status" aria-live="polite"></div>
                                                        </div>
                                                        <?php echo $this->pagination->create_links(); ?>                                                        
                                                    </div>
                                                    </div>
                                                    </div>

                                                    <!-- Modal -->
                                                    <div id="retailerProDeleteModal" class="modal fade" role="dialog">
                                                        <div class="modal-dialog">

                                                            <!-- Modal content-->
                                                            <form action="<?php echo base_url('admin/products/retailer_products_delete'); ?>" method="post">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                        <h4 class="modal-title">Bulk Product Delete</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <p>Are you sure you want to delete all products of this retailer?</p>
                                                                        <input type="hidden" id="ret_id" name="ret_id" value=""/>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="submit" class="btn btn-success">Submit</button>
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>				
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>