<?php /* Template Name: home */ ?>



<?php get_header();?>



<main id="mainContent" class="main-content">





	<!-- start: Hero Area -->

    <section class="section hero-area hero-images-slider" id="heroArea" >

		 <?php echo do_shortcode('[metaslider id=70]');?> 
	</section>

	<!-- end: Hero Area -->



	

	<!-- start: About Us Area -->

    <section class="section about-us-area ptb-60" id="about-us">

		<div class="container">

			<div class="row  mb-40 clearfix">

				<?php

					$my_id = 10;

					$post_id_10 = get_post($my_id);

					$key_2_value = get_post_meta( $my_id, 'about-content', true );

				?>

                <div class="col-sm-12">

					<div class="who-we-are">

						<h2 class="mb-10 section-title"><?php echo get_the_title($my_id);?></h2>

						<div class="sect_cont"><?php echo $key_2_value;?></div>

						<br/>

						<!-- <a href="<?php //echo get_the_permalink($my_id);?>">Read more...</a> -->

					</div>

				</div>                

			</div>

			 
		</div> 

	</section>

    <!-- start: About Us Area -->


	

			<!-- start: industry-served Area -->

            <section class="section industry-served-area portfolio-area portfolio-area-1 pt-40 pb-80" id="category">

                <div class="container">

                    <div class="row mb-30">

                        <div class="col-lg-7 col-md-8 col-sm-10 col-xs-12 col-xs-center t-center mb-40 title-container">

                           <h2 class="section-title mb-20 font-22 t-uppercase">Category</h2>

                            <div class="line"></div>

                        </div>

                    </div>

                    

                    <div class="portfolio-wrapper row">

					<?php

        $childArgs = array(

            'sort_order' => 'ASC',

            'sort_column' => 'menu_order',

            'child_of' => 14

        );

        $childList = get_pages($childArgs);?>

		<?php foreach ($childList as $child) {

// $featured_img_url = get_the_post_thumbnail_url('full');			?>

                        <div class="portfolio-single mix col-xs-12 col-sm-6 col-md-4 construction repairing architecture"  style="background-image:url('<?php echo get_the_post_thumbnail_url($child->ID);?>');    height: 252px;" >

                        	<h2 class="industry-title"><a href="<?php echo get_the_permalink($child->ID);?>"><?php echo $child->post_title; ?></a></h2>

                           <?php //echo get_the_post_thumbnail($child->ID);?>

                            <!-- <div class="portfolio-hover">

                                <div class="inner-hover">

                                    <h2 class="h5 t-uppercase pb-10 mb-5"><a href="<?php //echo get_the_permalink($child->ID);?>"><?php //echo $child->post_title; ?></a></h2>

                                    

                                </div>

                            </div> -->

                        </div>

						<?php }

        ?>

                       

                       

                        

                        

                       

                    </div>

                </div>

            </section>

			<!-- end: industry-served Area --> 

			 
			<!-- start: How it works -->

<section class="section our-client-area features-area features-area-1 ptb-60 bg-gray" id="howitworks">

    <div class="container">

        <div class="row mb-30">

            <div class="col-lg-7 col-md-8 col-sm-10 col-xs-12 col-xs-center t-center mb-40 title-container">

               <h2 class="section-title mb-20 font-22 t-uppercase">So, how does tiplur <strong>work?</strong></h2>
			   <h3>Simply and efficiently.</h3>


            </div>

        </div>

        <div class="clearfix">
		  <div class="col-sm-4">
               
                <p>How Works</p>

          </div>
		  <div class="col-sm-8">
				<p>Open the app and it instantly connects you to the licensed liquor stores in your neighbourhood. You select an outlet and see the entire catalogue (category and sub category wise along with prices). The prices are fair and transparent. You choose your order and confirm the same. The retailer confirms receipt of your request and the same will be delivered at your doorstep (at no extra cost). You pay on delivery by cash, card or wallet.</p>
			
		  </div>
		 </div>
    </div>

</section>






<section class="section industry-served-area portfolio-area portfolio-area-1 pt-40 pb-80">

    <div class="container">

        <div class="row mb-30">

            <div class="col-lg-7 col-md-8 col-sm-10 col-xs-12 col-xs-center t-center mb-40 title-container">

                <h2 class="section-title mb-20 font-22 t-uppercase">So, what next?</h2>
				<h3>Two steps: One simple and one sensible.</h3>


            </div>

        </div>

        <div class="clearfix">
		  <div class="col-sm-4">
               
                <p>Two Steps</p>

          </div>
		  <div class="col-sm-8">
				<ol>
					<li> Download the tiplur app from Google playstore. It’s currently available only for Android users. </li>
					<li> Register with your details. <br>
It’s the same as any other app except that you need to upload a valid photo ID.</li>

				</ol>
				<p>(Regulation demands it, rightfully. So that it’s not misused by any person who is not under the state-mandated age limit.).</p>
		  </div>
		 </div>

    </div>

</section>

<!-- end: How it works -->
			 

			 <!-- start: our-client Area -->

			 <section  class="section our-client-area features-area features-area-1 ptb-60 bg-gray" >

                <div class="container">

                    <div class="row mb-30">

                        <div class="col-lg-7 col-md-8 col-sm-10 col-xs-12 col-xs-center t-center mb-40 title-container">

                         <!--<h2 class="section-title mb-20 font-22 t-uppercase">OUR CLIENTS</h2>  

                            <div class="line"></div>-->

                        </div>

                    </div>

                    

                    <div class="portfolio-wrapper row">

					

                        <div class="">

                           <!-- <?php //echo do_shortcode('[gs_logo]');?>  -->
						   <p>Tiplur is committed to strict adherence of all regulatory standards such as minimum age limit, ID proof, quantities as per state norms etc.</p>

                        </div>

						

                    </div>

                </div>

            </section>

			 <!-- start: our-client Area -->

			 

			 

			 <!-- start: contact-us Area -->

			 <section class="section portfolio-area contact-us-area portfolio-area-1 pt-40 pb-80" id="contact-us">

                <div class="container">

                    <div class="row mb-30">

                        <div class="col-lg-7 col-md-8 col-sm-10 col-xs-12 col-xs-center t-center mb-40 title-container">

                           <h2 class="section-title mb-20 font-22 t-uppercase">CONTACT</h2>

                            <div class="line"></div>

                        </div>

                    </div>

                    

                  

                    <div class="row row-tb-15 mb-40 clearfix">

					

                        <div class="col-sm-6">

                            <div class="who-we-are">

                                <h5 class="mb-10">Contact Us</h5>

							<!--	<ul>

									<li><strong>Tiplur</strong></li>

									<li><span class="glyphicon glyphicon-map-marker"></span> <strong>Address :</strong> 12345, Space park, gurgaon, 122001, Haryana</li>

									<li><span class="glyphicon glyphicon-phone"></span> <strong>Phone :</strong> <a href="tel:+91-129-4132509"> 91-129-123456789</a></li>

									<li><span class="glyphicon glyphicon-envelope"></span> <strong>Mail :</strong> <a href="mailto:info@chemilac.com"> tiplur.com</a></li>

								</ul> -->

                        	
								
							<?php echo do_shortcode( '[contact-form-7 id="864" title="Contact form 1"]' ); ?>
							
						  </div> 
						</div>

                        <div class="col-sm-6">

                        <!--  <iframe class="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3507.5174769431496!2d77.30149631461211!3d28.463958582484608!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390ce740cfffffff%3A0x688d711e628149d!2sThe+Chemicals+of+India!5e0!3m2!1sen!2sin!4v1498196718698" height="250" frameborder="0" allowfullscreen="allowfullscreen"></iframe>  -->
						
							<div class="who-we-are">
								<h5 class="mb-10">Visit Us</h5>

								<ul>

									<li><strong>Tiplur</strong></li>

									<li><span class="glyphicon glyphicon-map-marker"></span> <strong>Address :</strong> 12345, Space park, gurgaon, 122001, Haryana</li>

									<li><span class="glyphicon glyphicon-phone"></span> <strong>Phone :</strong> <a href="tel:+91-129-4132509"> 91-129-123456789</a></li>

									<li><span class="glyphicon glyphicon-envelope"></span> <strong>Mail :</strong> <a href="mailto:info@chemilac.com"> tiplur.com</a></li>

								</ul>

                        </div>

                    </div>

                   

                </div>

            </section>

             <!-- end: contact-us Area -->

			 

</main>



<?php get_footer();?>