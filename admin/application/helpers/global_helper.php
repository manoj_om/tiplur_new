<?php

if (!function_exists('pagination_formatting')) {

    function pagination_formatting() {
        $CI = &get_instance();

        $CI->page_config["full_tag_open"] = '<div class="dataTables_paginate paging_simple_numbers" id="editable_paginate"><ul class="pagination">';
        $CI->page_config["full_tag_close"] = '</ul>';
        $CI->page_config["first_link"] = "First";
        $CI->page_config["first_tag_open"] = "<li>";
        $CI->page_config["first_tag_close"] = "</li>";
        $CI->page_config["last_link"] = "Last";
        $CI->page_config["last_tag_open"] = "<li>";
        $CI->page_config["last_tag_close"] = "</li>";
        $CI->page_config['next_link'] = 'Next';
        $CI->page_config['next_tag_open'] = '<li>';
        $CI->page_config['next_tag_close'] = '<li>';
        $CI->page_config['prev_link'] = 'Prev';
        $CI->page_config['prev_tag_open'] = '<li>';
        $CI->page_config['prev_tag_close'] = '<li>';
        $CI->page_config['cur_tag_open'] = '<li class="active"><a href="#">';
        $CI->page_config['cur_tag_close'] = '</a></li>';
        $CI->page_config['num_tag_open'] = '<li>';
        $CI->page_config['num_tag_close'] = '</li>';
        return $CI->page_config;
    }

}

//Check validate promotion start date is greater than end/expired date
if (!function_exists('date_greater')) {

    function date_greater($start,$end) {
        $start = strtotime(date($start,('d-m-Y')));
        $end = strtotime(date($end,('d-m-Y')));
        // $curr_date = strtotime(date('d-m-Y'));
        if ($start < $end) {
              return true; // exp
        } else {
          return false; // not exp

        }
    }
}

//check date format and convert in yyyy-mm-dd
if (!function_exists('convert_date_format')) {
function convert_date_format($old_date = '')
 {
 $old_date = trim($old_date);
 if (preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $old_date)) // MySQL-compatible YYYY-MM-DD format
 {
 $new_date = $old_date;
 }
 elseif (preg_match('/^(0[1-9]|[1-2][0-9]|3[0-1])-(0[1-9]|1[0-2])-[0-9]{4}$/', $old_date)) // DD-MM-YYYY format
 {
 $new_date = substr($old_date, 6, 4) . '-' . substr($old_date, 3, 2) . '-' . substr($old_date, 0, 2);
 }
 elseif (preg_match('/^(0[1-9]|[1-2][0-9]|3[0-1])\/(0[1-9]|1[0-2])\/[0-9]{4}$/', $old_date)) // DD/MM/YYYY format
 {
 $new_date = substr($old_date, 6, 4) . '-' . substr($old_date, 3, 2) . '-' . substr($old_date, 0, 2);
 }
 elseif (preg_match('/^(0[1-9]|1[0-2])\/(0[1-9]|[1-2][0-9]|3[0-1])\/[0-9]{4}$/', $old_date)) // MM/DD/YYYY format
 {
 $new_date = substr($old_date, 6, 4) . '-' . substr($old_date, 0, 2) . '-' . substr($old_date, 3, 2);
 }
 elseif (preg_match('/^(0[1-9]|[1-2][0-9]|3[0-1])-(0[1-9]|1[0-2])-[0-9]{2}$/', $old_date)) // DD-MM-YY format
 {
 $new_date = substr($old_date, 6, 4) . '-' . substr($old_date, 3, 2) . '-20' . substr($old_date, 0, 2);
 }
 else // Any other format. Set it as an empty date.
 {
 $new_date = '0000-00-00';
 }
 return $new_date;
 }
}



// Promotion Inactive after expired date is over
if (!function_exists('promotion_expired')) {

    function promotion_expired($date,$promotion_id) {
        $thiss = & get_instance();
        $date = strtotime($date);
        $curr_date = strtotime(date('d-m-Y'));
        if ($date >= $curr_date) {
          //echo "if"; die;
            return true; // exp
        } else {
          $thiss->db->set('promo_status','1');
          $thiss->db->where('promotion_id',$promotion_id);
          $thiss->db->update('ti_promotion');
        //echo   $thiss->db->last_query(); die;
          return 'false'; // not exp
        }
    }
}

// Get promotion Hot to Participated points
if (!function_exists('get_participateRules')) {
        function get_participateRules() {
          $CI = & get_instance();
         $row = $CI->db->select('rules')->get('how_to_participate');
         return $row->row();
         // echo $CI->db->last_query();
      }
}
// Promotion barcode exist for participated users on bar-code
if (!function_exists('barcode_exist')) {
      function barcode_exist($user_id,$barcode,$promotion_id) {
          $thiss = & get_instance();
       return  $res =  $thiss->db->select('*')->where(array('barcode' => $barcode,'barcode_status' => '1'))->get('ti_participated_users')->num_rows();
      // echo $thiss->db->last_query(); die;
    }
}
// Get promotion Details
if (!function_exists('get_promotionDetails')) {
        function get_promotionDetails($promotion_id) {
          $CI = & get_instance();
        return $row = $CI->db->select('*')->where(array('promo_status' => '0','deleted' => '0','promotion_id' =>$promotion_id ))->get('ti_promotion')->row();
        // echo $CI->db->last_query();
      }
}
// Get promotion Hot to Participated points
if (!function_exists('get_participateRules')) {
        function get_participateRules() {
          $CI = & get_instance();
         $row = $CI->db->select('rules')->get('how_to_participate');
         return $row->row();
         // echo $CI->db->last_query();
      }
}
// Get promotion Hot to Participated points
if (!function_exists('promoted_users')) {
        function promoted_users($promotion_id) {
          $CI = & get_instance();
        return $row = $CI->db->select('count(*) as total')->where('promotion_id', $promotion_id )->get('participated_users')->row();

        //  echo $CI->db->last_query();
      }
}
// Get Extension of image and banner
if (!function_exists('image_extension')) {
        function image_extension($img) {
          $promo_img = explode("." , $img);
        return  $ext_banner = $promo_img[1];
      }
}

if (!function_exists('barcode_validate')) {

	function barcode_validate($barcode) {
		$thiss = &get_instance();
		$result = $thiss->db->select('*')->where(array('promo_status' => '0' , 'deleted' => '0' ))->get('ti_promotion')->result_array();
		return $arr ? $arr :false;
	}

}


if (!function_exists('breadcrumbs')) {

    function breadcrumbs($links = array()) {
        $ex_links = array();
        foreach ((array) $links as $title => $link) {

            $ex_links = explode('/', $link);
        }
        echo'<div class="row wrapper border-bottom white-bg page-heading bradPading"><div class="col-lg-8"> <ol class="breadcrumb"><li> <a href="' . base_url() . '">Home</a></li>';
        foreach ((array) $links as $link => $title) {
            //prd($link);
            if ($title == 0) {
                echo"<li><span><a href='" . base_url($link) . "'>" . $title . "</a></li>";
            } else {
                $link2 = $ex_links[0] . "/" . $link;
                echo"<li></span><a href='" . base_url($link2) . "'>" . $link . "</a></li>";
            }
        }
        //print_r($arr);
        echo"</ol> </div> </div>";
    }

}
if (!function_exists('time_ago')) {

    function time_ago($date) {

        if (empty($date)) {
            return "No date provided";
        }

        $time = date('H', strtotime($date)) != '00' ? '(' . date('h:i A', strtotime($date)) . ')' : '';
        $periods = array("second", "minute", "hour", "day");
        $lengths = array("60", "60", "24", "7");
        $now = time();
        $unix_date = strtotime($date);

        // check validity of date

        if (empty($unix_date)) {
            return "Bad date";
        }

        // is it future date or past date
        if ($now > $unix_date) {
            $difference = $now - $unix_date;
            $tense = "ago";
        } else {
            $difference = $unix_date - $now;
            $tense = "from now";
        }
        for ($j = 0; $difference >= $lengths[$j] && $j < count($lengths) - 1; $j++) {
            $difference /= $lengths[$j];
        }
        $difference = round($difference);
        if ($difference != 1) {
            $periods[$j] .= "s";
        }

        if ($difference >= 60 && $periods[$j] == 'days') {
            return date('d-M-Y H:i A', strtotime($date));
        } else {
            if ($tense != "from now") {
                return "$difference $periods[$j] {$tense} {$time}";
            } else {
                return "Just Now";
            }
        }
    }

}

if (!function_exists('site_date')) {

    function site_date($date = false, $if_not = 'Not Define!') {
        if ($date == '0000-00-00') {
            $date = false;
        }
        return $date ? date('d-m-Y', strtotime($date)) : $if_not;
    }

}

if (!function_exists('site_time')) {

    function site_time($time = null, $format = 1) {
        if ($format == 1) {
            return date('H:i', strtotime($time));
        } elseif ($format == 2) {
            return date('H:i A', strtotime($time));
        }
    }

}

if (!function_exists('site_date_time')) {

    function site_date_time($site_date_time = false, $if_not = 'Not Define!') {
        if ($site_date_time == '0000-00-00') {
            $site_date_time = false;
        }
        return $site_date_time ? date('d-m-Y H:i A', strtotime($site_date_time)) : $if_not;
    }

}

if (!function_exists('db_date')) {

    function db_date($date = false) {
        return $date ? date('Y-m-d', strtotime($date)) : date('Y-m-d');
    }

}

if (!function_exists('db_date_time')) {

    function db_date_time($date_time = false) {
        return $date_time ? date('Y-m-d H:i:s', strtotime($date_time)) : date('Y-m-d H:i:s');
    }

}


if (!function_exists('expired')) {

    function expired($date) {
        $date = strtotime($date);
        $curr_date = strtotime(date('d-m-Y'));
        if ($date > $curr_date) {
            return false; // not exp
        } else {
            return true; // exp
        }
    }

}



if (!function_exists('pr')) {

    function pr($arr = 'No Data') {
        echo "<pre>";
        print_r($arr);
        echo "</pre>";
    }

}

if (!function_exists('prd')) {

    function prd($arr = 'No Data') {
        echo "<pre>";
        print_r($arr);
        echo "</pre>";
        die;
    }

}



if (!function_exists('img_src')) {

    function img_src($pic_path, $name, $filetype = 'image') {
        $default_image = "no_image.jpg";
        $pic = (trim($name) and file_exists(FCPATH . $pic_path . trim($name))) ? $pic_path . $name : 'assets/uploads/' . $default_image;
        return base_url($pic);
    }

}


if (!function_exists('encrypt')) {

    function encrypt($id) {
        $key = hash('sha256', 'asetrdtcb');
        $iv = substr(hash('sha256', 'jjedgdfvv'), 0, 16);
        $encrypted = openssl_encrypt($id, "AES-256-CBC", $key, 0, $iv);
        return base64_encode($encrypted);
    }

}


if (!function_exists('decrypt')) {

    function decrypt($encrypted) {
        $key = hash('sha256', 'asetrdtcb');
        $iv = substr(hash('sha256', 'jjedgdfvv'), 0, 16);
        $id = openssl_decrypt(base64_decode($encrypted), "AES-256-CBC", $key, 0, $iv);
        return $id;
    }

}

if (!function_exists('current_user')) {

    function current_user() {
        $thiss = &get_instance();
        return $thiss->session->userdata();
    }

}

if (!function_exists('user_data')) {

    function user_data($id) {
        $thiss = &get_instance();
        $res = $thiss->db->where('id', $id)->get('users')->row();
        return $res;
    }

}

if (!function_exists('read_more')) {

    function read_more($string, $limit = 20, $dash = true, $read_more = false) {
        $real_conten = $string;
        $string = strip_tags($string);
        if (strlen($string) > $limit) {
            $string = substr($string, 0, $limit);
            // $string = substr($stringCut, 0, strrpos($stringCut, ' '));
            if ($dash) {
                $string .= '...';
            }
            $return_html = html_entity_decode($string);
            if ($read_more) {
                $return_html = $return_html . read_more_poup($real_conten);
            }
        } else {
            $return_html = html_entity_decode($string);
        }
        return $return_html;
    }

}


if (!function_exists('unseen_notification_count')) {

    function unseen_notification_count($user_id) {
        $thiss = & get_instance();
        return $thiss->db->where(array('notified_to' => $user_id, 'seen' => '0'))->get('notification_details')->num_rows();
    }

}

if (!function_exists('unseen_notification_records')) {

    function unseen_notification_records($user_id) {
        $thiss = & get_instance();
        return $thiss->db->where(array('notified_to' => $user_id))->limit(10)->order_by('id', 'desc')->get('notification_details')->result();
    }

}
if (!function_exists('product_type')) {

    function product_type() {
        return array('' => 'Select type', 1 => 'Liquor', 2 => 'Snacks');
    }

}

if (!function_exists('status')) {

    function status() {
        return array('' => 'Select status', 1 => 'Active', 2 => 'Inactive');
    }

}

if (!function_exists('is_available')) {

	function is_available() {
		return array('' => 'Select', 1 => 'Yes', 2 => 'No');
	}

}


if (!function_exists('is_permit')) {

	function is_permit() {
  //  print_r(array('' => 'Select', 1 => 'Yes', 2 => 'No')); die("jghjghg");
		return array('' => 'Select', 1 => 'Yes', 2 => 'No');
	}

}

if (!function_exists('food_type')) {

    function food_type() {
        return array(1 => 'Veg', 2 => 'Non-Veg');
    }

}

if (!function_exists('is_cod')) {

    function is_cod() {
        return array('' => 'Select', 1 => 'Available', 2 => 'Not available');
    }

}
if (!function_exists('in_store')) {

    function in_store() {
        return array('' => 'Select', 1 => 'Available', 2 => 'Not available');
    }

}
if (!function_exists('print_status')) { //javascript href link print

    function print_status($val) {

        if ((int) $val == 1) {
            echo '<span class="label label-success">Active</span>';
        } elseif ((int) $val == 8) {
            echo '<span class="label label-danger">Doc Rejected</span>';
        } elseif ((int) $val == 3) {
            echo '<span class="label label-danger">Approve/Reject</span>';
        }
        elseif ((int) $val == 4) {
            echo '<span class="label label-danger">Rejected</span>';
        }
        else {
            echo '<span class="label label-warning">Inactive</span>';
        }
    }

}
if (!function_exists('product_cat')) {

    function product_cat($id = false, $type = false) {
        $CI = & get_instance();

        $CI->db->where(array('status' => '1', 'parent_id' => '0'));

        if (!empty($type)) {
            $CI->db->where('is_liquor', $type);
        }

        $categories = $CI->db->order_by('title', 'ASC')->get('category')->result();

        //echo '<option value="">Select catogory</option>';
        if ($categories) {
            foreach ((array) $categories as $val) {
                if ($val->id == $id) {
                    echo '<option value="' . $val->id . '" selected >' . $val->title . '</option>';
                } else {
                    echo '<option value="' . $val->id . '">' . $val->title . '</option>';
                }
            }
        }
    }

}
if (!function_exists('pub_cat_count')) {

    function pub_cat_count() {
        $CI = & get_instance();

        $CI->db->where(array('status' => '1', 'parent_id' => '0','is_pub'=>'1'));

        $categories = $CI->db->get('category')->result();
          return count($categories);
    }

}
if (!function_exists('product_cat_subcat')) {

    function product_cat_subcat($cat_id, $sub_cat_id = false) {
        $CI = & get_instance();

        $categories = $CI->db->where(array('status' => '1', 'parent_id' => $cat_id))->get('category')->result();
        if ($categories) {
            foreach ((array) $categories as $val) {
                if ($val->id == $sub_cat_id) {
                    echo '<option value="' . $val->id . '" selected  >' . $val->title . '</option>';
                } else {
                    echo '<option value="' . $val->id . '">' . $val->title . '</option>';
                }
            }
        }
    }

}


if (!function_exists('retailer_subcat')) {

    function retailer_subcat($retailer_id) {
        $CI = & get_instance();

        $categories = $CI->db->select('GROUP_CONCAT(DISTINCT(sub_category_id) SEPARATOR ",") as sub_cat')->where(array('status' => '1', 'retailer_id' => $retailer_id))->get('product')->row();
        return $categories;
    }

}



if (!function_exists('roles')) {

    function roles($id = false) {
        $CI = & get_instance();
        $ignore = array('role_id' => '1');
        $roles = $CI->db->where_not_in('role_id', $ignore)->get('roles')->result();
        if ($roles) {
            foreach ((array) $roles as $val) {
                if ($val->role_id == $id) {
                    echo '<option value="' . $val->role_id . '" selected >' . ucfirst($val->role_name) . '</option>';
                } else {
                    echo '<option value="' . $val->role_id . '">' . ucfirst($val->role_name) . '</option>';
                }
            }
        }
    }

}
if (!function_exists('get_rolename')) {

    function get_rolename($id) {
        $CI = & get_instance();
        $roles = $CI->db->where('role_id', $id)->get('roles')->row();
        return $roles;
        // if ($roles) {
        //     foreach ((array) $roles as $val) {
        //         if ($val->role_id == $id) {
        //             echo '<option value="' . $val->role_id . '" selected >' . ucfirst($val->role_name) . '</option>';
        //         } else {
        //             echo '<option value="' . $val->role_id . '">' . ucfirst($val->role_name) . '</option>';
        //         }
        //     }
        // }
    }

}


if (!function_exists('print_flash_message')) {

    function print_flash_message() {
        $thiss = &get_instance();
        $message = $thiss->session->flashdata();
        if (isset($message['flash_status']) and $message['flash_status'] == 'success') {
            echo '<div class="alert alert-success"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> ';
            echo $message['flash_message'];
            echo '</div>';
        } elseif (isset($message['flash_status']) and $message['flash_status'] == 'error') {
            echo '<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
            echo $message['flash_message'];
            echo '</div>';
        }
    }

}


if (!function_exists('set_flash_message')) {

    function set_flash_message($type = 'error', $message = 'There is something wrong, Please contact to admin.') {
        $thiss = &get_instance();
        $thiss->session->set_flashdata(array('flash_message' => $message, 'flash_status' => $type));
    }

}



if (!function_exists('Fcm_Users')) {

    function Fcm_Users($user_id = null, $condition = false) {
        $CI = & get_instance();
        if ($condition == true) {
            $where = array('is_logged_in' => '0');
        } else {
            $where = array('user_id' => $user_id, 'is_logged_in' => '1');
        }

        $result = $CI->db->select('deviceId,deviceType,fcmId,user_id')->where($where)->get('device_info')->result_array();
        return $result ? $result : false;
    }

}


if (!function_exists('is_admin')) {

    function is_admin($url = 'base') {
        $thiss = &get_instance();
        $user_role = $thiss->session->userdata('role_id');

        $url = ($url == 'base') ? base_url() : $url;
        if ($user_role != 1) {
            set_flash_message('error', "you can't access that page");
            redirect($url);
        } else {
            return true;
        }
    }

}


if (!function_exists('print_order_status')) {

    function print_order_status($val) {
        switch ($val) {
            case 1: //Order Placed

                $status = '<span class="label label-primary">' . order_status(1)->title . '</span>';

                break;
            case 2: //shipped

                $status = '<span class="label label-danger">' . order_status(2)->title . '</span>';
                break;
            case 3: // in transit

                $status = '<span class="label label-warning">' . order_status(3)->title . '</span>';

                break;
            case 4: // delivery

                $status = '<span class="label label-primary">' . order_status(4)->title . '</span>';

                break;
            case 5: // delivered

                $status = '<span class="label label-success">' . order_status(5)->title . '</span>';

                break;
            case 6: // cancel

                $status = '<span class="label label-danger">' . order_status(6)->title . '</span>';

                break;

            case 7: // cancel

                $status = '<span class="label label-default">' . order_status(7)->title . '</span>';

                break;
        }

        return $status;
    }

}

if (!function_exists('order_status')) {

    function order_status($id = false) {
        $CI = &get_instance();

        if (!empty($id)) {
            $CI->db->where('id', $id);
        }

        $query = $CI->db->get('order_status');

        if (!empty($id)) {
            $result = $query->row();
        } else {
            $result = $query->result();
        }

        return $result ? $result : false;
    }

}

if (!function_exists('sub_category_type')) {

    function sub_category_type() {
        return array('' => 'Select', 1 => 'Domestic', 2 => 'Imported');
    }

}
if (!function_exists('is_liquor')) {

    function is_liquor() {
        return array('' => 'Select', 1 => 'Liquor', 2 => 'Food');
    }

}

if (!function_exists('retailers')) {

    function retailers() {
        $thiss = &get_instance();
		$status = array('1','9');
    $role_id = array('2','4');
        $res = $thiss->db->select('id,display_name,status')->where(array('deleted' => '0'))->where_in('role_id',$role_id)->where_in('status',$status)->get('users')->result();
        return $res ? $res : false;
    }

}

if (!function_exists('state_dropdown')) {

    function state_dropdown($selected = "", $class = "", $layout = true) {

        $CI = & get_instance();

        $CI->db->select('id,name');
        $CI->db->where('status', '1');
        $CI->db->from('states');

        $query = $CI->db->get();
        $result = $query->result_array();
        $options = array("" => "Select State") + array_column($result, 'name', 'id');
        if ($layout) {
            echo form_dropdown(array('name' => 'state_id', 'class' => $class, 'id' => 'state_id'), $options, $selected, 'State');
        } else {
            echo '<select id="state_id" class="' . $class . '" >';
            foreach ($options as $id => $option) {
                if ($id == $selected) {
                    echo '<option value="' . $id . '"  selected >' . $option . '</option>';
                } else {
                    echo '<option value="' . $id . '" >' . $option . '</option>';
                }
            }
            echo '</select>';
        }
    }

}

if (!function_exists('status_dropdown')) {

    function status_dropdown($selected = "", $layout = true, $class = "form-control", $options = false) {
        if (!$options) {
        	$CI = & get_instance();
        	$url = $CI->uri->segment_array();
        	//echo end($url);die;
        	if(strtolower(end($url))=='customers'){
        		$options = array("" => "Select Status", 1 => "Active", 0 => "Inactive", 2 => "Track Users");
        	}else{
        		$options = array("" => "Select Status", 1 => "Active", 0 => "Inactive");
        	}

        }

        if ($layout) {
            echo form_dropdown(array('name' => 'status', 'class' => $class), $options, $selected, 'Status');
        } else {
            echo '<select name="status" id="status" class="' . $class . '" >';
            foreach ($options as $id => $option) {
                if ($id == $selected) {
                    echo '<option value="' . $id . '"  selected >' . $option . '</option>';
                } else {
                    echo '<option value="' . $id . '" >' . $option . '</option>';
                }
            }
            echo '</select>';
        }
    }

}


if (!function_exists('role_dropdown')) {

    function role_dropdown($selected = "", $layout = true, $class = "form-control", $options = false) {
        if (!$options) {
        	$CI = & get_instance();
        	$url = $CI->uri->segment_array();
        	//echo end($url);die;
        	if(strtolower(end($url))=='pending_request'||strtolower(end($url))=='users'){
        		$options = array("" => "Select Role", 2 => "Retailer", 4 => "Pub");
        	}

        }

        if ($layout) {
            echo form_dropdown(array('name' => 'role', 'class' => $class), $options, $selected, 'Role');
        } else {
            echo '<select name="role" id="role" class="' . $class . '" >';
            foreach ($options as $id => $option) {
                if ($id == $selected) {
                    echo '<option value="' . $id . '"  selected >' . $option . '</option>';
                } else {
                    echo '<option value="' . $id . '" >' . $option . '</option>';
                }
            }
            echo '</select>';
        }
    }

}



if (!function_exists('products_dropdown')) {

    function products_dropdown($selected = "", $class = "", $layout = true) {

        $CI = & get_instance();

        $CI->db->select('id,title');
        $CI->db->where('status', '1');
        $CI->db->order_by('title', 'ASC');
        $CI->db->from('product');

        $query = $CI->db->get();
        $result = $query->result_array();
        $options = array("" => "Select Product") + array_column($result, 'title', 'id');
        if ($layout) {
            echo form_dropdown(array('name' => 'product_id', 'class' => $class, 'id' => 'product_id'), $options, $selected, 'Product');
        } else {
            echo '<select id="product_id" class="' . $class . '" >';
            foreach ($options as $id => $option) {
                if ($id == $selected) {
                    echo '<option value="' . $id . '"  selected >' . $option . '</option>';
                } else {
                    echo '<option value="' . $id . '" >' . $option . '</option>';
                }
            }
            echo '</select>';
        }
    }

}

if (!function_exists('rating_calculation')) {

    function rating_calculation($data) {
        $rating = 0;
        $total = 0;
        $overall = 0;
        foreach ($data as $val) {
            foreach ($val as $k => $v) {
                $overall = $overall + $k * $v;
                $total = $total + $v;
            }
        }
        if ($total > 0) {
            $rating = $overall / $total;
        }
        return $rating;
    }

}

if (!function_exists('get_order_details')) {

    function get_order_details($id) {

        $thiss = & get_instance();

        $thiss->db->select('order_detail.*,order.shipping_charge,order.internet_charge,u1.display_name as retailer,u2.display_name as user,u2.email as user_email,payment_option.title,users_address.name as name,users_address.id as address_id,users_address.mobile,users_address.country,users_address.city,users_address.state_id,users_address.address,users_address.state,users_address.country,users_address.is_default,order_detail.status,(select group_concat(p.title separator "<@>") from ti_order_detail od left join ti_product p on od.product_id=p.id where od.order_id="' . $id . '" GROUP by od.order_id) as product_id,(select group_concat(`price` separator "<@>") from ti_order_detail od where od.order_id="' . $id . '" GROUP by od.order_id) as price,(select group_concat(`quantity` separator "<@>") from ti_order_detail od where od.order_id="' . $id . '" GROUP by od.order_id) as quantity,SUM(price*quantity) as total,(select group_concat(`weight` separator "<@>") from ti_order_detail od where od.order_id="' . $id . '" GROUP by od.order_id) as weight');
        $thiss->db->join('users u1', 'u1.id = order_detail.retailer_id', 'left');
        $thiss->db->join('users u2', 'u2.id = order_detail.user_id', 'left');
        $thiss->db->join('order', 'order.order_number = order_detail.order_id', 'left');
        $thiss->db->join('payment_option', 'payment_option.id = order_detail.payment_mode', 'left');
        $thiss->db->join('users_address', 'users_address.id = order_detail.address', 'left');
        $thiss->db->where('order_detail.order_id', $id);
        $thiss->db->order_by('order_detail.creation_date', 'desc');
        $res = $thiss->db->get('order_detail');

        //echo $this->db->last_query(); die;
        return $res->row();
    }

}


if (!function_exists('smtp_mail')) {

    function smtp_mail($useremail, $message, $subject = null, $attachment = null) {
        $CI = get_instance();
        $CI->load->library('email');

        $config['protocol'] = "mail";

        // gmail settings
        $config['smtp_host'] = "mail.tiplur.in:2080";
        $config['smtp_port'] = "465";
        $config['smtp_user'] = "admin@tiplur.in";
        $config['smtp_pass'] = "?MQ~VdD]JHfh";

        // $config['smtp_host'] = "smtp.gmail.com";
        // $config['smtp_port'] = "26";
        // $config['smtp_user'] = "onlifeom@gmail.com";
        // $config['smtp_pass'] = "Admin@123";
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";
        $CI->email->initialize($config);
      //  $CI->email->set_newline("\r\n");
        $CI->email->from('admin@tiplur.in');

        $CI->email->to($useremail);

        if (!empty($subject)) {
            $CI->email->subject($subject);
        } else {
            $CI->email->subject('Tiplur');
        }
        $CI->email->message($message);

        if ($attachment) {
            $CI->email->attach($attachment);
        }


        if ($CI->email->send()) {

            return 1;
        } else {
            	echo $CI->email->print_debugger();

            return 0;
        }


    }

}

if (!function_exists('get_status')) { //javascript href link print

    function get_status($val) {

        if ((int) $val == 1) {
            return 'Active';
        } else {
            return 'Inactive';
        }
    }

}

if (!function_exists('store_detail_by_retailer_id')) {

    function store_detail_by_retailer_id($retailer_id) {
        $thiss = &get_instance();
        $thiss->db->select('store.*,users.status as retailer_status');
        $thiss->db->where("store.retailer_id", $retailer_id);
        $thiss->db->join("users","users.id=store.retailer_id","left");
        $res = $thiss->db->get('store')->row();
	return $res;
    }

}

if (!function_exists('get_state_id')) {

	function get_state_id($user_id) {
		$thiss = &get_instance();
		$res = $thiss->db->select('state_id')->where('id', $user_id)->get('users')->row();
		return $res->state_id;
	}

}

if (!function_exists('get_state_detail')) {

    function get_state_detail($state_id) {
        $thiss = &get_instance();
        $res = $thiss->db->select('max_limit_weight')->where('id', $state_id)->get('states')->row();
        return $res->max_limit_weight;
    }

}

if (!function_exists('is_logged_in')) {

	function is_logged_in($device_id, $user_id) {
		$thiss = &get_instance();
		$res = $thiss->db->select('*')->where(array('deviceId' => $device_id, 'user_id' => $user_id))->get('device_info')->result();
		if(sizeof($res)>0){
			return true;
		}else{
			return false;
		}
	}

}

if (!function_exists('sms_notification')) {

	function sms_notification($msg, $mobile) {
		$thiss = &get_instance();
		$sender_id = 'TIPLUR';
    $user_id = urlencode('tiplurhttp');
    $pwd     = urlencode('TM@tip11');
    $message = urlencode($msg);

   $URL ="https://msg.tweakmedia.in/smpp/sendsms?username=".$user_id."&password=".$pwd."&to=".$mobile."&from=".$sender_id."&text=".$message;
   $arrContextOptions=array(
      "ssl"=>array(
            "verify_peer"=>false,
            "verify_peer_name"=>false,
        ),
    );
   //print_r($URL); die();
 $result=$response = file_get_contents($URL, 'false', stream_context_create($arrContextOptions));
		//$URL ="http://49.50.77.216/API/SMSHttp.aspx?UserId=".$user_id."&pwd=".$pwd."&Message=".$message."&Contacts=".$mobile."&SenderId=".$sender_id."&ServiceName=S/MSOTP";
		$ch = curl_init($URL);
		curl_setopt($ch, CURLOPT_URL, $URL);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER , 1);
		$result = curl_exec($ch);
  //   if(curl_error($ch)){
  //     echo 'Curl error: ' . curl_error($result);
  // }
		curl_close($ch);
	//print_r($result); die();
//$result = Http_get("https://msg.tweakmedia.in/smpp/", 80, "/sendsms", array("username" => $user_id, "password" => $pwd, "to" => $mobile,"from"=>$sender_id, "text" => $message));
return $result;

// $url="login.bulksmsgateway.in/sendmessage.php?user=".urlencode($username)."&password=".urlencode($password)."&mobile=".urlencode($number)."&sender=".urlencode($sender)."&message=".urlencode($message)."&type=".urlencode('3');
//             $ch = curl_init($url);
//             curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//             echo $curl_scraped_page = curl_exec($ch);
//             dd($curl_scraped_page);
//             curl_close($ch);
	}

}

// if (!function_exists('curl_method')) {
//
// 	function curl_method($url,,$data,$header) {
// 		$thiss = &get_instance();
// 		$res = $thiss->db->where('id', $id)->get('users_address')->row();
// 		return $res;
// 	}
//
// }


if (!function_exists('user_address')) {

	function user_address($id) {
		$thiss = &get_instance();
		$res = $thiss->db->where('id', $id)->get('users_address')->row();
		return $res;
	}

}

if (!function_exists('payment_option')) {

	function payment_option($payment_mode) {
        $ids=explode(',',$payment_mode);
   // var_dump($ids);
		$thiss = &get_instance();
        $res = $thiss->db->select('GROUP_CONCAT(title) as title')->where_in('id', $ids)->get('payment_option')->row();
       // print_r($res); die();
		return $res;
	}

}

if (!function_exists('product_detail')) {

	function product_detail($id) {
		$thiss = &get_instance();
		$res = $thiss->db->where('id', $id)->get('product')->row();
		return $res;
	}

}


if (!function_exists('get_products_limit')) {

	function get_products_limit($state_id) {
		$thiss = &get_instance();
		$result = $thiss->db->where('state_id', $state_id)->get('product_limit')->result_array();
		$arr = !empty($result) ? array_column($result,'max_limit','brandname'):'';
		return $arr ? $arr :false;
	}

}


if (!function_exists('payment_option')) {

	function payment_option($payment_mode) {
        $ids=explode(',',$payment_mode);
   // var_dump($ids);
		$thiss = &get_instance();
        $res = $thiss->db->select('GROUP_CONCAT(title) as title')->where_in('id', $ids)->get('payment_option')->row();
       // print_r($res); die();
		return $res;
	}

}


/*if (!function_exists('brands_list')) {

    function brands_list() {
        $CI = & get_instance();

        $brands = $CI->db->distinct()->select('brand_name')->where(array('promo_status' => '0','deleted' => '0'))->get('ti_promotion')->result();
        // echo $CI->db->last_query();
        // $brands = $CI->db->order_by('brand_name', 'ASC')->get('promotion')->array_result();
        // prd($brands); die;
        //echo '<option value="">Select catogory</option>';
        if ($brands) {
            foreach ( $brands as $val) {
                if ($val->brand_name) {
                    echo '<option value="' . $val->brand_name . '" selected >' . $val->brand_name . '</option>';
                } else {
                    echo '<option value="' . $val->brand_name . '">' . $val->brand_name . '</option>';
                }
            }
        }
    }
}*/
