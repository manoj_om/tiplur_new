<?php

class User_model extends CI_Model {

    protected $projectTable = "social_users";
    protected $is_edit;

    function __construct() {
        parent::__construct();
    }

    public function get_login_validation_rules() {
        $form_validation = array(
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required|valid_email|max_length[255]'
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required|min_length[6]',
            ),
            array(
                'field' => 'otp',
                'rules' => array(array('otp', array($this, 'check_otp'))),
                'errors' => array(
                    'otp' => 'Invalid OTP',
                ),
            ),
            array(
                'field' => 'email_pass_match',
                'rules' => array(array('email_pass_match_msg', array($this, 'email_pass_match'))),
                'errors' => array(
                    'email_pass_match_msg' => 'Sorry! Username and Password not matching',
                ),
            )
        );

        $this->load->library('form_validation');
        $this->form_validation->set_rules($form_validation);
        return ($this->form_validation->run() === false) ? false : true;
    }

    function check_otp() {
        $otp = $this->input->post('otp');
        $session_otp = $this->session->userdata('auth_otp');
        
        if ($session_otp == $otp) 
		{
            return true;
        }else{  return false;  }
       
    }

    function form_validation() {
        //
        $this->is_edit = ($this->uri->segment(3) == 'edit') ? true : false;

        $arr = array(
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required|valid_email|max_length[255]'
            ),
            array(
                'field' => 'display_name',
                'label' => 'name',
                'rules' => 'required|min_length[3]|max_length[30]'
            ),
            array(
                'field' => 'mobile',
                'label' => 'phone number',
                'rules' => 'required|numeric|min_length[10]|max_length[10]'
            ),
            array(
                'field' => 'address',
                'label' => 'address',
                'rules' => 'required|min_length[7]|max_length[255]'
            ),
            array(
                'field' => 'role_id',
                'label' => 'roles',
                'rules' => 'required'
            )
        );

        if (!$this->is_edit || isset($_POST['password'])) {
            $arr[] = array(
                'field' => 'password',
                'label' => 'password',
                'rules' => array('required', 'min_length[8]', array('email_pass_match_msg', array($this, 'password_check'))),
                'errors' => array(
                    'email_pass_match_msg' => 'Sorry! please enter alpha numeric password',
                ),
            );
            $arr[] = array(
                'field' => 'confirm_password',
                'label' => 'password confirmation',
                'rules' => array('required', 'min_length[8]', 'matches[password]', array('email_pass_match_msg', array($this, 'password_check'))),
                'errors' => array(
                    'email_pass_match_msg' => 'Sorry!please enter alpha numeric password',
                ),
            );
        }
        //prd($arr);
        return $arr;
    }

    public function password_check($str) {
        if (preg_match('#[0-9]#', $str) && preg_match('#[a-zA-Z]#', $str)) {
            return TRUE;
        }
        return FALSE;
    }

    function email_pass_match() {
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $userdata = $this->db->where(array('email' => $email, 'password' => md5($password), 'status' => '1'))->get('users')->first_row();
        if (strlen($password) < 6) {
            return true;
        }
        return ($userdata) ? true : false;
    }

    public function get_resetpassword_validation_rules() {
        $form_validation = array(
            array(
                'field' => 'old_pass',
                'label' => 'Old Password',
                'rules' => array('required', array('old_password_match', array($this, 'match_password'))),
                'errors' => array('old_password_match' => "Old password does not match !!"),
            ),
            array(
                'field' => 'password',
                'label' => 'New Password',
                'rules' => 'required|min_length[8]',
            ),
            array(
                'field' => 'cpass',
                'label' => 'Password Confirmation',
                'rules' => 'required|min_length[8]|matches[password]',
            )
        );

        $this->load->library('form_validation');
        $this->form_validation->set_rules($form_validation);
        return ($this->form_validation->run() === false) ? false : true;
    }

    public function match_password($val) {
        if (!empty($val)) {
            $old_pass = md5($this->input->post('old_pass'));
            $userid = $this->session->userdata("id");
            $password_match = $this->db->where(array('id' => $userid))->get('users')->row();

            if ($old_pass == $password_match->password) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function ResetPassword() {

        $email = $this->session->userdata('email');
        $user_id = $this->session->userdata('id');

        $db_data['password'] = md5($this->input->post('password'));

        $this->db->where('id', $user_id);
        $update_result = $this->db->update('users', $db_data);
        return $update_result ? $update_result : false;
    }

    public function validate_email($email = null) {
        $query = $this->db->select('id,display_name')->where('email', $email)->get('users');
        if ($query->num_rows() > 0) {

            $this->load->helper('string');
            $randcode = sha1(random_string('alnum', 40) . $email);

            // Create the link to reset the password.
            $pass_link = base_url('reset-password?token=' . $randcode);

            $db_data['reset_hash'] = $randcode;
            $db_data['reset_time'] = date('Y-m-d H:i:s');
            $this->db->where('id', $query->row()->id);
            $update_result = $this->db->update('users', $db_data);


            if ($update_result) {
                // Now send the email 
                $this->load->library('email');
                $this->email->from('medsindia@admin..com', 'MedsIndia');
                $this->email->to("gautam.akriti@orangemantra.in");
                $this->email->subject('Changed Password Request Link');
                $message = '<div style="font-size:16px">Hello, ' . $query->row()->display_name . '</div>';
                $message.='<br/>';
                $message.= 'Please use the below link for reset password:';
                $message.='<br/>';
                $message.= $pass_link;
                $message.='<br/>';
                $message.="<div style='font-size:16px'>Thanks,</div>";
                $message.="<div style='font-size:16px'>Team MedsIndia</div>";
                $this->email->message($message);
                $this->email->set_mailtype("html");
                $this->email->send();
                return 1;
            } else {
                return 2;
            }
        } else {
            return 3;
        }
    }

    public function validate_token($token) {
        $query = $this->db->select('reset_time,email')->where('reset_hash', $token)->get('users')->row();
        if ($query) {
            $start_time = strtotime($query->reset_time);
            $end_time = strtotime(date('Y-m-d H:i:s'));
            $difference = round(abs($end_time - $start_time) / 60);
            if ($difference <= '86400') {
                return $query->email;
            } else {
                return false;
            }
        }
    }

    public function users_address_add_update($type) {
        $post_data = $this->input->post();
        $post_data['is_default'] = $this->input->post('default') ? $this->input->post('default') : '0';
        unset($post_data['default']);

        if ($type == 'update') {
            $post_data['modified_on'] = date('Y-m-d H:i:s');
            $id = $this->input->post('address_id');
            unset($post_data['address_id']);
            //update address
            $this->db->where('id', $id);
            $query = $this->db->update('users_address', $post_data);
            $result = $query ? $id : '';
        } else if ($type == 'delete') {
            $id = $this->input->post('address_id');
            $this->db->where('id', $id);
            $result = $this->db->delete('users_address');
        } else {
            $post_data['created_on'] = date('Y-m-d H:i:s');
            //insert address
            $this->db->insert('users_address', $post_data);
            $result = $this->db->insert_id();
        }

        return $result ? $result : false;
    }

    public function users_address_detail($id, $condition = false) {
        if ($condition == true) {
            $result = $this->db->select('id,user_id,name,address,country,city,state,pincode,mobile,is_default as default')->where(array('user_id' => $id, 'is_default' => '1'))->get('users_address')->result();
        } else {
            $result = $this->db->select('*')->where('id', $id)->get('users_address')->row();
        }

        return $result ? $result : false;
    }

    // users listing 
    public function users_details($filter = null, $limit = null, $offset = null, $count = true, $type = false) {

        if (isset($filter['name']) and $filter['name']) {
            $this->db->like("(display_name", trim($filter['name']));
            $this->db->or_like("email", trim($filter['name']));
            $this->db->or_where("mobile LIKE '%" . $filter['name'] . "%' ESCAPE '!' )");
        }

        if (isset($filter['status']) and $filter['status'] != '') {
            $this->db->where("users.status", $filter['status']);
        }
        if (isset($filter['state_id']) and $filter['state_id'] != '') {
            $this->db->where("users.state_id", $filter['state_id']);
        }
        if (isset($filter['role']) and $filter['role'] != '') {
            $this->db->where("users.role_id", $filter['role']);
        }
        if ($limit) {
            $this->db->limit($limit, $offset);
        }

        if ($type == '2') {
            //echo $type; die;
              $this->db->where("users.role_id", '3');
		} else {
            $mstatus = array('9','10','3');
            $this->db->where_in("users.role_id", array('2','4'));
			$this->db->where_not_in("users.status", $mstatus);		
		}
        $this->db->where_not_in("id", $this->session->userdata('user_id'));
        $this->db->where("deleted", '0');
        $res = $this->db->select('users.*, roles.role_name as role, states.name as state, device_info.is_logged_in')
        ->join('roles', 'roles.role_id = users.role_id', 'left')
        ->join('states', 'states.id = users.state_id', 'left')
        ->join('device_info', 'device_info.user_id = users.id')
        ->order_by('users.id', 'desc')->get('users'); 
        //echo $this->db->last_query();
        return ($count) ? $res->num_rows() : $res->result();
    }


    // users listing 
    public function users_pending_details($filter = null, $limit = null, $offset = null, $count = true, $type = false) {

        if (isset($filter['name']) and $filter['name']) {
            $this->db->like("(display_name", trim($filter['name']));
            $this->db->or_like("email", trim($filter['name']));
            $this->db->or_where("mobile LIKE '%" . $filter['name'] . "%' ESCAPE '!' )");
        }

        if (isset($filter['status']) and $filter['status'] != '') {
            $this->db->where("users.status", $filter['status']);
        }
        if (isset($filter['state_id']) and $filter['state_id'] != '') {
            $this->db->where("users.state_id", $filter['state_id']);
        }
        if (isset($filter['role']) and $filter['role'] != '') {
            $this->db->where("users.role_id", $filter['role']);
        }
        if ($limit) {
            $this->db->limit($limit, $offset);
        }

        if ($type == '2') {
            //echo $type; die;
              $this->db->where("users.role_id", '3');
		} else {
            $mstatus = array('3');
            $this->db->where_in("users.role_id", array('2','4'));
            //$this->db->or_where("users.role_id", '4');
			$this->db->where_in("users.status", $mstatus);		
		}
        $this->db->where_not_in("id", $this->session->userdata('user_id'));
        $this->db->where("deleted", '0');
        $res = $this->db->select('users.*, roles.role_name as role, states.name as state, device_info.is_logged_in')
        ->join('roles', 'roles.role_id = users.role_id', 'left')
        ->join('states', 'states.id = users.state_id', 'left')
        ->join('device_info', 'device_info.user_id = users.id')
        ->order_by('users.id', 'desc')->get('users'); 
        //echo $this->db->last_query();
        return ($count) ? $res->num_rows() : $res->result();
    }


    // add users 
    public function add_users($data) {
        //prd($data);
        $res = $this->db->insert('users', $data);
        return ($res) ? $res : false;
    }

    // Users detail using id
    public function users_detail_by_id($id = false, $email = false) {
        if (!empty($id)) {
            $this->db->where("id", $id);
        }
        if (!empty($email)) {
            $this->db->where("email", $email);
        }
        $res = $this->db->get('users')->row();
        return $res;
    }

    // Update users details  
    public function update_users_details($data, $id = false) {
        $return = $this->db->where('id', $id)->update('users', $data);
        return $return;
    }

    // soft delete users   
    public function delete_users($id) {
        $return = $this->db->where('id', $id)->update('users', array('deleted' => '1'));
        return $return;
    }

    // Get product active count
    public function product_active_count() {
        if($_SESSION['role_id']=='2'||$_SESSION['role_id']=='4')
        {
            $this->db->where("retailer_id", $_SESSION['id']);
        }
        $return = $this->db->select("*")->where(array('deleted' => '0', 'status' => '1'))->get('product')->num_rows();
       // prd( ($return) ? $return : false);
        return ($return) ? $return : false;
    }

    // Get customers active  count
    public function customers_count() {
        $return = $this->db->select("*")->where(array('deleted' => '0', 'status' => '1', 'role_id' => '3'))->get('users')->num_rows();
        return ($return) ? $return : false;
    }

    // Get pending order
    public function process_order_count($type = true) {
        $this->db->where_in('status', array('2', '3', '4'));
        if (empty($type)) {
            $this->db->limit(10, 0);
        }
        $return = $this->db->order_by('created_on', 'DESC')->get('order')->result();
        return ($return) ? $return : false;
    }

    // Get resent order
    public function recent_order_count($type = true) {
        $this->db->where('status', 1);
        if (empty($type)) {
            $this->db->limit(10, 0);
        }
        $return = $this->db->order_by('created_on', 'DESC')->get('order')->result();
        return ($return) ? $return : false;
    }
    
    public function users_store($user_id){
    	$return = $this->db->select("*")->where(array('retailer_id' => $user_id))->get('store')->result();
    	return ($return) ? $return : false;
    }

		
	public function mapped_retailer_details($filter = null, $limit = null, $offset = null, $count = true) 
	{

        if (isset($filter['name']) and $filter['name']) {
            $this->db->like("(display_name", trim($filter['name']));
            $this->db->or_where("store.store_name LIKE '%" . $filter['name'] . "%' ESCAPE '!' )");
		}
		
		if (isset($filter['state_id']) and $filter['state_id'] != '') {
            $this->db->where("users.state_id", $filter['state_id']);
        }
		
        if ($limit) {
            $this->db->limit($limit, $offset);
        }
        $this->db->where("users.role_id", '2');
        $this->db->or_where("users.role_id", '4');
		$this->db->where_in("users.status", array('9','10'));
		$this->db->where_not_in("id", $this->session->userdata('user_id'));
        $this->db->where("deleted", '0');
        $res = $this->db->select('users.*, roles.role_name as role, states.name as state,store.store_name,store.licence')
        ->join('roles', 'roles.role_id = users.role_id', 'left')
        ->join('states', 'states.id = users.state_id', 'left')
		->join('store', 'store.retailer_id = users.id', 'left')
        ->order_by('ti_users.id', 'DESC')->get('users'); 
        return ($count) ? $res->num_rows() : $res->result();
    }
	
	public function retailerStoreDetails($filter = null, $limit = null, $offset = null, $count = true)
	{
		if (isset($filter['name']) and $filter['name']) {
            $this->db->or_where("store.store_name LIKE '%" . $filter['name'] . "%' ESCAPE '!' ");
		}
		
		if (isset($filter['type']) and $filter['type'] == '2') {
            $this->db->where_in("users.status", array('9','10'));
        }else if(isset($filter['type']) and $filter['type'] == '1')
		{
			$this->db->where_not_in("users.status", array('9','10'));
		}
		
		if (isset($filter['status']) and $filter['status']) {
            $this->db->where("store.status",$filter['status']);
		}
		
        if ($limit) {
            $this->db->limit($limit, $offset);
        }
        $this->db->where("users.role_id", '2');
        $this->db->or_where("users.role_id", '4');
		$this->db->where_not_in("id", $this->session->userdata('user_id'));
        //$this->db->where("deleted", '0');
        $res = $this->db->select('store.*, roles.role_name as role, users.display_name,users.status as user_status,users.role_id')
		->join('users', 'users.id = store.retailer_id', 'left')
        ->join('roles', 'roles.role_id = users.role_id', 'left')
        ->order_by('store.id', 'DESC')->get('store'); 
        //echo $this->db->last_query();
		return ($count) ? $res->num_rows() : $res->result();
		
	}
	
	public function updateStoreStatus($data,$storeId)
	{
		$return = $this->db->where('id', $storeId)->update('store', $data);
        return $return;
	}
	
}
