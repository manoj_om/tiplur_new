<?php

/**
 *  * this service class is responsible of all the application logic
 * related to API
 */
class Api_service extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->model('api/Api_dao');
        include_once './application/objects/Response.php';
    }

    //states
    public function State_list() {
        $response = new response ();
        try {

        		$result = $this->db->select('id,name,age,opening_time,closing_time,is_available')->where('status', '1')->order_by('name', 'ASC')->get('states')->result();

        		if ($result) {
        			$response->setStatus(1);
        			$response->setMsg('Success');
        			$response->setObjArray($result);
        		} else {
        			$response->setStatus(0);
        			$response->setMsg("Not Found");
        			$response->setObjArray('');
        		}

        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    //registration
    public function registration_process($mapped=false) {
        $Returndata = array();
        $response = new Response ();
        try {
            $post = $this->input->post();
            $apiDao = new Api_dao ();

			if($mapped == true):
                $isUserExist = $apiDao->isMappedRetailerExist($post,$mapped);
				$compare_status = '9';
			else:
                $isUserExist = $apiDao->isUserExist($post);
              if($post['is_retailer']):
				        $compare_status = '3';
              else:
                $compare_status = '1';
              endif;
			endif;

            if ($isUserExist->getStatus() == 1) {
                $user = $isUserExist->getObjArray();
                if ($user->status == $compare_status) {
                    $Returndata ['id'] = $user->id;
                    $Returndata ['role_id'] = $user->role_id;
					          $Returndata ['status'] = $user->status;
                    $Returndata ['email'] = $user->email;
                    $Returndata ['mobile'] = $user->mobile;
                    $Returndata ['username'] = $user->display_name;
                    $Returndata ['address'] = $user->address;
                    $Returndata ['dob'] = $user->dob ? $user->dob:'';
					$Returndata ['doc_file'] = ($user->doc_file != '') ? base_url() . 'assets/uploads/document_file/' . $user->doc_file : '';
                    $Returndata ['image'] = ($user->image != '') ? base_url() . 'assets/images/profile_image/' . $user->image : '';
                    $Returndata ['store'] = $user->store;

                    $updateDeviceData = $apiDao->updateDeviceData($user->id, $post['device_type'], $post['device_id'], $post['fcm_reg_id']);

                    if ($updateDeviceData) {
                        $response->setStatus(1);
                        $response->setMsg("Success");
                        $response->setObjArray($Returndata);
                    } else {
                        $response->setStatus(0);
                        $response->setMsg("Something went wrong");
                        $response->setObjArray('');
                    }
                } else {
                    $response->setStatus(0);
                    $response->setMsg("Inactive user");
                    $response->setObjArray('');
                }
            } else {
                $response->setStatus(0);
                $response->setMsg($isUserExist->getMsg());
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }

        return $response;
    }

    //Login api
    public function login() {
        $ReturnData = array();
        $postData = array();
        $response = new Response ();
        try {
            $post = $this->input->post();
            $apiDao = new Api_dao ();
            $checkUser = $this->validateUser($post['username'], $post['password'], $post['login_type']);
            if ($checkUser['status'] == '1') {
                $isUserExist = $checkUser['data'];
              //  prd($isUserExist);
                if ($isUserExist->status == '1' || $isUserExist->status == '8') {
                    //$randcode = substr(rand(1, 99999), 1, 5);
                    $is_default = 1;
                    $ReturnData ['id'] = $isUserExist->id;
                    $ReturnData ['role_id'] = $isUserExist->role_id;
                    $ReturnData['role'] = get_rolename($isUserExist->role_id)->role_name;
                    $ReturnData['sub_cat']=retailer_subcat($isUserExist->id)->sub_cat;
                    $ReturnData ['email'] = $isUserExist->email;
                    $ReturnData ['mobile'] = $isUserExist->mobile;
                    $ReturnData ['username'] = $isUserExist->display_name;
                    $ReturnData ['dob'] = $isUserExist->dob;
                    $terms = $this->is_terms_accepted($ReturnData ['id'], $isUserExist->state_id);
                    $ReturnData ['is_terms_accepted'] = $terms != NULL ? $terms[0]->is_accepted : '0';
                    $ReturnData ['doc_file'] = ($isUserExist->doc_file != '') ? base_url() . 'assets/uploads/document_file/' . $isUserExist->doc_file : '';
                    $ReturnData ['image'] = ($isUserExist->image != '') ? base_url() . 'assets/images/profile_image/' . $isUserExist->image : '';
                    $get_default_address = $apiDao->get_address($ReturnData ['id'], $is_default);
                    if ($get_default_address) {
                        $ReturnData['address'] = $get_default_address[0];
                        $ReturnData['address']->state_id = $isUserExist->state_id;
                    } else {
                        $ReturnData['address'] = NULL;
                    }

                    if (!$post['is_retailer']) { //if customer
                        $cart['user_id'] = $ReturnData ['id'];

                        $getCart = $apiDao->get_cart($cart);

                        $cartCount = 0;
                        $retailer_id = NULL;
                        if (sizeof($getCart) > 0) {
                            $liquorCount = sizeof($getCart['liquor']);
                            $snacksCount = sizeof($getCart['snacks']);

                            $carts['liquor'] = $getCart['liquor'];
                            $carts['snacks'] = $getCart['snacks'];

                            foreach ($carts as $val) {

                                $productCount = sizeof($val);
                                $cartCount = intval($cartCount + $productCount);

                                if ($productCount > 0) {
                                    $retailer_id = $val[0]['retailer_id'];
                                }
                            }
                            /* echo "Retailer Id " .  $retailer_id;
                              echo "<br>"; */
                            $retailer = user_data($retailer_id);
                            //print_r($retailer);die;
                            $store['user_id'] = $retailer_id;
                            $getStore = $apiDao->get_store($store);
                            //$cartData['total_item'] = $cartCount;
                            $cartData['total_item'] = array('liquor' => $liquorCount, 'snacks' => $snacksCount);
                            $cartData['retailer'] = array("id" => $retailer->id, "name" => $retailer->display_name, "store" => $getStore->store_name);
                        }
                        $ReturnData ['cart'] = $cartCount != 0 ? $cartData : "";
                    } else { //if retailer
                        $store['user_id'] = $ReturnData ['id'];

                        $getStore = $apiDao->get_store($store);
                        $ReturnData ['store'] = sizeof($getStore) > 0 ? $getStore : "";
                        $data = array();
                        $data['retailer_id'] = $ReturnData ['id'];
                        $data['order_id'] = '0';
                        $ReturnData ['order_count'] = sizeof($apiDao->requested_orders($data));
                    }




                    $updateDeviceData = $apiDao->updateDeviceData($isUserExist->id, $post['device_type'], $post['device_id'], $post['fcm_reg_id']);
                    if ($updateDeviceData) {
                        $response->setStatus(1);
                        $response->setMsg("Valid user");
                        $response->setObjArray($ReturnData);
                    } else {
                        $response->setStatus(0);
                        $response->setMsg("Database error");
                        $response->setObjArray('');
                    }
                } else if ($isUserExist->status == '3') {
                  $response->setStatus(0);
                  $response->setMsg("Pending for approval from admin");
                  $response->setObjArray('');
                }
                 else {
                    $response->setStatus(0);
                    $response->setMsg("Inactive user");
                    $response->setObjArray('');
                }
            } else if ($checkUser['status'] == '2') {
                $response->setStatus($checkUser['status']);
                $response->setMsg($checkUser['msg']);
                $response->setObjArray('');
            } else {
                $response->setStatus(0);
                $response->setMsg("username and password combination is wrong.");
                $response->setObjArray('');
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }

        return $response;
    }

    //check user exist
    public function isUserExist($unique_id, $type) {
        $response = new Response ();
        try {
            $apiDao = new Api_dao ();
            $response = $apiDao->isUserExist($unique_id, $type);
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    //permit list
    public function is_available_permit_in_state() {
        $response = new response ();
        try {
          $post = $this->input->post();
          $state_id = $post['state_id'];
           $res = $this->db->select('id,name,age,opening_time,closing_time,is_available,is_permit,shipping_charge,internet_charge')->where(array('id'=>$state_id))->get('states')->row();

             $return['id']=$res->id;
             $return['name']=$res->name;
             $return['age']=$res->age;
             $return['opening_time']=$res->opening_time;
             $return['closing_time']=$res->closing_time;
             $return['is_available']=$res->is_available;
             $return['is_permit']=$res->is_permit;
             $return['shipping_charge']=($res->shipping_charge)?$res->shipping_charge:'0';
             $return['internet_charge']=($res->internet_charge)?$res->internet_charge:'0';


           if ($return) {
             $response->setStatus(1);
             $response->setMsg('Success');
             $response->setObjArray($return);
           } else {
             $response->setStatus(0);
             $response->setMsg("Not Found");
             $response->setObjArray('');
           }

        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

// shipping Charge

    public function shipping_charge_in_state() {
        $response = new response ();
        try {
            $data = $this->input->post();
            $apiDao = new Api_dao ();

            $res = $apiDao->shipping_charge_in_state($data);
            if ($res) {
                $response->setStatus(1);
                $response->setMsg('success');
                $response->setObjArray($res);
            } else {
                $response->setStatus(0);
                $response->setMsg("Not Found");
                $response->setObjArray('');
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    public function shipping_charge_in_state_extra() {
        $response = new response ();
        try {
            $data = $this->input->post();
            $apiDao = new Api_dao ();

            $res = $apiDao->shipping_charge_in_state_extra($data);
          //  prd($res);
            if ($res) {

                $response->setStatus(1);
                $response->setMsg('success');
                $response->setObjArray($res);
            } else {

                $response->setStatus(0);
                $response->setMsg("Not Found");
                $response->setObjArray('');
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }


    //face OCR

    public function face_ocr() {
        $response = new response ();
        try {
            $data = $this->input->post();
            $apiDao = new Api_dao ();
            $res_truth = $apiDao->face_ocr($data);
            $res=json_decode($res_truth);
            //prd($res->status);
            if ($res->status==1) {
                 $res->msg->user_id=$data['user'];
                $response->setStatus(1);
                $response->setMsg('success');
                $response->setObjArray($res->msg);
            } else {
            prd($res);
                $response->setStatus(0);
                $response->setMsg("Not Found");
                $response->setObjArray($res->msg);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }


    //face OCR

    public function idSearch() {
        $response = new response ();
        try {
            $data = $this->input->post();
            $apiDao = new Api_dao ();
            $res_truth = $apiDao->idSearch($data);
            $res=json_decode($res_truth);
            //prd($res);
            if ($res->status==1) {
                $response->setStatus(1);
                $response->setMsg('success');
                $response->setObjArray($res);
            }
            else {

                $response->setStatus(0);
                $response->setMsg("Not Found");
                $response->setObjArray('');
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    //Validate user
    public function validateUser($username, $password, $login_type) {

        $response = array();

        $this->db->where('mobile', $username);
        $this->db->or_where('email', $username);
        $is_user_exist = $this->db->get('users');
        if ($is_user_exist->num_rows() > 0) {
            //$this->db->where('mobile', $username);
            //$this->db->or_where('email', $username);
            $this->db->where('(mobile = "'.$username.'" OR  email = "'.$username.'")');
            if ($login_type == LOGIN_BY_PASSWORD) {
                $this->db->where('password', md5($password));
            }

            $result = $this->db->get('users');
            if ($result->num_rows() > 0) {
                $user_id = $result->row()->id;
                $query = user_data($user_id);
                $response['status'] = '1';
                $response['msg'] = 'User exist.';
                $response['data'] = $query;
            } else {
                $response['status'] = '0';
                $response['msg'] = 'Invalid credentials.';
                $response['data'] = '';
            }
        } else {
            $response['status'] = '2';
            $response['msg'] = 'You are not a registered user.';
            $response['data'] = '';
        }
        return $response;
    }

    //update users detail
    public function update_user_profile() {
        $response = new response ();
        try {

            $data = $this->input->post();

            $apiDao = new Api_dao ();
            //Add This LOC 24-FEB-2018
            $email_exist = $this->db->where(array('email'=>$data['email'],'id !='=>$data['id']))->get('users');
            if ($email_exist->num_rows() > 0)
			{
                $response->setStatus(0);
                $response->setMsg("Email-id already exists");
                $response->setObjArray('');
			} else {
               $response = $apiDao->update_profile($data);
            }

        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }
     

    //update product detail
        public function update_product() {
            $response = new response ();
            try {

                $data = $this->input->post();

                $apiDao = new Api_dao ();

                   $response = $apiDao->update_product($data);


            } catch (Exception $e) {
                $response->setStatus(- 1);
                $response->setMsg($e->getMessage());
                $response->setError($e->getMessage());
                log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
            }
            return $response;
        }

    //update order permit
    public function order_permit() {
        $response = new response ();
        try {

            $data = $this->input->post();

            $apiDao = new Api_dao ();
            //Add This LOC 24-FEB-2018
            $email_exist = $this->db->where(array('email'=>$data['email'],'id !='=>$data['id']))->get('users');
            if ($email_exist->num_rows() > 0)
      {
                $response->setStatus(0);
                $response->setMsg("Email-id already exists");
                $response->setObjArray('');
      } else {
               $response = $apiDao->order_permit($data);
            }

        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }



    public function search_retailers() {
        $response = new response ();
        try {
            $data = $this->input->post();
            $apiDao = new Api_dao ();

			//ADD THIS LOC ON APRIL-26-2018 (in case of different location, remove previous cart data of user)
			if($data['user_id'] != '0')
			{
				$this->removePreviousCart($data);
			} unset($data['current_city_id']);
			// if($data['state_id'] != '13')
      //       {
			$is_available = $apiDao->is_available_in_state($data['state_id']);
            if ($is_available) {
                $ids = NULL;
                if ($data['user_id'] != '0') {
                    $get_blocked_retailers = $apiDao->is_order_declined($data['user_id']);
                    if ($get_blocked_retailers) {
                        $retailers = array();
                        foreach ($get_blocked_retailers as $val) {

                            array_push($retailers, $val->retailer_id);
                        }

                        $ids = join("','", $retailers);
                    }
                }
                //print_r($ids);

				//Add this LOC on March 12 - 2018 (update fcm_reg_id)
				if(!empty($data['fcm_reg_id']))
				{
				   $user_device_info = $this->db->where('user_id',$data['user_id'])->get('device_info')->row();
				   if($user_device_info)
				   {   $device['fcmId'] = $data['fcm_reg_id'];
			           $this->db->where('id', $user_device_info->id);
					   $this->db->update('device_info',$device);
				   }
				}
				//end here

                //print_r($ids);die;
                $res = $apiDao->find_reatilers($data, $ids);
                if ($res) {
                    $response->setStatus(1);
                    $response->setMsg('success');
                    $response->setObjArray($res);
                } else {
                    $response->setStatus(0);
                    $response->setMsg("Not Found");
                    $response->setObjArray(array());
                }
            } else {
                $response->setStatus(0);
                $response->setMsg("Thanks for being on tiplur, as of now our services are not available here.");
                $response->setObjArray('');
            }
        // }else
        // {
        //     $res['is_maintenance'] = '1';
        //     $response->setStatus(25);
        //     $response->setMsg("Thanks for being on tiplur, as of now our services are not available here.");
        //     $response->setObjArray($res);
        // }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    public function all_categories() {
        $response = new response ();
        try {
            $apiDao = new Api_dao ();
            $res = $apiDao->get_all_categories();
            if ($res) {
                $response->setStatus(1);
                $response->setMsg('success');
                $response->setObjArray($res);
            } else {
                $response->setStatus(0);
                $response->setMsg("Not Found");
                $response->setObjArray('');
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    public function subcategories_list() {
        $response = new response ();
        try {
            //$category_id = $this->input->post('category_id');
            //$this->load->model('Category/category_model');
            //$res = $this->category_model->subcategory_listing($category_id);
            $data = $this->input->post();
            $apiDao = new Api_dao ();

            $res = $apiDao->subcategory_listing($data);
            if ($res) {
                $response->setStatus(1);
                $response->setMsg('success');
                $response->setObjArray($res);
            } else {
                $response->setStatus(0);
                $response->setMsg("Not Found");
                $response->setObjArray('');
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    public function pubsubcategories_list() {
        $response = new response ();
        try {
            //$category_id = $this->input->post('category_id');
            //$this->load->model('Category/category_model');
            //$res = $this->category_model->subcategory_listing($category_id);
            $data = $this->input->post();
            $apiDao = new Api_dao ();

            $res = $apiDao->pubsubcategories_list($data);
            if ($res) {
                $response->setStatus(1);
                $response->setMsg('success');
                $response->setObjArray($res);
            } else {
                $response->setStatus(0);
                $response->setMsg("Not Found");
                $response->setObjArray('');
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    } 
    /* validate mobile number */

    public function validate_mobile() {
        $response = new response ();
        try {
            //When user trying to login
            if ($this->input->post('is_login') == 1) {
                $mobile = $this->input->post('mobile');
                $randcode = rand(10000, 99999);
                $message = "Dear Customer, Your OTP is " . $randcode . " Have a pleasant day!";
                 $postData['otp'] = $randcode;
                sms_notification($message,$mobile);
                /** Send otp

                $sender_id = 'TIPLUR';
                $user_id = urlencode('rakshat.chopra@gmail.com');
                $pwd = urlencode('RC@123tm');
                $message = urlencode("Dear Customer, Your OTP is " . $randcode . " Have a pleasant day!");

                $URL = "http://49.50.77.216/API/SMSHttp.aspx?UserId=" . $user_id . "&pwd=" . $pwd . "&Message=" . $message . "&Contacts=" . $mobile . "&SenderId=" . $sender_id . "";
                $ch = curl_init($URL);
                curl_setopt($ch, CURLOPT_URL, $URL);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $result = curl_exec($ch);
                curl_close($ch);*/

                $response->setStatus(1);
                $response->setMsg("Valid Mobile Number");
              // $response->setObjArray('');
               $response->setObjArray($postData);
            } else {
                $mobile = $this->input->post('mobile');
                $randcode = rand(10000, 99999);
                $postData['otp'] = $randcode;
                $message = "Dear Customer, Your OTP is " . $randcode . " Have a pleasant day!";
                sms_notification($message,$mobile);
                /** Send otp

                $sender_id = 'TIPLUR';
                $user_id = urlencode('rakshat.chopra@gmail.com');
                $pwd = urlencode('RC@123tm');
                $message = urlencode("Dear Customer, Your OTP is " . $randcode . " Have a pleasant day!");

                $URL = "http://49.50.77.216/API/SMSHttp.aspx?UserId=" . $user_id . "&pwd=" . $pwd . "&Message=" . $message . "&Contacts=" . $mobile . "&SenderId=" . $sender_id . "";
                $ch = curl_init($URL);
                curl_setopt($ch, CURLOPT_URL, $URL);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $result = curl_exec($ch);
                curl_close($ch);*/

                $response->setStatus(1);
                $response->setMsg("Valid Mobile Number");
                 $response->setObjArray($postData);
              //  $response->setObjArray('');
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    public function products_listing() {
        $response = new response ();
        try {
            $apiDao = new Api_dao();
            $result = $apiDao->get_product_listing();
            if ($result) {
                $response->setStatus(1);
                $response->setMsg("success");
                $response->setObjArray($result);
            } else {

                $response->setStatus(0);
                $response->setMsg("Not Found");
                $response->setObjArray('');
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    public function products_pub_listing() {
        $response = new response ();
        try {
            $apiDao = new Api_dao();
            $result = $apiDao->get_pub_product_listing();
            if ($result) {
                $response->setStatus(1);
                $response->setMsg("success");
                $response->setObjArray($result);
            } else {

                $response->setStatus(0);
                $response->setMsg("Not Found");
                $response->setObjArray('');
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }


    public function app_info() {
        $response = new response ();
        try {
            $res = $this->db->select('*')->get('app_info')->row();
            if ($res) {
                $data['version'] = $res->version;
                $data['contact_no'] = $res->contact_no;
                $data['contact_email'] = $res->contact_email;
                $data['privacy'] = '';
                $data['faq'] = '';
                $data['about_us'] = '';

                $response->setStatus(1);
                $response->setMsg('success');
                $response->setObjArray($data);
            } else {
                $response->setStatus(0);
                $response->setMsg("Not Found");
                $response->setObjArray('');
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /*
     * @author: Anjani Gupta
     * @date: 24/10/2017
     * @method: update_cart
     * @desc: add/delete cart products
     * @params: action, product_id, retailer_id, quantity, price
     */

    public function update_cart() {
        $response = new response ();

        try {
            $cart = $this->input->post();
            $apiDao = new Api_dao();
            $result = $apiDao->update_cart($cart);

            if ($result['status']) {
                $response->setStatus(1);
                $response->setMsg("Cart updated successfully.");
                $response->setObjArray(array("count" => $result['count']));
            } else {

                $response->setStatus(0);
                $response->setMsg($result['msg']);
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /*
     * @author: Anjani Gupta
     * @date: 24/10/2017
     * @method: get_cart
     * @desc: get cart detail
     * @params: user_id
     */

    public function get_cart() {
        $response = new response ();

        try {
            $cart = $this->input->post();
            $apiDao = new Api_dao();
            $result = $apiDao->get_cart($cart);

            if ($result) {
                $response->setStatus(1);
                $response->setMsg("Cart fetched successfully.");
                $response->setObjArray($result);
            } else {
                $response->setStatus(0);
                $response->setMsg("Cart is empty.");
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /*
     * @author: Anjani Gupta
     * @date: 24/10/2017
     * @method: add_address
     * @desc: add new address of user
     * @params: user_id, address, state, city, pincode
     */

    public function add_address() {
        $response = new response ();

        try {
            $post = $this->input->post();
            $address = array('user_id' => $post['user_id'], 'city_id' => $post['city_id'], 'state_id' => $post['state_id'], 'latitude' => $post['latitude'], 'longitude' => $post['longitude'], 'name' => $post['name'], 'mobile' => $post['mobile'], 'address' => $post['address'], 'city' => $post['city'], 'state' => $post['state'], 'pincode' => $post['pincode'], 'is_default' => $post['is_default'],'address2'=>($post['address2']) ? $post['address2']:'');
            $apiDao = new Api_dao();

            $is_available = $apiDao->is_available_in_state($post['state_id']);
            if ($is_available) {
                $result = $apiDao->add_address($address);

                if ($result) {
                    $response->setStatus(1);
                    $response->setMsg("Address added successfully.");
                    $address['id'] = $result;
                    $address['state_id'] = $post['state_id'];
                    $address['is_default'] = $post['is_default'];
                    $response->setObjArray($address);
                } else {
                    $response->setStatus(0);
                    $response->setMsg("Error in adding address.");
                    $response->setObjArray(NULL);
                }
            } else {
                $response->setStatus(0);
                $response->setMsg("Thanks for being on tiplur, as of now our services are not available here.");
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /*
     * @author: Anjani Gupta
     * @date: 25/10/2017
     * @method: address
     * @desc: get list of user address
     * @params: user_id
     */

    public function get_address() {
        $response = new response ();

        try {
            $post = $this->input->post();
            $user_id = $post['user_id'];
            $apiDao = new Api_dao();
            $result = $apiDao->get_address($user_id);

            if ($result) {
                $response->setStatus(1);
                $response->setMsg("Address fetched successfully.");
                $response->setObjArray($result);
            } else {
                $response->setStatus(0);
                $response->setMsg("No address found.");
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /*
     * @author: Anjani Gupta
     * @date: 25/10/2017
     * @method: terms
     * @desc: get terms & conditions by state id
     * @params: state_id
     */

    public function get_terms() {
        $response = new response ();

        try {
            $post = $this->input->post();
            $state_id = $post['state_id'];
            $is_retailer = $post['is_retailer'];
            $apiDao = new Api_dao();
            $result = $apiDao->get_terms($state_id, $is_retailer);

            if ($result) {
                $response->setStatus(1);
                $response->setMsg("Terms & Conditions fetched successfully.");
                $response->setObjArray($result);
            } else {
                $response->setStatus(0);
                $response->setMsg("No content found.");
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /*
     * @author: Anjani Gupta
     * @date: 25/10/2017
     * @method: empty_cart
     * @desc: empty cart data
     * @params: user_id, retailer_id
     */

    public function empty_cart() {
        $response = new response ();

        try {
            $post = $this->input->post();
            $apiDao = new Api_dao();
            $result = $apiDao->empty_cart($post);

            if ($result) {
                $response->setStatus(1);
                $response->setMsg("Cart empty.");
                $response->setObjArray($result);
            } else {
                $response->setStatus(0);
                $response->setMsg("Error while deleting.");
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /*
     * @author: Anjani Gupta
     * @date: 25/10/2017
     * @method: request_order
     * @desc: order request by user
     * @params: user_id, retailer_id, address_id, array of product object (product_id, quantity, price)
     */

    public function request_order(){
    	$response = new response ();

    	try
    	{
    		$post = $this->input->post();
    		$apiDao = new Api_dao();
		$is_valid_store = store_detail_by_retailer_id($post['retailer_id']);
		if($is_valid_store->status == '1' && $is_valid_store->retailer_status == '1')
		{
		  $is_valid_valid = $apiDao->get_user_data($post['user_id']);
		    if($is_valid_valid->status==DOCUMENT_REJECTED){
					$response->setStatus ( 2 );
					$response->setMsg ( "Sorry! Your document is not approved. Please upload a valid document." );
					$response->setObjArray ( NULL);
			}else if($is_valid_valid->status=='0' || $is_valid_valid->status=='2'){
					$response->setStatus ( 0 );
					$response->setMsg ( "Sorry! You are not an active user. Please contact to admin." );
					$response->setObjArray ( NULL);
			}else if($is_valid_valid->status=='1'){
					$result = $apiDao->complete_order($post);

					if($result['status']=='1')
					{

						//is retailer logged out?
						//$is_retailer_logged_out = $this->is_retailer_logged_out($post['retailer_id'], $result['msg']);
						$response->setStatus ( 1 );
						$response->setMsg ( "Order placed successfully." );
						$response->setObjArray ( array("order_id"=>$result['msg']) );

					}else if($result=='-1'){
						$response->setStatus ( 0 );
						$response->setMsg ( $result['msg'] );
						$response->setObjArray ( NULL);
					}else{
						$response->setStatus ( 0 );
						$response->setMsg ( $result['msg']);
						$response->setObjArray ( NULL);
					}
    		          }else{
				$response->setStatus ( 0 );
				$response->setMsg ("Sorry! You are not an active user. Please contact to admin.");
				$response->setObjArray ( NULL);
			   }
		}
		else{
			$response->setStatus ( 0 );
			$response->setMsg ("Sorry for the inconvenience,Please try your other nearby retailers.");
			$response->setObjArray ( NULL);
		}
    	} catch ( Exception $e ) {
    		$response->setStatus ( - 1 );
    		$response->setMsg ( $e->getMessage () );
    		$response->setError ( $e->getMessage () );
    		log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
    	}
    	return $response;

    }

    /*
     * @author: Anjani Gupta
     * @date: 25/10/2017
     * @method: request_order
     * @desc: order request by user
     * @params: order_id(optional), retailer_id, user_id
     */

    public function order_history() {
        $response = new response ();

        try {
            $post = $this->input->post();
            $apiDao = new Api_dao();
            $result = $apiDao->order_history($post);

            if ($result) {
                $response->setStatus(1);
                $response->setMsg("Order history fetched successfully.");
                $response->setObjArray($result);
            } else {
                $response->setStatus(0);
                $response->setMsg("No Result Found.");
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /*
     * @author: Anjani Gupta
     * @date: 26/10/2017
     * @method: update_rating
     * @desc: mark rating
     * @params: order_id, rating
     */

    public function update_rating() {
        $response = new response ();

        try {
            $post = $this->input->post();
            $apiDao = new Api_dao();
            $result = $apiDao->update_rating($post);

            if ($result) {
                $response->setStatus(1);
                $response->setMsg("Success.");
                $response->setObjArray(NULL);
            } else {
                $response->setStatus(0);
                $response->setMsg("Failed.");
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /*
     * @author: Anjani Gupta
     * @date: 27/10/2017
     * @method: get_ratings
     * @desc: fetch all product ratings
     * @params: order_id(optional), user_id
     */

    public function get_ratings() {
        $response = new response ();

        try {
            $post = $this->input->post();
            $apiDao = new Api_dao();
            $result = $apiDao->get_ratings($post);

            if ($result) {
                $response->setStatus(1);
                $response->setMsg("Success.");
                $response->setObjArray($result);
            } else {
                $response->setStatus(0);
                $response->setMsg("Failed.");
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /*
     * @author: Anjani Gupta
     * @date: 28/10/2017
     * @method: update_price
     * @desc: Update product price
     * @params: product_id, user_id, price
     */

    public function update_price() {
        $response = new response ();

        try {
            $post = $this->input->post();
            $apiDao = new Api_dao();
            $result = $apiDao->update_price($post);

            if ($result) {
                $response->setStatus(1);
                $response->setMsg("Price updated.");
                $response->setObjArray(NULL);
            } else {
                $response->setStatus(0);
                $response->setMsg("Failed.");
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /*
     * @author: Anjani Gupta
     * @date: 28/10/2017
     * @method: remove_product
     * @desc: Remove product
     * @params: product_id, user_id
     */

    public function remove_product() {
        $response = new response ();

        try {
            $post = $this->input->post();
            $apiDao = new Api_dao();
            $result = $apiDao->remove_product($post);

            if ($result) {
                $response->setStatus(1);
                $response->setMsg("Product deleted.");
                $response->setObjArray(NULL);
            } else {
                $response->setStatus(0);
                $response->setMsg("Failed.");
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /*
     * @author: Anjani Gupta
     * @date: 28/10/2017
     * @method: in_out_stock_product
     * @desc: Update stock of product
     * @params: product_id, user_id
     */

    public function in_out_stock_product() {
        $response = new response ();

        try {
            $post = $this->input->post();
            $apiDao = new Api_dao();
            $result = $apiDao->in_out_stock_product($post);

            if ($result) {
                $response->setStatus(1);
                $response->setMsg("Product Stock Updated.");
                $response->setObjArray(NULL);
            } else {
                $response->setStatus(0);
                $response->setMsg("Failed.");
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }




    /*
     * @author: Anjani Gupta
     * @date: 28/10/2017
     * @method: requested_orders
     * @desc: list of requested orders
     * @params: retailer_id
     */

    public function requested_orders() {
        $response = new response ();

        try {
            $post = $this->input->post();
            $apiDao = new Api_dao();
            $result = $apiDao->requested_orders($post);

            if (sizeof($result) > 0) {
                $response->setStatus(1);
                $response->setMsg("Success.");
                $response->setObjArray($result);
            } else {
                $response->setStatus(0);
                $response->setMsg("Failed.");
                $response->setObjArray($result);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /*
     * @author: Anjani Gupta
     * @date: 31/10/2017
     * @method: respond_order_request
     * @desc: confirm/decline order request
     * @params: order_id
     */

    public function respond_order_request() {
        $response = new response ();

        try {
            $post = $this->input->post();

            $apiDao = new Api_dao();
            if ($post['status'] == ORDER_ACCEPTED) {
                $result = $apiDao->respond_order_request($post);
                if ($result) {
                    $response->setStatus(1);
                    $response->setMsg("Success.");
                    $response->setObjArray("Order is accepted successfully.");
                } else {
                    $response->setStatus(0);
                    $response->setMsg("Failed.");
                    $response->setObjArray(NULL);
                }
            } elseif ($post['status'] == ORDER_DECLINED) {
                $result = $apiDao->respond_order_request($post);
                if ($result) {
                    $response->setStatus(1);
                    $response->setMsg("Success.");
                    $response->setObjArray("Order is declined successfully.");
                } else {
                    $response->setStatus(0);
                    $response->setMsg("Failed.");
                    $response->setObjArray(NULL);
                }
            } elseif ($post['status'] == ORDER_ONGOING) {
                $result = $apiDao->respond_order_request($post);
                if ($result) {
                    $response->setStatus(1);
                    $response->setMsg("Success.");
                    $response->setObjArray("Order is out for delivery.");
                } else {
                    $response->setStatus(0);
                    $response->setMsg("Failed.");
                    $response->setObjArray(NULL);
                }
            } elseif ($post['status'] == ORDER_DELIVERED) {

                $result = $apiDao->respond_order_request($post);
                if ($result) {
                    $response->setStatus(1);
                    $response->setMsg("Success.");
                    $response->setObjArray("Order is delivered successfully.");
                } else {
                    $response->setStatus(0);
                    $response->setMsg("Failed.");
                    $response->setObjArray(NULL);
                }
            } elseif ($post['status'] == ORDER_FAILED) {
                $result = $apiDao->respond_order_request($post);
                if ($result) {
                    $response->setStatus(1);
                    $response->setMsg("Success.");
                    $response->setObjArray("Sorry! This order is failed to deliver.");
                } else {
                    $response->setStatus(0);
                    $response->setMsg("Failed.");
                    $response->setObjArray(NULL);
                }
            } else {
                $response->setStatus(0);
                $response->setMsg("Sorry! This request is can not be proceed.");
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /*
     * @author: Anjani Gupta
     * @date: 02/11/2017
     * @method: get_retailer_orders
     * @desc: Get retailer orders
     * @params: retailer_id
     */

    public function get_retailer_orders() {
        $response = new response ();

        try {
            $post = $this->input->post();
            $apiDao = new Api_dao();
            $result = $apiDao->get_retailer_orders($post['retailer_id']);

            if (sizeof($result) > 0) {
                $response->setStatus(1);
                $response->setMsg("Success.");
                $response->setObjArray($result);
            } else {
                $response->setStatus(0);
                $response->setMsg("No data found.");
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /*
     * @author: Anjani Gupta
     * @date: 02/11/2017
     * @method: payment_details_submit
     * @desc: submit the data after completeting payment through payment gateway(payU)
     * @params:
     */

    public function payment_details_submit() {
        $response = new response ();

        try {
            $post = $this->input->post();
            $apiDao = new Api_dao();
            $result = $apiDao->payment_details_submit();

            if (sizeof($result) > 0) {
                $response->setStatus(1);
                $response->setMsg("Success.");
                $response->setObjArray($result);
            } else {
                $response->setStatus(0);
                $response->setMsg("No data found.");
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }
    /*
     * @author: Anjani Gupta
     * @date: 02/11/2017
     * @method: retailer_order_detail
     * @desc: Get retailer order detail
     * @params: retailer_id, order_id
     */

    public function retailer_order_detail() {
        $response = new response ();

        try {
            $post = $this->input->post();
            $apiDao = new Api_dao();
            $result = $apiDao->retailer_order_detail($post);

            if (sizeof($result) > 0) {
                $response->setStatus(1);
                $response->setMsg("Success.");
                $response->setObjArray($result);
            } else {
                $response->setStatus(0);
                $response->setMsg("No data found.");
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /*
     * @author: Anjani Gupta
     * @date: 02/11/2017
     * @method: update_address
     * @desc: Update user address
     * @params: address_id
     */

    public function update_address() {
        $response = new response ();

        try {
            $post = $this->input->post();

            $apiDao = new Api_dao();
            $result = $apiDao->update_address($post);

            if (sizeof($result) > 0) {
                $response->setStatus(1);
                $response->setMsg("Success.");
                $response->setObjArray($result);
            } else {
                $response->setStatus(0);
                $response->setMsg("Failed.");
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /*
     * @author: Anjani Gupta
     * @date: 06/11/2017
     * @method: delete_address
     * @desc: Delete user address
     * @params: id
     */

    public function delete_address() {
        $response = new response ();

        try {
            $post = $this->input->post();

            $apiDao = new Api_dao();
            $result = $apiDao->delete_address($post);

            if ($result) {
                $response->setStatus(1);
                $response->setMsg("Address deleted.");
                $response->setObjArray($result);
            } else {
                $response->setStatus(0);
                $response->setMsg("Failed.");
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /*
     * @method: update_terms
     * @date: 09-11-2017
     * @params: state_id, is_accepted, user_id
     */

    public function update_terms() {
        $response = new response ();

        try {
            $post = $this->input->post();

            $apiDao = new Api_dao();
            $result = $apiDao->update_terms($post);

            if ($result) {
                $response->setStatus(1);
                $response->setMsg("Terms & conditions accepted successfully.");
                $response->setObjArray($result);
            } else {
                $response->setStatus(0);
                $response->setMsg("Failed to accept terms & conditions.");
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    private function is_terms_accepted($user_id, $state_id) {
        $query = $this->db->where(array('user_id' => $user_id, 'state_id' => $state_id))->get('is_terms_accepted')->result();
        return $query ? $query : NULL;
    }

    /*
     * @method: holidays
     * @date: 14-11-2017
     * @params: state_id
     */

    public function get_holidays() {
        $response = new response ();

        try {
            $post = $this->input->post();

            $apiDao = new Api_dao();
            $result = $apiDao->get_holidays($post);

            if ($result) {
                $response->setStatus(1);
                $response->setMsg("Success.");
                $response->setObjArray($result);
            } else {
                $response->setStatus(0);
                $response->setMsg("Failed.");
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    public function retailer_rating() {
        $response = new response ();

        try {
            $post = $this->input->post();

            $apiDao = new Api_dao();
            $result = $apiDao->retailer_rating($post);

            if ($result) {
                $response->setStatus(1);
                $response->setMsg("Success.");
                $response->setObjArray($result);
            } else {
                $response->setStatus(0);
                $response->setMsg("Failed.");
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    public function request_count() {
        $response = new response ();

        try {
            $post = $this->input->post();

            $apiDao = new Api_dao();
            $result = $apiDao->request_count($post);

            $response->setStatus(1);
            $response->setMsg("Success.");
            $response->setObjArray($result);
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /* check rating is exist for the users last order  30-11-2017 */

    public function check_rating_existence() {
        $response = new response ();
        try {
            $user_id = $this->input->post('user_id');
            $apiDao = new Api_dao();
            $result = $apiDao->checkorder_rating($user_id);
            if ($result) {
                $response->setStatus(1);
                $response->setMsg("Success.");
                $response->setObjArray($result);
            } else {
                $response->setStatus(0);
                $response->setMsg("Success.");
                $response->setObjArray("true");
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    public function is_email_exist() {
        $response = new Response ();
        $data = array();
        try {
            $apiDao = new Api_dao();

            $post = $this->input->post();
            $data['email'] = $post['email'];
            $response = $apiDao->is_email_exist($data);
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    public function auto_reject_orders() {
        $apiDao = new Api_dao();
        $response = $apiDao->auto_reject_orders();
        return $response;
    }

    /**
     * @method: get_user_data
     * @param $user_id
     * @return user data
     */
    public function get_user_data() {
        $response = new Response ();
        try {
            $post = $this->input->post();
            $user_id = $post['user_id'];
            $apiDao = new Api_dao();
			$result = $apiDao->get_user_data($user_id);
			if(!empty($post['is_retailer']) && $post['is_retailer']=='1')
			{
				$store_data = store_detail_by_retailer_id($user_id);
				$address_data = $apiDao->get_address($user_id);
				$result->store_data  = ($store_data) ? $store_data:'';
				$result->address_data = ($address_data) ? $address_data:'';
				$response = $result;
			}else{
			  $response = 	$result;
			}

        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /**
     * @method: logout
     * @param  $user_id, $device_id
     * @return user data
     */
    public function logout() {
        $response = new Response ();
        try {
            $post = $this->input->post();
            $apiDao = new Api_dao();
            $result = $apiDao->logout($post);
            if ($result) {
            	$user = $this->db->select('role_id, mobile')->where(array('id'=>$post['user_id']))->get('users')->row();
            	if($user->role_id==ROLE_RETAILER){
            		$this->logout_msg($user->mobile);
            	}
            	$response->setStatus(1);
                $response->setMsg("Success.");
                $response->setObjArray("");
            } else {
                $response->setStatus(0);
                $response->setMsg("Failed.");
                $response->setObjArray("");
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }


    private function logout_msg($mobile){

    		//$mobile = $this->db->select('mobile')->where(array('id'=>$id))->get('users')->row()->mobile;

    		/** Send otp
    		$sender_id = 'TIPLUR';
    		$user_id = urlencode('rakshat.chopra@gmail.com');
    		$pwd     = urlencode('RC@123tm');
    		$message = urlencode("We observe that you are logged off from your Merchant Application. Kindly login so that you may receive orders.");

    		$URL ="http://49.50.77.216/API/SMSHttp.aspx?UserId=".$user_id."&pwd=".$pwd."&Message=".$message."&Contacts=".$mobile."&SenderId=".$sender_id."";
    		$ch = curl_init($URL);
    		curl_setopt($ch, CURLOPT_URL, $URL);
    		curl_setopt($ch, CURLOPT_RETURNTRANSFER , 1);
    		$result = curl_exec($ch);
    		curl_close($ch);*/

    	    $message ="We observe that you are logged off from your Merchant Application. Kindly login so that you may receive orders.";
    	   sms_notification($mobile, $message);
    }

    public function test(){
    	/* $mobile = "9716256074";
    	$sender_id = 'TIPLUR';
    	$user_id = urlencode('rakshat.chopra@gmail.com');
    	$pwd     = urlencode('RC@123tm');
    	//$message  = urlencode("Dear Customer, Your OTP is  Have a pleasant day!");
    	$message = urlencode("We observe that you are logged off from your Merchant Application. Kindly login so that you may receive orders.");

    	$URL ="http://49.50.77.216/API/SMSHttp.aspx?UserId=".$user_id."&pwd=".$pwd."&Message=".$message."&Contacts=".$mobile."&SenderId=".$sender_id."";
    	$ch = curl_init($URL);
    	curl_setopt($ch, CURLOPT_URL, $URL);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER , 1);
    	$result = curl_exec($ch);

    	curl_close($ch); */
    	$notify = new Push_notification();
    	$res = $notify->send_ios_notification("732178cac0ee4fa46c06c128e8a3f0ec7fee09acbe538539854410bee2059dd2", "hello");
    	return $res;
    }

    /**
     * @method: city_list
     * @param  $state_id
     * @return city list
     */
    public function city_list() {
    	$response = new Response ();
    	try {
    		$post = $this->input->post();
    		$apiDao = new Api_dao();
    		$result = $apiDao->get_cities($post);
    		if ($result) {
    			$response->setStatus(1);
    			$response->setMsg("Success.");
    			$response->setObjArray($result);
    		} else {
    			$response->setStatus(0);
    			$response->setMsg("Failed.");
    			$response->setObjArray("");
    		}
    	} catch (Exception $e) {
    		$response->setStatus(- 1);
    		$response->setMsg($e->getMessage());
    		$response->setError($e->getMessage());
    		log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
    	}
    	return $response;
    }

    /**
     * @method: get_city_state_id_by_name
     * @param  $state, $city array
     * @return city & state id
     */
    public function get_city_state_id_by_name() {
    	$response = new Response ();
    	try {
    		$post = $this->input->post();
    		$apiDao = new Api_dao();
    		$result = $apiDao->get_city_state_id_by_name($post);
    		if ($result)
			{
				if($result['status'] == '0')
				{
					unset($result['status']);
					$response->setStatus(0);
					$response->setMsg("Not Found");
					$response->setObjArray($result);
				}else{
					$response->setStatus(1);
					$response->setMsg("Success.");
					$response->setObjArray($result);
				}
    		} else {
    			$response->setStatus(0);
    			$response->setMsg("Failed.");
    			$response->setObjArray("");
    		}
    	} catch (Exception $e) {
    		$response->setStatus(- 1);
    		$response->setMsg($e->getMessage());
    		$response->setError($e->getMessage());
    		log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
    	}
    	return $response;
    }

    /**
     * @method: is_user_logged_in
     * @param  $device_id, $user_id
     * @return boolean
     */
   public function is_user_logged_in(){
    	$response = new Response ();
    	$post = $this->input->post();

    	$result = is_logged_in($post['device_id'], $post['user_id']);

    	if(!$result){

    		$response->setStatus(4);
    		$response->setMsg("User is logged off.");
    		$response->setObjArray(NULL);

    	}else{

    		$response->setStatus(1);
    		$response->setMsg("User is logged in.");
    		$response->setObjArray(NULL);

    	}

    	return $response;
    }

    /**
     * @method: notification_list
     * @param  $user_id
     * @return notification_list
     */
    public function notification_list(){
    	$response = new Response ();
    	try {
    		$post = $this->input->post();
    		$apiDao = new Api_dao();
    		$result = $apiDao->notification_list($post);
    		if (sizeof($result)>0) {
    			$response->setStatus(1);
    			$response->setMsg("Success.");
    			$response->setObjArray($result);
    		} else {
    			$response->setStatus(0);
    			$response->setMsg("No data found.");
    			$response->setObjArray("");
    		}
    	} catch (Exception $e) {
    		$response->setStatus(- 1);
    		$response->setMsg($e->getMessage());
    		$response->setError($e->getMessage());
    		log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
    	}
    	return $response;
    }

    public function get_notification_count(){
    	$post = $this->input->post();
    	$apiDao = new Api_dao();
    	$result = $apiDao->notification_list($post, true);
    	return sizeof($result);
    }

    /**** Mapped Retailer Profile Update (Change mapped retailer profile into Registered Retailer) ****/
	public function update_mapped_retailer()
	{
		$response = new response ();
        try {

            $data = $this->input->post();
            $apiDao = new Api_dao ();
            $email_exist = $this->db->where(array('email'=>$data['email'],'id !='=>$data['mapped_id']))->get('users');$mobile_exist = $this->db->where(array('mobile'=>$data['mobile'],'id !='=>$data['mapped_id']))->get('users');
            if ($email_exist->num_rows() > 0  || $mobile_exist->num_rows() > 0)
			{
                $response->setStatus(0);
                $response->setMsg("Email-id OR Mobile number already exists");
                $response->setObjArray('');
			} else
			{
                    $res = $apiDao->update_mapped_profile($data);
					if ($res->getStatus() == 1) {
                         $result = $res->getObjArray();
						 $Returndata ['id'] = $data['mapped_id'];
                         $Returndata ['role_id'] = $result['role_id'];
					     $Returndata ['status'] = $result['status'];
                         $Returndata ['email'] = $result['email'];
                         $Returndata ['mobile'] = $result['mobile'];
                         $Returndata ['username'] = $result['display_name'];
                         $Returndata ['address'] = $result['address'];
                         $Returndata ['dob'] = $result['dob'] ? $result['dob']:'';
					     $Returndata ['doc_file'] = ($result['doc_file'] != '') ? base_url() . 'assets/uploads/document_file/' . $result['doc_file'] : '';
                         $Returndata ['image'] = '';
                         $Returndata ['store'] = $result['store'];

						$updateDeviceData = $apiDao->updateDeviceData($data['mapped_id'], $data['device_type'], $data['device_id'], $data['fcm_reg_id']);
						if ($updateDeviceData)
						{
								$response->setStatus(1);
								$response->setMsg("Success");
								$response->setObjArray($Returndata);
						} else {
								$response->setStatus(0);
								$response->setMsg("Something went wrong");
								$response->setObjArray('');
						}

					}else {
                        $response->setStatus(0);
                        $response->setMsg("Something went wrong");
                        $response->setObjArray('');
                    }
			}

        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
	}

	public function removePreviousCart($data,$condition=false)
	{
		if($condition == true)
		{
			$cart_count = $this->db->select('COUNT(item_id) as item_id')->where('user_id',$data)->get('cart')->row();
			$result = isset($cart_count->item_id) ? $cart_count->item_id:'0';
		}else
		{
			//find the previous data of cart
			$previous_cart = $this->db->select('city_id')->where('user_id',$data['user_id'])->get('cart')->result();

			if(!empty($previous_cart) && $previous_cart[0]->city_id !='0')
			{
				$pre_city = isset($previous_cart[0]->city_id) ? $previous_cart[0]->city_id:'';
				if($pre_city != $data['current_city_id'])
				{
					$this->db->where('user_id',$data['user_id']);
					$result = $this->db->delete('cart');
				}
			}
		}

		return $result;
	}


	public function get_banner_images()
	{
		$banner = $this->db->select('id,CONCAT("'.base_url().'assets/images/banner_images/",image) as image')->where(array('type'=>'mobile','status'=>'1'))->get('banner_images')->result();
	    return $banner ? $banner :array();
	}


/******************************************************* Promotion *********************************************/

// All Promotion List Pincode-wise
public function brands_list ($brand_name) {

    $response = new response ();
    try {
      $apiDao = new Api_dao();
      $result = $apiDao->get_brandName($brand_name);
         // $result = $this->db->select('promotion_id,promotion_title')->where(array('pincode' => $pincode,'promo_status' => '0'))->order_by('', 'ASC')->get('ti_promotion')->result();
          // $this->db->last_query();
        if ($result) {
          $response->setStatus(1);
          $response->setMsg('Success');
          $response->setObjArray($result);
        } else {
          $response->setStatus(0);
          $response->setMsg("Not Found");
          $response->setObjArray('');
        }

    } catch (Exception $e) {
        $response->setStatus(- 1);
        $response->setMsg($e->getMessage());
        $response->setError($e->getMessage());
        log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
    }
    return $response;
}

// All Promotion List Pincode-wise
public function promotions_list ($userid) {

    $response = new response ();
    try {
      $apiDao = new Api_dao();
      $result = $apiDao->get_promotionList($userid);
            // echo $this->db->last_query(); die;
            // prd($result); die;
       if($result == 'inactive_user') {

           $response->setStatus(0);
           $response->setMsg("No Promotion for Inactive User");
           $response->setObjArray('');
      }elseif($result){
            $response->setStatus(1);
            $response->setMsg("Success");
            $response->setObjArray($result);
      }else{
          $response->setStatus(0);
          $response->setMsg("Promotion not Found for pincode $pincode");
          $response->setObjArray('');
        }

    } catch (Exception $e) {
        $response->setStatus(- 1);
        $response->setMsg($e->getMessage());
        $response->setError($e->getMessage());
        log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
    }
    return $response;
}
// All Promotion Details
public function promotions_details ($id,$promotion_id,$user_id,$is_participated,$barcode,$qty,$address,$reward_points) {

    $response = new response ();
    try {
      $apiDao = new Api_dao();
      $result = $apiDao->get_promotionDetails($id,$promotion_id,$user_id,$is_participated,$barcode,$qty,$address,$reward_points);
      // prd($result);
       // $result = $this->db->select('promotion_id,promotion_title')->where(array('pincode' => $pincode,'promo_status' => '0'))->order_by('', 'ASC')->get('ti_promotion')->result();
            // echo  $this->db->last_query(); die;
          if($result == 2) {
            $response->setStatus(0);
            $response->setMsg("Sorry! You have reached max limit of Participation for today");
            $response->setObjArray($result);
          } elseif($result) {
            $response->setStatus(1);
            $response->setMsg("Success");
            $response->setObjArray($result);
          }else{
            $response->setStatus(0);
            $response->setMsg("Not Found");
            $response->setObjArray('');
          }

    } catch (Exception $e) {
        $response->setStatus(- 1);
        $response->setMsg($e->getMessage());
        $response->setError($e->getMessage());
        log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
    }
    return $response;
}

// All Promotion Details
public function barcode_detail ($user_id,$brand_barcode,$brand_name,$promotion_id) {

    $response = new response ();
    try {
      $apiDao = new Api_dao();
      $result = $apiDao->get_barcode_detail ($user_id,$brand_barcode,$brand_name,$promotion_id);
// prd($result);
// echo $this->db->last_query(); die;
            if ($result == 1) {
            $response->setStatus(0);
            $response->setMsg('Sorry, this code is already submitted. If possible change it with another unit, and retry.');
            $response->setObjArray('');
          } elseif ($result['status'] === false) {
            $response->setStatus(0);
            $response->setMsg("Sorry, Invalid QR-code for ".$result['msg'].", kindly scan & submit code of listed brand to participate.");
            $response->setObjArray('');
        } else {
          $response->setStatus(1);
          $response->setMsg('Code Scanned and Successfully submitted. Best of luck !');
          $response->setObjArray($result);

        }

    } catch (Exception $e) {
        $response->setStatus(- 1);
        $response->setMsg($e->getMessage());
        $response->setError($e->getMessage());
        log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
    }
    return $response;
}

public
function total_reward_points ($user_id)
{
    $response = new response ();
    try {
      $apiDao = new Api_dao();
      $result = $apiDao->total_reward_points ($user_id);
     // prd($result);
          if ( !empty($result->total_rewardPoints)) {
                $response->setStatus(1);
                $response->setMsg('Success');
                $response->setObjArray($result);
            } else {
                $response->setStatus(0);
                $response->setMsg("No Reward Points found");
                $response->setObjArray('');
            }

    } catch (Exception $e) {
        $response->setStatus(- 1);
        $response->setMsg($e->getMessage());
        $response->setError($e->getMessage());
        log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
    }
    return $response;
}
 public
 function redeem_checkout ($user_id,$reward_points)
 {
    $response = new response ();
    try {
      $apiDao = new Api_dao();
      $result = $apiDao->redeem_checkout ($user_id,$reward_points);
          if ( $result['status'] == '0' ) {
                $response->setStatus(0);
                $response->setMsg($result['error']);
                $response->setObjArray('');
            } elseif($result) {
                 $response->setStatus(1);
                $response->setMsg('Success');
                $response->setObjArray($result);
            }else{
                $response->setStatus(0);
                $response->setMsg("Something went wrong");
                $response->setObjArray('');
            }

    } catch (Exception $e) {
        $response->setStatus(- 1);
        $response->setMsg($e->getMessage());
        $response->setError($e->getMessage());
        log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
    }
    return $response;
 }
// All Promotion Details
public function participatedUserHistory ($user_id) {

    $response = new response ();
    try {
      $apiDao = new Api_dao();
      $result = $apiDao->get_participatedUserHistory ($user_id);
          if ($result) {
          $response->setStatus(1);
          $response->setMsg('Success');
          $response->setObjArray($result);
        } else {
          $response->setStatus(0);
          $response->setMsg("No history found");
          $response->setObjArray('');
        }

    } catch (Exception $e) {
        $response->setStatus(- 1);
        $response->setMsg($e->getMessage());
        $response->setError($e->getMessage());
        log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
    }
    return $response;
}

/****************************************************************************************************************/

public function pubsubcategories_list() {
        $response = new response ();
        try {
            //$category_id = $this->input->post('category_id');
            //$this->load->model('Category/category_model');
            //$res = $this->category_model->subcategory_listing($category_id);
            $data = $this->input->post();
            $apiDao = new Api_dao ();

            $res = $apiDao->pubsubcategories_list($data);
            if ($res) {
                $response->setStatus(1);
                $response->setMsg('success');
                $response->setObjArray($res);
            } else {
                $response->setStatus(0);
                $response->setMsg("Not Found");
                $response->setObjArray('');
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    //add product detail
     public function add_product() {
        $response = new response ();
        try {

           // $data = $this->input->post();

            $apiDao = new Api_dao ();

               $response = $apiDao->add_product();


        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

}



?>
