<?php

class Category extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('category_model');
        $this->load->library('auth');
        $this->load->library('form_validation');
        $this->set_data = array('theme' => 'admin'); //define theme name
        if (!$this->auth->_is_logged_in()) {
            redirect(base_url());
        }
    }

    function filter() {
        return $this->input->get(array('title', 'type', 'parent_id', 'status', 'is_liquor'));
    }

    function index() {
        $this->load->library('pagination');

        $includeJs = array('/assets/js/category.js');
        //for adding js in footer
        $data['js'] = $includeJs;
        //prd($_GET);
        if (!empty($_GET['multi_delete'])) {

            $this->delete_cat();
        }

        $per_page = isset($_GET['per_page']) ? $_GET['per_page'] : false;
        unset($_GET['per_page']);
        $query_string = http_build_query($_GET);

        $site_url = !$query_string ? base_url(SITE_AREA . '/category') : base_url(SITE_AREA . '/category?' . $query_string);
        if ($per_page) {
            $_GET['per_page'] = $per_page;
        }

        $num_rows = $this->category_model->category_details($this->filter());
        $config = pagination_formatting();
        $limit = 10;
        $config['base_url'] = $site_url;
        $config['total_rows'] = $num_rows;
        $config['per_page'] = $limit;
        $config['num_links'] = 10;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $this->pagination->initialize($config);
        $offset = isset($_GET['per_page']) ? ($_GET['per_page'] - 1) * $limit : null;
        $records = $this->category_model->category_details($this->filter(), $limit, $offset, false);
        $data['result'] = $records;
        $this->theme($data);
    }

    function delete_cat() {
        $checked = $this->input->get('cat_id');
        if (is_array($checked) && count($checked)) {
            // If any of the deletions fail, set the result to false, so
            // failure message is set if any of the attempts fail, not just
            // the last attempt 
            $result = true;
            foreach ($checked as $pid) {
                $data = array('status' => '3');

                $updated = $this->category_model->update_categorie($data, $pid);
                if ($updated == false) {
                    $result = false;
                }
            }
            if ($result) {
                set_flash_message($type = 'success', $message = count($checked) . ' row deleted successfully.');
                redirect('admin/category');
            } else {
                set_flash_message($type = 'error', $message = 'something wrong');
                redirect('admin/category');
            }
        } else {
            set_flash_message($type = 'error', $message = 'Please select category.');
            redirect('admin/category');
        }
    }

    /*  Script for add categorie */

    function create() {
        $includeJs = array('/assets/js/category.js');
        //for adding js in footer
        $data['js'] = $includeJs;
        if (isset($_POST['save'])) {

            $this->form_validation->set_rules($this->category_model->get_form_validation_rules());
            //
            if ($this->form_validation->run() === false) {
                set_flash_message($type = 'error', $message = 'Please enter category details.');
            } else {

                if ($this->save_categories('insert')) {
                    set_flash_message($type = 'success', $message = 'Category add successfully.');

                    redirect('admin/category');
                }
            }
        }
        $this->theme($data);
    }

    /*  script for delete categorie */

    function delete_categorie() {
        $id = $this->input->post('cat_id');
        $res = $this->db->where('id', $id)->update('category', array('status' => '3'));
        $data['msg'] = set_flash_message($type = 'success', $message = 'Deleted successfully.');
        $data['status'] = ($res) ? true : false;
        echo json_encode($data);
    }

    /*  script for update categorie */

    function edit() {
        $includeJs = array('/assets/js/category.js');
        //for adding js in footer
        $data['js'] = $includeJs;
        $id = $this->uri->segment(4);
        $data['result'] = $this->category_model->categorie_detail($id);
        if (!$data['result']) {
            show_404();
        }
        if (isset($_POST['save'])) {
            // prd($_POST);
            $this->form_validation->set_rules($this->category_model->get_form_validation_rules());
            if ($this->form_validation->run() === false) {
                set_flash_message($type = 'error', $message = 'There is something wrong.');
            } else {
                if ($this->save_categories('update', $id)) {
                    set_flash_message($type = 'success', $message = 'Category Update successfully.');
                    redirect('admin/category');
                }
            }
        }

        $this->theme($data);
    }

    /*  script for save and update  categorie */

    private function save_categories($type = false, $id = false) {
        $data = $this->input->post(array('title', 'type', 'description', 'is_liquor'));
        $data['status'] = (isset($_POST['status']) && $_POST['status'] == 1) ? $_POST['status'] : '0';
        $data['type'] = (isset($_POST['type']) && $_POST['type']) ? $_POST['type'] : '0';
        $data['is_pub'] = (isset($_POST['is_pub']) && $_POST['is_pub'] == 1) ? $_POST['is_pub'] : '0';
        $data['parent_id'] = (isset($_POST['parent_id']) && $_POST['parent_id']) ? $_POST['parent_id'] : '0';
        $data['created_on'] = $data['modified_on'] = db_date_time();
        
        
        if ($type == 'insert') {
            
            $data ['icon'] = 'assets/images/cat_icons/'.$this->upload_image();
            
            $id = $this->category_model->add_categorie($data);
            if ($id) {
                $return = $id;
            }
        } elseif ($type == 'update') {
            unset($data['created_on']);
            
            $data ['icon'] = $this->upload_image();
            $return = $this->category_model->update_categorie($data, $id);
        }
        return $return;
    }

    private function upload_image() {
        $this->load->library('upload');
        $upload_path = FCPATH . "assets/images/cat_icons/";
        $h_file = $this->input->post('h_image');
        $input_field_name = 'image';
        $config = array(
            'upload_path' => $upload_path,
            'allowed_types' => 'gif|jpg|png|jpeg',
            'file_name' => time() . $_FILES[$input_field_name]['name'],
        );

        if (is_uploaded_file($_FILES[$input_field_name]['tmp_name'])) {

            $this->upload->initialize($config);
            if (!$this->upload->do_upload($input_field_name)) {
                $this->form_validation->set_message($input_field_name, $this->upload->display_errors());
                return false;
            } else {
                if ($h_file and file_exists($upload_path . '/' . $h_file)) {
                    unlink($upload_path . '/' . $h_file);
                }
                $uploaded = $this->upload->data();
                return $uploaded['file_name'];
            }
        } else {

            return $h_file;
        }
    }

}
