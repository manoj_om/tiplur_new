<?php

class Orders extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('orders_model');
        $this->load->library('auth');
        $this->load->library('form_validation');
        date_default_timezone_set('Asia/Kolkata');
        $this->set_data = array('theme' => 'admin'); //define theme name
        if (!$this->auth->_is_logged_in()) {
            redirect(base_url());
        }
    }

    function index() {
        //prd(is_admin());
        //pagination
        $this->load->library('pagination');
        $per_page = isset($_GET['per_page']) ? $_GET['per_page'] : false;
        unset($_GET['per_page']);
        $query_string = http_build_query($_GET);

        $site_url = !$query_string ? base_url(SITE_AREA . '/orders') : base_url(SITE_AREA . '/orders?' . $query_string);
        if ($per_page) {
            $_GET['per_page'] = $per_page;
        }
        $num_rows = $this->orders_model->orders_listing($this->filter());
        $config = pagination_formatting();
        $limit = 20;
        $config['base_url'] = $site_url;
        $config['total_rows'] = $num_rows;
        $config['per_page'] = $limit;
        $config['num_links'] = 5;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $this->pagination->initialize($config);

        $offset = isset($_GET['per_page']) ? ($_GET['per_page'] - 1) * $limit : null;
        $data['orders'] = $this->orders_model->orders_listing($this->filter(), $limit, $offset, false);
        //end here
        //prd($data['orders']);

        $data['css'] = array('/theme/admin/css/bootstrap-datetimepicker.css');
        $data['js'] = array('/theme/admin/js/moment.js', '/theme/admin/js/bootstrap-datetimepicker.js', '/assets/js/orders.js');
        $this->theme($data);
    }

    function filter() {
        return $this->input->get(array('retailer', 'status', 'from_dt', 'to_dt'));
    }

    function view_order() {
        //print_r(date('Y-m-d H:i:s'));
        $filter = $this->input->get(array('id', 'user_id', 'type'));

        $ids = array();

        if (isset($filter['id'])) {
            array_push($ids, $filter['id']);
        } else {
            if (isset($filter['user_id']) && isset($filter['type'])) {

                if ($filter['type'] == 'retailer' || $filter['type'] == 'user') {
                    $ids = $this->orders_model->get_user_orders($filter['user_id'], $filter['type']);
                }
            }
        }

        $data['ids'] = $ids;
        $data['js'] = array('/theme/admin/js/moment.js', '/theme/admin/js/bootstrap-datetimepicker.js', '/assets/js/orders.js');
        $data['status_history'] = $this->orders_model->getOrderHistory($filter['id']);
        $this->theme($data);

        //prd($order_details);
    }

	function abandoned_cart() {

		$this->load->library('pagination');
        $per_page = isset($_GET['per_page']) ? $_GET['per_page'] : false;
        unset($_GET['per_page']);
        $query_string = http_build_query($_GET);

        $site_url = !$query_string ? base_url(SITE_AREA . '/orders/abandoned_cart') : base_url(SITE_AREA . '/orders/abandoned_cart?' . $query_string);
        if ($per_page) {
            $_GET['per_page'] = $per_page;
        }
        $num_rows = $this->db->get('abandoned_cart_detail')->num_rows();
        $config = pagination_formatting();
        $limit = 20;
        $config['base_url'] = $site_url;
        $config['total_rows'] = $num_rows;
        $config['per_page'] = $limit;
        $config['num_links'] = 5;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $this->pagination->initialize($config);

        $offset = isset($_GET['per_page']) ? ($_GET['per_page'] - 1) * $limit : null;
        $data['result'] = $this->db->limit($limit, $offset)->order_by('created_on','desc')->get('abandoned_cart_detail')->result();

        $this->theme($data);

        //prd($order_details);
    }

    public function export_orders() {

        $filter = isset($_GET['filter']) ? $this->filter() : false;

        $reports = $this->orders_model->orders_listing($filter, false, false, false);
       // echo "<pre>"; print_r($reports);die;

        /* Start Logic for excel */
        $exceldata = array();
        $table = '<table><tr><th>Sr No.</th><th>Order No</th><th>Customer</th><th>Retailer</th><th>Shop</th><th>Delivery Address</th><th>Product Name</th><th>Quantity</th><th>Weight</th><th>Price</th><th>Total Amount</th><th>Payment Mode</th><th>Order Date</th><th>Status</th><th>Acceptance Date & Time</th><th>Ongoing Date & Time</th><th>Delivered Date & Time</th></tr>';

        $total_amount = 0;
		$order_id = null;
		$i = 1;
        foreach ($reports as $record) {

            //status date and time - add LOC on 9 july 2018
            $status_details = $this->db->select('status,created_date')->where('order_number',$record->order_id)->where_in('status',array('1','4','5'))->get('trans_order_status')->result_array();
			$status_arr   = !empty($status_details) ? array_column($status_details,'created_date','status'):'';

			//print_r($reports);
			$Order_Details = get_order_details($record->order_id);
            $products = explode('<@>',$Order_Details->product_id);
			$quantity = explode('<@>',$Order_Details->quantity);
			$price    = explode('<@>', $Order_Details->price);
            $weight   = explode('<@>', $Order_Details->weight);
			$j = 0;
            foreach($products as $p):
				$table .= '<tr>';
				$table .= ($order_id != $record->order_id) ? '<td>' . $i . '</td>' : '<td></td>';
				$table .= ($order_id != $record->order_id) ? '<td>' . $record->order_id . '</td>' : '<td></td>';
				$table .= ($order_id != $record->order_id) ? '<td>' . $record->user:'</td>'. '<td></td>';
				$table .= ($order_id != $record->order_id) ? '<td>' . $record->retailer:'</td>'. '<td></td>';
				$table .= ($order_id != $record->order_id) ? '<td>' . store_detail_by_retailer_id($record->retailer_id)->store_name:'</td>'. '<td></td>';
				$table .= ($order_id != $record->order_id) ? '<td>' . $record->address . ',' . $record->city . ',' . $record->state . ',' . $record->country :'</td>'. '<td></td>';
				$table .= '<td>' . $p . '</td>';
				$table .= '<td>' . $quantity[$j].'</td>';
				$table .= '<td>' . $weight[$j].'</td>';
				$table .= '<td>' . $price[$j].'</td>';
				$table .= ($order_id != $record->order_id) ? '<td>' . $Order_Details->total:'</td>'. '<td></td>';
				$table .= ($order_id != $record->order_id) ? '<td>' . $record->title:'</td>'. '<td></td>';
				$table .= ($order_id != $record->order_id) ? '<td>' . date('d M Y h:i A', strtotime($record->order_date)) :'</td>'. '<td></td>';
				$table .= ($order_id != $record->order_id) ? '<td>' . print_order_status($record->status) :'</td>'. '<td></td>';
				 $table .= ($order_id != $record->order_id  && !empty($status_arr[1])) ? '<td>' . date('d M Y h:i A', strtotime($status_arr[1])) :'</td>'. '<td></td>';
				$table .= ($order_id != $record->order_id && !empty($status_arr[4])) ? '<td>' . date('d M Y h:i A', strtotime($status_arr[4])) :'</td>'. '<td></td>';
				$table .= ($order_id != $record->order_id && !empty($status_arr[5])) ? '<td>' . date('d M Y h:i A', strtotime($status_arr[5])) :'</td>'. '<td></td>';
                $table .= '</tr>';
				$j++;
				$order_id = $record->order_id;
			endforeach;
			$i++;
            $table .= '<tr></tr>';
        }
        $table .= '</table>';
        $date = date('d-m-Y');

        header("Content-Type: application/vnd.ms-excel");
        header("Content-disposition: attachment; filename=Tiplur Order Details(" . $date . ").xls");
        echo $table;
        die();
        /* end Logic for excel */
    }

    function decline_order() {

        if (empty($_POST)) {
            show_404();
        }

        $id = $this->input->post('order_id');
        $reason = $this->input->post('reason');

        $order_details = get_order_details($id);
        //prd($order_details);
        $data = array('status' => '2', 'modification_date' => date('Y-m-d H:i:s'));

        $result = true;
        $updated = $this->orders_model->update_order($data, $id, $reason);
        //echo $updated;exit;
        if ($updated == false) {
            $result = false;
        }

        if ($result) {

            $order_details = get_order_details($id);

            $notify_data = array(
                'order_id' => 'Order Declined',
                'notified_to' => $order_details->user_id,
                'requested_by' => $this->session->userdata('id'),
                'type' => ORDER_DECLINED_NOTIFICATION,
                'title' => 'Order Declined',
                'message' => 'Your order is declined by admin',
                'reason' => $reason,
            );

            $notify = new Push_notification();
            $notify->send_notification($notify_data);

            //$email = $order_details->user_email;

            //$this->send_welcome_mail($email); //send mail

            set_flash_message($type = 'success', $message = 'Order declined successfully.');
            redirect('admin/orders');
        } else {
            set_flash_message($type = 'error', $message = 'something wrong.');
            redirect('admin/orders');
        }
    }

    function chnage_order_status() {

        if (empty($_POST)) {
            show_404();
        }

        $id = $this->input->post('order_id');
        $reason = $this->input->post('reason');
		$status = $this->input->post('status');
		$status_detail = order_status($status);
		$status_name = ($status_detail) ? $status_detail->title :'';

        $data = array('status' => $status, 'modification_date' => date('Y-m-d H:i:s'));

        $result = true;
        $updated = $this->orders_model->update_order($data, $id, $reason);
        //echo $updated;exit;
        if ($updated == false) {
            $result = false;
        }

        if ($result) {

            $order_details = get_order_details($id);
            if($status == '2'):
				$notify_data = array(
					'order_id' => 'Order Declined',
					'notified_to' => $order_details->user_id,
					'requested_by' => $this->session->userdata('id'),
					'type' => ORDER_DECLINED_NOTIFICATION,
					'title' => 'Order Declined',
					'message' => 'Your order is declined by admin',
					'reason' => $reason,
				);

				$notify = new Push_notification();
				$notify->send_notification($notify_data);
		    endif;

            //$email = $order_details->user_email;

            //$this->send_welcome_mail($email); //send mail

            set_flash_message($type = 'success', $message = 'Order status updated successfully.');
            redirect('admin/orders');
        } else {
            set_flash_message($type = 'error', $message = 'something wrong.');
            redirect('admin/orders');
        }
    }


        /*
	 * @author: Anjani Gupta
	 * @date: 30/10/2017
	 * @method: get_store
	 * @desc: Get store detail of user
	 * @params: user_id
	 */
	public function get_store($data){
        $query = $this->db->select('uadd.address,uadd.address2,store.id, min_order, store_name, opening_time, closing_time, licence, contact_person, contact_person_mobile, is_provide_snacks, payment_mode, store.status, store.creation_date')
        ->join('ti_users_address uadd', 'uadd.user_id = store.retailer_id')->where(array('retailer_id'=>$data['user_id']))
        ->get('store')->result();
		return $query? $query[0]:"";
    }
    
    public function pdf_generate()
    {
        $this->load->library('CustomFPDF');
        $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
                "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .  
                $_SERVER['REQUEST_URI']; 
                $order_number= basename($link);
        $query = "SELECT tu.display_name, od.retailer_id, od.weight, od.product_id, od.quantity, ord.shipping_charge, ord.internet_charge, od.price, od.address, od.payment_mode, od.status, od.creation_date requested_date,
						p.title, p.type, ua.name receiver, ua.mobile receiver_mob, ua.address receiver_add, ua.city, ua.state,ua.country, ua.pincode FROM ti_order_detail od INNER JOIN ti_product p ON od.product_id=p.id
						INNER JOIN ti_order ord ON ord.order_number=od.order_id INNER JOIN ti_delivery_address ua ON ua.id=od.address INNER JOIN ti_users tu ON tu.id=od.user_id WHERE od.order_id=".$this->db->escape($order_number);
            $result = $this->db->query($query)->result();
            $orderDetails = array();

			$orderDetails['retailer_id'] = $result[0]->retailer_id;
            $orderDetails['retailer_name'] = $result[0]->display_name;
            $orderDetails['order_number'] = $order_number;
			$x['user_id'] = $result[0]->retailer_id;
			$orderDetails['retailer_store'] = $this->get_store($x);
            $orderDetails['name'] = $result[0]->receiver;
            $orderDetails['mobile'] = $result[0]->receiver_mob;
            $orderDetails['address'] = $result[0]->receiver_add;
            $orderDetails['city'] = $result[0]->city;
            $orderDetails['state'] = $result[0]->state;
            $orderDetails['country'] = $result[0]->country;
            $orderDetails['pincode'] = $result[0]->pincode;
			$orderDetails['payment_mode'] = $result[0]->payment_mode;
			$orderDetails['status'] = $result[0]->status;
			$orderDetails['complete_status'] = $this->db->select('status, comments ,  delivered_at, created_date as status_date')->where('order_number', $order_number)->get('trans_order_status')->result();
			$orderDetails['shipping_charge'] = $result[0]->shipping_charge;
			$orderDetails['internet_charge'] = $result[0]->internet_charge;
			$orderDetails['requested_date'] = $result[0]->requested_date;
			$products = array();
			foreach ($result as $val){
				$product = array();
				$product['id'] = $val->product_id;
				$product['title'] = $val->title;
				$product['type'] = $val->type;
				$product['quantity'] = $val->quantity;
				$product['price'] = $val->price;
				$product['weight'] = $val->weight;
				array_push($products, $product);
			}

                $orderDetails['product'] = $products;
                $pdf = new CustomFPDF();
                $pdf->AliasNbPages();
                $pdf->SetFont('Times','',12);
                $pdf->SetTitle('Invoice');
                $pdf->SetAuthor($orderDetails['retailer_store']->store_name);
                $pdf->SetSubject('Invoice');
                $pdf->SetCreator($orderDetails['retailer_store']->store_name);
                $pdf->SetMargins(6,5,6);
                $pdf->AddFont('Montserrat','','montserratl.php');
                $pdf->AddFont('Montserrat','B','montserratsb.php');
                $pdf->addPage();
                $pdf->SetFont('Arial','B',8);
                $pdf->setBold(20);
                $pdf->setXY(25,25);
                $pdf->Cell(0,5,$orderDetails['retailer_store']->store_name);
                $pdf->setRegular(10);
                $pdf->Ln();
                $pdf->Ln();
                $pdf->setX(25);
                $pdf->setRegular(12);
                $pdf->multiCell(80,5,$orderDetails['retailer_store']->address);
                $pdf->Ln();
                $pdf->setX(25);
                $pdf->setBold(12);
                $pdf->Cell(25,3,'Mobile:');
                $pdf->setRegular(14);
                $pdf->Cell(25,3,$orderDetails['retailer_store']->contact_person_mobile);
                $pdf->ln();
                $pdf->ln();
                $pdf->setBold(20);
                $pdf->setXY(150,25);
                $pdf->Cell(50,5,'Tax Invoice',0,0,'R');
                $pdf->Ln();
                $pdf->Ln();
                $pdf->setX(100,50);
                $pdf->setBold(12);
                $pdf->Cell(50,5,'Date:',0,0,'R');
                $pdf->setRegular(14);
                $pdf->Cell(50,5,date('F d, Y',  strtotime($orderDetails['requested_date'])),0,0,'L');
                $pdf->setXY(100,50);
                $pdf->setBold(12);
                $pdf->Cell(50,5,'Order No:',0,0,'R');
                $pdf->setRegular(12);
                $pdf->Cell(50,5,$orderDetails['order_number'],0,0,'L');
                $pdf->Ln();
                $pdf->Ln();
                $pdf->Ln();
                $pdf->Ln();
                $pdf->Ln();
                $pdf->Ln();
                $pdf->setX(25);
                $pdf->setBold(13);
                $pdf->Line(25,$pdf->getY(),200,$pdf->getY());
                
                 $pdf->Ln();
                $pdf->setX(25);
                $pdf->Cell(25,7,'Bill To :');
                $pdf->setRegular(12);
                $pdf->Cell(50,7,$orderDetails['name']);
                $pdf->Ln();
                $pdf->setX(50);
                $pdf->Cell(50,7,$orderDetails['mobile']);
                $pdf->Ln();
                $pdf->setX(50);
                $pdf->MultiCell(50,7,$orderDetails['address'].' '.$orderDetails['city']);
                $pdf->setX(50);
                $pdf->MultiCell(50,7,$orderDetails['state'].','.$orderDetails['country'].'-'.$orderDetails['pincode']); 
                $pdf->Ln();
                $pdf->Ln();
                $pdf->setX(20);
                $pdf->SetFillColor(217,217,217);
                $pdf->SetDrawColor(171,171,174);
                $y = $pdf->getY();
                $pdf->MultiCell(10,6,'S. No.',1,'C',1);
                $pdf->setXY(30,$y);
                $pdf->MultiCell(60,12,'Item Name',1,'C',1);
                $pdf->setXY(90,$y);
                $pdf->MultiCell(25,12,'Quantity',1,'C',1);
                $pdf->setXY(115,$y);
                $pdf->MultiCell(25,12,'Unit Price',1,'C',1);
                $pdf->setXY(140,$y);
                $pdf->MultiCell(20,12,'Discount',1,'C',1);
                $pdf->setXY(160,$y);
                $pdf->MultiCell(40,12,'Total Amount',1,'C',1);
                //Table Data
                $pdf->Ln(0.1);
                $total_amount=0;
                foreach($orderDetails['product'] as $key=>$item){
                    $total_amount+=$item['price']*$item['quantity'];
                        $y = $pdf->getY();
                $pdf->setXY(30,$y);
                $pdf->MultiCell(60,6,$item['title'] ,1,'C');
                $h = $pdf->getY()-$y;
                $pdf->setXY(20,$y);
                $pdf->MultiCell(10,$h,++$key,1,'C');
                $pdf->setXY(90,$y);
                $pdf->MultiCell(25,$h,$item['quantity'],1,'C');
                $pdf->setXY(115,$y);
                $pdf->MultiCell(25,$h,$item['price'],1,'C');
                $pdf->setXY(140,$y);
                $pdf->MultiCell(20,$h,0,1,'C');
                $pdf->setXY(160,$y);
                $pdf->MultiCell(40,$h,$item['price']*$item['quantity'],1,'C');
                }
                //Table Footer
                $pdf->setX(20);
                $pdf->setBold(12);
                $pdf->Cell(100,6,'','LT');
                $pdf->setRegular(12);
                $pdf->setBold(12);
                $pdf->Cell(40,6,'Internet Chanrge',1);
                $pdf->setRegular(12);
                $pdf->getRupee($pdf->getX()+1,$pdf->getY()+1.5,3);
                $pdf->Cell(40,6,'   '.$orderDetails['internet_charge'],1);
                $pdf->Ln();
                $pdf->setX(20);
                $pdf->Cell(100,6,"",'L');
                $pdf->setBold(12);
                $pdf->Cell(40,6,'Shipping Charge',1);
                $pdf->setRegular(12);
                $pdf->getRupee($pdf->getX()+1,$pdf->getY()+1.5,3);
                $pdf->Cell(40,6,'   '.$orderDetails['shipping_charge'],1);
                $pdf->Ln();
                $pdf->setX(20);
                $pdf->Cell(100,6,'','L');
                $pdf->setBold(12);
                $pdf->Cell(40,6,'Total',1);
                $pdf->setRegular(12);
                $pdf->getRupee($pdf->getX()+1,$pdf->getY()+1.5,3);
                $pdf->Cell(40,6,'   '.($total_amount+$orderDetails['internet_charge']+$orderDetails['shipping_charge']),1);
                $pdf->Ln();
                $pdf->setRegular(12);
                $pdf->setX(62);
                $y=$pdf->getY();
                $f = new \NumberFormatter("en", \NumberFormatter::SPELLOUT);
                $pdf->MultiCell(138,8,ucwords($f->format(($total_amount+$orderDetails['internet_charge']+$orderDetails['shipping_charge']))).' Rupees Only ','BTR');
                $diff= $pdf->getY()-$y;
                $pdf->setXY(20,$y);
                $pdf->setBold(12);
                $pdf->Cell(42,$diff,'Amount (In Words):','LTB');
                $pdf->Ln();
                $pdf->Ln();
                $pdf->Ln();
                $pdf->Ln();
                $pdf->setRegular(15);
                $pdf->setX(30);
                $pdf->cell(160,6,'Thanking you',0,0,'C');
                $pdf->setX(20);
                $pdf->cell(180,20,'Please visit again',0,0,'C');
                $pdf->Output(); 
    }
     

}
