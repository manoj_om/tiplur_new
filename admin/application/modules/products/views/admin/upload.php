<div id="page-wrapper" class="gray-bg dashbard-1">
    <?php breadcrumbs(array('admin/products' => 'Manage Products', 'admin/products/upload' => 'Upload Products')); ?>

    <div class="row border-bottom">
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <?php print_flash_message(); ?>
                <div class="ibox float-e-margins">
                    <?php echo form_open_multipart(site_url('/admin/products/uploadfile'), 'class="form-horizontal"'); ?>
                    <div class="ibox-title">
                        <form action="<?php echo site_url('/admin/products/uploadfile'); ?>"
                              id="browseStudent" enctype="multipart/form-data" method="post">
                            <h2>Upload Data </h2>
                            <div class="ibox-tools" style="display: inline-block; float: right;">
                                <!-- <input type="submit" value="Upload" name="submit_file"
                                                                               class="btn btn-primary pull-right createuser" /> -->
                            </div>
							<div class="ibox-content contentBorder ">
								<div class="row contMargin">
									<div class="col-lg-12 col-md-12 col-sm-12 text-center uploadXml">
										<p>
											<span class="add_event1">
												<a href="<?php echo base_url('/assets/uploads/products-sample.xls'); ?>"
												   class=""><i class="fa fa-download" aria-hidden="true"></i>  Download Liquor excel template</a>
											</span>
										</p>

                    <p>
											<span class="add_event1">
												<a href="<?php echo base_url('/assets/uploads/pub-sample .xls'); ?>"
												   class=""><i class="fa fa-download" aria-hidden="true"></i>  Download Pub excel template</a>
											</span>
										</p>
                    
										<p>
											<span class="add_event1">
												<a href="<?php echo base_url('/assets/uploads/food-sample.xls'); ?>"
												   class=""><i class="fa fa-download" aria-hidden="true"></i>  Download Food excel template</a>
											</span>
										</p>
									</div>

									<div class="col-lg-12 col-md-12 col-sm-12">
										<input name="file" id="file" required="required" class="file form-control formWidht chooseBtnStyle" accept="xls" type="file" >
										<p class="chooseBtnStyle">Only .xls file is allowed<p/p>
									</div>
									<div class="col-sm-12 text-center" style="margin-top: 20px;">
										<input type="submit" value="Upload" name="submit_file"
											   class="btn btn-primary createuser" />
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>
