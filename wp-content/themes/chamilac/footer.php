<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>
        <footer>
			
			<section class="get-it">
				<div class="container">
					<div class="row">
						<hr />
						<!-- <div class="col-md-8">						
							<ul>
								<li><a href="#">About Us</a></li>
								<li><a href="#">Services</a></li>
								<li><a href="#">Privacy Policy</a></li>
								<li><a href="#">Terms &amp; Condition</a></li>
							</ul>
						</div> -->
						<div class="col-sm-12">
							<span class="copyright">Copyright <i class="fa fa-copyright" aria-hidden="true"></i> 2018, Tiplur. All Rights Reserved</span>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-8 col-sm-offset-2">
							<p class="lead message">Tiplur is committed to strict adherence of all regulatory standards such as minimum age limit, ID proof, quantities as per state norms etc.</p>
						</div>
					</div>
				</div>
			</section>
       
           <!-- <div class="sub-footer">
                <div class="container">
                    <div class="col-md-9 col-sm-8 "><h6 class="copyright">Copyright &copy; 2018, Tiplur.All rights reserved. </h6></div>
                    <div class="col-md-3 col-sm-4 ">
                       

                        </ul>
                    </div>					
                </div>
				<div> <h6 align="center">Tiplur is committed to strict adherence of all regulatory standards such as minimum <br>age limit, ID proof, quantities as per state norms etc.</h6></div>
            </div>  -->
        </footer>
        <!-- ���������������[ END FOOTER ]�����������E-->
    </div>
    <!-- �����������������������������������������E-->
    <!-- end: WRAPPER                               -->
    <!-- �����������������������������������������E-->
    <!-- �����������������������������������������E-->
    <!-- BACK TO TOP                               -->
    <!-- �����������������������������������������E-->
    <div id="backTop" class="back-top ">
        <i class="fa fa-angle-up" aria-hidden="true"></i>
    </div>

    <!-- �����������������������������������������E-->
    <!-- SCRIPTS                                   -->
    <!-- �����������������������������������������E-->
    <!-- (!) Placed at the end of the document so the pages load faster -->
    <!-- �����������������������������������������E-->
    <!-- Initialize jQuery library                 -->
    <!-- �����������������������������������������E-->
    <script src="<?php bloginfo('template_directory');?>/assets/js/jquery-1.12.3.min.js"></script>
    <!-- �����������������������������������������E-->
    <!-- Latest compiled and minified Bootstrap    -->
    <!-- �����������������������������������������E-->
    <script type="text/javascript" src="<?php bloginfo('template_directory');?>/assets/js/bootstrap.min.js"></script>
    <!-- �����������������������������������������E-->
    <!-- JavaScript Plugins                        -->
    <!-- �����������������������������������������E-->
    <!-- (!) Include all compiled plugins (below), or include individual files as needed -->
    <!-- Owl Carousel -->
    <script type="text/javascript" src="<?php bloginfo('template_directory');?>/assets/vendors/owl-carousel/owl.carousel.min.js"></script>
    <!-- Magnific popup -->
    <script type="text/javascript" src="<?php bloginfo('template_directory');?>/assets/vendors/magnific-popup/js/jquery.magnific-popup.min.js"></script>
    <!-- jQuery Appear -->
    <script type="text/javascript" src="<?php bloginfo('template_directory');?>/assets/vendors/jquery-appear/jquery.appear.js"></script>
    <!-- Vegas Slider -->
    <script type="text/javascript" src="<?php bloginfo('template_directory');?>/assets/vendors/vegas/vegas.min.js"></script>
    <!-- jQuery Easing v1.3 -->
    <script type="text/javascript" src="<?php bloginfo('template_directory');?>/assets/vendors/jquery.easing.1.3.min.js"></script>
    <!-- Bootstrap Touchspin -->
    <script type="text/javascript" src="<?php bloginfo('template_directory');?>/assets/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
    <!-- MixItUp v2.1.11 -->
    <script type="text/javascript" src="<?php bloginfo('template_directory');?>/assets/vendors/jquery.mixitup.js"></script>
    <!-- YTPlayer -->
    <script type="text/javascript" src="<?php bloginfo('template_directory');?>/assets/vendors/YTPlayer/js/jquery.mb.YTPlayer.min.js"></script>
    <!-- �����������������������������������������E-->
    <!-- Custom Theme JavaScript                   -->
    <!-- �����������������������������������������E-->
    <script type="text/javascript" src="<?php bloginfo('template_directory');?>/assets/js/main.js"></script>
<?php wp_footer(); ?>

<script>
/* SCRIPT FOR REMOVE CLASS AND ADD ACTIVE*/	
 jQuery(document).ready(function () {
	jQuery('.menu-item').click(function(e) {
		var url = jQuery(this).find('a').attr('href'); 
		var array = url.split('/');
		jQuery(this).find('a').attr('rel',array['3']); 
	});
       jQuery('.menu-item').removeClass('current-menu-item');
       var currentURL = document.URL;
		var array = currentURL.split('/'); 
		//var final_url = 'http://'+array['2']+'/'+array['3'];
      jQuery('a[href="' + currentURL + '"]').parent().addClass( 'active' );
	;
    });
/* SCRIPT FOR ACTIVE CLASS*/	
jQuery(document).ready(function () {
	jQuery('.menu-item').click(function(e) {
		jQuery('.menu-item').removeClass('active current-menu-item');
		jQuery(this).addClass('active');
		
		/*$('html, body').animate({
        scrollTop: $(this,'rel').offset().top -70
        }, 500);
		*/
		/* jQuery('html, body').animate({
			scrollTop: jQuery(jQuery.attr(this, 'rel')).offset().top
		  }, 500);

		  */
		  
	});
});
</script>
</body>
</html>




