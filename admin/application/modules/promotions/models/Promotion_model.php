<?php
    class Promotion_model extends CI_Model {

    function __construct()
    {
         parent::__construct();
    }
    // Get customers active  count
    public function get_part_customers_count() {

      $count = $this->db->select('*')->join('users', 'users.id = ti_participated_users.user_id', 'left')->where(array('users.deleted' => '0', 'users.status' => '1'))->get('ti_participated_users')->num_rows();
      return ($count) ? $count : false;
    }
    // Get Promotion active count
    public function get_promotion_list($status=false) {
      if($status == 'active'):
        $this->db->where(array('deleted' => '0', 'promo_status' => '0'));
      else:
        $this->db->where(array('deleted' => '0', 'promo_status' => '0'))->or_where(array('deleted' => '1', 'promo_status' => '1'));
      endif;
        $count = $this->db->select('*')->get('ti_promotion')->num_rows();
        return ($count) ? $count : false;
    }

    // Total users participated on promotions-wise count
  /*  public function usersCount($promo_id =false)
    {
        $count = $this->db->select('count(user_id) as total,promotion_id')->group_by('promotion_id')->get('participated_users')->result();
        return ($count) ? $count : false;
    }*/

    // Get all promotions Brands name to be Active
    public function get_avail_brands()
    {
      $count = $this->db->distinct()->select('brand_name')->get('ti_promotion')->num_rows();
      return ($count) ? $count : false;
    }

	// users listing
    public function users_details($filter = null, $limit = null, $offset = null, $count = true, $type = false) {
        // echo $filter['name']; die;
        // prd($filter);
        if (isset($filter['name']) and $filter['name'] !='') {
            $this->db->group_start();
               $this->db->like("users.display_name", trim($filter['name']));
              $this->db->or_where("promotion.promotion_id", trim($filter['name']));
              $this->db->or_like("promotion.city", trim($filter['name']));
              $this->db->or_where("users.mobile LIKE '%" . $filter['name'] . "%' ESCAPE '!' " );
            $this->db->group_end();
        }
        // Filter on active/inactive user
        if (isset($filter['status']) and $filter['status'] != '') {
            $this->db->where("users.status", $filter['status']);
        }
        // Filter on Date Beween user
        if ( (isset($filter['fromDate']) and $filter['fromDate'] != '') || (isset($filter['toDate']) and $filter['toDate'] != '') ) {
          $filter['fromDate'] = $filter['fromDate'].' '.'00:00:00';
          $filter['toDate'] = $filter['toDate'].' '.'23:59:59';

          $this->db->where(array('participated_on >=' => $filter['fromDate'],'participated_on <=' => $filter['toDate']));
        }

        if ($limit) {
            $this->db->limit($limit, $offset);
        }

        // $this->db->where_not_in("users.id", $this->session->userdata('user_id'));
        $this->db->where("users.deleted", '0',"users.status", '1','participated_users.barcode_status' , '1');
        // $this->db->where(array("users.deleted" => '0',"promotion.deleted" => '0', "promotion.promo_status" => '0'));
        $res = $this->db->select('users.*,ti_participated_users.*,promotion.promotion_img,promotion.prmototion_banner_url,promotion.expiry_date,cities.title as city,roles.role_name as role, states.name as state, device_info.is_logged_in')
        ->join('users', 'users.id = ti_participated_users.user_id', 'left')
        ->join('promotion', 'promotion.promotion_id = ti_participated_users.promotion_id', 'left')
        ->join('roles', 'roles.role_id = users.role_id', 'left')
        ->join('states', 'states.id = users.state_id', 'left')
        ->join('cities', 'cities.id = users.city_id', 'left')
        ->join('device_info', 'device_info.user_id = users.id','left')
        ->order_by('ti_participated_users.participated_on', 'desc')->get('ti_participated_users');
        // prd($res->result());
      // echo $this->db->last_query(); die;
        return ($count) ? $res->num_rows() : $res->result();
    }

    // Get all users specific for Promotion Id
    public function get_promoted_users($promotion_id,$limit = null, $offset = null, $count = true) {

      if ($limit) {
          $this->db->limit($limit, $offset);
      }
        $this->db->where("participated_users.promotion_id",  $promotion_id );
        $res = $this->db->select('participated_users.id as partic_id,participated_users.*,promotion.*,users.*')
        ->join('users' , 'users.id = participated_users.user_id','left')
        ->join('promotion' , 'promotion.promotion_id = participated_users.promotion_id','left')
        ->order_by('participated_users.participated_on','DESC')
        ->get('participated_users');

       return ($count) ? $res->num_rows() : $res->result();
    }



// Promotions listing
    public function promotions_details($promo_filter = null, $limit = null, $offset = null, $count = true) {
       // prd($promo_filter);
       //    echo $promo_filter['brand_name']; die;
       if (isset($promo_filter['brand_name']) and $promo_filter['city'] and $promo_filter['pincode']) {
            $this->db->like("(brand_name", htmlspecialchars(trim($promo_filter['brand_name'])));
            $this->db->or_where("city LIKE '%" . $promo_filter['city'] . "%' ESCAPE '!' ");
            $this->db->or_where("pincode LIKE '%" . $promo_filter['pincode'] . "%' ESCAPE '!' )");
        }
        if (isset($promo_filter['brand_name']) or $promo_filter['city'] or $promo_filter['pincode']) {
             $this->db->like("(brand_name", trim($promo_filter['brand_name']));
             $this->db->or_where("city LIKE '%" . trim($promo_filter['city']) . "%' ESCAPE '!' ");
             $this->db->or_where("pincode LIKE '%" . trim($promo_filter['pincode']) . "%' ESCAPE '!' )");
         }

        if (isset($promo_filter['city']) and $promo_filter['city'] != '') {
            $this->db->like("city", trim($promo_filter['city']));
            $this->db->where("city", $promo_filter['city']);
        }
        if (isset($promo_filter['pincode']) and $promo_filter['pincode'] != '') {
            $this->db->like("pincode", trim($promo_filter['pincode']));
            $this->db->where("pincode", $promo_filter['pincode']);
        }
        if (isset($promo_filter['brand_name']) and $promo_filter['brand_name'] != '') {
            $this->db->like("brand_name", htmlspecialchars(trim($promo_filter['brand_name'])));
             $this->db->or_where("brand_name", $promo_filter['brand_name']);
        }

        if (isset($promo_filter['promo_status']) and $promo_filter['promo_status'] != '') {
            $this->db->where("promo_status", $promo_filter['promo_status']);
        }

        if ($limit) {
            $this->db->limit($limit, $offset);
        }

        //$this->db->where_not_in("id", $this->session->userdata('user_id'));
        $this->db->where("deleted", '0');
        $res = $this->db->select('*')->order_by('released_date', 'desc')->get('ti_promotion');
        // echo $this->db->last_query(); die;
        return ($count) ? $res->num_rows() : $res->result();
    }
    // Get promotion detail particularly
    public function promotion_detail_by_id($id) {
        $res = $this->db->select('promotion_id,brand_barcode_750ml,brand_barcode_375ml,brand_barcode_180ml,brand_name,promotion_title,released_date,promotion_img,prmototion_banner_url,released_date,expiry_date,pincode,city,state,gifts_rewards,customer_care_no')->where(array("id" => $id, "deleted" => '0'))->get('ti_promotion')->row();
        return $res ? $res : false;
    }

    // Get Reward points Details on basis of user
    public function promotion_rewards_details($id) {
        $res = $this->db->select('participated_users.*,u.display_name')->join('users u','u.id=participated_users.user_id','left')->where(array("participated_users.id" => $id))->get('participated_users')->row();
        return $res ? $res : false;
    }

    // Update Reward Points
    public function update_rewards_details($id,$reward_point)
    {
      $data =array(  'id' => $id, 'reward_points' => $reward_point );
     $return = $this->db->where('id', $id)->update('participated_users', $data);
         return $return;
    }

    // Total rewards checkout point user-wise
    public function get_total_rewards_usersCount()
    {
       $res = $this->db->select('ti_redeem_points.user_id')->group_by('redeem_points.user_id')->get('redeem_points')->num_rows();
        return $res ? $res : false;
    }
    public function get_total_rewards_with_users($filter = null, $limit = null, $offset = null, $count = true)
    {


        //  if (isset($filter['name']) and $filter['name']) {
        //     $this->db->like("(display_name", trim($filter['name']));
        //     $this->db->or_like("email", trim($filter['name']));
        //     $this->db->or_where("mobile LIKE '%" . $filter['name'] . "%' ESCAPE '!' )");
        // }

        // if (isset($filter['status']) and $filter['status'] != '') {
        //     $this->db->where("users.status", $filter['status']);
        // }

        if ($limit) {
            $this->db->limit($limit, $offset);
        }

        // $this->db->where_not_in("id", $this->session->userdata('user_id'));
        // $this->db->where("deleted", '0');
       $res = $this->db->select('SUM(ti_redeem_points.redeem_points) as total_rewards,u.display_name,u.mobile')->join('users u','u.id=redeem_points.user_id','left')->group_by('redeem_points.user_id')->get('redeem_points');
        //echo $this->db->last_query();
          // echo  $this->db->last_query(); die;

        return ($count) ? $res->num_rows() : $res->result();



    }

    public function custom_form_validation_rules($data) {
         $arr = array();

        if (!empty($data)) {

            foreach ($data['brand_name'] as $key => $val) {
                $arr[] = array(
                    'field' => 'brand_name[' . $key . ']',
                    'label' => 'Brand name',
                    'rules' => 'required',
                );
            }
            foreach ($data['promotion_title'] as $key => $val) {
                $arr[] = array(
                    'field' => 'promotion_title[' . $key . ']',
                    'label' => 'promotion title',
                    'rules' => 'required',
                );
            }
            foreach ($data['promotion_img'] as $key => $val) {
               $arr[] = array(
                   'field' => 'promotion_img[' . $key . ']',
                   'label' => 'promotion img',
                   'rules' => 'required',
               );
           }
          /* foreach ($data['prmototion_banner_url'] as $key => $val) {
               $arr[] = array(
                   'field' => 'prmototion_banner_url [' . $key . ']',
                   'label' => 'prmototion banner url',
                   'rules' => 'required',
               );
           }*/
           foreach ($data['pincode'] as $key => $val) {
                $arr[] = array(
                    'field' => 'pincode[' . $key . ']',
                    'label' => 'pincode',
                    'rules' => 'required',
                );
            }
           foreach ($data['promotion_desc'] as $key => $val) {
               $arr[] = array(
                   'field' => 'promotion_desc[' . $key . ']',
                   'label' => 'promotion description',
                   'rules' => 'required',
               );
           }
            foreach ($data['released_date'] as $key => $val) {
                $arr[] = array(
                    'field' => 'released_date[' . $key . ']',
                    'label' => 'released date',
                    'rules' => 'required',
                );
            }
            foreach ($data['released_date'] as $key => $val) {
                $arr[] = array(
                    'field' => 'released_date[' . $key . ']',
                    'label' => 'released date',
                    'rules' => 'required',
                );
            }
            foreach ($data['released_time'] as $key => $val) {
                $arr[] = array(
                    'field' => 'released_time[' . $key . ']',
                    'label' => 'released Time',
                    'rules' => 'required',
                );
            }
            foreach ($data['expiry_date'] as $key => $val) {
                $arr[] = array(
                    'field' => 'expiry_date[' . $key . ']',
                    'label' => 'expiry date',
                    'rules' => 'required',
                );
            }
            foreach ($data['expiry_time'] as $key => $val) {
                $arr[] = array(
                    'field' => 'expiry_time[' . $key . ']',
                    'label' => 'Expiry Time',
                    'rules' => 'required',
                );
            }
            foreach ($data['reminder_time'] as $key => $val) {
                $arr[] = array(
                    'field' => 'reminder_time[' . $key . ']',
                    'label' => 'Reminder Time',
                    'rules' => 'required',
                );
            }
            foreach ($data['city'] as $key => $val) {
                $arr[] = array(
                    'field' => 'city[' . $key . ']',
                    'label' => 'city',
                    'rules' => 'required',
                );
            }
            foreach ($data['state'] as $key => $val) {
                $arr[] = array(
                    'field' => 'state[' . $key . ']',
                    'label' => 'state',
                    'rules' => 'required',
                );
            }
            foreach ($data['brand_barcode_180ml'] as $key => $val) {
                $arr[] = array(
                    'field' => 'brand_barcode_180ml[' . $key . ']',
                    'label' => 'brand_barcode_180ml',
                    'rules' => 'required',
                );
            }
            foreach ($data['brand_barcode_180ml'] as $key => $val) {
                $arr[] = array(
                    'field' => 'brand_barcode_180ml[' . $key . ']',
                    'label' => 'brand_barcode_180ml',
                    'rules' => 'required',
                );
            }
            foreach ($data['brand_barcode_750ml'] as $key => $val) {
                $arr[] = array(
                    'field' => 'brand_barcode_750ml[' . $key . ']',
                    'label' => 'brand_barcode_750ml',
                    'rules' => 'required',
                );
            }

             foreach ($data['reward_points_750ml'] as $key => $val) {

                $arr[] = array(
                    'field' => 'reward_points_750ml[' . $key . ']',
                    'label' => 'reward_points_750ml',
                    'rules' => 'required',
                );

            }
             foreach ($data['reward_points_375ml'] as $key => $val) {
                $arr[] = array(
                    'field' => 'reward_points_375ml[' . $key . ']',
                    'label' => 'reward_points_375ml',
                    'rules' => 'required',
                );
            }
             foreach ($data['reward_points_180ml'] as $key => $val) {
                $arr[] = array(
                    'field' => 'reward_points_180ml[' . $key . ']',
                    'label' => 'reward_points_180ml',
                    'rules' => 'required',
                );
            }
            foreach ($data['promotion_id'] as $key => $val) {
                $arr[] = array(
                    'field' => 'promotion_id[' . $key . ']',
                    'label' => 'promotion id',
                    'rules' => 'required',
                );
            }
    /*   foreach ($data['promotion_id'] as $key => $val) {

                $arr[] = array(
                    'field' => 'promotion_id[' . $key . ']',
                    'label' => 'brand promotion ID',
                    'rules' => array('required', 'min_length[1]', array('promotion_id', array($this, 'promotionId_check'))),
                    // 'rules' => array(array('brand_barcode','min_length[1]', array($this, 'barcode_check'))),
                    'errors' => array(
                        'promotion_id' => 'Sorry! Brand promotion ID exists in database',
                    ),
                );
            }*/
       foreach ($data['expiry_date'] as $key => $val) {

                $arr[] = array(
                    'field' => 'expiry_date[' . $key . ']',
                    'label' => 'Expired Date',
                    'rules' => array('required', 'min_length[1]', array('expiry_date', array($this, 'expiryDateCheck'))),
                    'errors' => array(
                        'expiry_date' => 'Sorry! Promotion Expired date must be Future Date.',
                    ),
                );
        }

      }
          //echo "<pre>";print_r($arr); die;
        $this->load->library('form_validation');
        $data1 = $this->form_validation->set_rules($arr);
        return ($this->form_validation->run() === false) ? false : true;
    }
    public function add_promotionData($data) {

        $this->db->insert('ti_promotion', $data);
        $res = $this->db->insert_id();
        return ($res) ? $res : false;
    }
  /*  public function promotionId_check($promo_id) {
      if (trim($promo_id)) {
             $array = array('promotion_id' => $promo_id);
            $res = $this->db->select('promotion_id')->where($array)->get("ti_promotion")->row();
            // echo  $this->db->last_query(); die;
             return ($res) ? false : $promo_id;
          }
    }*/
    // Check expired date validate
    public function expiryDateCheck($exp_date) {
      if (trim($exp_date)) {
            return (expired($exp_date)) ? $exp_date : false;
          }
    }
    // Check released date validate
  /*  public function releaseDate_check($relDate,$exp_date) {
      if (trim($relDate)) {
            return (date_greater($relDate,$exp_date)) ? $relDate : false;
          }
    }*/

    // Update Promotion ACTIVATE/de-activate details
     public function update_promotion_details($data, $id = false) {
         $return = $this->db->where('promotion_id', $id)->update('ti_promotion', $data);
         // echo  $this->db->last_query(); die;
         return $return;
     }
     // Update users details
     public function update_users_details($data, $id = false) {
         $return = $this->db->where('id', $id)->update('ti_users', $data);
         // echo  $this->db->last_query(); die;
         return $return;
     }
     // Inactive user get barcode for released
     public function userd_barcode($pid){
       $return = $this->db->select('user_id,barcode')->where('user_id', $pid)->get('participated_users')->result_array();
       // echo  $this->db->last_query(); die;
       return $return;
     }
     // Inactive user Remove barcode
     public function remove_barcode($pid,$val){
       $array  = array('user_id' => $pid, 'barcode' => $val);
       // $data = array('barcode_status' => '0');
        $return = $this->db->set('barcode_status' ,'0')->where($array)->update('participated_users');
        return $return;
     }

     public function update_promotion($data, $id) {
       // Update Promotion Details
         $return = $this->db->where('id', $id)->update('ti_promotion', $data);
         return $return;
     }
     // Add New Promotion Details
     public function add_promotion($data) {

         $this->db->insert('ti_promotion', $data);
         $res = $this->db->insert_id();
         return ($res) ? $res : false;
     }

  }
