<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Push_notification {

   private $ci;
   public function __construct() {
       $this->ci = & get_instance();
   }


   public function send_notification($data){

		//FCM ids of user

   		$fcm_ids_array = $this->get_fcm_ids($data['notified_to']);

		$registrationIds = $fcm_ids_array;
	//	prd($registrationIds);
		//save notification
		$notification_data = array();
		$notification_data['order_number'] = $data['order_id'];
		$notification_data['notified_to'] = $data['notified_to'];
		$notification_data['requested_by'] = $data['requested_by'];
		$notification_data['type'] = $data['type'];
		$notification_data['title'] = $data['title'];
		$notification_data['message'] = $data['message'];
		$notification_data['creation_date'] = date('Y-m-d H:i:s');

		$notification_id = $this->save_notification($notification_data);

		// prepare the message
		$msg = array('title' => $data['title'], 'image'=>$data['image'], 'message' => $data['message'], 'type' => $data['type'], 'order_id' => $data['order_id'], 'notification_id' => $notification_id, 'notified_to' =>$data['notified_to'], 'reason'=>$data['reason']);
		//android notification
		if(sizeof($registrationIds['android'])>0){
			$fields = array('registration_ids' => $registrationIds['android'], 'data' => $msg);
			$this->send_android_notification($fields);
		}
		//ios notification
		if(sizeof($registrationIds['ios'])>0){
			foreach ($registrationIds['ios'] as $val){
				$this->send_ios_notification($val, $msg);
			}
		}
	}

	private function get_fcm_ids($user_id){
		$query = $this->ci->db->select('deviceType,fcmId')->where(array('user_id'=>$user_id, 'is_logged_in'=>'1'))->get('device_info')->result();
		$ids = array();
		$and_ids = array();
		$ios_ids = array();
		foreach ($query as $row){
			//array_push($ids, $row->fcmId);
			if(strtolower($row->deviceType)=='android'){
				array_push($and_ids, $row->fcmId);
			}else{
				array_push($ios_ids, $row->fcmId);
			}
		}
		$ids['android'] = $and_ids;
		$ids['ios'] = $ios_ids;
		return $ids;
	}

	private function save_notification($data){
		$query = $this->ci->db->insert('notification', $data);
		return $this->ci->db->insert_id();
	}


	/*
	 function name: send_ios_notification
	 @param: deviceToken
	 @param: message
	 */
	 function send_ios_notification($fcmToken,$message){

		$passphrase = 'PushChat';
		$ctx = stream_context_create();
	       stream_context_set_option($ctx, 'ssl', 'local_cert', $_SERVER['DOCUMENT_ROOT'].'/admin/application/libraries/ProAPNS.pem');  //production
	//	stream_context_set_option($ctx, 'ssl', 'local_cert', 'Tiplure.pem'); //development
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
		// Open a connection to the APNS server
		$fp = stream_socket_client(
				//'ssl://gateway.sandbox.push.apple.com:2195', $err,  // For development
				 'ssl://gateway.push.apple.com:2195', $err, // for production
				$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
				var_dump(stream_socket_client(
					//'ssl://gateway.sandbox.push.apple.com:2195', $err,  // For development
					 'ssl://gateway.push.apple.com:2195', $err, // for production
					$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx));
		if (!$fp)
			exit("Failed to connect: $err $errstr" . PHP_EOL);

			//echo 'Connected to APNS' . PHP_EOL;
			// Create the payload body


			/*$body['aps'] = array(
					'alert' => trim($message['message']),
					'sound' => 'default',
					'content-available' => 1,
					'badge' => '0'
			);*/


		    $body['aps'] = array(
						"alert" => array("title"=> $message['title'],"body"=>trim($message['message']) ),
			            'sound' => 'default',
						"mutable-content" => 1,
						"badge" => 1,
			            'category'=> 'myCategory',
			            'content-available' => 1,
		               );
			$body['title'] = $message['title'];
			$body['type'] = $message['type'];
			$body['order_id'] = $message['order_id'];
			$body['notification_id'] = $message['notification_id'];
			$body['notified_to'] = $message['notified_to'];
			$body['reason'] = $message['reason'];
		    $body['attachment'] = $message['image'];
			// Encode the payload as JSON
			$payload = json_encode($body);
			// Build the binary notification
			$msg = chr(0) . pack('n', 32) . pack('H*', trim($fcmToken)) . pack('n', strlen($payload)) . $payload;
			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));
			if (!$result){
				//echo 'Message not delivered' . PHP_EOL;
			}
			else
			{
				//echo 'Message successfully delivered' . PHP_EOL;
				return $result;
			}
			// Close the connection to the server
			fclose($fp);

	}

	function send_android_notification($fields){
		//FCM credentials
	     	$fcmApiKey = FCM_API_KEY;
		$url = FCM_API_URL;

		$headers = array(
				'Authorization: key=' . $fcmApiKey,
				'Content-Type: application/json'
		);

		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, $url );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec( $ch );

		// Execute post

		if ($result === FALSE) {
			die('Curl failed: ' . curl_error($ch));
		}
		// Close connection
		curl_close($ch);

		return json_decode($result);

	}

}
