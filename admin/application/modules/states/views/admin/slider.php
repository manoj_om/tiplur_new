<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <?php if(!empty($states)): 
        $all_images = explode('<@>', $states->images);  
    ?>
    
	<!-- Wrapper for slides -->
    <div class="carousel-inner">
	<?php  $i=1;
	       foreach($all_images as $img): 
	      
	      
	?>
		<div class="item <?php if($i){echo 'active'; $i=0;} ?>">
			<img class="d-block img-fluid img-responsive" src="<?php echo img_src("assets/uploads/states/".$states->id."/",$img);?>">
		</div>
		<?php endforeach;  ?>
    </div>

	<?php if(count($all_images) > 1): ?>
    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
	<?php endif; 
              else:
                echo "Not Available";
   endif; ?>
  </div>

