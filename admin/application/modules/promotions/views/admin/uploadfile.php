<style>
    .ScrollStyle
    {
        max-height: 500px;
        overflow-y: scroll;
    }
</style>
<div id="page-wrapper" class="gray-bg dashbard-1">
    <?php breadcrumbs(array('admin/promotions' => 'Manage Promotion', 'admin/promotions/uploadfile' => 'View Promotions Data')); ?>

    <div class="row border-bottom">
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <?php print_flash_message(); ?>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h4>List of Products:</h4>
                    </div>
                    <?php if (empty($error_data)): ?>
                        <div class="well1 form-horizontal1 ibox-content borderNone ">
                            <?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
                            <div class="scrollit  ScrollStyle" style="margin-bottom: 30px;">
                                <table class="table">
                                    <thead>
                                        <tr class="textAlign">
                                            <th class="table-header">Promotion ID <span class="text-danger">*</span></th>
                                            <th class="table-header">Brand name <span class="text-danger">*</span></th>
                                            <th class="table-header">Promotion Description </th>
                                            <th class="table-header">Promotion Title <span class="text-danger">*</span></th>
                                            <th class="table-header">Promotion Image </th>
                                            <th class="table-header">Promotion Banner </th>
                                            <th class="table-header">Promotion Released <span class="text-danger">*</span></th>
                                            <th class="table-header">Promotion Released Time<span class="text-danger">*</span></th>
                                            <th class="table-header">Promotion Expired <span class="text-danger">*</span></th>
                                            <th class="table-header">Promotion Expired Time<span class="text-danger">*</span></th>
                                            <th class="table-header">Promotion Reminder Time<span class="text-danger">*</span></th>
                                            <th class="table-header">Pincode <span class="text-danger">*</span></th>
                                            <th class="table-header">City <span class="text-danger">*</span></th>
                                            <th class="table-header">State </th>
                                            <th class="table-header">Brands BarCode 750ml <span class="text-danger">*</span></th>
                                            <th class="table-header">Brands BarCode 375ml<span class="text-danger">*</span></th>
                                            <th class="table-header">Brands BarCode 180ml<span class="text-danger">*</span></th>
                                            <th class="table-header">Rewards & Gifts </th>
                                            <th class="table-header">Customer Care No.</th>
                                            <th class="table-header">Rewards Point 750ml</th>
                                            <th class="table-header">Rewards Point 375ml</th>
                                            <th class="table-header">Rewards Point 180ml</th>
                                          </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if (!empty($values)): $i = 0;
                                            foreach ($values as $key => $val): //echo "<pre>"; print_r($values); die('die');
                                                ?>
                                                <tr class = "table-rowd " >
                                                    <td>
                                                        <textarea rows="2" cols="150" required class="form-control inputWidhtOnewTwenty <?php echo (isset($val['promotion_id']) && $val['promotion_id'] != '#') ? '' : 'field-required'; ?>" name='promotion_id[<?php echo $i; ?>]' ><?php echo (isset($val['promotion_id']) && $val['promotion_id'] != '#') ? trim($val['promotion_id']) : ''; ?></textarea>
                                                        <span class='text-danger'><?php echo form_error('promotion_id[' . $i . ']'); ?></span>
                                                    </td>
                                                    <td>
                                                        <textarea rows="2" cols="150" required class="form-control inputWidhtOnewTwenty <?php echo (isset($val['brand_name']) && $val['brand_name'] != '#') ? '' : 'field-required'; ?>" name='brand_name[<?php echo $i; ?>]' ><?php echo (isset($val['brand_name']) && $val['brand_name'] != '#') ? trim($val['brand_name']) : ''; ?></textarea>
                                                        <span class='text-danger'><?php echo form_error('brand_name[' . $i . ']'); ?></span>
                                                    </td>
                                                    <td>
                                                        <textarea rows="2" cols="150" required class="form-control inputWidhtOnewTwenty <?php echo (isset($val['promotion_desc']) && $val['promotion_desc'] != '#') ? '' : 'field-required'; ?>" name='promotion_desc[<?php echo $i; ?>]' ><?php echo (isset($val['promotion_desc']) && $val['promotion_desc'] != '#') ? trim($val['promotion_desc']) : ''; ?></textarea>
                                                        <span class='text-danger'><?php echo form_error('promotion_desc[' . $i . ']'); ?></span>
                                                    </td>

                                                    <td>
                                                        <textarea rows="2" cols="150" required class="form-control inputWidhtOnewTwenty <?php echo (isset($val['promotion_title']) && $val['promotion_title'] != '#') ? '' : 'field-required'; ?>" name='promotion_title[<?php echo $i; ?>]' ><?php echo (isset($val['promotion_title']) && $val['promotion_title'] != '#') ? trim($val['promotion_title']) : ''; ?></textarea>
                                                        <span class='text-danger'><?php echo form_error('promotion_title[' . $i . ']'); ?></span>
                                                    </td>

                                                    <td>
                                                      <textarea rows="2" cols="150" required class="form-control inputWidhtOnewTwenty <?php echo (isset($val['promotion_img']) && $val['promotion_img'] != '#') ? '' : 'field-required'; ?>" name='promotion_img[<?php echo $i; ?>]' ><?php echo (isset($val['promotion_img']) && $val['promotion_img'] != '#') ? trim($val['promotion_img']) : ''; ?></textarea>
                                                      <span class='text-danger'><?php echo form_error('promotion_img[' . $i . ']'); ?></span>
                                                    </td>

                                                    <td>
                                                      <textarea rows="2" cols="150" required class="form-control inputWidhtOnewTwenty <?php echo (isset($val['prmototion_banner_url']) && $val['prmototion_banner_url'] != '#') ? '' : 'field-required'; ?>" name='prmototion_banner_url[<?php echo $i; ?>]' ><?php echo (isset($val['prmototion_banner_url']) && $val['prmototion_banner_url'] != '#') ? trim($val['prmototion_banner_url']) : ''; ?></textarea>
                                                      <span class='text-danger'><?php echo form_error('prmototion_banner_url[' . $i . ']'); ?></span>
                                                    </td>

                                                    <td >
                                                        <input type='text' required class="form-control inputWidhtOneEighty <?php echo (isset($val['released_date']) && $val['released_date'] != '#') ? '' : 'field-required'; ?>" name='released_date[<?php echo $i; ?>]' value='<?php echo (isset($val['released_date']) && $val['released_date'] != '#') ? $val['released_date'] : ''; ?>'>
                                                        <span class='text-danger'><?php echo form_error('released_date[' . $i . ']'); ?></span>
                                                    </td>
                                                    <td>
                                                        <input type='text' required class="form-control inputWidhtOneEighty <?php echo (isset($val['released_time']) && $val['released_time'] != '#') ? '' : 'field-required'; ?>" name='released_time[<?php echo $i; ?>]' value='<?php echo (isset($val['released_time']) && $val['released_time'] != '#') ? $val['released_time'] : ''; ?>'>
                                                        <span class='text-danger'><?php echo form_error('released_time[' . $i . ']'); ?></span>
                                                    </td>
                                                    <td>
                                                        <input type='text' required class="form-control inputWidhtOneEighty <?php echo (isset($val['expiry_date']) && $val['expiry_date'] != '#') ? '' : 'field-required'; ?>" name='expiry_date[<?php echo $i; ?>]' value='<?php echo (isset($val['expiry_date']) && $val['expiry_date'] != '#') ? $val['expiry_date'] : ''; ?>'>
                                                        <span class='text-danger'><?php echo form_error('expiry_date[' . $i . ']'); ?></span>
                                                    </td>
                                                    <td>
                                                        <input type='text' required class="form-control inputWidhtOneEighty <?php echo (isset($val['reminder_time']) && $val['reminder_time'] != '#') ? '' : 'field-required'; ?>" name='reminder_time[<?php echo $i; ?>]' value='<?php echo (isset($val['reminder_time']) && $val['reminder_time'] != '#') ? $val['reminder_time'] : ''; ?>'>
                                                        <span class='text-danger'><?php echo form_error('reminder_time[' . $i . ']'); ?></span>
                                                    </td>
                                                    <td>
                                                        <input type='text' required class="form-control inputWidhtOneEighty <?php echo (isset($val['expiry_time']) && $val['expiry_time'] != '#') ? '' : 'field-required'; ?>" name='expiry_time[<?php echo $i; ?>]' value='<?php echo (isset($val['expiry_time']) && $val['expiry_time'] != '#') ? $val['expiry_time'] : ''; ?>'>
                                                        <span class='text-danger'><?php echo form_error('expiry_time[' . $i . ']'); ?></span>
                                                    </td>
                                                    <td>
                                                        <input type='text'  class="form-control inputWidhtOneEighty <?php echo (isset($val['pincode']) && $val['pincode'] != '#') ? '' : 'field-required'; ?>" name='pincode[<?php echo $i; ?>]' value='<?php echo (isset($val['pincode']) && $val['pincode'] != '#') ? $val['pincode'] : ''; ?>'>
                                                        <span class='text-danger'><?php echo form_error('pincode[' . $i . ']'); ?></span>
                                                    </td>
                                                    <td >
                                                        <input type='text' required class="form-control inputWidhtOneEighty <?php echo (isset($val['city']) && $val['city'] != '#') ? '' : 'field-required'; ?>" name='city[<?php echo $i; ?>]' value='<?php echo (isset($val['city']) && $val['city'] != '#') ? $val['city'] : ''; ?>'>
                                                        <span class='text-danger'><?php echo form_error('city[' . $i . ']'); ?></span>
                                                    </td>
                                                    <td>
                                                        <input type='text' required class="form-control inputWidhtOneEighty <?php echo (isset($val['state']) && $val['state'] != '#') ? '' : 'field-required'; ?>" name='state[<?php echo $i; ?>]' value='<?php echo (isset($val['state']) && $val['state'] != '#') ? $val['state'] : ''; ?>'>
                                                        <span class='text-danger'><?php echo form_error('state[' . $i . ']'); ?></span>
                                                    </td>


                                                    <td>
                                                        <textarea rows="2" cols="150" required class="form-control inputWidhtOnewTwenty <?php echo (isset($val['brand_barcode_750ml']) && $val['brand_barcode_750ml'] != '#') ? '' : 'field-required'; ?>" name='brand_barcode_750ml[<?php echo $i; ?>]' ><?php echo (isset($val['brand_barcode_750ml']) && $val['brand_barcode_750ml'] != '#') ? trim($val['brand_barcode_750ml']) : ''; ?></textarea>
                                                        <span class='text-danger'><?php echo form_error('brand_barcode_750ml[' . $i . ']'); ?></span>
                                                    </td>
                                                    <td>
                                                        <textarea rows="2" cols="150" required class="form-control inputWidhtOnewTwenty <?php echo (isset($val['brand_barcode_375ml']) && $val['brand_barcode_375ml'] != '#') ? '' : 'field-required'; ?>" name='brand_barcode_375ml[<?php echo $i; ?>]' ><?php echo (isset($val['brand_barcode_375ml']) && $val['brand_barcode_375ml'] != '#') ? trim($val['brand_barcode_375ml']) : ''; ?></textarea>
                                                        <span class='text-danger'><?php echo form_error('brand_barcode_375ml[' . $i . ']'); ?></span>
                                                    </td>
                                                    <td>
                                                        <textarea rows="2" cols="150" required class="form-control inputWidhtOnewTwenty <?php echo (isset($val['brand_barcode_180ml']) && $val['brand_barcode_180ml'] != '#') ? '' : 'field-required'; ?>" name='brand_barcode_180ml[<?php echo $i; ?>]' ><?php echo (isset($val['brand_barcode_180ml']) && $val['brand_barcode_180ml'] != '#') ? trim($val['brand_barcode_180ml']) : ''; ?></textarea>
                                                        <span class='text-danger'><?php echo form_error('brand_barcode_180ml[' . $i . ']'); ?></span>
                                                    </td>

                                                      <td>
                                                        <textarea rows="2" cols="150" required class="form-control inputWidhtOnewTwenty <?php echo (isset($val['gifts_rewards']) && $val['gifts_rewards'] != '#') ? '' : 'field-required'; ?>" name='gifts_rewards[<?php echo $i; ?>]' ><?php echo (isset($val['gifts_rewards']) && $val['gifts_rewards'] != '#') ? trim($val['gifts_rewards']) : ''; ?></textarea>
                                                        <span class='text-danger'><?php echo form_error('gifts_rewards[' . $i . ']'); ?></span>
                                                    </td>
                                                      <td>
                                                        <textarea rows="2" cols="150" required class="form-control inputWidhtOnewTwenty <?php echo (isset($val['customer_care_no']) && $val['customer_care_no'] != '#') ? '' : 'field-required'; ?>" name='customer_care_no[<?php echo $i; ?>]' ><?php echo (isset($val['customer_care_no']) && $val['customer_care_no'] != '#') ? trim($val['customer_care_no']) : ''; ?></textarea>
                                                        <span class='text-danger'><?php echo form_error('customer_care_no[' . $i . ']'); ?></span>
                                                    </td>

                                                    <td>
                                                        <textarea rows="2" cols="150" required class="form-control inputWidhtOnewTwenty <?php echo (isset($val['reward_points_750ml']) && $val['reward_points_750ml'] != '#') ? '' : 'field-required'; ?>" name='reward_points_750ml[<?php echo $i; ?>]' ><?php echo (isset($val['reward_points_750ml']) && $val['reward_points_750ml'] != '#') ? trim($val['reward_points_750ml']) : ''; ?></textarea>
                                                        <span class='text-danger'><?php echo form_error('reward_points_750ml[' . $i . ']'); ?></span>
                                                    </td>
                                                    <td>
                                                        <textarea rows="2" cols="150" required class="form-control inputWidhtOnewTwenty <?php echo (isset($val['reward_points_375ml']) && $val['reward_points_375ml'] != '#') ? '' : 'field-required'; ?>" name='reward_points_375ml[<?php echo $i; ?>]' ><?php echo (isset($val['reward_points_375ml']) && $val['reward_points_375ml'] != '#') ? trim($val['reward_points_375ml']) : ''; ?></textarea>
                                                        <span class='text-danger'><?php echo form_error('reward_points_375ml[' . $i . ']'); ?></span>
                                                    </td>
                                                    <td>
                                                        <textarea rows="2" cols="150" required class="form-control inputWidhtOnewTwenty <?php echo (isset($val['reward_points_180ml']) && $val['reward_points_180ml'] != '#') ? '' : 'field-required'; ?>" name='reward_points_180ml[<?php echo $i; ?>]' ><?php echo (isset($val['reward_points_180ml']) && $val['reward_points_180ml'] != '#') ? trim($val['reward_points_180ml']) : ''; ?></textarea>
                                                        <span class='text-danger'><?php echo form_error('reward_points_180ml[' . $i . ']'); ?></span>
                                                    </td>
                                                </tr>
                                                <?php
                                                $i++;
                                            endforeach;
                                        endif;
                                        ?>
                                        </td>
                                    </tbody>
                                </table>
                            </div>
                            <?php if (!empty($values)): ?>
                                <input type='submit' name='upload' class='btn btn-primary createuser' value="Proceed Excel" /> &nbsp;OR&nbsp;
                            <?php endif; ?>
                            <a  href="<?php echo site_url('/admin/promotions/upload'); ?>" class="btn btn-danger" style="padding: 5px 30px;">Cancel</a>
                            <?php echo form_close(); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
