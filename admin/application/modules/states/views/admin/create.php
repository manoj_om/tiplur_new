<div id="page-wrapper" class="gray-bg dashbard-1">
    <!--Breadcrumbs -->
    <?php breadcrumbs(array('admin/states/create' => 'Add State'));$is_permit= is_permit(); ?>
    <div class="row border-bottom">
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <?php echo form_open_multipart($this->uri->uri_string(), 'class="form-horizontal"'); ?>
                <div class="ibox float-e-margins">
                    <div class="ibox-title addCatH1">
                        <h1>Add State</h1>
                        <div class="ibox-tools">
                        </div>
                    </div>
                    <div class="ibox-content contentBorder ">
                        <div class="row contMargin">

                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form-group formWidht">
                                    <label>State Name <span style="color: red;">*</span></label>
                                    <input class="form-control formWidht" type="text" placeholder="Name" value="<?php echo set_value('name', ''); ?>" name="name">
                                    <span class='error vlError'><?php echo form_error('name'); ?></span>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form-group formWidht">
                                    <label>Allowed Age Limit<span style="color: red;">*</span></label>
                                    <input class="form-control formWidht" type="text" placeholder="Age Limit" value="<?php echo set_value('age', ''); ?>" name="age">
                                    <span class='error vlError'><?php echo form_error('age'); ?></span>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form-group formWidht">
                                    <label>Opening Time<span style="color: red;">*</span> </label>
                                    <input type="text" placeholder="Opening Time" value="<?php echo set_value('opening_time', ''); ?>" name="opening_time" id="opening_time" class="form-control formWidht opening_time">
                                    <span class='error vlError'><?php echo form_error('opening_time'); ?></span>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form-group formWidht">
                                    <label>Closing Time<span style="color: red;">*</span></label>
                                    <input type="text" placeholder="Closing Time" name="closing_time" id="closing_time" value="<?php echo set_value('closing_time', ''); ?>" class="form-control formWidht closing_time">
                                    <span class='error vlError'><?php echo form_error('closing_time'); ?></span>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form-group formWidht">
                                    <label>Status<span style="color: red;">*</span></label>
                                    <select name="status" class="form-control">
                                        <?php
                                        $status = status();
                                        foreach ($status as $k => $val):
                                            ?>
                                            <option value="<?php echo $k; ?>" <?php echo ($k == @$_POST['status']) ? 'selected' : ''; ?>  ><?php echo $val; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <span class='error vlError'><?php echo form_error('status'); ?></span>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form-group formWidht">
                                    <label>Is available?<span style="color: red;">*</span></label>
                                    <select name="is_available" class="form-control">

                                        <?php

                                        $is_available= is_available();
                                        foreach ($is_available as $k => $val):
                                            ?>
                                            <option value="<?php echo $k; ?>" <?php echo ($k == @$_POST['is_available']) ? 'selected' : ''; ?>  ><?php echo $val; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <span class='error vlError'><?php echo form_error('is_available'); ?></span>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form-group formWidht">
                                    <label>Is permit ?<span style="color: red;">*</span></label>
                                    <select name="is_permit" class="form-control">

                                        <?php
                                          //print_r($is_permit()); die();
                                        $is_permit= is_permit();
                                        foreach ($is_permit as $k => $val):
                                            ?>
                                            <option value="<?php echo $k; ?>" <?php echo ($k == @$_POST['is_permit']) ? 'selected' : ''; ?>  ><?php echo $val; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <span class='error vlError'><?php echo form_error('is_permit'); ?></span>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form-group formWidht">
                                    <label>Shipping Charge <span style="color: red;">*</span></label>
                                    <input type="text" name="shipping_charge" id="shipping_charge" value="<?php echo set_value('shipping_charge', isset($result->shipping_charge) ? $result->shipping_charge : ''); ?>" class="form-control formWidht">
                                    <span class='error vlError'><?php echo form_error('shipping_charge'); ?></span>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form-group formWidht">
                                    <label>Internet Charge<span style="color: red;">*</span></label>
                                    <input type="text" placeholder="Internet charge" value="<?php echo set_value('internet_charge', isset($result->internet_charge) ? $result->internet_charge : ''); ?>" name="internet_charge" id="internet_charge" class="form-control formWidht">
                                    <span class='error vlError'><?php echo form_error('internet_charge'); ?></span>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12">    
                             <h2 style="display: inline-block;">Shipping Details</h2>
                         </div>
                            <div class="col-md-6" id="append_shipping">
                                <div class="col-lg-5 col-md-5 col-sm-5">
                                    <div class="form-group formWidht">
                                        <label>Amount <span style="color: red;">*</span></label>
                                        <input type="text" value="<?php echo set_value('amount', isset($result->amount) ? $result->amount : ''); ?>" name="amount[]" id="amount" class="form-control formWidht">
                                        <span class='error vlError'><?php echo form_error('amount[]'); ?></span>
                                    </div>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-5">
                                    <div class="form-group formWidht">
                                        <label>Shipping Charge <span style="color: red;">*</span></label>
                                        <input type="text" value="<?php echo set_value('shipping_charge', isset($result->shipping_charge) ? $result->shipping_charge : ''); ?>" name="shipping_charge[]" id="shipping_charge" class="form-control formWidht">
                                        <span class='error vlError'><?php echo form_error('shipping_charge'); ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 text-center" style="top: 25px">
                                <div>
                                    <a href="javascript:void(0)" class="btn btn-primary holidaybutton add_shipping" ><span class="glyphicon glyphicon-plus-sign" style="margin-right:4px;"></span>   Add More Inputs</a>
                                </div>
                            </div>
                               <!--                          <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form-group formWidht">
                                    <label>Max Weight </label>
                                    <input class="form-control formWidht" type="text" placeholder="max limit weight" value="<?php echo set_value('max_limit_weight', ''); ?>" name="max_limit_weight" maxlength="6">
                                    <span class='error vlError'><?php echo form_error('max_limit_weight'); ?></span>
                                </div>                                 
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form-group formWidht">
                                    <label>Brand Name</label>
                                    <select name="brand_name[]" id="brand_name" class="form-control" multiple>
                                        <option value="IMPORTED" <?php echo (in_array('IMPORTED', @$_POST['brand_name']) ) ? 'selected' : ''; ?> >IMPORTED</option>
                                        <option value="WINE" <?php echo (in_array('WINE', @$_POST['brand_name']) ) ? 'selected' : ''; ?> >WINE</option>
                                        <option value="DOMESTIC" <?php echo (in_array('DOMESTIC', @$_POST['brand_name']) ) ? 'selected' : ''; ?>>DOMESTIC</option>
                                        <option value="BEER" <?php echo (in_array('BEER', @$_POST['brand_name']) ) ? 'selected' : ''; ?>>BEER</option>
                                    </select>
                                </div>                                 
                            </div> -->
                        </div>
                        <div class="ibox-content contentBorder">
                            <div class="col-lg-12 col-md-12 col-sm-12 text-right">
                                <input type="submit" class="btn btn-primary block full-width m-b updateProductBtn" name="save" value="ADD STATE"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>
</div>


<!-- Shipping add remove-->
<div id="shipping_ch" style="display:none;">
    <div class="rm">
        <div class="col-lg-5 col-md-5 col-sm-5">
            <div class="form-group formWidht">
                <label>Amount <span style="color: red;">*</span></label>
                <input type="text" value="<?php echo set_value('amount'); ?>" name="amount[]" id="amount" class="form-control formWidht">
                <span class='error vlError'><?php echo form_error('amount[]'); ?></span>
            </div>
        </div>
        <div class="col-lg-5 col-md-5 col-sm-5">
            <div class="form-group formWidht">
                <label>Shipping Charge <span style="color: red;">*</span></label>
                <input type="text" value="<?php echo set_value('shipping_charge'); ?>" name="shipping_charge[]" id="shipping_charge" class="form-control formWidht">
                <span class='error vlError'><?php echo form_error('shipping_charge[]'); ?></span>
            </div>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-2 addProdctInputCont " style="top: 25px">
            <a href="javascript:void(0)" class="btn btn-primary holidaybutton remove_holiday" >X</a>
        </div>
    </div>
</div>