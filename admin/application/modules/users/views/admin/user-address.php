<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title"><?php echo (!empty($store_detail)) ? 'Store Detail' : 'User Address'; ?></h4>
</div>


<div class="modal-body">
<div class="alert alert-success time_msg" style="display:none;"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> Store Time Updated Successfully.</div>
    <?php if (!empty($store_detail)):
          $payment_mode=explode(',',$store_detail->payment_mode);
          $overlap = array_intersect($payment_mode, array('4','6','7','8'));
          $counts  = array_count_values($overlap);
	        $userdata = user_data($store_detail->retailer_id);
	?>
        <table class="table table-striped table-bordered" border="1">
            <thead>
                <tr>
                    <th>License No.</th>
                    <th>Store Name</th>
                    <th>Contact Person</th>
                    <th>Phone Number</th>
                    <th>Store Timing</th>
                     <th>Payment Mode</th> 
                    <th>Min Order</th>
                    <th>Provide Snacks</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?php echo $store_detail->licence; ?></td>
                    <td><?php echo $store_detail->store_name; ?></td>
                    <td><?php echo $store_detail->contact_person; ?></td>
                    <td><?php echo $store_detail->contact_person_mobile; ?></td>
                    <td><?php echo $store_detail->opening_time . "--" . $store_detail->closing_time; ?></td>
                    <td><?php echo payment_option($store_detail->payment_mode)->title;?></td>
                    <td><?php echo $store_detail->min_order; ?></td>
                    <td ><?php echo ($store_detail->is_provide_snacks == 1) ? 'YES' : 'NO'; ?></td>
                </tr>
            </tbody>
        </table>

        <form method="post" id="time-edit-form">
		<div class="row contMargin EDIT-TIIME" id="<?php echo $store_detail->id; ?>" style="display:none;">
		   <div class="col-lg-5 col-md-5 col-sm-5">
			  <label>Opening Time<span style="color: red;">*</span> </label>
		      <input type="text" placeholder="Opening Time" value="<?php echo set_value('opening_time', isset($store_detail->opening_time) ? $store_detail->opening_time : ''); ?>" name="opening_time" id="opening_time" class="form-control formWidht opening_time">
			</div>
            <div class="col-lg-5 col-md-5 col-sm-5">
               <label>Closing Time<span style="color: red;">*</span></label>
		       <input type="text" placeholder="Closing Time" name="closing_time" id="closing_time" value="<?php echo set_value('closing_time', isset($store_detail->closing_time) ? $store_detail->closing_time : ''); ?>" class="form-control formWidht closing_time">
			</div>
    </br>

         <div class="col-lg-5 col-md-5 col-sm-5">
         <label>Payment Mode<span style="color: red;">*</span> </label>
 </br>
         <input type="radio" name="check" <?php echo count($counts)>0 ?'checked':'';?> value="offline" onchange="ShowTimes('timehide',true)"> COD offline
         <div id="timehide" <?php echo count($counts)>0?'style="display:block"':'style="display:none"';?>>
             <input type="checkbox" name="ad_pack_id1[]" <?php echo (in_array('4',$payment_mode))?'checked':'';?> value="4" > CASH<br>
                 <input type="checkbox" name="ad_pack_id1[]" <?php echo (in_array('6',$payment_mode))?'checked':'';?>  value="6" > UPI/Wallet<br>
                 <input type="checkbox" name="ad_pack_id1[]" <?php echo (in_array('7',$payment_mode))?'checked':'';?>  value="7">  CARD-MASTER/VISA<br>
                 <input type="checkbox" name="ad_pack_id1[]" <?php echo (in_array('8',$payment_mode))?'checked':'';?>  value="8"> CARD-AMEX<br>
             </div>
         <br>
         <input type="radio" name="check" <?php echo (in_array('9',$payment_mode))?'checked':'';?> value="online" onchange="ShowTimes('timehide',false)"> Payment Gateway
         <br>
             <br>


 </div>
			<input type="hidden" value="<?php echo $store_detail->id; ?>" name="store_id"/>
			<input type="hidden" value="<?php echo $store_detail->retailer_id; ?>" name="retailer_id"/>

		</div>


        <hr>
        <h4>Address Detail</h4>
    <?php endif; ?>
    <ol>
        <?php

        if($addresses){
		        foreach ($addresses as $address): ?>
		            <li><strong><?php echo $address->address . "," . $address->city . "," . $address->state . "," . $address->country . "," . $address->pincode; ?></strong></li>
		        <?php endforeach;
        	}else{
        		echo "No address found.";
        	}
        ?>
    </ol>
</div>
<div class="modal-footer">
    <?php if(!empty($store_detail) && $userdata->status!='9' && $userdata->status!='10'): ?>
      <button type="button" class="btn btn-default retailer_time_edit">Edit Time & Payment Mode</button>
      <button type="button" class="btn btn-default retailer_time_update"  style="display:none;">Update Time & Payment Mode</button>
    <?php endif; ?>
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</form>
