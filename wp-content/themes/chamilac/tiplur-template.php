<?php
/*
  Template Name: tiplur-template
 */
?>
<!DOCTYPE html>
<html>
    <head>
        <meta name="google-site-verification" content="g7WWubPtWoXWRsireLKl_cuiumdfjXfupNEYZWZihvk" />
        <!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '1885261248382602');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=1885261248382602&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

        <title>Tiplur - discover licensed liquor store in your vicinity - app for liquor request</title>
        <meta name="Keywords" content="discover licensed liquor store in your vicinity - Alcohol delivery platform - Online licensed alcohol store app"/>
        <meta name="Description" content="Tiplur is a mobile platform which discovers and connects a user to licensed liquor
         vends in their vicinity. The good things in life have just gotten better. Unique & smart way to get connected to
         nearest licensed liquor retail Store. "/>
        <meta name="expires" http-equiv="Expires" content="Never">
        <meta http-equiv="CACHE-CONTROL" content="PUBLIC">
        <meta name="Copyright" content="Tiplur Beverage Technology Enabler Private Limited">
        <meta name="Publisher" content="http://tiplur.in">
        <meta name="Revisit-After" content="2 days">
        <meta name="classification" content="Website information">
        <meta name="distribution" content="Global">
        <meta name="Robots" content="INDEX, FOLLOW">
        <meta name="city" content="Gurgaon, Bengaluru">
        <meta name="country" content="India">
        <meta name="Audience" content="All, Business">
        <meta name="YahooSeeker" content="index, follow"/>
        <meta name="msnbot" content="index, follow"/>
        <meta name="googlebot" content="index, follow"/>
        <meta charset="utf-8" />
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- CSS
            ================================================== -->
        <!-- Bootstrap 3-->
        <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <!-- Template Styles -->
        <link href="<?php echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet" media="screen">
        <!-- Global site tag (gtag.js) - Google Analytics -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-170892151-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-170892151-1');
</script>
    <meta name="p:domain_verify" content="359a84894fb262fc6d2035fe8bc9e6d8"/>
    </head>
    <body>

        <!-- NAVBAR
            ================================================== -->
        <nav class="navbar navbar-default" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!--Replace text with your app name or logo image-->
                    <a class="navbar-brand" onclick="$('header').animatescroll({padding: 0});"><img src="<?php echo wp_upload_dir()['baseurl']; ?>/2018/01/tiplurlogo.png" alt="Tiplur Logo" class="img-responsive"></a>

                </div>
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav mobilemenu">
                        <li><a onclick="$('.about').animatescroll({padding: 70});">About Tiplur</a></li>
                        <li><a onclick="$('.category').animatescroll({padding: 70});">Category</a></li>
                        <li><a onclick="$('.works').animatescroll({padding: 70});">How it Work's	</a></li>
                        <li><a class="contact" onclick="$('.contactbox').animatescroll({padding: 70});">Contact Us</a></li>
                    </ul>
                </div>
            </div>
        </nav>


        <!-- HEADER
        ================================================== -->
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="pl20px">
                            <h1>The good things in life<br/> have just <strong>gotten better.</strong></h1>
                            <p class="lead">And this is just the <strong>START</strong></p>
                            <div class="store">
                                <a href="http://bit.ly/2nnmZGV" target="_blank"> <img src="<?php echo wp_upload_dir()['baseurl']; ?>/2018/01/google-play.png" class="img-responsive google-store" /></a>
                                <img src="<?php echo wp_upload_dir()['baseurl']; ?>/2018/01/free.png" class="img-responsive arrow" />
                            </div>
                            <div class="store">
                                <a href="https://apple.co/2tNMEio" target="_blank"><img src="<?php echo wp_upload_dir()['baseurl']; ?>/2018/01/apple-play.png" alt="liquor delivery app" class="img-responsive google-store" /></a>
                                <img src="<?php echo wp_upload_dir()['baseurl']; ?>/2018/01/free.png" alt="alcohol delivery app" class="img-responsive arrow" />
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="right-image">
                <img src="<?php echo wp_upload_dir()['baseurl']; ?>/2018/01/mobile.png" alt="delivery of alcohol" class="img-responsive" />
            </div>
        </header>
        <!-- Feature
                              ================================================== -->

        <section class="feature">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="feature-box">
                            <div class="feature-icon">
                                <img src="<?php echo wp_upload_dir()['baseurl']; ?>/2018/01/discover-icon.png" alt="online delivery of liquor" />
                            </div>
                            <div class="feature-content">
                                <h3>Discover</h3>
                                <p>Discover licensed alcohol retail stores near you, choose one of them and explore what’s in store for you. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="feature-box">
                            <div class="feature-icon">
                                <img src="<?php echo wp_upload_dir()['baseurl']; ?>/2018/01/browse.png" alt="licensed alcohol store near me" />
                            </div>
                            <div class="feature-content">
                                <h3>Browse catalogue</h3>
                                <p>Browse the store's catalogue which is stacked category & sub category wise to help you make a wise decision. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="feature-box">
                            <div class="feature-icon">
                                <img src="<?php echo wp_upload_dir()['baseurl']; ?>/2018/01/select-req-icon.png" alt="licensed liquor near me" />
                            </div>
                            <div class="feature-content">
                                <h3>Select &amp; Check MRP</h3>
                                <p>Select products and check the government approved MRP, so that you don’t pay more than what government has approved</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- ABOUT
            ================================================== -->
        <section class="about">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h3 class="heading">What Is <strong>Tiplur</strong></h3>
                        <p class="about-content"> Tiplur is a mobile platform which discovers and connects a user to licensed liquor vends in their vicinity.</p>
                    </div>
                </div>
            </div>
        </section>


        <!-- BIG DEAL
        ================================================== -->
        <section class="bigdeal">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h3 class="heading">Is It a <strong>big deal</strong></h3>
                        <p class="lead deal">You decide. Tiplur makes it <strong> safer, smarter & simpler. </strong></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3 brd-right">
                        <div class="ques">
                            <h3>HOW</h3>
                            <h4>So?</h4>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <p class="lead">Well, buying liquor isn’t a very pleasant experience yet. At times, it’s not safe, simple or even comfortable. Not all licensed retail outlets in your neighborhood are in convenient locations. There’s the hassle of parking, being among unruly strangers (at times) and the prospect of driving back with your purchase. Now, that’s not a good idea. Beyond these, imagine a lady who wants to buy, she could be a woman having guests over, Now how about technology making this entire process safe, smart and simple for everyone?</p>
                    </div>
                </div>
            </div>
        </section>


        <!-- DETAILS
            ================================================== -->
        <section class="detail">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 hidden-xs">
                        <div id="carousel-example-generic-2" class="carousel slide ">
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                <div class="item active">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="<?php echo wp_upload_dir()['baseurl']; ?>/2018/01/slide1.jpeg" alt="online liquor delivery app" class="img-responsive">
                                        </div>
                                        <div class="col-sm-6 ">
                                            <img src="<?php echo wp_upload_dir()['baseurl']; ?>/2018/01/slide2.jpeg" alt="online alcohol delivery app" class="img-responsive">
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="<?php echo wp_upload_dir()['baseurl']; ?>/2018/01/slide4.jpeg" alt="beer shop near me" class="img-responsive">
                                        </div>
                                        <div class="col-sm-6 ">
                                            <img src="<?php echo wp_upload_dir()['baseurl']; ?>/2018/01/slide5.jpeg" alt="alcohol shop near me" class="img-responsive">
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="<?php echo wp_upload_dir()['baseurl']; ?>/2018/01/slide6.jpeg" alt="liquor shop near me" class="img-responsive">
                                        </div>
                                        <div class="col-sm-6 ">
                                            <img src="<?php echo wp_upload_dir()['baseurl']; ?>/2018/01/slide8.jpeg" alt="wine shop near me" class="img-responsive">
                                        </div>
                                    </div>
                                </div><!--

                                <div class="item">
                                        <div class="row">
                                                <div class="col-sm-6">
                                                        <img src="<?php //echo wp_upload_dir()['url'];     ?>/slide7.jpeg" alt="alcohol store near me" class="img-responsive">
                                                </div>
                                                <div class="col-sm-6 hidden-xs">
                                                        <img src="<?php //echo wp_upload_dir()['url'];     ?>/slide8.jpeg" alt="liquor store near me" class="img-responsive">
                                                </div>
                                        </div>
                                </div> -->
                            </div>
                            <!-- Controls -->
                            <a class="left carousel-control" href="#carousel-example-generic-2" role="button" data-slide="prev">
                                <img src="<?php echo wp_upload_dir()['baseurl']; ?>/2018/01/left-arrow.png" alt="wine store near me" class="img-responsive">
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic-2" role="button" data-slide="next">
                                <img src="<?php echo wp_upload_dir()['baseurl']; ?>/2018/01/right-arrow.png" alt="online alcohol delivery app" class="img-responsive">
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-12 visible-xs">
                        <div id="carousel-example" class="carousel slide ">
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img src="<?php echo wp_upload_dir()['baseurl']; ?>/2018/01/slide1.jpeg" alt="app for online liquor delivery" class="img-responsive">
                                </div>
                                <div class="item">
                                    <img src="<?php echo wp_upload_dir()['baseurl']; ?>/2018/01/slide2.jpeg" alt="online alcohol delivery shop" class="img-responsive">
                                </div>
                                <div class="item">
                                    <img src="<?php echo wp_upload_dir()['baseurl']; ?>/2018/01/slide4.jpeg" alt="liquor shop near me" class="img-responsive">
                                </div>
                                <div class="item">
                                    <img src="<?php echo wp_upload_dir()['baseurl']; ?>/2018/01/slide5.jpeg" alt="wine store near me" class="img-responsive">
                                </div>
                                <div class="item">
                                    <img src="<?php echo wp_upload_dir()['baseurl']; ?>/2018/01/slide6.jpeg" alt="alcohol store near me" class="img-responsive">
                                </div>
                                <div class="item">
                                    <img src="<?php echo wp_upload_dir()['baseurl']; ?>/2018/01/slide8.jpeg" alt="wine shop near me" class="img-responsive">
                                </div>
                            </div>
                            <!-- Controls -->
                            <a class="left carousel-control" href="#carousel-example" role="button" data-slide="prev">
                                <img src="<?php echo wp_upload_dir()['baseurl']; ?>/2018/01/left-arrow.png" alt="liquor shop near me" class="img-responsive">
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example" role="button" data-slide="next">
                                <img src="<?php echo wp_upload_dir()['baseurl']; ?>/2018/01/right-arrow.png" alt="online liquor delivery app" class="img-responsive">
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <!-- CATEGORY
      ================================================== -->
        <section class="category">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-4">
                        <div class="category-box">
                            <a href="javascript:void(0)" class="subcategory">
                                <div class="tiplur-category">
                                    <img src="<?php echo wp_upload_dir()['baseurl']; ?>/2018/01/whiskey.png" alt="online wine delivery app" class="img-responsive">
                                    <h4>Whisky</h4>
                                </div>
                            </a>
                            <div class="subcategory-box">
                                <ul>
                                    <li>Single Malt</li>
                                    <li>Blended Scotch Whisky</li>
                                    <li>Irish Whisky</li>
                                    <li>Canadian Whisky</li>
                                    <li>Bourbon</li>
                                    <li>Bottled in India Scotch</li>
                                    <li>Bottled in Indian Bourbon</li>
                                    <li>Indian Premium Whisky</li>
                                    <li>Indian Whisky</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4">
                        <div class="category-box">
                            <a href="javascript:void(0)" class="subcategory">
                                <div class="tiplur-category">
                                    <img src="<?php echo wp_upload_dir()['baseurl']; ?>/2018/01/beer.png" alt="online beer delivery app" class="img-responsive center-block">
                                    <h4>Beer</h4>
                                </div>
                            </a>
                            <div class="subcategory-box">
                                <ul>
                                    <li>Mild Beer</li>
                                    <li>Strong Beer</li>
                                    <li>Craft Beer</li>
                                    <li>Imported Beer</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4">
                        <div class="category-box">
                            <a href="javascript:void(0)" class="subcategory">
                                <div class="tiplur-category">
                                    <img src="<?php echo wp_upload_dir()['baseurl']; ?>/2018/01/vodka.png" alt="online vodka delivery app" class="img-responsive center-block">
                                    <h4>vodka</h4>
                                </div>
                            </a>
                            <div class="subcategory-box">
                                <ul>
                                    <li>Imported Vodka</li>
                                    <li>Indian Vodka</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4">
                        <div class="category-box">
                            <a href="javascript:void(0)" class="subcategory">
                                <div class="tiplur-category">
                                    <img src="<?php echo wp_upload_dir()['baseurl']; ?>/2018/01/rum.png" alt="online rum delivery store" class="img-responsive center-block">
                                    <h4>Rum</h4>
                                </div>
                            </a>
                            <div class="subcategory-box">
                                <ul>
                                    <li>Imported Rum</li>
                                    <li>Dark Rum</li>
                                    <li>White Rum</li>
                                    <li>Flavoured Rum</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4">
                        <div class="category-box">
                            <a href="javascript:void(0)" class="subcategory">
                                <div class="tiplur-category">
                                    <img src="<?php echo wp_upload_dir()['baseurl']; ?>/2018/01/gin.png" alt="wine store near me" class="img-responsive center-block">
                                    <h4>Gin</h4>
                                </div>
                            </a>
                            <div class="subcategory-box">
                                <ul>
                                    <li>Imported Gin</li>
                                    <li>Indian Gin</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4">
                        <div class="category-box">
                            <a href="javascript:void(0)" class="subcategory">
                                <div class="tiplur-category">
                                    <img src="<?php echo wp_upload_dir()['baseurl']; ?>/2018/01/whitewine.png" alt="wine store near me" class="img-responsive center-block">
                                    <h4>wine</h4>
                                </div>
                            </a>
                            <div class="subcategory-box">
                                <ul>
                                    <li>Champagne/Prosecco/Sparkling Wine</li>
                                    <li>Australia</li>
                                    <li>Argentina</li>
                                    <li>California</li>
                                    <li>Chille</li>
                                    <li>France</li>
                                    <li>Germany</li>
                                    <li>Italy</li>
                                    <li>Indian</li>
                                    <li>South African </li>
                                    <li>Spain</li>
                                    <li>New Zealand</li>
                                    <li>Portugal</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4">
                        <div class="category-box">
                            <a href="javascript:void(0)" class="subcategory">
                                <div class="tiplur-category">
                                    <img src="<?php echo wp_upload_dir()['baseurl']; ?>/2018/01/vermouth.png" alt="alcohol store app" class="img-responsive centre-block">
                                    <h4>Tequila</h4>
                                </div>
                            </a>
                            <div class="subcategory-box">
                                <ul>
                                    <li>Imported Tequila</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4">
                        <div class="category-box">
                            <a href="javascript:void(0)" class="subcategory">
                                <div class="tiplur-category">
                                    <img src="<?php echo wp_upload_dir()['baseurl']; ?>/2018/01/cognac.png" alt="online brandy store app" class="img-responsive center-block">
                                    <h4>COGNAC/BRANDY </h4>
                                </div>
                            </a>
                            <div class="subcategory-box">
                                <ul>
                                    <li>Cognac/Brandy</li>
                                    <li>Indian Brandy</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <!-- TIPLUR WORKS
        ================================================== -->
        <section class="bigdeal works">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h3 class="heading">So, how does TIPLUR <strong>Work?</strong></h3>
                        <p class="lead deal"><strong> Simply and efficiently. </strong></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3 brd-right">
                        <div class="ques">
                            <h3>HOW</h3>
                            <h4>Work?</h4>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <p class="lead">Open the app and it instantly connects you to the licensed liquor stores in your neighborhood. You select an outlet and see the entire catalogue (category and sub category wise along with prices). The prices are as approved by the
government.</p>
                    </div>
                </div>
            </div>
        </section>


        <!-- WHAT NEXT
        ================================================== -->
        <section class="bigdeal whatnext">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h3 class="heading">So, What <strong>Next?</strong></h3>
                        <p class="lead deal">Two Steps: <strong> One simple and one sensible. </strong></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3 brd-right">
                        <div class="ques">
                            <h3>Just 2</h3>
                            <h4>STEPS?</h4>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <p class="lead">1. Download the tiplur app from playstore &amp; app store.</p>
                        <p class="lead">2. Register with your details.<br/> It’s the same as any other app except that you need to upload a valid photo ID.</p>
                        <p class="note lead">(Regulation demands it, rightfully. So that it’s not misused by any person who is not under the state-mandated age limit.)</p>
                    </div>
                </div>
            </div>
        </section>

        <section class="static">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2">
                        <p class="lead">Tiplur is committed to strict adherence of all regulatory standards such as minimum age limit, ID proof, quantities etc, as per state norms.</p>
                    </div>
                </div>
            </div>
        </section>
        <section class="static mrgn-top">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2">
                        <p class="lead">‘ Tiplur Beverage Technology Enabler Pvt Ltd’ now recognized under start-up india, by DIPP of Ministry of Commerce and Industries, Government of India</p>
                    </div>
                </div>
            </div>
        </section>


        <!-- CONTACT
         ================================================== -->
        <section class="contactbox">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 contact-form">
                        <h3>Contact us</h3>
                        <?php echo do_shortcode('[contact-form-7 id="864" title="Contact form 1"]'); ?>
                        <!--<form class="contact-form">
                                <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" class="form-control" placeholder="Name">
                                </div>
                                <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" class="form-control" placeholder="Email">
                                </div>
                                <div class="form-group">
                                        <label>Contact No.</label>
                                        <input type="text" class="form-control" placeholder="Contact Number">
                                </div>
                                <div class="form-group">
                                        <label>Message</label>
                                        <textarea placeholder="Message" class="form-control"></textarea>
                                </div>
                                <div class="form-group">
                                        <button type="submit" class="btn btn-warning">Send Message</button>
                                </div>
                        </form> -->
                    </div>
                    <!-- <div class="col-sm-5">
                            <h3>Visit Us</h3>
                            <h4>Tiplur</h4>
                            <div class="location">
                                    <img src="<?php echo wp_upload_dir()['baseurl']; ?>/2018/01/location.png" alt="" class="img-responsive" />
                                    <span>1245, Spaze Park, Gurgaon 122001</span>
                            </div>
                    </div> -->
                </div>
            </div>
        </section>

        <!-- GET IT
            ================================================== -->
        <section class="get-it">
            <div class="container">
                <div class="row">
                    <hr />
                    <div class="col-sm-12">
                        <span class="copyright"><img src="<?php echo wp_upload_dir()['baseurl']; ?>/2018/01/tiplurlogo.png" alt="online liquor request app" class="img-responsive"> name &amp; mark are registered trademarks of Tiplur Beverage Technology Enabler Private Limited</span>
                    </div>
                </div>
                <!-- <div class="row">
                        <div class="col-sm-8 col-sm-offset-2">
                                <p class="lead message">Tiplur is committed to strict adherence of all regulatory standards such as minimum age limit, ID proof, quantities as per state norms etc.</p>
                        </div>
                </div> -->
            </div>
        </section>

        <!-- SELF DECLARATION -->
        <div class="subcategories">
            <div id="declare" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <img src="<?php echo wp_upload_dir()['baseurl']; ?>/2018/01/tiplurlogo.png" alt="online alcohol rquest app" class="img-responsive">
                        </div>
                        <div class="modal-body">
                            <div class="form-group checkbox text-center">
                                <label>
                                    <input type="checkbox" class="myCheck form-control"> I am above the state mandated age to access this site
                                </label>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-default agelimit" disabled="disabled" data-dismiss="modal" >Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<!-- popup image -->
        <div class="subcategories categoriesNoti">
            <div id="image_popup" class="modal fade" role="dialog">

                <div class="modal-dialog popup_image_dialog">
                    <!-- Modal content-->

                    <div class="modal-content content-image">
                      <div class="close">&times;</div>
                        <div class="desktopnotification">
                            <img src="<?php echo wp_upload_dir()['baseurl']; ?>/2020/06/popup.jpeg" alt="online alcohol rquest app" class="img-responsive popup_image">
                        </div>
                        <div class="mobilenotification">
                            <img src="<?php echo wp_upload_dir()['baseurl']; ?>/2020/06/toplur-mobile-notifaction.jpg" alt="online alcohol rquest app" class="img-responsive popup_image">
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- JAVASCRIPT
                ================================================== -->
        <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/js/animatescroll.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/js/scripts.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/js/retina.min.js"></script>

        <!-- SUBCATEGORY MODAL
                ================================================== -->
        <div class="subcategories">
            <div class="modal fade" id="category" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">X</button>
                            <h4 class="modal-title">Wine</h4>
                        </div>
                        <div class="modal-body"></div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </div>

<script type="text/javascript">
_linkedin_partner_id = "263499";
window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];
window._linkedin_data_partner_ids.push(_linkedin_partner_id);
</script><script type="text/javascript">
(function(){var s = document.getElementsByTagName("script")[0];
var b = document.createElement("script");
b.type = "text/javascript";b.async = true;
b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
s.parentNode.insertBefore(b, s);})();
</script>
<noscript>
<img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=263499&fmt=gif" />
</noscript>
    </body>
</html>
