<div id="page-wrapper" class="gray-bg dashbard-1">
<?php breadcrumbs(array('admin/notification'=>'Notification'));  ?>
<div class="row border-bottom">
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h2>View Notification</h2>
                </div>
                <div class="ibox-content borderNone">
                    <div class="table-responsive">
					<?php echo  form_open($this->uri->uri_string(), array('class'=>"form-horizontal",'id'=>'cat-listing','method'=>'get') ); ?>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Receiver Name</th>
                                    <th>Sender Name </th>
                                    <th>Message</th>
									 <th>Date</th>
                                    <th>Read / Unread</th>
                                </tr>
								</thead>
								<tbody>
								   <tr>
									<td>
										<select name="sname" class="tableDrop input-sm form-control input-s-sm inline">
										<option value=""  >Please select</option>
										<?php $check = (!empty($_GET['sname']))?$_GET['sname']:''; foreach($users_listing as $val): ?>
										<option value="<?php echo $val->id; ?>" <?php echo  ($val->id== @$check)?'selected':''; ?>  ><?php echo ucfirst(trim($val->display_name)); ?></option>
										<?php endforeach; ?>
										</select>
									</td>
									<td>
									<select name="rname" class="tableDrop input-sm form-control input-s-sm inline">
									    <option value=""  >Please select</option>
										<?php $check = (!empty($_GET['rname']))?$_GET['rname']:''; foreach($users_listing as $val): ?>
										<option value="<?php echo $val->id; ?>" <?php echo  ($val->id== @$check)?'selected':''; ?>  ><?php echo ucfirst(trim($val->display_name)); ?></option>
										<?php endforeach; ?>
										</select>
									</td>
									<td>
									
									</td>
									<td>
									<input class="tableDrop input-sm form-control input-s-sm inline form_date" type="text" name="from_date" id="from_date" value="<?php echo (isset($_GET['from_date']) && $_GET['from_date'])?$_GET['from_date']:''; ?>" Placeholder="start date">
									<input class="tableDrop input-sm form-control input-s-sm inline to_date" type="text" name="to_date" id="to_date" value="<?php echo (isset($_GET['to_date']) && $_GET['to_date'])?$_GET['to_date']:''; ?>" Placeholder="end date">
									</td>
									 
									<td>
									<select name="status" class="tableDrop input-sm form-control input-s-sm inline">
										<?php $check = (!empty($_GET['status']))?$_GET['status']:''; $status = array('' => 'Select status', 1=> 'Read',2 => 'Unread');; foreach($status as $k=>$val): ?>
									 <option value="<?php echo $k; ?>" <?php echo  ($k == $check)?'selected':''; ?>  ><?php echo $val; ?></option>
									 <?php endforeach; ?>
									</select>
									</td>
									<td>
									<input  type="submit" name="filter" id="" class="btn btn-primary sbmtFilter"value="Filter">
										<?php if (isset($_GET['filter'])): ?>
											<a  href="<?php echo base_url(SITE_AREA.'/notification'); ?>" class="btn btn-warning resetFilter">Reset</a>
										<?php endif; ?>
									</td>
								</tr>
                                <?php if(!empty($records)):  foreach($records as $notification): ?>
                                <tr class="<?php echo ($notification->seen == 0)?'readNotify':'unReadNotify';?>"> 
                                    <td><?php  echo ucfirst(($notification->notified_to_name)? $notification->notified_to_name :'--'); ?></td>
                                    <td><?php  echo ucfirst(($notification->notified_by_name)? $notification->notified_by_name :'--'); ?></td>
                                    <td><?php echo wordwrap($notification->message,30,"<br>\n"); ?></td>
									<td><?php echo db_date($notification->created_on);?></td> 
                                    <td class="text-center"><?php  echo ($notification->seen == 1)?'<i class="fa fa-inbox" aria-hidden="true"></i> ':'<i class="fa fa-envelope-o" aria-hidden="true"></i>';?></td>
                                </tr>
								 <?php endforeach; else: ?>
										   <tr><td colspan="12">Result not found.</td></tr>
								<?php endif; ?>
                            </tbody>
                        </table>
						</form>
                    </div>
                </div>
            </div>
        </div>
    </div>
  <div class="row">
	<div class="col-sm-5">
		<div class="dataTables_info tableData" id="editable_info" role="status" aria-live="polite"></div>
	</div>
	<?php echo $this->pagination->create_links(); ?>
</div>
</div>
</div> 