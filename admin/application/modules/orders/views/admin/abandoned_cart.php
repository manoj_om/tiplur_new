<div id="page-wrapper" class="gray-bg dashbard-1">
    <?php //breadcrumbs(array('admin/orders' => 'Order List')); ?>
    <div class="row border-bottom">

    </div>
    <!--<div class="row  border-bottom headWidth dashboard-header">
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <h2 class="salesHead">Order List</h2>
                <select class="tableDrop headingSelect input-sm form-control input-s-sm inline">
                    <option value="0">Action</option>
                    <option value="1">Option 2</option>
                    <option value="2">Option 3</option>
                    <option value="3">Option 4</option>
                </select>
            </div>
        </div>
    </div>-->
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">                
                <div class="ibox float-e-margins">
                    <div class="ibox-title borderNone">
                        <h2>Abandoned Cart Details </h2>
                    </div>
                    <div class="ibox-content borderNone">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>                                        
                                        <th>Customer Name</th>
										<th>Email</th>
										<th>Mobile</th>
                                        <th>Products</th>
                                        <th>Date</th>
                                        <th>Cart Total</th>                                        
                                    </tr>
                                </thead>
                                <tbody>                                
                                    <?php
                                    if (!empty($result)):
                                        foreach ($result as $row):                                            
                                            ?>
                                            <tr>
                                                <td><?php echo $row->display_name; ?></td>
												<td><?php echo $row->email; ?></td>
												<td><?php echo $row->mobile; ?></td>                                                
                                                <td>
												<ul><?php
                                                        $products = explode('<@>', $row->product);
                                                        $quantity = explode('<@>', $row->quantity);
                                                        $price = explode('<@>', $row->price);
														$weight = explode('<@>', $row->weight);
                                                        $i = 0;
														$total = 0;
                                                        foreach ($products as $product) {
								$total += $price[$i]; // * $quantity[$i];
								$actual_price = ($quantity[$i] > 1) ? $price[$i]/$quantity[$i]:$price[$i];
                                                            ?>
                                                            <li><?php echo $product . "($weight[$i])  ".intval($actual_price)." <strong>x</strong> " . $quantity[$i] . ' = Rs.' . ($price[$i]); ?></li>

                                                            <?php
                                                            $i++;
                                                        }
                                                        ?></ul></td>
                                                <td><?php echo date('d-M-Y',strtotime($row->created_on)); ?></td>         
                                                <td><?php echo 'Rs.' . $total; ?></td>
                                            </tr>
                                            <?php
                                        endforeach;
                                    else:
                                        echo "<tr><td colspan='12'>No Result Found</td></tr>";
                                    endif;
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <div class="dataTables_info tableData" id="editable_info" role="status" aria-live="polite"></div>
            </div>
            <?php echo $this->pagination->create_links(); ?>
        </div>
    </div>
</div>


