<?php

class Notification extends MX_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('notification_model');
		$this->load->library('form_validation');
		$this->set_data = array('theme' => 'admin'); //define theme name
		if (!$this->auth->_is_logged_in()) {
			redirect(base_url());
		}
	}
	
	// Get filter value
	function filter() {
		return $this->input->get(array('rname', 'sname', 'msg', 'status', 'from_date', 'to_date'));
	}
	
	function index() {
		// check users is login
		// start logic for paginaton
		//        $this->load->library('pagination');
		//        $per_page = isset($_GET['per_page']) ? $_GET['per_page'] : false;
		//        unset($_GET['per_page']);
		//        $query_string = http_build_query($_GET);
		//        $site_url = !$query_string ? base_url(SITE_AREA . '/notification') : base_url(SITE_AREA . '/notification?' . $query_string);
		//        if ($per_page) {
		//            $_GET['per_page'] = $per_page;
		//        }
		//        $num_rows = $this->notification_model->viewNotificationList($this->filter());
		//        $config = pagination_formatting();
		//        $limit = 10;
		//        $config['base_url'] = $site_url;
		//        $config['total_rows'] = $num_rows;
		//        $config['per_page'] = $limit;
		//        $config['num_links'] = 5;
		//        $config['use_page_numbers'] = TRUE;
		//        $config['page_query_string'] = TRUE;
		//        $this->pagination->initialize($config);
		//        $offset = isset($_GET['per_page']) ? ($_GET['per_page'] - 1) * $limit : null;
		//        $records = $getting = $this->notification_model->viewNotificationList($this->filter(), $limit, $offset, false);
		//        // user list for dropdown
		//        $data['users_listing'] = $this->notification_model->users_listing();
		//
		//        $data['records'] = $records;
		//        $data['css'] = array('/theme/admin/css/bootstrap-datetimepicker.css');
		//        $data['js'] = array('/assets/js/orders.js', '/theme/admin/js/moment.js', '/theme/admin/js/bootstrap-datetimepicker.js');
		//        $this->theme($data);
	}
	
	public function get_notification() {
		$badge_count = unseen_notification_count(current_user()['id']);
		$ajax_data['html'] = $this->load->view('admin/notification_listing', array('user_id' => current_user()['id']), true);
		$ajax_data['badge_count'] = $badge_count;
		echo json_encode($ajax_data);
	}
	
	function update_notification() {
		$this->db->where(array('notified_to' => current_user()['id'], 'seen' => '0'))->update('notification', array('seen' => '1'));
		echo "Notification successfully updated!";
		return true;
	}
	
	function send_notifications() {
		
		if (isset($_POST['save'])) {
			
			$this->form_validation->set_rules($this->notification_model->get_form_validation_rules());
			//
			if ($this->form_validation->run() === false) {
				set_flash_message($type = 'error', $message = 'Please enter all required details.');
			} else {
				
				if ($this->save_notification()) {
					set_flash_message($type = 'success', $message = 'Notification sent successfully.');
					
					redirect('admin/notification/send_notifications');
				}
			}
		}
		$data['js'] = array('/assets/js/notification.js');
		$this->theme($data);
	}
	
	private function save_notification() {
		
		$data = $this->input->post(array('title', 'type', 'message'));
		
		if(!empty($_FILES['image']['name'])):
            $data['image']  = $this->uploadNotificationImage();
        endif;
		$imageUrl = isset($data['image']) ? base_url()."assets/uploads/notification_images/".$data['image'] :'';
		
		
		//for testing
		/*
			$val = "4a1b434878c2fdabe4451b6121311846c8747b031b61785657b00e9ca5c625a4"; //for ios
			//$val   = array("c2r3yymenXc:APA91bGLoMw7Ra-uy3-4ycSkzhRBH4LpyHj3sR3MpPLxURJHBPY-ICu7Dwne3HIPDisFIQeNtyqts87hH_TjRnbPqndWrUNcGjWC1vPjmWdYHRvTRS3y17gU0udtArYXajv8VT6YBbedIW9DM6Yse6MC6aHob2iG8Q");  //for android
			$msg = array('title' => "hi", 'image'=>$imageUrl, 'message' => "Test Image notification", 'type' => "11", 'order_id' => "556", 'notification_id' => "6", 'notified_to' =>"4", 'reason'=>"test"); 
			$fields = array('registration_ids' => $val, 'data' => $msg);
            $notify = new Push_notification();
			$notify->send_ios_notification($val, $msg);
			//$notify->send_android_notification($fields);
		*/
		
		
		if ($data['type'] == 'user') {
			
			$this->db->join('ti_device_info', 'ti_device_info.user_id=ti_users.id', 'left');
			$users = $this->db->where(array('ti_users.role_id' => '3', 'ti_users.status' => '1', 'ti_users.deleted' => '0', 'is_logged_in' => '1'))->get('users')->result();
			
		} elseif ($data['type'] == 'retailer') {
			$this->db->join('ti_device_info', 'ti_device_info.user_id=ti_users.id', 'left');
			$users = $this->db->where(array('ti_users.role_id' => '2', 'ti_device_info.deviceType' => 'android', 'ti_users.status' => '1', 'ti_users.deleted' => '0', 'is_logged_in' => '1'))->get('users')->result();
			//echo $this->db->last_query();
		} else {
			
			$this->db->join('ti_device_info', 'ti_device_info.user_id=ti_users.id', 'left');
			$users = $this->db->where(array('ti_users.status' => '1', 'ti_users.deleted' => '0', 'is_logged_in' => '1'))->get('users')->result();
			
		}
		
		// prd($users);
		
		foreach ($users as $user) {
			
			$notify_data = array(
					'order_id' => 'General Notification',
					'notified_to' => $user->user_id,
					'requested_by' => $this->session->userdata('id'),
					'type' => GENERAL_NOTIFICATION,
					'title' => $data['title'],
					'message' => $data['message'],
					'reason' => 'General Notification',
				    'image' => $imageUrl
			);
			
			$notify = new Push_notification();
			$notify->send_notification($notify_data);
		}
		
		$id = 1;
		
		if ($id) {
			$return = $id;
		}
		
		return $return;
	}
	
	
	private function uploadNotificationImage()
	{
        $this->load->library('upload');
        $upload_path = FCPATH . "assets/uploads/notification_images/";
        $input_field_name = 'image';
        $config = array(
            'upload_path' => $upload_path,
            'allowed_types' => '*',
            'file_name' => time() . $_FILES[$input_field_name]['name'],
        );
        if (is_uploaded_file($_FILES[$input_field_name]['tmp_name'])) {
            $this->upload->initialize($config);
            if (!$this->upload->do_upload($input_field_name)) { 
                $this->form_validation->set_message($input_field_name, $this->upload->display_errors());
                return false;
            } else { 
                $uploaded = $this->upload->data();
                return $uploaded['file_name'];
            }
        } else {
            return $h_file;
        }
    }
	
}
