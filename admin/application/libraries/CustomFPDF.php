<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH . 'third_party/fpdf182/fpdf.php');

class CustomFPDF extends FPDF {

	public function header(){
        $this->SetFont('Arial','B',15);
        $this->Cell(80);
        $this->Cell(20,7,'Cash Memo');
        $this->Ln(20);
	}
	
	// Page footer
	public function Footer()
	{
	   $this->SetTextColor(0,0,0);
	   $this->setRegular(15);
	   $this->SetY(-15);
	//    $this->Cell(50,-2,"Thanking you",0,0,'L');
	//   // $this->Ln();
	//    $this->Cell(150,-2,"Please visit again",0,0,'R'); 
	//    $this->Ln(0.8);
	   foreach(range(1,65) as $value){
		   $this->Cell(1.5,0.05,'');
		   $this->Cell(1.5,0.05,'',1);
	   }
	   $this->Ln(0.8);
	   $this->Cell(200,6,'E. & O.E | Page '.$this->PageNo().' of {nb}',0,0,'R');
	}


//Set Bold
public function setBold($size)
{
	$this->SetFont('Arial','B',$size);
}
//Set Regular
public function setRegular($size)
{
	$this->SetFont('Arial','',$size);
}

//Set Rupee Symbol
public function getRupee($x,$y,$size)
{
	$this->Image('./assets/images/rupee-black.png',$x,$y,$size);
}
    public function getInstance(){
        return new CustomFPDF();
    }
}
?>