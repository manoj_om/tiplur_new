<?php //prd($results);?>
<style>
    h2.text-center.alert.alert-info {
    width: 100%;
}
</style>
<div id="page-wrapper" class="gray-bg dashbard-1">
    <?php breadcrumbs(array('admin/promotions' => 'Promotion Management', 'admin/promotions/reward_details_edit/' . @$this->uri->segment(4) => 'Update Reward Points')); ?>

    <div class="row border-bottom">
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12" style="margin-top: 40px;">
                <?php print_flash_message(); ?>
                <div class="ibox float-e-margins">
                    <?php echo form_open_multipart($this->uri->uri_string(), 'class="form-horizontal"'); ?>
                    <div class="ibox-title">
                        <div class="col-md-12">
                                <h2 class="text-center alert alert-info">User Promotion Details</h2>
                        </div>
                    </div>

                    <div class="ibox-content contentBorder">
                        <div class="row">

                           <!-- Hidden Id for update Reward points -->
                             <input type="hidden" placeholder="Promotion ID" name="id" id="id" value="<?php echo set_value('id', trim($id)); ?>" class="form-control formWidht">
                            
                          <div class="col-lg-6 col-md-6 col-sm-6 AddProdctInputCont">
                                <div class="form-group formWidht">
                                    <label>Promotion ID <span>*</span></label>
                                    <input type="text" placeholder="Promotion ID" name="promotion_id" id="promotion_id" value="<?php echo set_value('promotion_id', trim($result->promotion_id)); ?>" class="form-control formWidht" readonly>
                                    <span class='error vlError'><?php echo form_error('promotion_id'); ?></span>
                                </div>
                            </div>

                               <div class="col-lg-6 col-md-6 col-sm-6 AddProdctInputCont">
                                <div class="form-group formWidht">
                                    <label>User Name <span>*</span></label>
                                    <input type="text"  name="display_name" id="display_name" value="<?php echo set_value('display_name', trim($result->display_name)); ?>" class="form-control formWidht" readonly>
                                    <span class='error vlError'><?php echo form_error('display_name'); ?></span>
                                </div>
                            </div>
                          <div class="col-lg-6 col-md-6 col-sm-6 AddProdctInputCont">
                                <div class="form-group formWidht">
                                    <label>BarCode <span>*</span></label>
                                    <input type="text" placeholder="Title" name="barcode" id="barcode" value="<?php echo set_value('barcode', trim($result->barcode)); ?>" class="form-control formWidht" readonly>
                                    <span class='error vlError'><?php echo form_error('barcode'); ?></span>
                                </div>
                            </div>
                          <div class="col-lg-6 col-md-6 col-sm-6 AddProdctInputCont">
                                <div class="form-group formWidht">
                                    <label>Created On <span>*</span></label>
                                    <input type="text" placeholder="Title" name="created_datetime" id="created_datetime" value="<?php echo set_value('created_datetime', trim($result->created_datetime)); ?>" class="form-control formWidht" readonly>
                                    <span class='error vlError'><?php echo form_error('created_datetime'); ?></span>
                                </div>
                          </div>

                          <div class="col-lg-6 col-md-6 col-sm-6 AddProdctInputCont">
                                <div class="form-group formWidht">
                                    <label>Brand Bottle Qty<span>*</span></label>
                                    <input type="text" placeholder="Title" name="qty" id="qty" value="<?php echo set_value('qty', trim($result->qty)); ?>" class="form-control formWidht"readonly>
                                    <span class='error vlError'><?php echo form_error('qty'); ?></span>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-6 AddProdctInputCont">
                                <div class="form-group formWidht">
                                    <label>Reward Points<span>*</span></label>
                                    <input type="text" placeholder="Reward Points" name="reward_points" id="reward_points" value="<?php echo set_value('reward_points', trim($result->reward_points)); ?>" class="form-control formWidht">
                                    <span class='error vlError'><?php echo form_error('reward_points'); ?></span>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12  text-center" style="">
                                <button type="submit" name="update" class="btn btn-primary block full-width m-b updateProductBtn addBtn" value="reward_points">UPDATE</button>
                            </div>
                        </div>
                      </div>
                    </div>
            </div>
        </div>
        </form>
    </div>
</div>
</div>
