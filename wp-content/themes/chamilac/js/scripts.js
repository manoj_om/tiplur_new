

//iphone carousel animation
$(window).load(function () {
	//var tr = $('body').hasClass( "checked" );
	var tr = getCookie('agelimitbar');
	//alert(tr);
	if(tr == null){
		$('#declare').modal({backdrop: 'static', keyboard: false});
	}
	else
		{
				$('#image_popup').modal('show');
				var span = document.getElementsByClassName("close")[0];

				span.onclick = function() {
					console.log($('#image_popup').modal('hide'));
					$('#image_popup').modal('hide');
			//	modal.style.display = "none";
			}
		}

$('.agelimit').click(function() {
	console.log('enter');
	$('#image_popup').modal('show');
	var span = document.getElementsByClassName("close")[0];
	span.onclick = function() {
		$('#image_popup').modal('hide');
//	modal.style.display = "none";
	}
});
// 	$('#image_popup').modal('show');
// 	// Get the <span> element that closes the modal
// var span = document.getElementsByClassName("close")[0];
//
// // When the user clicks on <span> (x), close the modal
// span.onclick = function() {
//   modal.style.display = "none";
// }

	$('header').addClass("animated fadeIn");
	$('.carousel-iphone').addClass("animated fadeInLeft");

	$('#carousel-example-generic-1').carousel()

});

// Fixed navbar
$(window).scroll(function () {

var scrollTop = $(window).scrollTop();

	if (scrollTop > 200) {
		$('.navbar-default').css('display', 'block');
		$('.navbar-default').addClass('fixed-to-top');
	} else if (scrollTop == 0)   {
		$('.navbar-default').removeClass('fixed-to-top');
	}


});

$('.myCheck').click(function() {
	if(this.checked) {
		$('.agelimit').prop("disabled", false);
		$('.body').addClass('checked');
		setCookie('agelimitbar','true','100');
	}
	else{
		$('.agelimit').prop("disabled", true);
	}
});

$('.subcategory').on('click',function(){
	var data = $(this).next(".subcategory-box").html();
	var title = $(this).find("h4").text();
	$('.subcategories .modal-body').html(data);
	$('.subcategories .modal-title').html(title);
	$('#category').modal('show');
	$('.subcategories .modal-backdrop.fade.in').modal('hide');
});
$('.mobilemenu a').on('click', function(){
	$('.navbar-collapse').removeClass('in').addClass('collapse');
})


// toggle visibility for css3 animations
$(document).ready(function() {
	$('header').addClass('visibility');
	$('.carousel-iphone').addClass('visibility');
	$('.payoff h1').addClass('visibility');
	$('.features .col-md-4').addClass('visibility');
	$('.social .col-md-12').addClass('visibility');
});


// Parallax Content

function parallax() {

		// Turn parallax scrolling off for iOS devices

		    var iOS = false,
		        p = navigator.platform;

		    if (p === 'iPad' || p === 'iPhone' || p === 'iPod') {
		        iOS = true;
		    }

		var scaleBg = -$(window).scrollTop() / 3;

        if (iOS === false) {
            $('.payoff').css('background-position-y', scaleBg - 150);
            $('.social').css('background-position-y', scaleBg + 200);
        }

}

function navbar() {

	if ($(window).scrollTop() > 1) {
	    $('#navigation').addClass('show-nav');
	} else {
	    $('#navigation').removeClass('show-nav');
	}

}

$(document).ready(function () {

	var browserWidth = $(window).width();

	if (browserWidth > 560){

		$(window).scroll(function() {
			parallax();
			navbar();
		});

	}





});


$(window).resize(function () {

	var browserWidth = $(window).width();

	if (browserWidth > 560){

		$(window).scroll(function() {
			parallax();
			navbar();
		});

	}

});


// iPhone Header Carousel
$('header .carousel').carousel({
  interval: 3000
})

// iPhone Features Carousel
$('.detail .carousel').carousel({
  interval: 4000
})


function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}
function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
