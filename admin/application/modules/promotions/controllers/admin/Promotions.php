<?php
class Promotions extends MX_Controller {

    function __construct() {
        parent::__construct();
         $this->load->model('Promotion_model');
         $this->load->model('orders/orders_model');
        $this->load->library('auth');
        $this->load->library('form_validation');
        $this->set_data = array('theme' => 'admin'); //define theme name
        if (!$this->auth->_is_logged_in()) {
            redirect(base_url());
        }
    }
	function filter() {
        return $this->input->get(array('name', 'contact', 'fromDate', 'status','toDate'));
    }
    function promo_filter() {
        return $this->input->get(array('brand_name','released_date','pincode','city'));
    }
	function index() {

        if (!$this->auth->_is_logged_in()) {
            redirect(base_url('admin'));
        }
          // smtp_mail($mail='kumar.tanuj@orangemantra.in' , $msg ='hii tanuj');
          // echo "string"; die;

        $includeJs = array('/assets/js/promotion.js');
        //for adding js in footer
        $data['js'] = $includeJs;

        if (!empty($_GET['multi_activate'])) {
            $this->promotion_status('activate');
        }

        if (!empty($_GET['multi_deactivate'])) {
            $this->promotion_status('de-activate');
        }

        $this->load->library('pagination');
        $per_page = isset($_GET['per_page']) ? $_GET['per_page'] : false;
        unset($_GET['per_page']);
        $query_string = http_build_query($_GET);
        $site_url = !$query_string ? base_url(SITE_AREA . '/promotions') : base_url(SITE_AREA . '/promotions?' . $query_string);
        if ($per_page) {
            $_GET['per_page'] = $per_page;
        }
        $num_rows = $this->Promotion_model->promotions_details($this->promo_filter(), false, false, true);
        $config = pagination_formatting();
        $limit = 20;
        $config['base_url'] = $site_url;
        $config['total_rows'] = $num_rows;
        $config['per_page'] = $limit;
        $config['num_links'] = 5;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config["cur_page"] = $per_page;
        $this->pagination->initialize($config);
        $offset = isset($_GET['per_page']) ? ($_GET['per_page'] - 1) * $limit : null;
        $records = $getting = $this->Promotion_model->promotions_details($this->promo_filter(), $limit, $offset, false);
        // $count   = $this->Promotion_model->usersCount();
        // $records = array_merge($records , $count);
        $data['result'] = $records;
		    $data['view'] = 'promotion';
         // prd($records);
        $this->theme($data);
    }

    function reward_details_edit()
    {
        // prd($_POST);
         $data['js'] = array('/assets/js/promotion.js');
        $id = $this->uri->segment(4);
        $data['result'] = $this->Promotion_model->promotion_rewards_details($id);
        $data['id']=$id;
         // prd($data['result']);
        if (!$data['result']) {
            show_404();
        }
        if (isset($_POST['update']) == 'reward_points') {

            $update = $this->Promotion_model->update_rewards_details($_POST['id'],$_POST['reward_points']);
            if(isset($update)){
                set_flash_message($type = 'success', $message = 'Users Reward Points has been Updated successfully.');
                redirect('admin/promotions');
            }
        }
        $this->theme($data);
    }
	// Customers List
    function participate_user() {

        if (!$this->auth->_is_logged_in()) {
            redirect(base_url());
        }
        $includeJs = array('/assets/js/promotion.js');
        //for adding js in footer
        $data['js'] = $includeJs;
        if (!empty($_GET['multi_activate'])) {
          $this->users_status('activate', 'customer');
        }

        if (!empty($_GET['multi_deactivate'])) {
            $this->users_status('de-activate', 'customer');
        }

        $this->load->library('pagination');
        $per_page = isset($_GET['per_page']) ? $_GET['per_page'] : false;
        unset($_GET['per_page']);
        $query_string = http_build_query($_GET);
        $site_url = !$query_string ? base_url(SITE_AREA . '/promotions/participate_user') : base_url(SITE_AREA . '/promotions/participate_user?' . $query_string);
        if ($per_page) {
            $_GET['per_page'] = $per_page;
        }
        $num_rows = $this->Promotion_model->users_details($this->filter(), false, false, true, '2');
        $config = pagination_formatting();
        $limit = 10;
        $config['base_url'] = $site_url;
        $config['total_rows'] = $num_rows;
        $config['per_page'] = $limit;
        $config['num_links'] = 5;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config["cur_page"] = $per_page;
        $this->pagination->initialize($config);
        $offset = isset($_GET['per_page']) ? ($_GET['per_page'] - 1) * $limit : null;
        $records = $getting = $this->Promotion_model->users_details($this->filter(), $limit, $offset, false, '1');
        $data['result'] = $records;
		  // prd($data); die;
        $this->theme($data);
    }
    // participated users for specific PROMOTION ID
    public function promoted_users($promotion_id)
    {
      // echo $promotion_id =   trim($this->input->post('promotion_id')); die;
      $this->load->library('pagination');
      $per_page = isset($_GET['per_page']) ? $_GET['per_page'] : false;
      unset($_GET['per_page']);
      $query_string = http_build_query($_GET);
      $site_url = !$query_string ? base_url(SITE_AREA . '/promotions/participate_user') : base_url(SITE_AREA . '/promotions/participate_user?' . $query_string);
      if ($per_page) {
          $_GET['per_page'] = $per_page;
      }
      $num_rows = $this->Promotion_model->get_promoted_users($promotion_id,false,false,true);
      $config = pagination_formatting();
      $limit = 10;
      $config['base_url'] = $site_url;
      $config['total_rows'] = $num_rows;
      $config['per_page'] = $limit;
      $config['num_links'] = 5;
      $config['use_page_numbers'] = TRUE;
      $config['page_query_string'] = TRUE;
      $config["cur_page"] = $per_page;
      $this->pagination->initialize($config);
      $offset = isset($_GET['per_page']) ? ($_GET['per_page'] - 1) * $limit : null;
      $records = $getting = $this->Promotion_model->get_promoted_users($promotion_id,$limit, $offset, false);
      //$records = $getting = $this->Promotion_model->users_details($this->filter(), $limit, $offset, false, '1');
      $data['result'] = $records;
    // prd($records); die;
     $this->theme($data);

    }
    public function dashboard() {

        if (!$this->auth->_is_logged_in()) {
            redirect(base_url());
        }
       
       $data['participate_users_count'] = $this->Promotion_model->get_part_customers_count();
       $data['promotion_active_count'] = $this->Promotion_model->get_promotion_list($status='active');
       $data['promotion_total_count'] = $this->Promotion_model->get_promotion_list($status='total');
       $data['brands_count'] = $this->Promotion_model->get_avail_brands();
       $data['total_reward_points'] = $this->Promotion_model->get_total_rewards_usersCount();
       // prd($data['total_reward_points']);
       $this->theme($data);
    }

    public function userwise_total_rewards()
    {
        // $data['userwise_reward_points'] = $this->Promotion_model->get_total_rewards_with_users();
        // prd($data);

         if (!$this->auth->_is_logged_in()) {
            redirect(base_url());
        }
        $includeJs = array('/assets/js/promotion.js');
        //for adding js in footer
        $data['js'] = $includeJs;
        // if (!empty($_GET['multi_activate'])) {
        //   $this->users_status('activate', 'customer');
        // }

        // if (!empty($_GET['multi_deactivate'])) {
        //     $this->users_status('de-activate', 'customer');
        // }

        $this->load->library('pagination');
        $per_page = isset($_GET['per_page']) ? $_GET['per_page'] : false;
        unset($_GET['per_page']);
        $query_string = http_build_query($_GET);
        $site_url = !$query_string ? base_url(SITE_AREA . '/promotions') : base_url(SITE_AREA . '/promotions?' . $query_string);
        if ($per_page) {
            $_GET['per_page'] = $per_page;
        }
        $num_rows = $this->Promotion_model->get_total_rewards_with_users($this->filter(), false, false, true);
        $config = pagination_formatting();
        $limit = 10;
        $config['base_url'] = $site_url;
        $config['total_rows'] = $num_rows;
        $config['per_page'] = $limit;
        $config['num_links'] = 5;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config["cur_page"] = $per_page;
        $this->pagination->initialize($config);
        $offset = isset($_GET['per_page']) ? ($_GET['per_page'] - 1) * $limit : null;
        $records = $getting = $this->Promotion_model->get_total_rewards_with_users($this->filter(), $limit, $offset, false);
        $data['result'] = $records;
          // prd($data); die;
        $this->theme($data);




    }

	/*  script for delete participated user */
    function user_delete() {
        $id = $this->input->post('user_id');
        $res = $this->db->where('id', $id)->update('users', array('deleted' => '1'));
        $data['msg'] = set_flash_message($type = 'success', $message = 'Deleted successfully.');
        $data['status'] = ($res) ? true : false;
        echo json_encode($data);
    }

    /*  script for delete promotion */
    function delete_promotion() {
        $promotion_id = $this->input->post('promotion_id');
        $res = $this->db->where('promotion_id', $promotion_id)->update('ti_promotion', array('deleted' => '1'));
        $data['msg'] = set_flash_message($type = 'success', $message = 'Deleted successfully.');
        $data['status'] = ($res) ? true : false;
        echo json_encode($data);
    }

    /*  script for update promotion */
    function edit() {
        $data['js'] = array('/assets/js/promotion.js');
         $id = $this->uri->segment(4);
        $data['result'] = $this->Promotion_model->promotion_detail_by_id($id);
        // prd($data['result']);
        if (!$data['result']) {
            show_404();
        }
        if (isset($_POST['promotion_title'])) {

            if ($this->save_promotion('update', $id)) {
                set_flash_message($type = 'success', $message = 'Product Update successfully.');
                redirect('admin/promotions');
            }
        }
        $this->theme($data);
    }

    // Insert and update promotions
    private function save_promotion($type = false, $id = false) {
        $data = $this->input->post(array('promotion_id', 'promotion_title', 'brand_name', 'brand_barcode_180ml', 'brand_barcode_375ml', 'brand_barcode_750ml'));
        // prd($data);

        $data['uploaded_on'] = $data['modified_on'] = db_date_time();
        //$data['image'] = $this->upload_image();

        $final_array = array();
        if ($type == 'insert') {
            $id = $this->Promotion_model->add_promotion($data);
        } elseif ($type == 'update') {
        // echo'test'.  $data ['promotion_img'] = $this->update_upload_image();

            unset($data['uploaded_on']);
            $id = $this->Promotion_model->update_promotion($data, $id);
        }
        return $id;
    }

    // Upload promotion image and banner during promotion UPDATE
    private function update_upload_image() {
        $this->load->library('upload');
        $upload_path = FCPATH . "assets/images/promotion_image/";
      echo  $h_file = $this->input->post('promotion_img');
      die;  $input_field_name = 'image';
        $config = array(
            'upload_path' => $upload_path,
            'allowed_types' => 'gif|jpg|png|jpeg',
            'file_name' => time() . $_FILES[$input_field_name]['name'],
        );

        if (is_uploaded_file($_FILES[$input_field_name]['tmp_name'])) {

            $this->upload->initialize($config);
            if (!$this->upload->do_upload($input_field_name)) {
                $this->form_validation->set_message($input_field_name, $this->upload->display_errors());
                return false;
            } else {
                if ($h_file and file_exists($upload_path . '/' . $h_file)) {
                    unlink($upload_path . '/' . $h_file);
                }
                $uploaded = $this->upload->data();
                return $uploaded['file_name'];
            }
        } else {

            return $h_file;
        }
    }


// User DEACTIVATE/Acitvate
  function users_status($status, $role_type = 'customer') {

        $checked = $this->input->get('users_id');
 // prd($checked); die;
        if (is_array($checked) && count($checked)) {
            // If any of the deletions fail, set the result to false, so
            // failure message is set if any of the attempts fail, not just
            // the last attempt
            $result = true;
            foreach ($checked as $pid) {

                if ($status == 'activate') {
                    $data = array('status' => '1');
                  $updated = $this->Promotion_model->update_users_details($data, $pid);
                } else {
                    $data = array('status' => '0');
                    $updated = $this->Promotion_model->update_users_details($data, $pid);
                    if(!empty($updated)):
                        $barcode = $this->Promotion_model->userd_barcode($pid);
                //      prd($barcode);
                    foreach ($barcode as $val ) {
                      $barcode = $this->Promotion_model->remove_barcode($pid,$val['barcode']);
                  //   lq();
                    }
                  endif;
                  //  $email = user_data($pid)->email;
                    $email = 'kumar.tanuj@orangemantra.in';
                    $this->user_inactive_mail($email); //send mail
                }
                //echo $updated;exit;
                if ($updated == false) {
                    $result = false;
                }
            }
            if ($result) {
                set_flash_message($type = 'success', $message = count($checked) . ' row updated successfully.');
                ($role_type == 'customer') ? redirect('admin/promotions/participate_user') : redirect('admin/promotions');
            } else {
                set_flash_message($type = 'error', $message = 'something wrong.');
                ($role_type == 'customer') ? redirect('admin/promotions/participate_user') : redirect('admin/promotions');
            }
        } else {
            set_flash_message($type = 'error', $message = 'Please select users.');
            ($role_type == 'customer') ? redirect('admin/promotions/participate_user') : redirect('admin/promotions');
        }
    }

    // Promotion de-activate
    function promotion_status($status) {

     $checked = $this->input->get('promotion_id');
 
        if (is_array($checked) && count($checked)) {
            // If any of the deletions fail, set the result to false, so
            // failure message is set if any of the attempts fail, not just
            // the last attempt
            $result = true;
            foreach ($checked as $pid) {

                if ($status == 'activate') {
                    $data = array('promo_status' => '0');
                } else {
                    $data = array('promo_status' => '1');
                  }

                $updated = $this->Promotion_model->update_promotion_details($data, $pid);
                //echo $updated;exit;
                if ($updated == false) {
                    $result = false;
                }
            }
            if ($result) {

                set_flash_message($type = 'success', $message = count($checked) . ' row updated successfully.');
                ($role_type == 'customer') ? redirect('admin/promotions/participate_user') : redirect('admin/promotions');
            } else {
                set_flash_message($type = 'error', $message = 'something wrong.');
                ($role_type == 'customer') ? redirect('admin/promotions/participate_user') : redirect('admin/promotions');
            }
        } else {
            set_flash_message($type = 'error', $message = 'Please select users.');
            ($role_type == 'customer') ? redirect('admin/promotions/participate_user') : redirect('admin/promotions');
        }
    }
    // User documet Reject by Admin
    function reject_doc() {

        if (empty($_POST)) {
            show_404();
        }

        $user_id = $this->input->post('user_id');
        $data = array('status' => '8');

        $result = true;
        $updated = $this->Promotion_model->update_users_details($data, $user_id);
        //echo $updated;exit;
        if ($updated == false) {
            $result = false;
        }
        if ($result) {

            $notify_data = array(
                'order_id' => 'Document Rejected',
                'notified_to' => $user_id,
                'requested_by' => $this->session->userdata('id'),
                'type' => DOCUMENT_REJECTED,
                'title' => 'Document Rejected',
                'message' => 'Your Document is rejected by admin',
                'reason' => 'Invalid Document',
            );

            $notify = new Push_notification();
            $notify->send_notification($notify_data);

            $email = user_data($user_id)->email;

            $this->send_welcome_mail($email); //send mail

            set_flash_message($type = 'success', $message = 'Document rejected successfully.');
            redirect('admin/promotions/participate_user');
        } else {
            set_flash_message($type = 'error', $message = 'something wrong.');
            redirect('admin/promotions/participate_user');
        }
    }
    // Sent mail to user-inactive and remove barcode
    public function user_inactive_mail($email) {

        $subject = "Tiplur | Invalid/Incomplete Profile Details";
        $msg = '<table style="margin: 0 auto; background: #f5f5f5;color: #444;font-size: 14px;border: 1px solid #ffde16;border-collapse: collapse;" width="600">
     <tr><!-- Header -->
      <td style="margin:10px auto;background:#fff">
       <!--img style="width:70px;margin:0 auto;display:block" src="http://tiplur.in/theme/site/img/assets/logo.png"-->
       <img style="width:70px;margin:0 auto;display:block" src="http://demo5.mobdigi.com/admin/theme/site/img/assets/logo.png">
      </td>
     </tr>
     <tr><!-- Body -->
      <td>
       <table style="width:100%;border-collapse: collapse;">
        <tr>
         <td style="font-size:16px;color:#000;font-weight:600;padding:8px;text-align:left">
          Dear Patron,
         </td>
        </tr>
        <tr>
          <td style="padding:8px;text-align:left;margin-bottom:10px">
           <p>We at tiplur ensure that all our users, have a safe, simple and smart experience while exploring the platform. We have observed that the personal details as entered for registration are incomplete/ incorrect. Would request you to kindly go to your profile on the application, and update the same.</p>
           <p style="margin-bottom:10px;">In case this has caused any inconvenience to you, we are extremely sorry for that, but you would agree, non-availability of correct and proper address, phone number and email Id may cause last minute inconvenience to you when you place an order with merchant, since the merchant needs to have correct coordinates to make delivery.</p>
           <p>Sincerely yours,<br>Tiplur Team</p>
            </td>
        </tr>
       </table>
        </td>
      </tr>
      <tr><!-- Footer -->
       <td style="text-align:center;font-size:10px;background:#ffde16;color:#000;padding: 5px;">&#x24B8; Tiplur 2018</td>
      </tr>
      </table>';
         smtp_mail($email, $msg, $subject);

    }
    // User Documents reject email $notify
    public function send_welcome_mail($email) {

        $subject = "Tiplur | Valid Document Request";
        $msg = "<table style='margin: 0 auto; background: #f5f5f5;color: #444;font-size: 14px;border: 1px solid #ffde16;border-collapse: collapse;' width='600'>
				     <tr><!-- Header -->
				      <td style='margin:10px auto;background:#fff'>
				       <!--img style='width:70px;margin:0 auto;display:block' src='http://tiplur.in/theme/site/img/assets/logo.png'-->
				       <img style='width:70px;margin:0 auto;display:block' src='http://demo5.mobdigi.com/admin/theme/site/img/assets/logo.png'>
				      </td>
				     </tr>
				     <tr><!-- Body -->
				      <td>
				       <table style='width:100%;border-collapse: collapse;'>
				        <tr>
				         <td style='font-size:16px;color:#000;font-weight:600;padding:8px;text-align:left'>
				          Dear Patron,
				         </td>
				        </tr>
				        <tr>
				          <td style='padding:8px;text-align:left;margin-bottom:10px'>
				           <p>
								On the onset let me thank you for downloading tiplur on your phone.
								We at tiplur ensure that all our users, are above the age as mandated by the respective state in which they reside.
								We have observed that you have not uploaded a valid govt issued photo ID, this is required at our end to validate the age credentials, adhering to the state laws.
							</p>
				            <p style='margin-bottom:10px;'>
				          		Would request you to kindly go on your profile, where we have an option for you to upload a valid ID, as to activate your account.
				            </p>
				            <p style='margin-bottom:5px;'>
				           		In case this has caused any inconvenience to you, we are extremely sorry for that, but you would agree, that we don't want any user below the age mandated by state, accessing the platform.
				            </p>
				             <p>Sincerely yours,<br>Tiplur Team</p>
				            </td>
				        </tr>
				       </table>
				        </td>
				      </tr>
				      <tr><!-- Footer -->
				       <td style='text-align:center;font-size:10px;background:#ffde16;color:#000;padding: 5px;'>&#x24B8; Tiplur 2018</td>
				      </tr>
     			 </table>";
         smtp_mail($email, $msg, $subject);
       }
    // Promotion Data Excel Uploader
    function upload(){
       $this->theme();
    }

    // Begin to Uploading Excel data
    public function uploadfile() {

        $data['temp'] = '';
        $file = (!empty($_FILES['file']['name'])) ? $_FILES['file']['name'] : $_SESSION["file_name"];
        $ext = pathinfo($file, PATHINFO_EXTENSION);
        if ($ext == 'xls') {
            if (empty($_SESSION["file_name"])) {
                $path = FCPATH . 'assets/uploads/uploaded_xls/';
                $file_name = time() . $_FILES['file']['name'];
                $final = $path . '/' . $file_name;
                if (move_uploaded_file($_FILES['file']['tmp_name'], $final)) {
                    $_SESSION["file_name"] = $file_name;
                }
            }
            if (!empty($_SESSION["file_name"])) {
                $inputFileName = FCPATH . 'assets/uploads/uploaded_xls/' . @$_SESSION["file_name"];
                if (!empty($_POST['upload'])) {
                    $this->preview_data();
                }

                $this->load->library('PHPExcel/PHPExcel');
                // read file data
                $objReader = PHPExcel_IOFactory::createReaderForFile($inputFileName);
                $objReader->setReadDataOnly(true);
                $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
                //get only the Cell Collection
                $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

                //Extract to a PHP readable array format
                $i = 1;
                $heder_defination = array("A" => "promotion_id","B" => "brand_name","C" => "promotion_title","D" => "promotion_img","E" => "promotion_desc","F" => "prmototion_banner_url", "G" => "released_date","H" => "released_time","I" => "expiry_date","J" => "expiry_time","K" => "reminder_time","L" => "pincode",
                "M" => "city","N" => "state","O" => "brand_barcode_750ml","P" => "brand_barcode_375ml","Q" => "brand_barcode_180ml","R" => "gifts_rewards","S" => "customer_care_no",'T'=>'reward_points_750ml','U' =>'reward_points_375ml','V'=>'reward_points_180ml');
                foreach ($cell_collection as $k => $cell) {

                    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
                    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();

                    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
                    //header will/should be in row 1 only. of course this can be modified to suit your need.
                    if ($row == 1) {
                        $header[$row][$column] = $data_value;
                        $arry_keys = $data_value;
                    } else {
                        $arr_data[$row][$heder_defination[$column]] = trim($data_value);
                    }
                    $i++;
                }
                // prd($data_value);
                //send the data in an array format
                $data['header'] = $header;
                $validataion = array_diff_assoc($heder_defination, $data['header'][1]);

                if (empty($validataion)) {

                    $data['values'] = $arr_data;

                    if (!empty($data['values'])) {
                        $excel_data = array();
                        foreach ($data['header'][1] as $column) {
                            $excel_data[$column] = array_column($data['values'], $column);
                        }
                        // prd($excel_data);
                          $this->preview_data($excel_data);
                    }
                } else {
                    $data['error_data'] = 'hi';
                    set_flash_message($type = 'error', $message = 'Required  excel format not same plz download correct format ' . anchor('assets/uploads/product.xls', 'click here', 'class="link-class"'));
                    redirect('admin/products/upload');
                }
            }
        } else {
            set_flash_message($type = 'error', $message = 'Required  excel format not same plz download correct format ' . anchor('assets/uploads/product.xls', 'click here', 'class="link-class"'));
            redirect('admin/products/upload');
        }
        $this->theme($data);
    }
     public function preview_data($set_data = false) {

       $userId = $this->session->userdata('id');
       $role_id =  $this->session->userdata('role_id');
        // form or excel data
        $post_data = ($set_data && empty($_POST['upload'])) ? $set_data : $this->input->post();

        $this->form_validation->set_data($post_data);
        $this->form_validation->set_rules($this->Promotion_model->custom_form_validation_rules($post_data));
  // form validation
        if ($this->form_validation->run() === false) {
            set_flash_message($type = 'error', $message = 'Check required fields in Excel.');
            $data['data'] = $this->form_validation->error_array();
            return false;
        } else if (!empty($post_data)) {

            $final_array = array();
            /* prepare product data */
           foreach ($post_data['brand_name'] as $k => $val) {  //prd($val);

                $final_array['promotion_id']          = isset($post_data['promotion_id'][$k]) ? $post_data['promotion_id'][$k] : '' ;
                $final_array['brand_name']            = isset($post_data['brand_name'][$k]) ? $post_data['brand_name'][$k] : '' ;
                $final_array['promotion_title']       = isset($post_data['promotion_title'][$k]) ? $post_data['promotion_title'][$k] : '' ;
                $final_array['promotion_img']         = isset($post_data['promotion_img'][$k]) ?   $data['promotion_img'] = $this->upload_image($post_data['promotion_img'][$k])  : '';
              // $final_array['promotion_img']         = isset($post_data['promotion_img'][$k]) ?  $post_data['promotion_img'][$k]  : '';
                $final_array['promotion_desc']        = isset($post_data['promotion_desc'][$k]) ? $post_data['promotion_desc'][$k] : '';
                $final_array['terms_id'] = isset($post_data['promotion_desc'][$k]) ? $this->save_terms_conditions($final_array['promotion_id'],$final_array['brand_name'], $final_array['promotion_desc'] ) : '';
                // $final_array['prmototion_banner_url'] = ($post_data['prmototion_banner_url'][$k]) ? $post_data['prmototion_banner_url'][$k] : '';
                $final_array['prmototion_banner_url'] = isset($post_data['prmototion_banner_url'][$k]) ?   $data['prmototion_banner_url'] = $this->upload_image($post_data['prmototion_banner_url'][$k])  : '';
                $final_array['released_time']         = isset($post_data['released_time'][$k]) ? Excel_timeFormat($post_data['released_time'][$k]) : '00:00:00' ;
                $final_array['released_date']         = db_date($post_data['released_date'][$k]);
                $final_array['expiry_date']           = db_date($post_data['expiry_date'][$k]);
                $final_array['expiry_time']          = isset($post_data['expiry_time'][$k]) ? Excel_timeFormat($post_data['expiry_time'][$k]) : '23:59:59' ;
                $final_array['reminder_time']          = isset($post_data['reminder_time'][$k]) ? Excel_timeFormat($post_data['reminder_time'][$k]) : '23:59:59' ;
                $final_array['pincode']               = isset($post_data['pincode'][$k]) ? $post_data['pincode'][$k] : '';
                $final_array['city']                  = isset($post_data['city'][$k]) ? $post_data['city'][$k] : '';
                $final_array['state']                 = isset( $post_data['state'][$k] ) ? $post_data['state'][$k] : '';
                // $final_array['brand_barcode']         = isset($post_data['brand_barcode'][$k]) ? $this->get_brandCode($post_data['brand_barcode'][$k]) : '';  die;
                $final_array['brand_barcode_750ml']         = isset($post_data['brand_barcode_750ml'][$k]) ? $post_data['brand_barcode_750ml'][$k] : '';
                $final_array['brand_barcode_375ml']         = isset($post_data['brand_barcode_375ml'][$k]) ? $post_data['brand_barcode_375ml'][$k] : '';
              
                $final_array['brand_barcode_180ml']         = isset($post_data['brand_barcode_180ml'][$k]) ? $post_data['brand_barcode_180ml'][$k] : '';

                 $final_array['reward_points_750ml']         = isset($post_data['reward_points_750ml'][$k]) ? $post_data['reward_points_750ml'][$k] : '';
                
                 $final_array['reward_points_375ml']         = isset($post_data['reward_points_375ml'][$k]) ? $post_data['reward_points_375ml'][$k] : '';
                
                 $final_array['reward_points_180ml']         = isset($post_data['reward_points_180ml'][$k]) ? $post_data['reward_points_180ml'][$k] : '';
               
                $final_array['gifts_rewards']         = isset($post_data['gifts_rewards'][$k]) ? $post_data['gifts_rewards'][$k] : '' ;
                $final_array['customer_care_no']      = isset($post_data['customer_care_no'][$k]) ? $post_data['customer_care_no'][$k] : '' ;
                $final_array['role_id']             = $role_id;
                $final_array['uploaded_on'] = db_date_time();
              /* insert Promotions Data */
              // prd($final_array); die;
                $id = $this->Promotion_model->add_promotionData($final_array);
            }
            unlink('./assets/uploads/uploaded_xls/' . $_SESSION["file_name"]);
            unset($_SESSION["file_name"]);
            set_flash_message($type = 'success', $message = 'Promotions Data added successfully.');
            redirect('admin/promotions/upload');
      }
    }
    public function save_terms_conditions($promotion_id , $brand_name , $term ){
      $terms = array('promotion_id' => $promotion_id , 'brand_name' => $brand_name , 'terms_conditions' => $term);
      $this->db->insert('promotions_terms_condtions', $terms);
      $res = $this->db->insert_id();
      return ( $res ) ? $res : false;
    }
    public function upload_image($img_url) {
      // $img_url = 'https://www.pernod-ricard-india.com/sites/default/files/tab-slider-img1_2.jpg';
      $upload_path = FCPATH . "assets/images/promotion_image/";
      // Extract the filename
      $filename = rand(1,100000).'_'.substr($img_url, strrpos($img_url, '/') + 1);
      // prd($filename);
      $file = $upload_path.$filename;
      // Save file wherever you want
      $move = file_put_contents($file, file_get_contents($img_url));
        if($move):
          return $filename;
        else:
          return false;
        endif;
    }

    // Export paticipated users in Excel
    public function export_partiUsers() {

        $filter = isset($_GET['filter']) ? $this->filter() : false;

        $reports = $this->Promotion_model->users_details($filter , false, false, false,false);
        // prd($reports);
        /* Start Logic for excel */
        $exceldata = array();
        $table = '<table><tr><th>user_name</th><th>email</th><th>promotion_id</th><th>city</th><th>Quantity</th><th>QR-Code</th><th>created_on</th><th>participated_on</th></tr>';
        foreach ($reports as $record) {
            //print_r($reports);

            $table .= '<tr>';
            $table .='<td>' . ucfirst($record->display_name) . '</td>';
            $table .= '<td>' . $record->email . '</td>';
            $table .= '<td>' . $record->promotion_id . '</td>';
            $table .= '<td>' . $record->city . '</td>';
            $table .= '<td>' . $record->qty . '</td>';
            $table .= '<td>' . $record->barcode . '</td>';
            $table .= '<td>' . site_date_time($record->created_on) . '</td>';
            $table .= '<td>' . site_date_time($record->participated_on) . '</td>';
            $table .= '</tr>';
        }
        $table .= '</table>';
        $date = date('d-m-Y');

        header("Content-Type: application/vnd.ms-excel");
        header("Content-disposition: attachment; filename=Participated Users(" . $date . ").xls");
        echo $table;
        die();
        /* end Logic for excel */
    }
}
