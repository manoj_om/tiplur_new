<?php //echo "<pre>";print_r($result);echo "</pre>"; die();
$brand_name=isset($result->brand_name)? explode(',', $result->brand_name):'';
?>
<div id="page-wrapper" class="gray-bg dashbard-1">
    <?php breadcrumbs(array('admin/states' => 'State Management', 'admin/states/edit/' . @$this->uri->segment(4) => 'Update State')); ?>

    <div class="row border-bottom">
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12" style="margin-top: 40px;">
                <?php print_flash_message(); ?>
                <div class="ibox float-e-margins">
                    <?php echo form_open_multipart($this->uri->uri_string(), 'class="form-horizontal"'); ?>
                    <div class="ibox-title">
                        <h2 style="display: inline-block;">Update State</h2>
                        <div class="ibox-tools" style="display: inline-block; float: right; top: -60px;">

                        </div>

                    </div>

                    <div class="ibox-content contentBorder">
                        <div class="row">

                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form-group formWidht">
                                    <label>State Name <span style="color: red;">*</span></label>
                                    <input type="text" value="<?php echo set_value('name', isset($result->name) ? $result->name : ''); ?>" class="form-control formWidht" disabled>
                                    <input type="hidden" name="name" value="<?php echo $result->name;?>">
                                    <span class='error vlError'><?php echo form_error('age'); ?></span>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form-group formWidht">
                                    <label>Allowed Age Limit<span style="color: red;">*</span></label>
                                    <input type="text" placeholder="Age" value="<?php echo set_value('age', isset($result->age) ? $result->age : ''); ?>" name="age" id="age" class="form-control formWidht">
                                    <span class='error vlError'><?php echo form_error('age'); ?></span>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form-group formWidht">
                                    <label>Opening Time<span style="color: red;">*</span> </label>
                                    <input type="text" placeholder="Opening Time" value="<?php echo set_value('opening_time', isset($result->opening_time) ? $result->opening_time : ''); ?>" name="opening_time" id="opening_time" class="form-control formWidht opening_time" required>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form-group formWidht">
                                    <label>Closing Time<span style="color: red;">*</span></label>
                                    <input type="text" placeholder="closing Time" name="closing_time" id="closing_time" value="<?php echo set_value('closing_time', isset($result->closing_time) ? $result->closing_time : ''); ?>" class="form-control formWidht closing_time" required>
                                    <span class='error vlError'><?php echo form_error('closing_time'); ?></span>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form-group formWidht">
                                    <label>Status   <span>*</span></label>
                                    <?php $check = ($result->status == 0) ? '2' : '1' ?>
                                    <select name="status" class="form-control m-b addContDrop ">
                                        <?php
                                        $status = status();
                                        foreach ($status as $k => $val):
                                            ?>
                                            <option value="<?php echo $k; ?>" <?php echo ($k == $check) ? 'selected' : ''; ?>  ><?php echo $val; ?></option>
                                        <?php endforeach; ?>
                                        <span class='error vlError'><?php echo form_error('status'); ?></span>
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form-group formWidht">
                                    <label>Is available?   <span>*</span></label>
                                    <?php

                                    $check = ($result->is_available== 0) ? '2' : '1' ?>
                                    <select name="is_available" class="form-control m-b addContDrop ">
                                        <?php
                                        $is_available= is_available();
                                        foreach ($is_available as $k => $val):
                                            ?>
                                            <option value="<?php echo $k; ?>" <?php echo ($k == $check) ? 'selected' : ''; ?>  ><?php echo $val; ?></option>
                                        <?php endforeach; ?>
                                        <span class='error vlError'><?php echo form_error('is_available'); ?></span>
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form-group formWidht">
                                    <label>Is permit?<span style="color: red;">*</span></label>
                                    <?php

                                    $check = ($result->is_permit== 0) ? '2' : '1' ?>
                                    <select name="is_permit" class="form-control">

                                        <?php

                                        $is_permit= is_permit();
                                        foreach ($is_permit as $k => $val):
                                            ?>
                                            <option value="<?php echo $k; ?>" <?php echo ($k == $check) ? 'selected' : ''; ?>  ><?php echo $val; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <span class='error vlError'><?php echo form_error('is_permit'); ?></span>
                                </div>
                            </div>
                            <!-- <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form-group formWidht">
                                    <label>Shipping Charge <span style="color: red;">*</span></label>
                                    <input type="text" value="<?php echo set_value('shipping_charge', isset($result->shipping_charge) ? $result->shipping_charge : ''); ?>" name="shipping_charge" id="shipping_charge" class="form-control formWidht">
                                    <span class='error vlError'><?php echo form_error('shipping_charge'); ?></span>
                                </div>
                            </div> -->

                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form-group formWidht">
                                    <label>Internet Charge<span style="color: red;">*</span></label>
                                    <input type="text" placeholder="Internet charge" value="<?php echo set_value('internet_charge', isset($result->internet_charge) ? $result->internet_charge : ''); ?>" name="internet_charge" id="internet_charge" class="form-control formWidht">
                                    <span class='error vlError'><?php echo form_error('internet_charge'); ?></span>
                                </div>
                            </div>
                            
                               
                            
                            
                        <div class="col-lg-12 col-md-12 col-sm-12">    
                             <h2 style="display: inline-block;">Shipping Details</h2>
                         </div>
                            <div class="col-md-6" id="append_shipping">
                                <?php
                                $shipping_array = json_decode($result->shipping_charge);
                                // prd($shipping_array);
                                ?>
                                <!-- if Holidays exist -->

                                <?php
                                if (!empty($shipping_array)):
                                    foreach ($shipping_array as $key => $val):
                                        ?>
                                        <div class="rm">
                                            <div class="col-lg-5 col-md-5 col-sm-5">
                                                <div class="form-group formWidht">
                                                    <label>Amount <span style="color: red;">*</span></label>
                                                    <input type="text" value="<?php echo set_value('amount', isset($key) ? $key : ''); ?>" name="amount[]" id="amount" class="form-control formWidht">
                                                    <span class='error vlError'><?php echo form_error('amount[]'); ?></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-5 col-md-5 col-sm-5">
                                                <div class="form-group formWidht">
                                                    <label>Shipping Charge <span style="color: red;">*</span></label>
                                                    <input type="text" value="<?php echo set_value('shipping_charge', isset($val) ? $val : ''); ?>" name="shipping_charge[]" id="shipping_charge" class="form-control formWidht">
                                                    <span class='error vlError'><?php echo form_error('shipping_charge'); ?></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-sm-2 addProdctInputCont" style="top: 25px">
                                                <a href="javascript:void(0)" class="btn btn-primary holidaybutton remove_holiday">X</a>
                                            </div>
                                        </div>
                                        <?php
                                    endforeach;
                                endif;
                                ?>

                                <!-- if holidays data is blank -->
                                <?php if (!$shipping_array): ?>
                                    <div class="col-lg-5 col-md-5 col-sm-5">
                                        <div class="form-group formWidht">
                                            <label>Amount <span style="color: red;">*</span></label>
                                            <input type="text" value="<?php echo set_value('amount', isset($result->amount) ? $result->amount : ''); ?>" name="amount[]" id="amount" class="form-control formWidht">
                                            <span class='error vlError'><?php echo form_error('amount[]'); ?></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-5 col-md-5 col-sm-5">
                                        <div class="form-group formWidht">
                                            <label>Shipping Charge <span style="color: red;">*</span></label>
                                            <input type="text" value="<?php echo set_value('shipping_charge', isset($result->shipping_charge) ? $result->shipping_charge : ''); ?>" name="shipping_charge[]" id="shipping_charge" class="form-control formWidht">
                                            <span class='error vlError'><?php echo form_error('shipping_charge'); ?></span>
                                        </div>
                                    </div>
                                <?php endif; ?>

                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 text-center" style="top: 25px">
                                <div>
                                    <a href="javascript:void(0)" class="btn btn-primary holidaybutton add_shipping" ><span class="glyphicon glyphicon-plus-sign" style="margin-right:4px;"></span>   Add More Inputs</a>
                                </div>
                            </div>
                            <!--  <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form-group formWidht">
                                    <label>Max Weight </label>
                                    <input class="form-control formWidht" type="number" placeholder="max limit weight" value="<?php echo set_value('max_limit_weight', isset($result->max_limit_weight)?$result->max_limit_weight:''); ?>" name="max_limit_weight" maxlength="6" min="1">
                                    <span class='error vlError'><?php echo form_error('max_limit_weight'); ?></span>
                                </div>                                 
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form-group formWidht">
                                    <label>Brand Name</label>
                                    <select name="brand_name[]" id="brand_name" class="form-control" multiple>
                                        <option value="IMPORTED" <?php echo (in_array('IMPORTED',$brand_name) ) ? 'selected' : ''; ?> >IMPORTED</option>
                                        <option value="WINE" <?php echo (in_array('WINE', $brand_name) ) ? 'selected' : ''; ?> >WINE</option>
                                        <option value="DOMESTIC" <?php echo (in_array('DOMESTIC', $brand_name) ) ? 'selected' : ''; ?>>DOMESTIC</option>
                                        <option value="BEER" <?php echo (in_array('BEER', $brand_name) ) ? 'selected' : ''; ?>>BEER</option>
                                    </select>
                                    <span class='error vlError'><?php echo form_error('brand_name'); ?></span>
                                </div>                                 
                            </div> -->
                        </div>
                        </div>
                        <br/><br/>

                        <div class="row">
                            <div class="ibox-title">
                                <h2 style="display: inline-block;">State Holidays</h2>
                            </div>
                            <div class="ibox-content contentBorder">
                                <div class="row">
                                    <div class="valueScrollAdd1">
                                        <div id="append_holidays">
                                            <?php
                                            $holiday_dates = $result->holiday_date ? explode('<@>', $result->holiday_date) : array();
                                            $holiday_titles = $result->holiday_title ? explode('<@>', $result->holiday_title) : array();
                                            $hids = $result->holiday_id ? explode('<@>', $result->holiday_id) : array();
                                            ?>
                                            <!-- if Holidays exist -->

                                            <?php
                                            if (!empty($holiday_dates)):
                                                foreach ($holiday_dates as $key => $val):
                                                    ?>
                                                    <div class="rm">
                                                        <div class="col-lg-5 col-md-5 col-sm-5 addProdctInputCont ">
                                                            <div class="form-group formWidht">
                                                                <input type="text" placeholder="Date" name="holiday_date[]" value="<?php echo $val; ?>" class="form-control formWidht holiday_date">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-5 col-md-5 col-sm-5 addProdctInputCont ">
                                                            <div class="form-group formWidht">
                                                                <input type="text" placeholder="Title" name="title[]" value="<?php if(isset($holiday_titles[$key])) echo $holiday_titles[$key]; ?>" class="form-control formWidht">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-2 addProdctInputCont">
                                                            <a href="javascript:void(0)" class="btn btn-primary remove_holiday">X</a>
                                                        </div>
                                                        <input type="hidden" name="hid[]" value="<?php echo $hids[$key]; ?>" />
                                                    </div>
                                                    <?php
                                                endforeach;
                                            endif;
                                            ?>

                                            <!-- if holidays data is blank -->
                                            <?php if (!$holiday_dates): ?>
                                                <div class="col-lg-5 col-md-5 col-sm-5 addProdctInputCont ">
                                                    <div class="form-group formWidht">
                                                        <input type="text" placeholder="Date" name="holiday_date[]" value="" class="form-control formWidht holiday_date">
                                                    </div>
                                                </div>
                                                <div class="col-lg-5 col-md-5 col-sm-5 addProdctInputCont ">
                                                    <div class="form-group formWidht">
                                                        <input type="text" placeholder="Title" name="title[]" value="" class="form-control formWidht">
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                                        <div>
                                            <a href="javascript:void(0)" class="btn btn-primary holidaybutton add_holiday" ><span class="glyphicon glyphicon-plus-sign" style="margin-right:4px;"></span>   Add More Holiday</a>
                                        </div>
                                    </div>


                                </div>
                            </div>

                        </div><br/><br/>

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12  text-center" style="">
                                <input type="submit" name="save" value="UPDATE " class="btn btn-primary block full-width m-b updateProductBtn"/>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
        </form>
    </div>
</div>
</div>


<!-- Holiday add remove-->
<div id="holidays" style="display:none;">
    <div class="rm">
        <div class="col-lg-5 col-md-5 col-sm-5 addProdctInputCont ">
            <div class="form-group formWidht">
                <input type="text" placeholder="Date" name="holiday_date[]"  required  value="" class="form-control formWidht hdate">
            </div>
        </div>
        <div class="col-lg-5 col-md-5 col-sm-5 addProdctInputCont ">
            <div class="form-group formWidht">
                <input type="text" placeholder="Title" name="title[]"  required  value="" class="form-control formWidht">
            </div>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-2 addProdctInputCont ">
            <a href="javascript:void(0)" class="btn btn-primary holidaybutton remove_holiday" >X</a>
        </div>
    </div>
</div>

<!-- Shipping add remove-->
<div id="shipping_ch" style="display:none;">
    <div class="rm">
        <div class="col-lg-5 col-md-5 col-sm-5">
            <div class="form-group formWidht">
                <label>Amount <span style="color: red;">*</span></label>
                <input type="text" value="<?php echo set_value('amount'); ?>" name="amount[]" id="amount" class="form-control formWidht">
                <span class='error vlError'><?php echo form_error('amount[]'); ?></span>
            </div>
        </div>
        <div class="col-lg-5 col-md-5 col-sm-5">
            <div class="form-group formWidht">
                <label>Shipping Charge <span style="color: red;">*</span></label>
                <input type="text" value="<?php echo set_value('shipping_charge'); ?>" name="shipping_charge[]" id="shipping_charge" class="form-control formWidht">
                <span class='error vlError'><?php echo form_error('shipping_charge[]'); ?></span>
            </div>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-2 addProdctInputCont " style="top: 25px">
            <a href="javascript:void(0)" class="btn btn-primary holidaybutton remove_holiday" >X</a>
        </div>
    </div>
</div>
