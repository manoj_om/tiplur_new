<?php

/**
 * Description : This Dao class is responsible of all the database related operation for API
 */
class Api_dao extends CI_Model {
	public function __construct() {
		parent::__construct ();
		include_once './application/objects/Response.php';
		date_default_timezone_set ( 'Asia/Calcutta' );
		$this->load->helper ( 'date' );

		// echo date('Y-m-d H:i:s'); die();
	}


	public function is_email_exist($post) {
		$response = new Response ();
		try {

			$query = $this->db->where(array('email'=>$post['email']))->get('users');
			//echo $this->db->last_query();

			if ($query->num_rows () > 0)
			{

				//send mail
				$mail = $this->send_mail($post['email']);
				if($mail){
					$response->setStatus ( 1 );
					$response->setMsg ( "Mail sent" );
					$response->setObjArray ( NULL );
				}else{
					$response->setStatus ( 0 );
					$response->setMsg ( "Error in sending mail" );
					$response->setObjArray ( NULL );
				}

			} else
			{
				$response->setStatus ( 0 );
				$response->setMsg ( "User does not exist" );
				$response->setObjArray ( NULL );
			}
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}

	//***************  check user exists **************\\

	public function isUserExist($post)
	{
		$response = new Response ();
		try {

			//$query = $this->db->or_where(array('mobile'=>$post['mobile'],'email'=>$post['email']))->get('users');
			//echo $this->db->last_query();
			$query1 = $this->db->where(array('mobile'=>$post['mobile']))->get('users');
			$query2 = $this->db->where(array('email'=>$post['email']))->get('users');


			if ($query1->num_rows () > 0 && $query2->num_rows () == 0)
			{
				$response->setStatus ( 0 );
				$response->setMsg ( "Mobile number already exist." );
				$response->setObjArray ( $query1->row() );
			}else if ($query2->num_rows () > 0 && $query1->num_rows () == 0)
			{
				$response->setStatus ( 0 );
				$response->setMsg ( "Email already exist." );
				$response->setObjArray ( $query1->row() );
			}else if ($query2->num_rows () > 0 && $query1->num_rows () > 0)
			{
				$response->setStatus ( 0 );
				$response->setMsg ( "Mobile number & email already exist." );
				$response->setObjArray ( $query1->row() );
			}else
			{
				$result  = $this->create_new_user($post);
				if($result)
				{
					$response->setStatus ( 1 );
					$response->setMsg ( "New User" );
					$response->setObjArray ( $result );
				}else{

					$response->setStatus ( 0 );
					$response->setMsg ( "something went wrong" );
					$response->setObjArray ( '' );

				}
			}
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}

	//************** End here ****************** \\

	public function create_new_user($post,$mapped=false)
	{

		$this->db->trans_start();

		$password = $post['password'];
		$insert_data = array();

		$insert_data['email'] = $post['email'] ? $post['email']:'';
		$insert_data['mobile'] = $post['mobile'] ? $post['mobile']:'';
		$insert_data['lat'] = $post['latitude'];
		$insert_data['lng'] = $post['longitude'];
		$insert_data['display_name'] = $post['username'];
		$insert_data['dob'] = convert_date_format($post['dob']) ;
		$insert_data['state_id'] = $post['state_id'];

		$insert_data['created_on'] = date('Y-m-d H:i:s');
		$insert_data['password'] = $password ? md5($password):'';
		$insert_data['city_id'] = $post['city_id'];

		if($post['is_retailer']==1){
			$insert_data['role_id'] = 2;
			$insert_data['status'] = ($mapped == true) ? '9':'3';
		}elseif($post['is_retailer']==2){
			$insert_data['status'] = ($mapped == true) ? '9':'3';
			$insert_data['role_id'] = 4;
		}
		else{
			$insert_data['status'] = ($mapped == true) ? '9':'1';
			$insert_data['role_id'] = 3;
		}
//prd($insert_data);
		//upload Id Proof
		if (isset($_FILES ['doc_file']))
		{
	      $path = './assets/uploads/document_file/';
		  $file = $this->upload_image('doc_file',$_FILES ['doc_file']['tmp_name'],$path);
		  $insert_data['doc_file'] = $file ['upload_data'] ['file_name'];
		}
		if(isset( $_FILES ['image'] ))
	    {
			$path = './assets/images/profile_image/';
		    $file = $this->upload_image('image',$_FILES ['image']['tmp_name'],$path);
		    $insert_data['image'] = $file ['upload_data'] ['file_name'];
		}

		$insert_result  = $this->db->insert('users',$insert_data);
		$last_id  = $this->db->insert_id();
		if($last_id)
		{
				$user_info = user_data($last_id);

				$user_info->address = "";
				//for retailer
				if($post['is_retailer']){

					$address = array('user_id' => $last_id, 'state_id' => $post['state_id'],'city_id' => $post['city_id'], 'name' => $post['username'], 'mobile' => $post['mobile'], 'address' => $post['address'], 'city' => $post['city'], 'state' => $post['state'], 'pincode' => $post['pincode'], 'is_default' => 1, 'created_on' => date('Y-m-d H:i:s'));
					$insert_address = $this->add_address($address);

					$address['id'] = $insert_address;

					$store = array();
					$store['store_name'] = $post['store_name'];
					$store['location_id'] = $insert_address;
					$store['licence'] = $post['licence'];
					$store['contact_person'] = $post['cp_name'] ? $post['cp_name']:'';
					$store['contact_person_mobile'] = $post['cp_mobile'] ? $post['cp_mobile']:'';
					$store['is_provide_snacks'] = $post['is_provide_snacks'] ? $post['is_provide_snacks']:'0';
					$store['min_order'] = $post['min_order'] ? $post['min_order']:'';
					$store['opening_time'] = $post['start_time'] ? $post['start_time']:'';
					$store['closing_time'] = $post['end_time'] ? $post['end_time']:'';
					$store['estimated_day'] = ($post['estimated_day'] || $post['estimated_day']==0) ? $post['estimated_day']:'';
					$store['estimated_time'] = $post['estimated_time'] ? $post['estimated_time']:'';
					$store['payment_mode'] = $post['payment_mode'] ? $post['payment_mode']:'';
					$store['grawler_charges'] = $post['grawler_charges'] ? $post['grawler_charges']:'';
					$store['grawler_price'] = $post['grawler_price'] ? $post['grawler_price']:'';
					$store['microbrewer_pub_speciality'] = $post['microbrewer_pub_speciality'] ? $post['microbrewer_pub_speciality']:'';
					$store['retailer_id'] = $last_id;
					$store['creation_date'] = date('Y-m-d H:i:s');
					$store['store_id'] = $this->add_store($store);
					$user_info->store = $store;
					$user_info->address = $address;

				}else{
					//send welcome mail to users
				//	$this->send_welcome_mail($post['email']);
				}

		}

		return $user_info? $user_info: false;
	}




	//**  Update Device information  **********\\

	public function updateDeviceData($userId, $deviceType, $deviceId, $fcmRegId)
	{
		$fcmRegId  = ($fcmRegId) ? $fcmRegId:'';
		$result = $this->db->where(array('deviceId' => $deviceId, 'user_id' => $userId))->get('device_info');
        if ($result->num_rows() > 0) {
            // update
        	$update_data = array('fcmId' => $fcmRegId, 'deviceType' => $deviceType, 'is_logged_in' => '1', 'modificationDate' => date('Y-m-d H:i:s'));
            $this->db->where(array('deviceId' => $deviceId, 'user_id' => $userId));
            $result = $this->db->update('device_info', $update_data);
            $this->db->trans_complete();
            return $result ? true : false;
        } else {

            // insert
        	/* $insert_data = array('fcmId' => $fcmRegId, 'deviceType' => $deviceType, 'is_logged_in' => '1', 'user_id' => $userId, 'deviceId' => $deviceId, 'creation_date' => date('Y-m-d H:i:s'));
            $result = $this->db->insert('device_info', $insert_data);
            $this->db->trans_complete();
            return $result ? true : false; */

        	//maintain history
        	$result = $this->db->where(array('user_id' => $userId))->get('device_info');
        	if ($result->num_rows() > 0) {
        		foreach ($result->result() as $row){
        			$insert_data = array('deviceId' => $row->deviceId, 'deviceType' => $row->deviceType, 'fcmId' => $row->fcmId, 'user_id' => $row->user_id, 'creation_date' => date('Y-m-d H:i:s'));
        			$this->db->insert('device_info_history', $insert_data);
        		}

        		//delete from table device_info
        		$this->db->delete('device_info', array('user_id' => $userId));
        		$insert_data = array('deviceId' => $deviceId, 'deviceType' => $deviceType, 'fcmId' => $fcmRegId, 'user_id' => $userId, 'is_logged_in' => '1', 'creation_date' => date('Y-m-d H:i:s'));
        		$result = $this->db->insert('device_info', $insert_data);
        		$this->db->trans_complete();
        		return $result ? true : false;
        	}else{
        		$insert_data = array('deviceId' => $deviceId, 'deviceType' => $deviceType, 'fcmId' => $fcmRegId, 'user_id' => $userId, 'is_logged_in' => '1', 'creation_date' => date('Y-m-d H:i:s'));
        		$result = $this->db->insert('device_info', $insert_data);
        		$this->db->trans_complete();
        		return $result ? true : false;
        	}
        }
	}
	//************** End here ****************** \\

	//update profile
	public function update_profile($data)
	{

		$response = new response ();
		$updated_data = array ();
		$user_id = $data ['id'];
		$db_data = array();
		$db_data['display_name'] = $data['username'];
		$db_data['email'] = $data['email'];
		//$db_data['dob'] = isset($data['dob'])?date('Y-m-d',strtotime($data['dob'])):NULL;
		$db_data['modified_on'] = date('Y-m-d H:i:s');
		$db_data['status'] = STATUS_ACTIVE;

		if(isset( $_FILES ['image'] ))
	    {
			$path = './assets/images/profile_image/';
		    $file = $this->upload_image('image',$_FILES ['image']['tmp_name'],$path);
		    $db_data['image'] = $file ['upload_data'] ['file_name'];
		}

		//upload Id Proof
		if (isset($_FILES ['doc_file']))
		{
			$path = './assets/uploads/document_file/';
			$file = $this->upload_image('doc_file',$_FILES ['doc_file']['tmp_name'],$path);
			$db_data['doc_file'] = $file ['upload_data'] ['file_name'];
		}


		// check exist user
		$check = $this->db->select ( 'id, image' )->where ( 'id', $user_id )->get ( 'users' )->first_row ();

		if ($check->id)
		{
			// update user details
			$res = $this->db->where ( 'id', $user_id )->update ('users', $db_data);
			if ($res)
			{

				$updated_user =  user_data($user_id);
				//return data
				$updated_data ['id'] = $updated_user->id;
				$updated_data ['email'] = $updated_user->email;
				$updated_data ['mobile'] = $updated_user->mobile;
				$updated_data ['display_name'] = $updated_user->display_name;
				$updated_data ['dob'] = $updated_user->dob;
				$updated_data ['address'] =  $this->get_address($updated_user->id, 1);
				$updated_data ['image'] = ($updated_user->image) ? base_url ( 'assets/images/profile_image/' . $updated_user->image ) :'';
				$updated_data ['doc_file'] = ($updated_user->doc_file) ? base_url ( 'assets/uploads/document_file/' . $updated_user->doc_file ) :'';

				if($data['is_retailer']){

					$store = array();

					$store['id'] = $data['store_id'];
					$store['store_name'] = $data['store_name'];
					$store['licence'] = $data['licence'];
					$store['contact_person'] = $data['cp_name'];
					$store['contact_person_mobile'] = $data['cp_mobile'];
					$store['is_provide_snacks'] = $data['is_provide_snacks'];
					$store['min_order'] = $data['min_order'];
					$store['modification_date'] = date('Y-m-d H:i:s');

					$update_store = $this->update_store($store);
					$updated_data ['store'] = $update_store;

				}

				$response->setStatus ( 1 );
				$response->setMsg ( "User Details updated successfully." );
				$response->setObjArray ( $updated_data );
			} else {
				$response->setStatus ( 0 );
				$response->setMsg ( "somethins going wrong" );
				$response->setObjArray ( $updated_data );
			}
		} else {
			$response->setStatus ( 0 );
			$response->setMsg ( "User does not exist. " );
		}
		return $response;

	}

/***********************************************************************************************/
	//update product
	public function update_product($data)
	{

		$response = new response ();
		$updated_data = array ();
		$product_id = $data ['id'];
		$db_data = array();
		$db_data['description'] = $data['description'];
		$db_data['price'] = $data['price'];
		$db_data['weight'] = $data['quantity'];
		$db_data['title'] = $data['title'];
		$db_data['IBU'] = ($data['IBU'])?$data['IBU']:'';
		$db_data['ABV'] = ($data['ABV'])?$data['ABV']:'';

		if(isset( $_FILES ['image'] ))
		{
			$path = './assets/images/product_image/';
			$file = $this->upload_image('image',$_FILES ['image']['tmp_name'],$path);
			$db_data['image'] = $file ['upload_data'] ['file_name'];
		}

		// check exist product
		$check = $this->db->select ( 'id, image' )->where ( 'id', $product_id )->get ( 'product' )->first_row ();

		if ($check->id)
		{
			// update user details
			$res = $this->db->where ( 'id', $product_id )->update ('product', $db_data);
			if ($res)
			{
				$response->setStatus ( 1 );
				$response->setMsg ( "Product Details updated successfully." );
				$response->setObjArray ( $updated_data );
			} else {
				$response->setStatus ( 0 );
				$response->setMsg ( "somethins going wrong" );
				$response->setObjArray ( $updated_data );
			}
		} else {
			$response->setStatus ( 0 );
			$response->setMsg ( "product does not exist. " );
		}
		return $response;

	}

	/***************************************************************************************************/

/************************************************************************ permit file upload *****************************************************/
	public function order_permit($data)
  {

 	 $response = new response ();
 	 $updated_data = array ();
 	 $db_data = array();
	 $db_data['user_id'] = $data ['id'];
	 $db_data['permit_avail'] = $data['permit_avail'];

 	 if(isset( $_FILES ['image'] ))
 		 {
 		 $path = './assets/images/permit_image/';
 			 $file = $this->upload_image('image',$_FILES ['image']['tmp_name'],$path);
 			 $db_data['permit_image'] = $file ['upload_data'] ['file_name'];
 	 }


 	 // check exist user
 	 $check = $this->db->select ( 'id, image' )->where ( 'id', $db_data['user_id'] )->get ( 'users' )->first_row ();

 	 if ($check->id)
 	 {
		// prd($db_data);
 		 // update user details
 		 $res = $this->db->insert ('permit_user_details', $db_data);
		 $res_id=$this->db->insert_id();
 		 if ($res)
 		 {


 			 //return data
 			 $db_data ['id'] = $res_id;
 			 $db_data ['permit_image'] = ($db_data['permit_image']) ? base_url ( 'assets/images/permit_image/' . $db_data['permit_image'] ) :'';



 			 $response->setStatus ( 1 );
 			 $response->setMsg ( "Permit Details inserted successfully." );
 			 $response->setObjArray ( $db_data );
 		 } else {
 			 $response->setStatus ( 0 );
 			 $response->setMsg ( "somethins going wrong" );
 			 $response->setObjArray ( $db_data );
 		 }
 	 } else {
 		 $response->setStatus ( 0 );
 		 $response->setMsg ( "User does not exist. " );
 	 }
 	 return $response;

  }

/************************************************************************ end permit file upload *****************************************************/

/************************************************************shipping charge calculate************************************************************************/
public function shipping_charge_in_state($data)
{
	if($data['state_id']==26)
	{
		$state=$this->db->select('shipping_charge')->where('id',$data['state_id'])->get('states')->first_row();
		$charge=json_decode($state->shipping_charge,'true');
		if($data['amount'] <= 1000 ){
                $shipping_charge = 100;
            }
            else if($data['amount'] > 1000 && $data['amount'] <= 1500 ){
                $shipping_charge = 125;
            }
            else if( $data['amount'] > 1500 && $data['amount'] <= 2000 ){
                $shipping_charge = 150;
            }
						else if( $data['amount'] > 2000 && $data['amount'] <= 2500 ){
                $shipping_charge = 175;
            }
						else if($data['amount'] > 2500 && $data['amount'] <= 3000 ){
								$shipping_charge = 200;
						}
						else if( $data['amount'] > 3000 && $data['amount'] <= 3500 ){
								$shipping_charge = 225;
						}
						else if( $data['amount'] > 3500 && $data['amount'] <= 4000 ){
								$shipping_charge = 250;
						}
						else if( $data['amount'] > 4000 && $data['amount'] <= 4500 ){
								$shipping_charge = 275;
						}
            else if( $data['amount'] > 4500 ){
                $shipping_charge = 300;
            }
	}
	else {
		$shipping_charge=0;
	}
	$result['shipping_charge']=$shipping_charge;
	return $result;
}


public function shipping_charge_in_state_extra($data)
{
	$state=$this->db->select('shipping_charge')->where('id',$data['state_id'])->get('states')->first_row();
		$charge=json_decode($state->shipping_charge,'true');
		// prd($charge);
		if(!empty($charge)){
			$amount = [];
			$sh_charge = [];
			foreach($charge as $key => $value){
				 array_push($amount, $key);
				 array_push($sh_charge, $value);
			}
			for($i=0;$i<count($amount);$i++){
				if($i==0){
					if($data['amount'] <= $amount[$i]){
						$shipping_charge = $sh_charge[$i];
					}
				}
				if($i==(count($amount)-1))
				{
					if($data['amount'] >= $amount[$i]){
						$shipping_charge = $sh_charge[$i];
					}
				}
				else{

					if($data['amount'] >= $amount[$i] && $data['amount'] <= ($amount[$i+1]-1)){
						$shipping_charge = $sh_charge[$i];
					}
				}
			}
		}
		else {
			$shipping_charge=0;
		}
		$shipping_charge = (!$shipping_charge)?'0':$shipping_charge;
		$result['shipping_charge']=$shipping_charge;
		return $result;
}
/*******************************************************************end shipping charge calculate****************************************************************/

/********************************************Face OCR********************************************/
public function face_ocr($data)
{
//	prd($_FILES);
	$body = array('transID'=>rand(),'docType'=>201);
	$ch = curl_init();
curl_setopt($ch, CURLOPT_URL,'http://truthscreen.com/api/v2.2/faceapi/token');
curl_setopt($ch, CURLOPT_HTTPHEADER, array('username: test@tiplur.in'));
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//pr($ch);
$response = curl_exec($ch);
curl_close($ch);
$decrypt_encrypted_string=$this->decrypt_encrypted_string($response,'face');
$decrypt_encrypted_string_token=json_decode($decrypt_encrypted_string);
$tokenEncrypt=$this->tokenEncrypt(array('token'=>$decrypt_encrypted_string_token->msg->secretToken));
$faceapi_verify=$this->faceapi_verify(array('tsTransID'=>$decrypt_encrypted_string_token->msg->tsTransID,'secretToken'=>$tokenEncrypt,
'Image'=>$_FILES['Image'],'document'=>$_FILES['document']));
	$decrypt_encrypted_string_last=$this->decrypt_encrypted_string($faceapi_verify,'face');
	//prd($decrypt_encrypted_string_last);
	return $decrypt_encrypted_string_last;
}

//decrypt_encrypted_string step 2
public function decrypt_encrypted_string($body,$title)
{

	$ch = curl_init();

	if($title=='face'){
		//pr($body);pr($title);
		$data=json_encode(json_decode($body));
	}
	elseif($title=='idSearch'){
		$data=$body;
	}

curl_setopt($ch, CURLOPT_URL,'http://truthscreen.com/InstantSearch/decrypt_encrypted_string');
curl_setopt($ch, CURLOPT_HTTPHEADER, array('username: test@tiplur.in','Content-Type: application/json'));
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//pr($ch);
$response  = curl_exec($ch);
curl_close($ch);
//pr($response);
	return $response;
}


//tokenEncrypt step 3
public function tokenEncrypt($body)
{
	$ch = curl_init();

curl_setopt($ch, CURLOPT_URL,'http://truthscreen.com/api/v2.2/faceapi/tokenEncrypt');
curl_setopt($ch, CURLOPT_HTTPHEADER, array('username: test@tiplur.in'));
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//pr($ch);
$response  = curl_exec($ch);
curl_close($ch);

	return $response;
}

//faceapi verify step 4

public function faceapi_verify($body)
{
	$ch = curl_init();
	//prd($body);
	$body['image']=new CURLFile ($body['Image']['tmp_name'], $body['Image']['type'], $body['Image']['name']);
	$body['document']=new CURLFile ($body['document']['tmp_name'], $body['document']['type'], $body['document']['name']);

curl_setopt($ch, CURLOPT_URL,'http://truthscreen.com/api/v2.2/faceapi/verify');
curl_setopt($ch, CURLOPT_HTTPHEADER, array('username: test@tiplur.in','Content-Type: multipart/form-data'));
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//pr($ch);
$response  = curl_exec($ch);
curl_close($ch);
//pr($response);
	return $response;
}

/*******************************************************************end face OCR****************************************************************/

/******************************************************** id search api*********************************************************************/

public function idSearch($data)
{
  $front_image=base64_encode(file_get_contents($_FILES['front_image']['tmp_name']));
	$back_image=base64_encode(file_get_contents($_FILES['back_image']['tmp_name']));
	$body = json_encode(array('transID'=>100,'docType'=>$data['doc_type'],'front_image'=>$front_image,'back_image'=>$back_image));
	$ch = curl_init();
curl_setopt($ch, CURLOPT_URL,'https://www.truthscreen.com/InstantSearch/encrypted_string');
curl_setopt($ch, CURLOPT_HTTPHEADER, array('username: test@tiplur.in','Content-Type: application/json'));
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$response = curl_exec($ch);
curl_close($ch);
//$idSearch_token=json_decode($response);

   $id_search_ocr=$this->id_search_ocr(array('requestData'=>$response));

	 $decrypt_encrypted_string_last=$this->decrypt_encrypted_string($id_search_ocr,'idSearch');

	return $decrypt_encrypted_string_last;
}

//decrypt_encrypted_string step 2
public function id_search_ocr($body)
{
	$ch = curl_init();
	$data=json_encode($body);
curl_setopt($ch, CURLOPT_URL,'https://www.truthscreen.com/api/v2.2/ocr');
curl_setopt($ch, CURLOPT_HTTPHEADER, array('username: test@tiplur.in','Content-Type: application/json'));
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$response  = curl_exec($ch);
curl_close($ch);
//prd($response);
	return $response;
}
/***********************************************************end id search**********************************************************************/


	public function upload_image($name,$tmp_name,$path)
	{
		$config ['upload_path'] = $path;
		$config ['allowed_types'] = 'jpg|png|jpeg|pdf';;
		$config ['file_name'] = md5 ( uniqid ( rand (), true ) );
		$this->load->library ( 'upload', $config );
		$this->upload->initialize ( $config );

		$sizeImage = getimagesize ( $tmp_name );
		$width = $sizeImage [0];
		$height = $sizeImage [1];
		if (! $this->upload->do_upload ($name) ) {

			echo $this->upload->display_errors ();
		}
         else {

			$data = array (
					'upload_data' => $this->upload->data ()
			);
			// print_r($data); die();
			return $data;
		}
	}

	public function find_reatilers($data, $ids)
	{

		$status  = (isset($data['is_map']) && $data['is_map']=='1') ? " IN ('1','9')": "IN('1')";
		$radius = '5';
		$device_info = " INNER JOIN ti_device_info ON ti_device_info.user_id=ti_users.id";
		if(isset($data['cat_id']))
		{
			$user_id=$this->db->select('retailer_id')->where('sub_category_id',$data['cat_id'])->group_by('retailer_id')->get('ti_product');
			$arr = array_map (function($value){
		 return $value->retailer_id;
	 } , $user_id->result());
	 //prd($arr);
	 $arr=join(',', $arr);
	 if($arr)
	 {
		 $cat_user="ti_users.id IN (".$arr.")";
	 }
	 else{
					 $cat_user="ti_users.id IN ('')";
	 }

	 if($ids){
		 $retailers  = 	$this->db->query('SELECT ti_device_info.is_logged_in, ti_users.*, ti_store.min_order, ( 6371 * acos( cos( radians('.$data['latitude'].') ) * cos( radians( `lat` ) ) * cos( radians( `lng` ) - radians('.$data['longitude'].') ) + sin( radians('.$data['latitude'].') ) * sin( radians( `lat` ) ) ) ) AS distance FROM `ti_users` INNER JOIN `ti_store` ON ti_users.id=ti_store.retailer_id '.$device_info.' WHERE ti_users.status '.$status.'  AND ti_users.state_id='.$data['state_id'].' AND (ti_users.role_id="4") AND ti_users.id NOT IN ("'.$ids.'") AND ti_device_info.is_logged_in="1"  HAVING distance <= '.$radius.' ORDER BY distance ASC')->result();
	 }else{
		 $retailers  = 	$this->db->query('SELECT ti_device_info.is_logged_in, ti_users.*, ti_store.min_order, ( 6371 * acos( cos( radians('.$data['latitude'].') ) * cos( radians( `lat` ) ) * cos( radians( `lng` ) - radians('.$data['longitude'].') ) + sin( radians('.$data['latitude'].') ) * sin( radians( `lat` ) ) ) ) AS distance FROM `ti_users` INNER JOIN `ti_store` ON ti_users.id=ti_store.retailer_id '.$device_info.' WHERE ti_users.status '.$status.' AND ti_users.state_id='.$data['state_id'].' AND (ti_users.role_id="4")  AND '.$cat_user.' AND ti_device_info.is_logged_in="1" HAVING distance <= '.$radius.' ORDER BY distance ASC')->result();
	 }
		}
		else
		{
		if($ids){
			$retailers  = 	$this->db->query('SELECT ti_device_info.is_logged_in, ti_users.*, ti_store.min_order, ( 6371 * acos( cos( radians('.$data['latitude'].') ) * cos( radians( `lat` ) ) * cos( radians( `lng` ) - radians('.$data['longitude'].') ) + sin( radians('.$data['latitude'].') ) * sin( radians( `lat` ) ) ) ) AS distance FROM `ti_users` INNER JOIN `ti_store` ON ti_users.id=ti_store.retailer_id '.$device_info.' WHERE ti_users.status '.$status.'  AND ti_users.state_id='.$data['state_id'].' AND (ti_users.role_id='.ROLE_RETAILER.' OR ti_users.role_id="4") AND ti_users.id NOT IN ("'.$ids.'") AND ti_device_info.is_logged_in="1"  HAVING distance <= '.$radius.' ORDER BY distance ASC')->result();
		}else{
			$retailers  = 	$this->db->query('SELECT ti_device_info.is_logged_in, ti_users.*, ti_store.min_order, ( 6371 * acos( cos( radians('.$data['latitude'].') ) * cos( radians( `lat` ) ) * cos( radians( `lng` ) - radians('.$data['longitude'].') ) + sin( radians('.$data['latitude'].') ) * sin( radians( `lat` ) ) ) ) AS distance FROM `ti_users` INNER JOIN `ti_store` ON ti_users.id=ti_store.retailer_id '.$device_info.' WHERE ti_users.status '.$status.' AND ti_users.state_id='.$data['state_id'].' AND (ti_users.role_id='.ROLE_RETAILER.' OR ti_users.role_id="4")  AND ti_device_info.is_logged_in="1" HAVING distance <= '.$radius.' ORDER BY distance ASC')->result();
		}
	}
		//echo $this->db->last_query(); die;

		if($retailers)
		{
			foreach($retailers as $res)
			{

				$user = array("user_id"=>$res->id);
			        $store_data = $this->get_store($user);
			      if($store_data->status == '1' && ($res->status == '1' || $res->status == '9'))
			      {
			        $current_time = strtotime(date('h:i A'));
					$return['id'] = $res->id;
					$return['username'] = $res->display_name;
					$return['status'] = $res->status;
					$return['role'] = get_rolename($res->role_id)->role_name;
					$return['sub_cat']=retailer_subcat($res->id)->sub_cat;
					$return['email'] = $res->email;
					$return['mobile'] = $res->mobile;
					$addrss = $this->get_address($return['id'], 1);
					$return['address'] = $addrss?$addrss:array();
					$return['distance'] = $res->distance;
					$return['latitude'] = $res->lat;
					$return['longitude'] = $res->lng;
					$return['speciality'] = $res->microbrewer_pub_speciality;
					$return['image']=($re->image)?base_url ( 'assets/images/profile_image/'.$re->image ):'';
					$return['min_order'] = $res->min_order;
					$return['store_id'] = $store_data->id;
					$return['store_name'] = $store_data->store_name;
					$return['opening_time'] = $store_data->opening_time;
					$return['closing_time'] = $store_data->closing_time;
					$return['estimated_day'] = $store_data->estimated_day;
					$return['estimated_time'] = $store_data->estimated_time;
					$return['is_provide_snacks'] = $store_data->is_provide_snacks;
					$return['payment_mode'] = $store_data->payment_mode;
          if($res->id =='2257')
					{
$return['is_valid_time'] = 1;
					}
					//time compare condition 15-MARCH-2018
					elseif(($current_time > strtotime($store_data->opening_time)) && ($current_time < strtotime($store_data->closing_time))){
					  $return['is_valid_time'] = 1;
					}else{ $return['is_valid_time'] = 0; }


					/*else{
						$return['store_id'] = 0;
						$return['store_name'] = "";
						$return['is_provide_snacks'] = "";
					}*/

				$x['retailer_id'] = $return['id'];
				$rating = $this->retailer_rating($x);

				if(sizeof($rating)>0){
					$return['rating'] = $rating['rating'];
				}else{
					$return['rating'] = 0;
				}

				$result[] = $return;
			    }
			}

			return $result ? $result :false;
		}else {
			return false;
		}

	}


	public function get_product_listing()
	{

		 $retailer_id = $this->input->post('retailer_id');
		 $type = $this->input->post('type');

		 if($type==1){ //liquor
			 //prd($this->input->post('is_retailer'));
			 if($this->input->post('is_retailer')==1)
			 {
				 $subcategory_id=retailer_subcat($retailer_id)->sub_cat;
				// prd($sub_cat);
			 }
				 else
				 	{// code...}
					$subcategory_id = $this->input->post('subcategory_id');
				 }

		 	//$where = array('sub_category_id'=>$subcategory_id, 'retailer_id'=>$retailer_id, 'type' => $type, 'status'=>'1', 'deleted'=>'0');
		 	$where = "pr.retailer_id=".$this->db->escape($retailer_id)." AND pr.type=".$this->db->escape($type). " AND pr.status='1' AND pr.deleted='0' ";
		 	$query = "SELECT
						pr.id,
						pr.category_id,
						cat.title category_name,
						pr.sub_category_id,
						(SELECT subcat.title FROM ti_category subcat WHERE pr.sub_category_id=subcat.id) subcategory_name,
						CONCAT("."'".base_url ( 'assets/images/product_image/' )."'".",pr.image ) product_image,
						pr.retailer_id,
						CONCAT("."'".base_url ( 'assets/images/product_image/' )."'".",pr.image ) image,
						user.display_name retailer_name,
						pr.title,
						pr.quantity,
						pr.weight,
						pr.description,
						pr.type,
						pr.IBU,
						pr.ABV,
						pr.price,
						pr.special_price,
						pr.food_category,
						pr.in_store
						FROM ti_product pr
						INNER JOIN ti_category cat ON pr.category_id=cat.id
						INNER JOIN ti_users user ON pr.retailer_id=user.id
						WHERE pr.sub_category_id IN (".$subcategory_id.") AND ". $where. " ORDER BY pr.title ASC";


		 }else{ //snacks
		 	//$where = array('retailer_id'=>$retailer_id, 'type' => $type, 'status'=>'1', 'deleted'=>'0');
		 	$where = "pr.retailer_id=".$this->db->escape($retailer_id)." AND pr.type=".$this->db->escape($type). " AND pr.status='1' AND pr.deleted='0'";
		 	$query = "SELECT pr.id, pr.category_id, cat.title category_name, pr.sub_category_id, (SELECT subcat.title FROM ti_category subcat WHERE pr.sub_category_id=subcat.id) subcategory_name, pr.retailer_id, user.display_name retailer_name, pr.title, pr.quantity, pr.weight, pr.description, pr.type, pr.price, pr.special_price, pr.food_category
					FROM ti_product pr INNER JOIN ti_category cat ON pr.category_id=cat.id INNER JOIN ti_users user ON pr.retailer_id=user.id WHERE ". $where. " ORDER BY pr.title ASC";
 }


//		 $query = $this->db->select('id, category_id, category_name, sub_category_id, subcategory_name, retailer_id, retailer_name, title, quantity, weight, description, type, price, special_price, food_category')->where($where)->get('product_detail')->result();
		// echo $this->db->last_query($query);die;
		 //get selected quantity

		 /* $query = "SELECT ol.max_limit, pr.id, pr.category_id, cat.title category_name, pr.sub_category_id, (SELECT subcat.title FROM ti_category subcat WHERE pr.sub_category_id=subcat.id) subcategory_name, pr.retailer_id, user.display_name retailer_name, pr.title, pr.quantity, pr.weight, pr.description, pr.type, pr.price, pr.special_price, pr.food_category
					FROM ti_product pr INNER JOIN ti_category cat ON pr.category_id=cat.id INNER JOIN ti_users user ON pr.retailer_id=user.id INNER JOIN ti_order_limit ol ON ol.category_id = pr.category_id WHERE ". $where. " ORDER BY pr.title ASC"; */
		 $res = $this->db->query($query);
		// echo $this->db->last_query($query);die;
		 if(sizeof($res->result())>0){
		 	foreach ($res->result() as $row){
		 		//$food = array();
		 		if($row->food_category==1){ //Liquor
		 			//$food[$row->food_category] = "Liquor";
		 			$row->food_category = "Liquor";
		 		}else if($row->food_category==2){ //veg
		 			//$food[$row->food_category] = "Veg";
		 			$row->food_category = "Veg";
		 		}else if($row->food_category==3){ //non-veg
		 			//$food[$row->food_category] = "Non-veg";
		 			$row->food_category = "Non-veg";
		 		}
		 		if(!$this->input->post('is_retailer')){
		 			$quantity = $this->db->select('quantity, weight')->where(array('user_id'=>$this->input->post('user_id'), 'product_id'=>$row->id, 'retailer_id'=>$this->input->post('retailer_id')))->get('cart')->result();
		 			$row->quantity_added = sizeof($quantity)>0?$quantity[0]->quantity:0;
		 			if($type==2){ //snacks
		 				$row->weight_added = $quantity[0]->weight?$quantity[0]->weight:"";
		 			}
		 		}

		 	}

		 	if($type==2){ //snacks
		 		return $this->group_by_id($res->result());
		 	}else{
		 		return $res->result();
		 	}
		 }else{
		 	return false;
		 }


	}

/******************************pub product listing****************************************************/

public function get_pub_product_listing()
{

	 $retailer_id = $this->input->post('retailer_id');
	 $type = $this->input->post('type');

	 if($type==1){ //liquor
		 //prd($this->input->post('is_retailer'));
		 if($this->input->post('is_retailer')==1)
		 {
			 $subcategory_id=retailer_subcat($retailer_id)->sub_cat;
			// prd($sub_cat);
		 }
			 else
				{
				$subcategory_id = $this->input->post('subcategory_id');
			 }


		$where = "pr.retailer_id=".$this->db->escape($retailer_id)." AND pr.type=".$this->db->escape($type). " AND pr.status='1' AND pr.deleted='0' ";
		$query = "SELECT
					pr.id,
					pr.category_id,
					cat.title category_name,
					pr.sub_category_id,
					(SELECT subcat.title FROM ti_category subcat WHERE pr.sub_category_id=subcat.id) subcategory_name,
					pr.retailer_id,
					CONCAT("."'".base_url ( 'assets/images/product_image/' )."'".",pr.image ) image,
					user.display_name retailer_name,
					pr.title,
					pr.quantity,
					pr.weight,
					pr.description,
					pr.type,
					pr.IBU,
					pr.ABV,
					pr.price,
					pr.special_price,
					pr.food_category,
					pr.in_store
					FROM ti_product pr
					INNER JOIN ti_category cat ON pr.category_id=cat.id
					INNER JOIN ti_users user ON pr.retailer_id=user.id
					WHERE pr.sub_category_id IN (".$subcategory_id.") AND ". $where. " GROUP BY pr.sub_category_id ASC";


	 }else{ //snacks

		$where = "pr.retailer_id=".$this->db->escape($retailer_id)." AND pr.type=".$this->db->escape($type). " AND pr.status='1' AND pr.deleted='0'";
		$query = "SELECT pr.id, pr.category_id, cat.title category_name, pr.sub_category_id, (SELECT subcat.title FROM ti_category subcat WHERE pr.sub_category_id=subcat.id) subcategory_name, pr.retailer_id, user.display_name retailer_name, pr.title, pr.quantity, pr.weight, pr.description, pr.type, pr.price, pr.special_price, pr.food_category
				FROM ti_product pr INNER JOIN ti_category cat ON pr.category_id=cat.id INNER JOIN ti_users user ON pr.retailer_id=user.id WHERE ". $where. " ORDER BY pr.title ASC";
}

	 $res = $this->db->query($query);
	 echo $this->db->last_query($query);die;
	 if(sizeof($res->result())>0){
		foreach ($res->result() as $row){
			//$food = array();
			if($row->food_category==1){ //Liquor
				//$food[$row->food_category] = "Liquor";
				$row->food_category = "Liquor";
			}else if($row->food_category==2){ //veg
				//$food[$row->food_category] = "Veg";
				$row->food_category = "Veg";
			}else if($row->food_category==3){ //non-veg
				//$food[$row->food_category] = "Non-veg";
				$row->food_category = "Non-veg";
			}
			if(!$this->input->post('is_retailer')){
				$quantity = $this->db->select('quantity, weight')->where(array('user_id'=>$this->input->post('user_id'), 'product_id'=>$row->id, 'retailer_id'=>$this->input->post('retailer_id')))->get('cart')->result();
				$row->quantity_added = sizeof($quantity)>0?$quantity[0]->quantity:0;
				if($type==2){ //snacks
					$row->weight_added = $quantity[0]->weight?$quantity[0]->weight:"";
				}
			}

		}

		if($type==2){ //snacks
			return $this->group_by_id($res->result());
		}else{
			return $res->result();
		}
	 }else{
		return false;
	 }


}

/************************************************************************************/
	/*
	 * @author: Anjani Gupta
	 * @date: 24/10/2017
	 * @method: update_cart
	 * @desc: add/delete cart products
	 * @params: action, product_id, retailer_id, quantity, price
	 */

	public function update_cart($cart){
		$response = array();
		$product =	array();
		$product['product_id'] = $cart['product_id'];
		$product['retailer_id']= $cart['retailer_id'];
		$product['user_id']= $cart['user_id'];
		$product['quantity']= $cart['quantity'];
		$product['price']= $cart['price'];
		$product['weight']= $cart['weight'];
		$state_id= get_state_id($product['retailer_id']);
		//print_r(array($product));die;

		/* 20 MARCH 2018 - NEW LOC For Check Limit */
           //$check_order_limit = $this->get_todays_order($cart['user_id'], array($product), $state_id,true);
		/* END HERE */

		$stateData = get_state_detail($state_id);
		// prd($stateData);
		if($stateData != 0){
			$check_order_limit = $this->check_weight($cart['user_id'], array($product), $state_id,true);
		}else{
			$check_order_limit = $this->get_todays_order($cart['user_id'], array($product), $state_id,true);
		}

		if($check_order_limit['status']){

			$where = array('product_id'=>$cart['product_id'],'retailer_id'=>$cart['retailer_id'],'user_id'=>$cart['user_id']);
			if($cart['action']=='add'){ //insert into cart
				$query = $this->db->where($where)->get('cart');
				if(sizeof($query->result())>0){
					$data = array(
							'quantity' => $cart['quantity'],
							'price' => $cart['price']*$cart['quantity'],
							'weight' => $cart['weight'],
							'city_id' => $cart['city_id'] ? $cart['city_id']:'0',
							'abandoned_cart_flag' =>'0'
					);
					$this->db->where($where);
					$result = $this->db->update('cart', $data);
				}else{
					$data = array(
							'product_id' => $cart['product_id'],
							'retailer_id' => $cart['retailer_id'],
							'user_id' => $cart['user_id'],
							'quantity' => $cart['quantity'],
							'price' => $cart['price']*$cart['quantity'],
							'weight' => $cart['weight'],
							'city_id' => $cart['city_id'] ? $cart['city_id']:'0',
							'abandoned_cart_flag' =>'0',
							'created_on' => date('Y-m-d H:i:s')
					);
					$result = $this->db->insert('cart', $data);
				}
			}else if($cart['action']=='minus'){ //remove from cart if quantity is zero
				if($cart['quantity']!=0){
					$data = array(
							'quantity' => $cart['quantity'],
							'price' => $cart['price']*$cart['quantity'],
							'weight' => $cart['weight'],
							'abandoned_cart_flag' =>'0',
							'city_id' => $cart['city_id'] ? $cart['city_id']:'0'
					);
					$this->db->where($where);
					$result = $this->db->update('cart', $data);
				}else{
					$result = $this->db->delete('cart', $where);
					//remove form cart if only snacks available in cart
					$this->remove_cart_product($cart);
				}
			}else{ //remove product if action is delete
				$result = $this->db->delete('cart', $where);
				//remove form cart if only snacks available in cart
				$this->remove_cart_product($cart);
			}

			if($result){
				$response['status'] = true;
				$q1 = "SELECT COUNT(*) count_l FROM ti_cart INNER JOIN ti_product ON ti_cart.product_id = ti_product.id WHERE ti_product.type=1 AND ti_cart.user_id=".$this->db->escape($cart['user_id']);
				$q2 = "SELECT COUNT(*) count_s FROM ti_cart INNER JOIN ti_product ON ti_cart.product_id = ti_product.id WHERE ti_product.type=2 AND ti_cart.user_id=".$this->db->escape($cart['user_id']);
				$r1= $this->db->query($q1);
				$r2= $this->db->query($q2);
				$response['count'] = array('liquor'=>$r1->result()[0]->count_l, 'snacks'=>$r2->result()[0]->count_s);
			}else{
				$response['status'] = false;
				$response['count'] = 0;
				$response['msg'] = "Error while updating cart.";
			}

		}else{
			$response['status'] = false;
			$response['count'] = 0;
			$response['msg'] = $check_order_limit['msg'];  //"Sorry! Your order is exceeding the limit.";
		}

		return $response;

	}

	/*
	 * @author: Anjani Gupta
	 * @date: 24/10/2017
	 * @method: get_cart
	 * @desc: get cart detail
	 * @params: user_id
	 */
	public function get_cart($cart){
		$cartData = array();
		$liquor = array();
		$snacks = array();
		$where = array('cart.user_id'=>$cart['user_id']);
		$this->db->select('cart.product_id, cart.retailer_id, cart.user_id, cart.quantity, cart.weight, cart.price total_price, cart.city_id,product.title, product.type, product.price unit_price');
		$this->db->from('cart');
		$this->db->where($where);
		$this->db->join('product', 'cart.product_id = product.id');
		$query = $this->db->get();

		if(sizeof($query->result())>0){

			foreach ($query->result() as $row){
				if($row->type==1){ //liquor
					$row->weight = $row->weight;
					array_push($liquor, (array)$row);
				}else{ //snacks
					array_push($snacks, (array)$row);
				}
				$retailerId = $row->retailer_id;
			}
			$cartData['liquor'] = $liquor;
			$cartData['snacks'] = $snacks;
			$store =  $this->db->select('*')->where(array('retailer_id'=>$retailerId))->get('store')->row();
			$cartData['is_provide_snacks'] = $store->is_provide_snacks;
			$cartData['payment_mode'] = $store->payment_mode;
		}
		return $cartData;
	}

	/*
	 * @author: Anjani Gupta
	 * @date: 24/10/2017
	 * @method: add_address
	 * @desc: add multiple addresses of user
	 * @params: user_id, address, state, city, pincode
	 */

	public function add_address($user){
		if($user['is_default']=='1'){
			$userData = array('is_default'=>'0');
			$this->db->where(array('user_id'=>$user['user_id']))->update('users_address', $userData);
			$this->db->where(array('id'=>$user['user_id']))->update('users', array('state_id'=>$user['state_id'], 'city_id'=>$user['city_id']));
		}

	 	$query = $this->db->insert('users_address', $user);
	 	return $query ? $this->db->insert_id() : false;
	}



	/*
	 * @author: Anjani Gupta
	 * @date: 24/10/2017
	 * @method: payment_details_submit
	 * @desc: add multiple addresses of user
	 * @params:
	 */

	public function payment_details_submit(){
		$data=$this->input->post();

		$status=array('payment_status'=>$data['status']);
		$payemt_order=$this->db->select('*')->where(array('orderId'=>$data['orderId']))->get('payments')->result();
		if(count($payemt_order)>0)
		{
			$this->db->where(array('orderId' => $data['orderId']));
		$query=	$this->db->update('payments', $data);
		}
			else {
				$query = $this->db->insert('payments', $data);
			}
			if($data['status']=='Failure'||$data['status']=='failure')
			{
				$status_order=array('status'=>'6');
				$this->db->where(array('order_id' => $data['orderId']));
				$this->db->update('order_detail', $status_order);
				$this->db->where(array('order_number' => $data['orderId']));
				$this->db->update('trans_order_status', $status_order);

			}
			else {
				$this->db->where(array('order_id' => $data['orderId']));
				$this->db->update('order_detail',array('status'=>'7'));
				$query = "SELECT od.retailer_id,od.product_id,ord.total_amount,od.address,od.quantity,od.payment_mode, od.user_id FROM ti_order_detail od
						INNER JOIN ti_order ord ON ord.order_number=od.order_id WHERE od.order_id=".$this->db->escape($data['orderId']);
            $result = $this->db->query($query)->result();
					//	prd($result);
						$total_unit = 0;
						$pro_details = "";
						$pro = array();
						foreach ($result as $val){
							$total_unit = $total_unit + $val->quantity;
							$pro_details = product_detail($val->product_id);
						//	prd($pro_details);
							array_push($pro, $pro_details->title);
		          	}
							//	prd($pro);
				/*
				 * send notification to retailer to accept/decline order
				 * send text message to retailer to accept/decline order
				 */
				$getUser = user_data($result[0]->user_id);
        $row['order_id'] = $data['orderId'];
				$row['title'] = "New Order Request";
				$row['message'] = "Order requested by ".$getUser->display_name.". Please confirm the order.";
				$row['notified_to'] = $result[0]->retailer_id;
				$row['requested_by'] = $result[0]->user_id;
				$row['type'] = ORDER_REQUEST_NOTIFICATION;
				$row['reason'] = "";

				//-------1. PUSH NOTIFICATION----------//
				$notify = new Push_notification();
				$notify->send_notification($row);

				 /*****************************total amount*****************************************/
				/** $total_amount=($amount!=0||$amount!='')?$amount:$total_amount; ***/
				//-------2. TEXT MESSAGE NOTIFICATION----------//
				//Implement SMS gateway here
				$retailerData = user_data($result[0]->retailer_id);
				//	$mob_array = array('9454197032');
				//$mob_array = array('9599766074','9716256074', $retailerData->mobile);
				$mob_array = array('8800793801', '9929334048', '9718891005', $retailerData->mobile);
				$getUserAddress = user_address($result[0]->address);
				$prods = implode(",", $pro);
				$msg = "PLEASE ACCEPT OR DECLINE THE ORDER ON TIPLUR APP  FROM ".store_detail_by_retailer_id($result[0]->retailer_id)->store_name."  FOR ".$getUser->display_name.", ".$getUserAddress->address.", ".$prods.", total units ".$total_unit." AND value Rs ".$result[0]->total_amount.". Customer mobile number " .$getUser->mobile;
				for($i=0;$i<sizeof($mob_array);$i++){
					sms_notification($msg, $mob_array[$i]);
				}

			}
		$this->db->where(array('order_id' => $data['orderId']));
		$this->db->update('order_detail', $status);
	 	return $query ? $data : false;
	}

	/*
	 * @author: Anjani Gupta
	 * @date: 25/10/2017
	 * @method: address
	 * @desc: get list of user address
	 * @params: user_id
	 */
	public function get_address($user_id, $is_default=false){
		if($is_default){
			//$this->db->join('users', 'users.id = users_address.user_id');
			$query = $this->db->select('users_address.*')->where(array('users_address.user_id'=>$user_id, 'users_address.is_default'=>$is_default, 'users_address.status'=>STATUS_ACTIVE))->get('users_address')->result();
		}else{
			//$this->db->join('users', 'users.id = users_address.user_id');
			$query = $this->db->select('users_address.*,states.is_available')->join('states','states.id=users_address.state_id','left')->where(array('users_address.user_id'=>$user_id, 'users_address.status'=>STATUS_ACTIVE))->get('users_address')->result();
		}
		return $query ? $query :false;
	}

	 /*
	 * @author: Anjani Gupta
	 * @date: 25/10/2017
	 * @method: terms
	 * @desc: get terms & conditions by state id
	 * @params: state_id
	 */

	public function get_terms($state_id, $is_retailer=NULL){
		if($is_retailer){
			$query = $this->db->select('*')->where(array('state_id'=>$state_id, 'is_retailer' => $is_retailer))->get('terms_n_conditions')->result();
		}else{
			$query = $this->db->select('*')->where(array('state_id'=>$state_id, 'is_retailer' => '0'))->get('terms_n_conditions')->result();
		}
		//convert_html_to_text



		return $query ? $query :false;
	}


	/*
	 * @author: Anjani Gupta
	 * @date: 25/10/2017
	 * @method: empty_cart
	 * @desc: empty cart data
	 * @params: user_id, retailer_id
	 */

	public function empty_cart($data){
		$where = array('user_id'=>$data['user_id'], 'retailer_id'=>$data['retailer_id']);
		$result = $this->db->delete('cart', $where);
		return $result;
	}


	/*
	 * @author: Anjani Gupta
	 * @date: 25/10/2017
	 * @method: complete_order
	 * @desc: order request by user
	 * @params: user_id, retailer_id, address_id, array of product object (product_id, quantity, price)
	 */
	public function complete_order($data){

		$product = json_decode($data['product'],true);

		$user_id = $data['user_id'];
		$categoryIds = $this->get_category_ids($product);
		//print_r($categoryIds);die;

		$state_id = get_state_id($data['retailer_id']);
		$stateData = get_state_detail($state_id);
		// prd($stateData);
		if($stateData != 0){
			$check_todays_order = $this->check_weight($user_id, $product, $state_id);
		}else{
			$check_todays_order = $this->get_todays_order($user_id, $product, $state_id);
		}

		if($check_todays_order['status']){

			$status = array();

				$this->db->trans_start();
				$insert_data = array();
				$pro = array();
				$row = array();
				$permit_id=($data['permit_id'])?$data['permit_id']:'';
				$amount=($data['total_amount'])?$data['total_amount']:'';
				$shipping_charge=($data['shipping_charge'])?$data['shipping_charge']:'';
				$internet_charge=($data['internet_charge'])?$data['internet_charge']:'';
				$row['instruction'] = $data['instruction'] ? $data['instruction']:'';
				$row['user_id'] = $user_id;
				$row['order_id'] = $this->generate_order_number($row['user_id'],$permit_id,$amount,$shipping_charge,$internet_charge);
				$row['retailer_id'] = $data['retailer_id'];
				//$row['address'] = $data['address_id'];
				$row['address'] = $this->add_delivery_address($data['address_id'], $row['order_id']);
				$total_unit = 0;
				$total_amount = 0;
				$pro_details = "";
				foreach ($product as $val){
					$row['product_id'] = $val['product_id'];
					$row['quantity'] = $val['quantity'];
					$row['price'] = $val['price'];
					$row['weight'] = $val['weight'];
					$row['modification_date'] = date('Y-m-d H:i:s');
					$row['creation_date'] = date('Y-m-d H:i:s');
					$row['payment_mode'] = $data['payment_mode'];
					if($data['payment_mode']==9)
					{
						$row['status'] = '3';
					}

						$row['permit_id'] = $permit_id;

					$total_unit = $total_unit + $val['quantity'];
					$total_amount = $total_amount + intval($val['price']*$val['quantity']);

					$pro_details = product_detail($val['product_id']);
					array_push($pro, $pro_details->title);

					array_push($insert_data, $row);
				}

				$this->db->insert_batch( 'order_detail', $insert_data );
				$this->db->insert('trans_order_status',array("order_number"=>$row['order_id'], "created_date" => date('Y-m-d H:i:s')));
				$this->empty_cart($data);
				$this->db->trans_complete();
				if ($this->db->trans_status() === FALSE)
				{
					$status['status'] = "0";
					$status['msg'] = "Error while placing order.";
				}else{
					if($data['payment_mode']!=9)
				 {
					/*
					 * send notification to retailer to accept/decline order
					 * send text message to retailer to accept/decline order
					 */
					$getUser = user_data($row['user_id']);
					$row['title'] = "New Order Request";
					$row['message'] = "Order requested by ".$getUser->display_name.". Please confirm the order.";
					$row['notified_to'] = $data['retailer_id'];
					$row['requested_by'] = $data['user_id'];
					$row['type'] = ORDER_REQUEST_NOTIFICATION;
					$row['reason'] = "";

					//-------1. PUSH NOTIFICATION----------//
					$notify = new Push_notification();
					$notify->send_notification($row);

					 /*****************************total amount*****************************************/
					 $total_amount=($amount!=0||$amount!='')?$amount:$total_amount;
					//-------2. TEXT MESSAGE NOTIFICATION----------//
					//Implement SMS gateway here
					$retailerData = user_data($data['retailer_id']);
					$storeDetail=store_detail_by_retailer_id($data['retailer_id']);
					//$mob_array = array('9599766074','9716256074', $retailerData->mobile);
					$mob_array = array('8800793801', '9929334048', '9718891005', $retailerData->mobile);
					$getUserAddress = user_address($data['address_id']);
					$prods = implode(",", $pro);
					$msg = "PLEASE ACCEPT OR DECLINE THE ORDER ON TIPLUR APP  FROM ".store_detail_by_retailer_id($data['retailer_id'])->store_name."  FOR ".$getUser->display_name.", ".$getUserAddress->address.", ".$prods.", total units ".$total_unit." AND value Rs ".$total_amount.". Customer mobile number " .$getUser->mobile;
					for($i=0;$i<sizeof($mob_array);$i++){
						sms_notification($msg, $mob_array[$i]);
					}
        }
					$status['status'] = "1";
					$status['msg'] = $row['order_id'];

				}

		}else{
			$status['status'] = "-1";
			$status['msg'] = $check_todays_order['msg'];
		}
		return $status;
	}

	/*
	 * @author: Anjani Gupta
	 * @date: 26/10/2017
	 * @method: generate_order_number
	 * @desc: Generate order number
	 * @params:
	 */
	private function generate_order_number($user_id,$permit_id,$amount,$shipping_charge,$internet_charge){
		$prefix = "TIP";
		$random_number = time();
		$state = $this->get_state_prefix($user_id);
		$user_id = $user_id;
		$order_number = $prefix.$random_number.$state.$user_id;
		$this->db->insert('order',array('order_number'=>$order_number, 'user_id'=>$user_id,'permit_image_id'=>$permit_id,'total_amount'=>$amount,'shipping_charge'=>$shipping_charge,'internet_charge'=>$internet_charge, 'created_date' => date('Y-m-d H:i:s')));
		return $order_number;
	}

	private function get_state_prefix($user_id){
		$query = "SELECT st.name FROM ti_states st INNER JOIN ti_users user ON st.id=user.state_id WHERE user.id=".$this->db->escape($user_id);
		$result = $this->db->query($query);
		$state = $result->row()->name;
		$prefix = strtoupper(substr($state, 0, 3));
		return $prefix;
	}

	/*
	 * @author: Anjani Gupta
	 * @date: 25/10/2017
	 * @method: request_order
	 * @desc: order request by user
	 * @params: order_id(optional), retailer_id, user_id
	 */

	public function order_history($data){
		$orderArray = array();

		if($data['order_id']!='0'){
			$orders = $this->db->where(array('user_id'=>$data['user_id'],'order_number'=>$data['order_id']))->order_by('created_date','DESC')->get('order')->result();
		}else{
			$orders = $this->db->where('user_id',$data['user_id'])->order_by('created_date','DESC')->get('order')->result();
		}

		foreach ($orders as $row){
			$x=array();
			$query = "SELECT tu.display_name, od.retailer_id, od.weight, od.product_id, od.quantity, od.price, od.address, od.payment_mode, od.status, od.creation_date requested_date,
						p.title, p.type, ua.name receiver, ua.mobile receiver_mob, ua.address receiver_add, ua.city, ua.state, ua.pincode FROM ti_order_detail od INNER JOIN ti_product p ON od.product_id=p.id
						INNER JOIN ti_delivery_address ua ON ua.id=od.address INNER JOIN ti_users tu ON tu.id=od.user_id WHERE od.order_id=".$this->db->escape($row->order_number);
			$result = $this->db->query($query)->result();

			$orderDetails = array();

			$orderDetails['retailer_id'] = $result[0]->retailer_id;
			$orderDetails['retailer_name'] = $result[0]->display_name;
			$x['user_id'] = $result[0]->retailer_id;
			$orderDetails['retailer_store'] = $this->get_store($x);
			$orderDetails['address'] = array('name'=>$result[0]->receiver, 'mobile'=>$result[0]->receiver_mob, 'address'=>$result[0]->receiver_add, 'city'=>$result[0]->city, 'state'=>$result[0]->state, 'pincode'=>$result[0]->pincode, 'is_default'=>'1');
			$orderDetails['payment_mode'] = $result[0]->payment_mode;
			$orderDetails['status'] = $result[0]->status;
			$orderDetails['complete_status'] = $this->db->select('status, comments ,  delivered_at, created_date as status_date')->where('order_number', $row->order_number)->get('trans_order_status')->result();
			$orderDetails['shipping_charge'] = $row->shipping_charge;
			$orderDetails['internet_charge'] = $row->internet_charge;
			$orderDetails['total_amount'] = $row->total_amount;
			$orderDetails['requested_date'] = $result[0]->requested_date;

			if($orderDetails['status']!=ORDER_REQUESTED){
				$interval  = round(abs(strtotime(date('Y-m-d H:i:s')) - strtotime($result[0]->requested_date))/60);
				if($interval<=60){
					$orderDetails['time_remaining'] = 60-$interval." Minutes";
				}else{
					$orderDetails['time_remaining'] = 0;
				}
			}else{
				$orderDetails['time_remaining'] = 0;
			}

			$products = array();
			foreach ($result as $val){
				$product = array();
				$product['id'] = $val->product_id;
				$product['title'] = $val->title;
				$product['type'] = $val->type;
				$product['quantity'] = $val->quantity;
				$product['price'] = $val->price;
				$product['weight'] = $val->weight;
				array_push($products, $product);
			}

			$orderDetails['product'] = $products;
			$orderDetails['rating'] = $this->get_ratings($data);

			$orderArray[$row->order_number] = $orderDetails;
		}

		return $orderArray;
	}

	public function get_all_categories($cat_id=false){
		$type = $this->input->post('type');
		if($cat_id){
			if($type=='pub')
		{
			$where = array('parent_id'=>0, 'is_liquor'=>1,'is_pub'=>'1', 'id'=>$cat_id, 'status' => '1');
		}
		else
		{
			$where = array('parent_id'=>0, 'is_liquor'=>1,'is_pub'=>'0', 'id'=>$cat_id, 'status' => '1');
		}
		}else{
			if($type=='pub')
			{
				$where = array('parent_id'=>0, 'is_liquor'=>1,'is_pub'=>'1', 'status' => '1');
			}
			else
			{
				$where = array('parent_id'=>0, 'is_liquor'=>1,'is_pub'=>'0', 'status' => '1');
			}
		}

		$category = $this->db->select('id, title, description, icon')->where($where)->get('category')->result();


		foreach ($category as $row){
			$row->icon = base_url().$row->icon;
		}
		return $category;
	}

	/*
	 * @author: Anjani Gupta
	 * @date: 26/10/2017
	 * @method: update_rating
	 * @desc: mark rating
	 * @params: order_id, rating
	 */
	public function update_rating($data){
		$query = $this->db->where(array('order_number'=>$data['order_id']))->get('rating')->result();
		$retailer_id = $this->db->select('retailer_id')->where(array('order_id'=>$data['order_id']))->limit(1)->get('order_detail')->result();
		if(sizeof($query)>0){
			$update_data = array('rating' => $data['rating'], 'user_id' => $data['user_id'], 'retailer_id' => $retailer_id[0]->retailer_id);
			$result = $this->db->where(array('order_number' => $data['order_id']))->update('rating', $update_data);
		}else{
			$insert_data = array('order_number' => $data['order_id'], 'rating' => $data['rating'], 'user_id' => $data['user_id'], 'retailer_id' =>  $retailer_id[0]->retailer_id, 'creation_date' => date('Y-m-d H:i:s'));
			$result = $this->db->insert('rating', $insert_data);
		}
		return $result;
	}

	/*
	 * @author: Anjani Gupta
	 * @date: 27/10/2017
	 * @method: get_ratings
	 * @desc: fetch all product ratings
	 * @params: order_id(optional), user_id
	 */
	public function get_ratings($data){
		if($data['order_id']!=0){
			$where = "WHERE ti_order.user_id=".$this->db->escape($data['user_id']);
		}else{
			$where = "WHERE ti_order.user_id=".$this->db->escape($data['user_id'])." AND ti_order.order_number=".$this->db->escape($data['order_id']);
		}

		$query = "SELECT ti_order.order_number, ti_rating.rating,  ti_rating.creation_date FROM ti_order INNER JOIN ti_rating ON ti_rating.order_number=ti_order.order_number " . $where;

		$res = $this->db->query($query)->result();
		return $res;
	}

	private function add_store($data){
		$query = $this->db->insert('store', $data);
		return $query ? $this->db->insert_id() : NULL;
	}

	/*
	 * @author: Anjani Gupta
	 * @date: 28/10/2017
	 * @method: update_price
	 * @desc: Update product price
	 * @params: product_id, user_id, price
	 */
	public function update_price($data){
		$update_data = array('price' => $data['price'], 'modified_on' => now());
		$result = $this->db->where(array('retailer_id' => $data['user_id'], 'id' => $data['product_id']))->update('product', $update_data);
		$this->db->insert('trans_product_price', array('product_id' => $data['product_id'], 'retailer_id' => $data['user_id'], 'price' => $data['price'], 'updated_at' => date('Y-m-d H:i:s')));
		return $result;
	}

	/*
	 * @author: Anjani Gupta
	 * @date: 28/10/2017
	 * @method: in_out_stock_product
	 * @desc: Update stock of product
	 * @params: product_id, user_id
	 */
	public function in_out_stock_product($data){
		$update_data = array('in_store' => $data['value'], 'modified_on' => now() );
		$result = $this->db->where(array('retailer_id' => $data['retailer_id'], 'id' => $data['product_id']))->update('product', $update_data);
		return $result;
	}

	/*
	 * @author: Anjani Gupta
	 * @date: 28/10/2017
	 * @method: remove_product
	 * @desc: Remove product
	 * @params: product_id, user_id
	 */
	public function remove_product($data){
		$update_data = array('status' => '2', 'modified_on' => now() );
		$result = $this->db->where(array('retailer_id' => $data['user_id'], 'id' => $data['product_id']))->update('product', $update_data);
		return $result;
	}


	/*
	 * @author: Anjani Gupta
	 * @date: 30/10/2017
	 * @method: get_store
	 * @desc: Get store detail of user
	 * @params: user_id
	 */
	public function get_store($data){
		$query = $this->db->select('id, min_order, store_name, opening_time,estimated_day,estimated_time, closing_time, licence, contact_person, contact_person_mobile, is_provide_snacks, payment_mode, status, creation_date')->where(array('retailer_id'=>$data['user_id']))->get('store')->result();
		return $query? $query[0]:"";
	}

	/*
	 * @author: Anjani Gupta
	 * @date: 28/10/2017
	 * @method: requested_orders
	 * @desc: list of requested orders
	 * @params: retailer_id
	 */
	public function requested_orders($data){
		$order = array();
		$orders = array();
		$time_limit = '10';
		$current_date = date('Y-m-d H:i:s');

		if($data['order_id']!='0'){

			$where = " `od`.`order_number` = '".$data['order_id']."' AND MINUTE(TIMEDIFF( '".$current_date."', `od`.`created_date` )) < ".$time_limit." AND `odd`.`retailer_id`=".$data['retailer_id']. " AND `odd`.`status`=". ORDER_REQUESTED;
		}else{

			$where = " MINUTE(TIMEDIFF( '".$current_date."', `od`.`created_date` )) < ".$time_limit." AND `odd`.`retailer_id`=".$data['retailer_id']. " AND `odd`.`status`=". ORDER_REQUESTED;
		}

		$query = "SELECT `od`.`order_number`,`od`.`shipping_charge`,`od`.`internet_charge`, `odd`.`payment_mode`,
		 DATE(`od`.`created_date`) createdDate, `od`.`created_date`, `odd`.`status`, `odd`.`address`, `odd`.`user_id`, `odd`.`status`,
		`odd`.`instruction`
					FROM `ti_order` od
					INNER JOIN `ti_order_detail` odd  on `od`.`order_number` = `odd`.`order_id`
					WHERE ".$where." GROUP BY  `od`.`order_number`";
		$res = $this->db->query($query);

		//echo $this->db->last_query();
		//print_r($res->result());
		//echo strtotime(date('Y-m-d H:i:s'));
		//echo "<br>";

		if(sizeof($res->result())>0){
			foreach ($res->result() as $row){

				if(strtotime(date('Y-m-d'))==strtotime(date($row->createdDate))){
					$interval  = abs(strtotime(date('Y-m-d H:i:s')) - strtotime($row->created_date));
					$time_remaining = ($time_limit*60)-round($interval / 60*60);
					if($time_remaining>0){
						$order['order_number'] = $row->order_number;
						$order['shipping_charge'] = $row->shipping_charge;
						$order['internet_charge'] = $row->internet_charge;
						$order['status'] = $row->status;
						$order['requested_time'] = $row->created_date;
						$order['time_left'] = gmdate("i:s", ($time_limit*60)-round($interval / 60*60));
						$order['time_remaining'] = $time_remaining;
						$order['requested_by'] = $this->db->select('display_name')->where(array('id'=>$row->user_id))->get('users')->result()[0]->display_name;
						$order['mobile_no'] = $this->db->select('mobile')->where(array('id'=>$row->user_id))->get('users')->result()[0]->mobile;
						$order['delivery_address'] = $this->db->select('name, mobile, address, city, state, pincode')->where(array('id'=>$row->address))->get('delivery_address')->result()[0];
						$pr = array();
						$pr['order_id'] = $order['order_number'];
						$pr['user_id'] = $row->user_id;
						$order['product'] = $this->order_history($pr)[$order['order_number']]['product'];
						$order['payment_mode'] = $row->payment_mode;
						$order['instruction'] = $row->instruction;
						array_push($orders, $order);
					}
				}
			}
		}


		return $orders;
	}

	/*
	 * @author: Anjani Gupta
	 * @date: 31/10/2017
	 * @method: respond_order_request
	 * @desc: confirm/decline order request
	 * @params: order_id
	 */
	public function respond_order_request($data){

		$this->db->trans_start();

		$update_data = array('status' => $data['status'], 'modification_date' => date('Y-m-d H:i:s'));
		$this->db->where(array('order_id' => $data['order_id']));
		$this->db->update('order_detail', $update_data);

		if($data['reason']){
			$comments = $data['reason'];
		}else{
			$comments = "";
		}

		$insert_data = array('order_number' => $data['order_id'], 'delivered_at' => $data['delivered_at'], 'comments' => $comments, 'status' => $data['status'], 'created_date' => date('Y-m-d H:i:s'));
		$this->db->insert('trans_order_status', $insert_data);

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return false;
		}else{

			/*
			 * send notification to retailer to accept/decline order
			 * send text message to retailer to accept/decline order
			 */
			$row = array();

			$order_data = $this->db->where( array('order_id' => $data['order_id']) )->group_by('order_id')->get('order_detail')->result();
			$retailer = user_data($order_data[0]->retailer_id);
			//user data - 05 Mar 2018
			$user_data = user_data($order_data[0]->user_id);

			if($data['status']==ORDER_ACCEPTED){
				$row['message'] = "Your order ".$data['order_id']." is accepted by the retailer ". $retailer->display_name;
				$row['title'] = "Order Confirmation";
				$row['type'] = ORDER_CONFIRMATION_NOTIFICATION;
			}else if($data['status']==ORDER_DECLINED){
				$row['message'] = "Your order ".$data['order_id']." is declined by the retailer ". $retailer->display_name;
				$row['type'] = ORDER_DECLINED_NOTIFICATION;
				$row['title'] = "Order Declined";
			}else if($data['status']==ORDER_ONGOING){
				$row['message'] = "Your order ".$data['order_id']." is out for by the retailer ". $retailer->display_name;
				$row['type'] = ORDER_ONGOING_NOTIFICATION;
				$row['title'] = "Order Ongoing";
			}else if($data['status']==ORDER_DELIVERED){
				$row['message'] = "Your order ".$data['order_id']." is delivered by the retailer ". $retailer->display_name;
				$row['type'] = ORDER_DELIVERED_NOTIFICATION;
				$row['title'] = "Order Delivered";
			}else if($data['status']==ORDER_FAILED){
				$row['message'] = "Your order ".$data['order_id']." is failed due to no response from retailer ". $retailer->display_name;
				$row['type'] = ORDER_FAILED_NOTIFICATION;
				$row['title'] = "Order Failed";
			}else {
				$row['message'] = "";
				$row['type'] = "";
				$row['title'] = "Order Confirmation";
			}

			$row['order_id'] = $data['order_id'];
			$row['notified_to'] = $order_data[0]->user_id;
			$row['requested_by'] = $order_data[0]->retailer_id;
			$row['reason'] = $comments;


			//-------1. PUSH NOTIFICATION----------//
			 $notify = new Push_notification();
			$notify->send_notification($row);
			//-------2. TEXT MESSAGE NOTIFICATION----------//
			//Implement SMS gateway here
			if($data['status']==ORDER_DELIVERED)
			{
                           $msg  = "Thanks for being on Tiplur. Merchant has confirmed the delivery of your Order. Kindly reply Yes or No at 9929334048. You may also share your precious feedback on the no. as stated above.";
		           sms_notification($msg,$user_data->mobile);
 			}

			return true;
		}

	}
	/*
	 * @author: Anjani Gupta
	 * @date: 31/10/2017
	 * @method: check_order_status
	 * @desc: check order status
	 * @params: order_id
	 */
	public function check_order_status($data){
		$query = "SELECT status FROM `ti_order_detail` WHERE `order_id` = ".$this->db->escape($data['order_id'])." GROUP BY order_id";
		$res = $this->db->query($query);
		$status = $res->row()->status;
		return $status;
	}
	/*
	 * Description: Product grouping by category id
	 */
	private function group_by_id($data){


		foreach($data as $pr)
		{
			$categories[$pr->category_name][] = $pr;
		}
		return $categories;
	}
	/*
	 * @author: Anjani Gupta
	 * @date: 02/11/2017
	 * @method: get_retailer_orders
	 * @desc: Get retailer orders
	 * @params: retailer_id
	 */
	public function get_retailer_orders($retailer_id){
		$finalPath = 	base_url().'assets/images/permit_image/';
		$this->db->select('order_detail.modification_date,od.shipping_charge,od.internet_charge,od.total_amount, order_detail.user_id, order_detail.weight,ps.permit_image,ps.permit_avail, order_detail.order_id, order_detail.status,  order_detail.creation_date,  users.display_name order_by, order_status.title status_text');
		$this->db->join('users', 'users.id = order_detail.user_id', 'inner');
		$this->db->join('order_status', 'order_status.id = order_detail.status', 'inner');
		$this->db->join('permit_user_details ps', 'ps.id = order_detail.permit_id', 'left');
		$this->db->join('order od', 'od.order_number = order_detail.order_id', 'left');
		$this->db->where(array('retailer_id'=>$retailer_id));
		$this->db->group_by('order_detail.order_id');
		$this->db->order_by('order_detail.status');
		$query = $this->db->get('order_detail')->result();

		$orders = array();
		$order = array();
		$count = 0;
		$key = $query[0]->status;
		$keyName = $query[0]->status_text;
		foreach ($query as $row){
			$count++;
			if($key==$row->status){
				array_push($order, $row);
				if($count==sizeof($query)){
					$orders[$keyName] = $order;
					$order= array();
					array_push($order, $row);
					$key = $row->status;
					$keyName = $row->status_text;
				}
			}else{
				$orders[$keyName] = $order;
				$order= array();
				array_push($order, $row);
				$key = $row->status;
				$keyName = $row->status_text;

				if($count==sizeof($query)){
					$orders[$keyName] = $order;
					$order= array();
					array_push($order, $row);
				}

			}

		}

		return $orders;

	}

	/*
     * @author: Anjani Gupta
     * @date: 02/11/2017
     * @method: retailer_order_detail
     * @desc: Get retailer order detail
     * @params: retailer_id, order_id
     */
	public function retailer_order_detail($data){
		$query = "SELECT tu.display_name, od.user_id, od.product_id,ord.shipping_charge, ord.internet_charge, od.weight, od.quantity, od.price, od.address, od.payment_mode, od.status, od.creation_date requested_date, od.order_id order_number,
						p.title, p.type, ua.name receiver, ua.mobile receiver_mob, ua.address receiver_add, ua.city, ua.state, ua.pincode,ps.permit_image,ps.permit_avail FROM ti_order_detail od INNER JOIN ti_product p ON od.product_id=p.id
						INNER JOIN ti_order ord ON ord.order_number=od.order_id INNER JOIN ti_delivery_address ua ON ua.id=od.address INNER JOIN ti_users tu ON tu.id=od.user_id LEFT JOIN ti_permit_user_details ps ON ps.id = od.permit_id WHERE od.order_id=".$this->db->escape($data['order_id'])." AND od.retailer_id=".$this->db->escape($data['retailer_id']);

		$result = $this->db->query($query)->result();

		$orderDetails = array();

		$orderDetails['user_id'] = $result[0]->user_id;
		$orderDetails['order_by'] = $result[0]->display_name;
		$orderDetails['shipping_charge'] = $result[0]->shipping_charge;
		$orderDetails['internet_charge'] = $result[0]->internet_charge;
		$orderDetails['address'] = array('name'=>$result[0]->receiver, 'mobile'=>$result[0]->receiver_mob, 'address'=>$result[0]->receiver_add, 'city'=>$result[0]->city, 'state'=>$result[0]->state, 'pincode'=>$result[0]->pincode, 'is_default'=>'1');
		$orderDetails['payment_mode'] = $result[0]->payment_mode;
		$orderDetails['status'] = $result[0]->status;
		$orderDetails['complete_status'] = $this->db->select('status, delivered_at, created_date as status_date')->where('order_number', $result[0]->order_number)->get('trans_order_status')->result();
		$orderDetails['requested_date'] = $result[0]->requested_date;
		$orderDetails['permit_image'] = ($result[0]->permit_image!='')?base_url("/assets/images/permit_image/" . $result[0]->permit_image):'';
		$orderDetails['permit_avail']=($result[0]->permit_avail!='')? $result[0]->permit_avail:'';
		$products = array();
		foreach ($result as $row){

				$product = array();
				$product['id'] = $row->product_id;
				$product['title'] = $row->title;
				$product['type'] = $row->type;
				$product['quantity'] = $row->quantity;
				$product['price'] = $row->price;
				$product['weight'] = $row->weight;
				array_push($products, $product);

		}

		$orderDetails['product'] = $products;

		return $orderDetails;
	}

	private function group_by_key($array, $key) {
		$return = array();
		foreach($array as $val) {
			$return[$val[$key]][] = $val;
		}
		return $return;
	}

	//update user address
	public function update_address($address){
		$this->db->trans_start();

		$data=array();
		$data['name'] = $address['name'];
		$data['mobile'] = $address['mobile'];
		$data['address'] = $address['address'];
		$data['city'] = $address['city'];
		$data['state'] = $address['state'];
		$data['pincode'] = $address['pincode'];
		$data['is_default'] = $address['is_default'];
		$data['modified_on'] = date('Y-m-d H:i:s');
		$data['city_id'] = $address['city_id'];
		$data['state_id'] = $address['state_id'];

		//added on 12-march-2018
		$data['address2'] = $address['address2'];



		if($address['is_retailer']){ //retailer
			//udapte user table
			$this->db->where (array('id' => $address['user_id']))->update ('users', array('lat'=>$address['latitude'], 'lng'=>$address['longitude']));
		}else{ //customer
			if($data['is_default']=='1'){
				//set is_default to zero for all address
				$this->db->where (array('user_id' => $address['user_id']))->update ('users_address', array('is_default'=>'0', 'modified_on'=>date('Y-m-d H:i:s')));
			}
		}

		$this->db->where (array('id' => $address['id']))->update ('users_address', $data);

		//$this->db->select('users_address.*, users.lat latitude, users.lng longitude');
		if($address['is_retailer']){
		   $this->db->select('users_address.*, users.lat latitude, users.lng longitude');
		}else{
           $this->db->select('users_address.*');
		}
		$this->db->from('users_address');
		$this->db->join('users', 'users.id = users_address.user_id');
		$this->db->where (array('users_address.id' => $address['id']));
		$res = $this->db->get ()->result();



		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return false;
		}else{
			return $res[0];
		}
	}

	//update retailer store
	private function update_store($store){

		$this->db->where (array('id' => $store['id']))->update ('store', $store);
		$res = $this->db->where (array('id' => $store['id']))->get ('store')->result();
		return $res[0];

	}

	//delete user address
	public function delete_address($address){
		$this->db->trans_start();
		$data=array();
		$data['is_default'] = '0';
		$data['status'] = STATUS_DELETE;
		$data['modified_on'] = date('Y-m-d H:i:s');

		$check_addres = $this->db->where (array('user_id' => $address['user_id']))->get ('users_address')->result();

		if(sizeof($check_addres)==1){
			$this->db->where (array('id' => $address['id']))->update ('users_address', $data);
		}else if(sizeof($check_addres)>1){ //set default address randomaly for multiple address
			$status = $this->db->where (array('id' => $address['id']))->update ('users_address', $data);
			if($status){

				$data=array();
				$data['is_default'] = '1';
				$data['modified_on'] = date('Y-m-d H:i:s');
				$query = "SELECT * FROM ti_users_address WHERE user_id=".$this->db->escape($address['user_id']). " AND id!=".$this->db->escape($address['id']). " AND STATUS=".STATUS_ACTIVE;
				$result = $this->db->query($query);

				$addressId = $result->result()[0]->id;

				$this->db->where (array('id' => $addressId))->update ('users_address', $data);
			}
		}

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return false;
		}else{
			return true;
		}
	}

	private function remove_cart_product($cart){
		$get_cart_product = "SELECT COUNT(*) count FROM ti_cart INNER JOIN ti_product ON ti_cart.product_id=ti_product.id WHERE ti_product.type=1 AND ti_cart.retailer_id=".$this->db->escape($cart['retailer_id'])." AND ti_cart.user_id=".$this->db->escape($cart['user_id']);
		$res = $this->db->query($get_cart_product);
		if($res->result()[0]->count=='0'){
			$data=array();
			$data['user_id'] = $cart['user_id'];
			$data['retailer_id'] = $cart['retailer_id'];
			return $this->empty_cart($data);
		}
	}
	/*
	 * @method: check order limit when user place any order
	 *
	 */
	private function check_order_limit($product, $state_id){
		$catIds = array('7'=>0, '9'=>0);
		$cats = array();
		$status = true;
		$domestic_status = true;
		$imported_status = true;
		$final_status = FALSE;
		$nonCat = array('domestic'=>0, 'imported'=>0);

		foreach ($product as $val){

			$product_id = $val['product_id'];
			$quantity = $val['quantity'];
			$price = $val['price'];
			$weight = intval($val['weight']);
			$selected_limit = $quantity * $weight;

			$get_cat = $this->get_cat_by_product($product_id);
			$cat_id = $get_cat->category_id;
			$sub_cat_id = $get_cat->sub_category_id;

			if($cat_id=='7' || $cat_id=='9'){ //For Beer(9) & Wine(7)
				if(sizeof($cats)>0){
					if(!in_array($cat_id, $cats)){
						array_push($cats, $cat_id);
						$catIds[$cat_id] = $selected_limit;
					}else{
						$catIds[$cat_id] = $catIds[$cat_id]+$selected_limit;
					}
				}else{
					array_push($cats, $cat_id);
					$catIds[$cat_id] = $selected_limit;
				}
			}else{
				//1=>Domestic
				//2=>Imported
				$prod_type = $this->db->select('type')->where(array('id'=>$sub_cat_id))->get('category')->row()->type;
				if($prod_type=='1'){
					$nonCat['domestic'] = intval($nonCat['domestic'])+intval($selected_limit);
				}else{
					$nonCat['imported'] = intval($nonCat['imported'])+intval($selected_limit);
				}
			}

		}


		$product_id = $product[0]['product_id'];
		$get_cat = $this->get_cat_by_product($product_id);
		$cat_id = $get_cat->category_id;

		if($cat_id=='7'){ //For Wine(7)
			if(intval(WINE_LIMIT)<intval($catIds['7'])){
				$status = false;
			}
		}else if($cat_id=='9'){ //For Beer(9)
			if(intval(BEER_LIMIT)<intval($catIds['9'])){
				$status = false;
			}
		}else{
			if($state_id=='29'){ //Rajsthan
				if(intval($nonCat['domestic']) > intval(DOMESTIC_PRODUCT_LIMIT_RAJSTHAN)){
					$domestic_status= false;
				}
				if(intval($nonCat['imported']) > intval(IMPORTED_PRODUCT_LIMIT_RAJSTHAN)){
					$imported_status = false;
				}
			}else{
				if(intval($nonCat['domestic']) > intval(DOMESTIC_PRODUCT_LIMIT)){
					$domestic_status= false;
				}
				if(intval($nonCat['imported']) > intval(IMPORTED_PRODUCT_LIMIT)){
					$imported_status = false;
				}
			}

		}

		if($status && $domestic_status && $imported_status){
			$final_status = TRUE;
		}

		return $final_status;
	}

	private function get_cat_by_product($product_id){
		$query = $this->db->select('*')->where(array('id'=>$product_id, 'type' => '1'))->get('product')->result();
		return $query ? $query[0] : NULL;
	}


	/*
	 * @method: update_terms
	 * @date: 09-11-2017
	 * @params: state_id, is_accepted, user_id
	 */

	public function update_terms($terms){
		$data = array();
		$data['state_id'] = $terms['state_id'];
		$data['user_id	'] = $terms['user_id'];
		$data['is_accepted'] = $terms['is_accepted'];

		$query = $this->db->where(array('state_id'=>$terms['state_id'], 'user_id'=>$terms['user_id']))->get('is_terms_accepted')->result();

		if(sizeof($query)>0){
			$data['modification_date'] = date('Y-m-d H:i:s');
			$response = $this->db->where(array('state_id'=>$terms['state_id'], 'user_id'=>$terms['user_id']))->update('is_terms_accepted', $data);
		}else{
			$data['creation_date'] = date('Y-m-d H:i:s');
			$response = $this->db->insert('is_terms_accepted', $data);
		}

		return $response;
	}

	public function pubsubcategories_list($data){
		$res = array();
	//	$cat = $this->db->select('id category_id, title category_name')->where('id',$data['category_id'])->get('category')->result();
		$subcategory = $this->db->select('id, title name, type, description,parent_id')->where('parent_id != ','0')->where(array('is_pub'=>'1','status' => '1'))->get('category')->result();
		
		return $subcategory;

	}

	public function subcategory_listing($data){
		$res = array();
		$cat = $this->db->select('id category_id, title category_name')->where('id',$data['category_id'])->get('category')->result();
		$subcategory = $this->db->select('id, title name, type, description')->where(array('parent_id'=>$data['category_id'],'status' => '1'))->get('category')->result();
		if($subcategory){
			$res['category_id'] = $cat[0]->category_id;
			$res['category_name'] = $cat[0]->category_name;
			$res['subcategories'] = $subcategory;
		}
		return $res;

	}

	private function check_weight($user_id, $products, $state_id ,$condition=false){
		$response = array();
		$status_all = true;
		$status_imported = true;
		$status_wine = true;
		$status_domestic = true;
		$status_beer = true;
		$cat_name = "";
		$allowed_limits = array();
		$allowed_limits_noncat= array();

		//ADD LOC 17-APRIL-2018 limits for products order
		$LIMIT     = get_products_limit($state_id);
		$stateData = $this->db->select('max_limit_weight, brand_name')->where('id',$state_id)->get('states')->row();
		$brand_name = explode(',', $stateData->brand_name);
            $array = [];
            $have_limit = [];
            $total_weight = '';
            $total_limit = '';
            foreach($brand_name as $stateData_){

                if($stateData_ == 'IMPORTED'){
                    $weight1 = 'imported';
                    $IMPORTED  = $LIMIT['IMPORTED'] ? $LIMIT['IMPORTED']:'0';
                    array_push($array, $weight1);
                    array_push($have_limit, $IMPORTED);
                }
                if($stateData_ == 'WINE'){
                    $weight2 = 'wine';
                    $WINE      = $LIMIT['WINE'] ? $LIMIT['WINE']:'0';
                    array_push($array, $weight2);
                    array_push($have_limit, $WINE);
                }
                if($stateData_ == 'DOMESTIC'){
                    $weight3 = 'domestic';
                    $DOMESTIC  = $LIMIT['DOMESTIC'] ? $LIMIT['DOMESTIC']:'0';
                    array_push($array, $weight3);
                    array_push($have_limit, $DOMESTIC);
                }
                if($stateData_ == 'BEER'){
                    $weight4 = 'beer';
                    $BEER      = $LIMIT['BEER'] ? $LIMIT['BEER']:'0';
                    array_push($array, $weight4);
                    array_push($have_limit, $BEER);
                }
            }

		// prd($products);
		$WINE      = $LIMIT['WINE'] ? $LIMIT['WINE']:'0';
		$BEER      = $LIMIT['BEER'] ? $LIMIT['BEER']:'0';
		$DOMESTIC  = $LIMIT['DOMESTIC'] ? $LIMIT['DOMESTIC']:'0';
		$IMPORTED  = $LIMIT['IMPORTED'] ? $LIMIT['IMPORTED']:'0';
		//END HERE


		$prevQuantity = $this->get_prev_order_weight($user_id);
		$currentQuantity = $this->get_current_order_weight($products);
		if($condition==true)
		{
		  $prevCartQuantity  = $this->get_prev_cart_weight($user_id,$products[0]['product_id']);
		  $sum_quantity = $this->get_total_weight($prevQuantity, $currentQuantity, $prevCartQuantity);

		}else{
		  $sum_quantity = $this->get_total_weight($prevQuantity, $currentQuantity);
		}

		//$allowed_limits['all'] = intval($sum_quantity['wine']) + intval($sum_quantity['domestic']) + intval($sum_quantity['imported']);
		foreach($array as $array_){
                $total_weight += intval($sum_quantity[$array_]);
            }
        $allowed_limits['all'] = intval($total_weight);

        foreach($have_limit as $have_limit_){
                $total_limit += intval($have_limit_);
            }
        $max_limit = intval($total_limit);

		//check limits for category Wine(7)
		//$max_limit = intval($WINE)+intval($DOMESTIC)+intval($IMPORTED);
		//$have_limit = 2500; //2.5L
		if($max_limit > $stateData->max_limit_weight || $max_limit < $stateData->max_limit_weight || $have_limit == 0){
			$max_limit = intval($stateData->max_limit_weight);
		}else{
			// $max_limit = intval($WINE)+intval($DOMESTIC)+intval($IMPORTED);
			$max_limit = intval($total_limit);
		}

		if(intval($allowed_limits['all']) > intval($max_limit)){

			$status_all= false;
			$cat_name = "all";

		}

		if(!in_array('IMPORTED', $brand_name)){
			$allowed_limits['imported']= intval($IMPORTED)-intval($sum_quantity['imported']);
			if(intval($allowed_limits['imported'])<0){
				$status_imported = false;
				$cat_name = "imported";
			}
		}
		if(!in_array('WINE', $brand_name)){
			$allowed_limits['wine']= intval($WINE)-intval($sum_quantity['wine']);
			if(intval($allowed_limits['wine'])<0){
				$status_wine= false;
				$cat_name = "wine";
			}
		}
		if(!in_array('DOMESTIC', $brand_name)){
			$allowed_limits['domestic']= intval($DOMESTIC)-intval($sum_quantity['domestic']);
			if(intval($allowed_limits['domestic'])<0){
				$status_domestic= false;
				$cat_name = "domestic";
			}
		}
		if(!in_array('BEER', $brand_name)){
			$allowed_limits['beer']= intval($BEER)-intval($sum_quantity['beer']);
			if(intval($allowed_limits['beer'])<0){
				$status_beer= false;
				$cat_name = "beer";
			}
		}

		if(!$status_all){
			$response['status'] = false;
			$response['msg'] = "Sorry! Your order limit is exceeded for the category ".$stateData->brand_name." products.";
			$response['data'] = NULL;
		}else if(!$status_imported){
			$response['status'] = false;
			$response['msg'] = "Sorry! Your order limit is exceeded for the category Imported products.";
			$response['data'] = NULL;
		}else if(!$status_wine){
			$response['status'] = false;
			$response['msg'] = "Sorry! Your order limit is exceeded for the category Wine products.";
			$response['data'] = NULL;
		}else if(!$status_domestic){
			$response['status'] = false;
			$response['msg'] = "Sorry! Your order limit is exceeded for the category Domestic products.";
			$response['data'] = NULL;
		}else if(!$status_beer){
			$response['status'] = false;
			$response['msg'] = "Sorry! Your order limit is exceeded for the category Beer products.";
			$response['data'] = NULL;
		}else{
			$response['status'] = true;
			$response['msg'] = "Allowed to book your order.";
			$response['data'] = $allowed_limits;
		}
		return $response;
	}

	/*
	 * Check per day order limit for a single user
	 *
	 */
	private function get_todays_order($user_id, $products, $state_id ,$condition=false){
		$response = array();
		$status_wine = true;
		$status_beer = true;
		$domestic_status = true;
		$imported_status = true;
		$cat_name = "";
		$allowed_limits = array();
		$allowed_limits_noncat= array();

		//ADD LOC 17-APRIL-2018 limits for products order
		$LIMIT     = get_products_limit($state_id);
		// prd($products);
		$WINE      = $LIMIT['WINE'] ? $LIMIT['WINE']:'0';
		$BEER      = $LIMIT['BEER'] ? $LIMIT['BEER']:'0';
		$DOMESTIC  = $LIMIT['DOMESTIC'] ? $LIMIT['DOMESTIC']:'0';
		$IMPORTED  = $LIMIT['IMPORTED'] ? $LIMIT['IMPORTED']:'0';
		//END HERE


		$prevQuantity = $this->get_prev_order_quantity($user_id);
		$currentQuantity = $this->get_current_order_quantity($products);
		if($condition==true)
		{
		  $prevCartQuantity  = $this->get_prev_cart_quantity($user_id,$products[0]['product_id']);
		  $sum_quantity = $this->get_total_quantity($prevQuantity, $currentQuantity, $prevCartQuantity);

		}else{
		  $sum_quantity = $this->get_total_quantity($prevQuantity, $currentQuantity);
		}

		//check limits for category Wine(7) & Beer(9)
		$allowed_limits['wine'] = intval($WINE)-intval($sum_quantity['wine']);
		$allowed_limits['beer']= intval($BEER)-intval($sum_quantity['beer']);

		if(intval($allowed_limits['wine'])<0){

			$status_wine= false;
			$cat_name = "wine";

		}

		if(intval($allowed_limits['beer'])<0){

			$status_beer= false;
			$cat_name = "beer";

		}

		//check limits for category Domestic
			$allowed_limits_noncat['domestic'] = intval($DOMESTIC)-intval($sum_quantity['domestic']);
			if(intval($allowed_limits_noncat['domestic'])<0){
					$domestic_status = false;
			}

		//check limits for category Imported
			$allowed_limits_noncat['imported'] = intval($IMPORTED)-intval($sum_quantity['imported']);
			if(intval($allowed_limits_noncat['imported'])<0){
				$imported_status = false;
			}


		/*if($state_id=='29'){ //Rajsthan
			$allowed_limits_noncat['domestic'] = intval(DOMESTIC_PRODUCT_LIMIT_RAJSTHAN)-intval($sum_quantity['domestic']);

			if(intval($allowed_limits_noncat['domestic'])<0){
				$domestic_status = false;
			}

			//check limits for category Imported
			$allowed_limits_noncat['imported'] = intval(IMPORTED_PRODUCT_LIMIT_RAJSTHAN)-intval($sum_quantity['imported']);

			if(intval($allowed_limits_noncat['imported'])<0){
				$imported_status = false;
			}
		}else{
			//check limits for category Domestic

			$allowed_limits_noncat['domestic'] = intval(DOMESTIC_PRODUCT_LIMIT)-intval($sum_quantity['domestic']);
			if(intval($allowed_limits_noncat['domestic'])<0){
				$domestic_status = false;
			}

			//check limits for category Imported
			$allowed_limits_noncat['imported'] = intval(IMPORTED_PRODUCT_LIMIT)-intval($sum_quantity['imported']);
			if(intval($allowed_limits_noncat['imported'])<0){
				$imported_status = false;
			}

		}*/


		if(!$status_wine && !$status_beer && !$domestic_status && !$imported_status){
			$response['status'] = false;
			$response['msg'] = "Sorry! Your order limit is exceeded for the category Beer and Wine and Domestic & Imported products.";
			$response['data'] = NULL;
		}else if(!$status_wine && !$status_beer && !$domestic_status && $imported_status){
			$response['status'] = false;
			$response['msg'] = "Sorry! Your order limit is exceeded for the category Beer and Wine and Domestic products.";
			$response['data'] = NULL;
		}else if(!$status_wine && !$status_beer && $domestic_status && !$imported_status){
			$response['status'] = false;
			$response['msg'] = "Sorry! Your order limit is exceeded for the category Beer and Wine and Imported products.";
			$response['data'] = NULL;
		}else if(!$status_wine && $status_beer && !$domestic_status && !$imported_status){
			$response['status'] = false;
			$response['msg'] = "Sorry! Your order limit is exceeded for the category Wine and Domestic & Imported products.";
			$response['data'] = NULL;
		}else if($status_wine && !$status_beer && !$domestic_status && !$imported_status){
			$response['status'] = false;
			$response['msg'] = "Sorry! Your order limit is exceeded for the category Beer and Domestic & Imported products.";
			$response['data'] = NULL;
		}else if($status_wine && $status_beer && !$domestic_status && !$imported_status){
			$response['status'] = false;
			$response['msg'] = "Sorry! Your order limit is exceeded for Domestic & Imported products..";
			$response['data'] = NULL;
		}else if($status_wine && !$status_beer && $domestic_status && !$imported_status){
			$response['status'] = false;
			$response['msg'] = "Sorry! Your order limit is exceeded for category Beer and Imported products.";
			$response['data'] = NULL;
		}else if($status_wine && !$status_beer && !$domestic_status && $imported_status){
			$response['status'] = false;
			$response['msg'] = "Sorry! Your order limit is exceeded for Beer & Domestic products.";
			$response['data'] = NULL;
		}else if(!$status_wine && $status_beer && !$domestic_status && $imported_status){
			$response['status'] = false;
			$response['msg'] = "Sorry! Your order limit is exceeded for category Wine and Imported products.";
			$response['data'] = NULL;
		}else if(!$status_wine && !$status_beer && $domestic_status && $imported_status){
			$response['status'] = false;
			$response['msg'] = "Sorry! Your order limit is exceeded for category Wine and Beer.";
			$response['data'] = NULL;
		}else if(!$status_wine && $status_beer && $domestic_status && $imported_status){
			$response['status'] = false;
			$response['msg'] = "Sorry! Your order limit is exceeded for category Wine.";
			$response['data'] = NULL;
		}else if($status_wine && !$status_beer && $domestic_status && $imported_status){
			$response['status'] = false;
			$response['msg'] = "Sorry! Your order limit is exceeded for category Beer.";
			$response['data'] = NULL;
		}else if($status_wine && $status_beer && !$domestic_status && $imported_status){
			$response['status'] = false;
			$response['msg'] = "Sorry! Your order limit is exceeded for Domestic products.";
			$response['data'] = NULL;
		}else if($status_wine && $status_beer && $domestic_status && !$imported_status){
			$response['status'] = false;
			$response['msg'] = "Sorry! Your order limit is exceeded for Imported products.";
			$response['data'] = NULL;
		}else{
			$response['status'] = true;
			$response['msg'] = "Allowed to book your order.";
			$response['data'] = $allowed_limits;
			$response['data_dom'] = $allowed_limits_noncat['domestic'];
			$response['data_imp'] = $allowed_limits_noncat['imported'];
		}

		return $response;
	}

	private function get_category_ids($product){
		$cat_ids = array();
		$product_ids = array();
		foreach ($product as $val){
			array_push($product_ids, $val['product_id']);
		}

		$ids = join("','", $product_ids);

		$query = "SELECT DISTINCT category_id FROM ti_product WHERE id IN ('$ids')";
		$res = $this->db->query($query)->result_array();


		foreach ($res as $row){
			$catids[] = $row['category_id'];
		}

		return $catids;
	}

	/*
	 * @method: holidays
	 * @date: 14-11-2017
	 * @params: state_id
	 */

	public function get_holidays($data){
		$query = $this->db->where('state_id',$data['state_id'])->get('state_holidays')->result();
		return $query?$query:NULL;
	}

	public function retailer_rating($data){
		$rating_data = array();
		$response = array();
		$users = 0;
		$query = "SELECT r.rating, COUNT(r.rating) rating_count, (SELECT count(DISTINCT user_id) FROM ti_rating rt WHERE rt.rating = r.rating AND rt.retailer_id=".$this->db->escape($data['retailer_id']).") users FROM ti_rating r WHERE r.retailer_id=".$this->db->escape($data['retailer_id'])." GROUP BY r.rating ORDER BY r.rating";
		$res = $this->db->query($query);
		$rd=array();
		if($res->num_rows()>0){

			 foreach ($res->result() as $row){
			 	$r = array();
			 	$rd[] =  $row->rating;
				$r[$row->rating] = $row->users;
				array_push($rating_data, $r);
				$users = $users + $row->users;
			}
			$result = (array)$res->result();
			$myRatings = array(1,2,3,4,5);
			$diff= array_diff($myRatings,$rd);
			//print_r($diff);die;
			/* for($i=1;$i<count($res->result());$i++){
				$rd[$i]	= $res->result()[$i];
			}
			 */
			$m =array();
			$t=false;
			foreach ($diff as $d=>$val){
				$t=$val;
				$m[]=array('rating'=>$val,'rating_count'=>0,'users'=>0);
			}
			array_splice($result,1,0,$m);
			$response["rating"] = round(rating_calculation($rating_data),1);
			$response["users"] = $users;
			$response["rating_detail"] = $result;
		}
		return $response;

	}

	public function request_count($data){
		//$query = "SELECT COUNT(DISTINCT order_id) request_count FROM `ti_order_detail` WHERE `retailer_id`=".$this->db->escape($data['retailer_id'])." AND status=".ORDER_REQUESTED;
		//$res = $this->db->query($query);
		//return $res->row()->request_count;
		$r=array();
		$r['retailer_id'] = $data['retailer_id'];
		$r['order_id'] = '0';
		$order_count = sizeof($this->requested_orders($r));
		return $order_count;
	}

	public function checkorder_rating($user_id)
	{
		$query =  $this->db->query('SELECT orders.order_number from  ti_order orders INNER JOIN ti_order_detail od ON orders.order_number=od.order_id WHERE orders.user_id = '.$user_id.' AND od.status='.ORDER_DELIVERED.'  order by orders.created_date desc limit 1')->row();
		if($query)
		{
		  $query1 = $this->db->query('SELECT id ,order_number,rating from ti_rating WHERE order_number = "'.$query->order_number.'"')->row();
		  return (empty($query1)) ?	$query->order_number :false;

		}else{ return false; }

	}

	/*
	 * Forgot password email
	 */
	private function send_mail($email){

		$subject = "Tiplur | Forgot Password";
		$randcode = $this->generate_password(6);
		$msg = "Your new login password is ".$randcode;

		$result = smtp_mail($email, $msg, $subject);

		if($result){
			$update_data = array('password' => md5($randcode));
			$this->db->where(array('email' => $email));
			return  $this->db->update('users', $update_data);
		}else{
			return false;
		}

	}

	public function send_welcome_mail($email){

		$subject = "Tiplur | Welcome";
		$msg = '<table style="margin: 0 auto; background: #f5f5f5;color: #444;font-size: 14px;border: 1px solid #ffde16;border-collapse: collapse;" width="600">
					<tr><!-- Header -->
						<td style="margin:10px auto;background:#fff">
							<img style="width:70px;margin:0 auto;display:block" src="http://tiplur.in/theme/site/img/assets/logo.png">
						</td>
					</tr>
					<tr><!-- Body -->
						<td>
							<table style="width:100%;border-collapse: collapse;">
								<tr>
									<td style="font-size:16px;color:#000;font-weight:600;padding:8px;text-align:left">
										Dear Patron,
									</td>
								</tr>
								<tr>
									 <td style="padding:8px;text-align:left;margin-bottom:10px">
										 <p>Welcome to tiplur. Thanks for registering with this new-age service that lets you
											enjoy the whole experience of engaging with your favourite tipple.
											To begin with, you can connect to your closest licensed liquor store and
											 request for home delivery of your favourite brands of spirits & beer.
											 The intent is to make the entire purchase process smarter, simpler and safer.
										  </p>
										  <p style="margin-bottom:10px;">
												We sincerely hope you enjoy this service and use it regularly.
												While we have made sure every aspect of tiplur (from ordering to delivery to payments) is smooth and efficient,
												we cannot rule out the possibility of a hiccup here and there. Just in case you happen to experience an issue,
												 we request you to do both the following: </p> <p style="margin-bottom:5px;">1. Forgive us,
												and trust us to get your issue sorted.
												</p>
											 <p style="margin-bottom:5px;">
											2. Write to us so that we fix the problem.
											(Our brand line reads, `Sit back &amp; relax�,
											and we truly want you to experience it.)
											</p>
												 <p>On behalf of the entire team at tiplur,
												I am sending you this <span style="font-weight:600;color:#ff8c00">"Welcome Mail"</span>
												with purpose and pride. You can write to me at
												<a style="color:#0000ff" href="mailto:rakshat@tiplur.in">rakshat@tiplur.in</a>
												Look forward to your encouragement and support.
												</p>
 												<p>Sincerely,<br>Rakshat Chopra</p>
 											</td>
								</tr>
							</table>
								</td>
						</tr>
						<tr><!-- Footer -->
						 <td style="text-align:center;font-size:10px;background:#ffde16;color:#000;padding: 5px;">&#x24B8; Tiplur 2018</td>
						</tr>
						</table>';
		smtp_mail($email, $msg, $subject);

	}

	private function generate_password($length){
		$alphabets = range('A','Z');
		$numbers = range('0','9');
		$additional_characters = array('$','@');
		$final_array = array_merge($alphabets, $numbers, $additional_characters);

		$password = '';

		while($length--) {
			$key = array_rand($final_array);
			$password .= $final_array[$key];
		}

		return $password;
	}

	public function is_available_in_state($state_id){
		$query = $this->db->select('status')->where(array('id'=>$state_id))->get('states')->row()->status;
		if($query=='1'){
			return true;
		}else{
			return false;
		}
	}

	private function add_delivery_address($address_id, $order_id){
		$query = $this->db->where(array('id'=>$address_id))->get('users_address')->row();
		$inserData = array();
		$inserData['order_id'] = $order_id;
		$inserData['state_id'] = $query->state_id;
		$inserData['city_id'] = $query->city_id;
		$inserData['latitude'] = $query->latitude;
		$inserData['longitude'] = $query->longitude;
		$inserData['name'] = $query->name;
		$inserData['mobile'] = $query->mobile;
		$inserData['address'] = $query->address;
		$inserData['country'] = $query->country;
		$inserData['city'] = $query->city;
		$inserData['state'] = $query->state;
		$inserData['pincode'] = $query->pincode;
		$res = $this->db->insert('delivery_address',$inserData);
		return $res?$this->db->insert_id():false;
	}

	/*
	 * @method: is_order_declined
	 * @desc: check if any order is declined by retailer for any user
	 * @date: 2017-12-13
	 */
	public function is_order_declined($user_id){
		$current_date = date('Y-m-d');
		$query = "SELECT DISTINCT order_id, retailer_id, user_id, status, modification_date declined_at FROM ti_order_detail WHERE date(modification_date)='".$current_date."' AND user_id=".$user_id." AND status=".ORDER_DECLINED;
		$res = $this->db->query($query);
		if ($res->num_rows () > 0)
		{
			$retailer_ids = $res->result();
		}else{
			$retailer_ids = false;
		}
		return $retailer_ids;
	}


	/*
	 * @method: is_store_open
	 * @desc: check if store is open
	 * @date: 2017-12-14
	 */

	public function is_store_open($retailer_id){

	}

	/*
	 * @method: auto_reject_orders
	 * @desc: Reject orders which have been not responded by retailers
	 * @date: 2017-12-14
	 */

	public function auto_reject_orders(){
		$response = array();
		$date = date('Y-m-d');
		$date_time = date('Y-m-d H:i:s');

		$currentDate = strtotime($date_time);
		$pastDate = $currentDate-(60*10); //get date & time before 10 minutes
		$formatDate = date("Y-m-d H:i:s", $pastDate);

		$query = "SELECT DISTINCT order_id FROM `ti_order_detail` WHERE creation_date < '".$formatDate."' AND date(`creation_date`) = '".$date."' AND status = ".ORDER_REQUESTED;
		$result = $this->db->query($query);

		if ($result->num_rows () > 0)
		{
			//mark all order status FAILED
			foreach ($result->result() as $row){
				$data = array();
				$data['order_id'] = $row->order_id;
				$data['delivered_at']= "";
				$data['reason']= "Auto rejected order.";
				$data['status']= ORDER_FAILED;
				$res = $this->respond_order_request($data);
				array_push($response, array($data['order_id'] => $res));
			}


		}
		return $response;
	}


	public function is_registered_mobile($mobile){
		$query = $this->db->select('*')->like('mobile', $mobile)->get('users')->result();
		return $query;
	}

	/**
	 * @method: get_user_data
	 * @param $user_id
	 * @return user data
	 */
	public function get_user_data($user_id){
		$query = $this->db->select('*')->where(array('id' => $user_id))->get('users')->result();
		unset($query[0]->password);
		return $query[0];
	}

	/**
	 * @method: logout
	 * @param $user_id, $device_id
	 * @return user data
	 */
	public function logout($data){

		$update_data = array();
		$update_data['is_logged_in'] = '0';
		$update_data['modificationDate'] = date('Y-m-d H:i:s');
		$res = $this->db->where (array('user_id' => $data['user_id'], 'deviceId' => $data['device_id']))->update ('device_info', $update_data);
		return $res;

	}


	/**
	 * @method: city_list
	 * @param  $state_id
	 * @return city list
	 */
	public function get_cities($data) {
		$query = $this->db->select('id, title')->where(array('state_id' => $data['state_id'], 'status' => STATUS_ACTIVE))->get('cities')->result();
		return $query;
	}


	/**
	 * @method: city_list
	 * @param  $state_id
	 * @return city list
	 */
	public function get_city_state_id_by_name($data) {
		$response = array();

		$cities= json_decode($data['city'],true);

		$state = $this->db->select('*')->like(array('name' => $data['state']))->get('states')->result();
		if(sizeof($cities)==1){
			$city = $this->db->select('id')->like(array('title' => $cities[0]))->get('cities')->result();
		}else{
			$this->db->or_like(array('title' => $cities[0]));
			$this->db->or_like(array('title' => $cities[1]));
			$city = $this->db->select('id')->get('cities')->result();
			//echo $this->db->last_query();die;
		}

		if(sizeof($state)>0){
			$response['state'] = $state[0];
			$response['holidays'] = $this->get_holidays(array('state_id'=>$state[0]->id));
			$response['is_holiday'] = $this->is_holiday($state[0]->id);
			$response['is_maintenance'] = '0';
			$response['is_maintenance_msg'] = 'The application will remain under maintenance mode till we have resolved the glitch, which we are sure will be done in next few hours.';
		}else{
			$response['state'] = "No state found";
			$response['status'] = 0;
		}

		if(sizeof($city)>0){
			$response['city'] = $city[0];
		}else{
			$response['city'] = "No city found";
			$response['status'] = 0;
		}
		//prd($response);
		return $response;
	}

	/**
	 * @method: notification_list
	 * @param  $user_id
	 * @return notification_list
	 */
	public function notification_list($data, $count=false){

		if($count){
			$query = "SELECT * FROM `ti_notification` WHERE creation_date >= ( CURDATE() - INTERVAL 1 DAY ) AND notified_to = ".$this->db->escape($data['user_id']). " AND seen = '0'";
			$res = $this->db->query($query);
		}else{
			$this->db->where ( 'notified_to', $data['user_id'] )->update('notification', array('seen'=>'1'));
			$query = "SELECT * FROM `ti_notification` WHERE creation_date >= ( CURDATE() - INTERVAL 1 DAY )  AND notified_to = ".$this->db->escape($data['user_id'])."
					ORDER BY `ti_notification`.`creation_date` DESC";
			$res = $this->db->query($query);
		}

		return $res->result();

	}

	private function is_holiday($state_id){
		$current_date = date('Y-m-d');
		$query = "SELECT * FROM `ti_state_holidays` WHERE state_id = ".$this->db->escape($state_id). " AND holiday_date = ".$this->db->escape($current_date);
		$res = $this->db->query($query);
		if ($res->num_rows () > 0){
			return true;
		}else{
			return false;
		}
	}


	/* Mapped Retailers - 10 March 2018 */
	public function isMappedRetailerExist($post)
	{
		$response = new Response ();
		try {
			    $result  = $this->create_new_user($post,true);
				if($result)
				{
					$response->setStatus ( 1 );
					$response->setMsg ( "New User" );
					$response->setObjArray ( $result );
				}else{

					$response->setStatus ( 0 );
					$response->setMsg ( "something went wrong" );
					$response->setObjArray ( '' );

				}

		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}


	/******* update mapped retailer Profile 12-March-2018 *******/
	public function update_mapped_profile($post)
	{
		$response = new response ();
		$updated_data = array ();
		$user_id = $post['mapped_id'];
		$userdata = user_data($user_id);
		if(!empty($userdata))
		{
			$this->db->trans_start();
			//******* Update user table *******//
			$password = $post['password'];
			$updated_data['email']   = $post['email'] ? $post['email']:'';
			$updated_data['mobile']  = $post['mobile'] ? $post['mobile']:'';
			$updated_data['lat']     = $post['latitude'];
			$updated_data['lng']     = $post['longitude'];
			$updated_data['display_name'] = $post['username'];
			$updated_data['dob']        = $post['dob'] ? $post['dob']:'';
			$updated_data['state_id']   = $post['state_id'];
			$updated_data['status']     = '1';
			$updated_data['modified_on']= date('Y-m-d H:i:s');
			$updated_data['password']   = $password ? md5($password):'';
			$updated_data['city_id']    = $post['city_id'];
			$updated_data['role_id']     = 2;

			//upload Id Proof
			if (isset($_FILES ['doc_file']))
			{
			  $path = './assets/uploads/document_file/';
			  $file = $this->upload_image('doc_file',$_FILES ['doc_file']['tmp_name'],$path);
			  $updated_data['doc_file'] = $file ['upload_data'] ['file_name'];
			}

			$this->db->where('id',$user_id);
			$update_result  = $this->db->update('users',$updated_data);
			//******** End Here ********//
			if($update_result)
			{
				$store_details = store_detail_by_retailer_id($user_id);
				/*** update address data and store data ***/

				//address
				$addressdata     = array('user_id' => $user_id, 'state_id' => $post['state_id'], 'name' => $post['username'], 'mobile' => $post['mobile'], 'address' => $post['address'], 'city' => $post['city'], 'state' => $post['state'], 'pincode' => $post['pincode'], 'is_default' => 1, 'modified_on' => date('Y-m-d H:i:s'));
				$this->db->where('id',$store_details->location_id);
				$this->db->update('users_address',$addressdata);
				$addressdata['id'] = $store_details->location_id;

				//store
				$storedata     = array('id'=>$store_details->id,'store_name'=>$post['store_name'],'location_id'=>$store_details->location_id,'licence'=>$post['licence'],'contact_person'=>$post['cp_name'],'contact_person_mobile'=>$post['cp_mobile'],'is_provide_snacks'=>$post['is_provide_snacks'],'min_order'=>$post['min_order'],'opening_time'=>$post['start_time'],'closing_time'=>$post['end_time'],'payment_mode'=>$post['payment_mode'],'retailer_id'=>$user_id,'modification_date'=>date('Y-m-d H:i:s'));
				$storedata['store_id'] = $this->update_store($storedata);

				$updated_data['store'] = $storedata['store_id'];
				$updated_data['address'] = $addressdata;

		        $response->setStatus ( 1 );
				$response->setMsg ( "User Details updated successfully." );
				$response->setObjArray ( $updated_data );
			} else {
				$response->setStatus ( 0 );
				$response->setMsg ( "somethins going wrong" );
				$response->setObjArray ( $updated_data );
			}
		} else {
			$response->setStatus ( 0 );
			$response->setMsg ( "User Id does not exist. " );
		}
		return $response;
	}

	/*
	 * @method: get_prev_order_quantity
	 */
	private function get_prev_order_quantity($user_id){
		$response = array('beer'=>0, 'wine'=>0, 'domestic'=>0, 'imported'=>0);

		$catIds = array('7'=>0, '9'=>0);
		$cats = array();
		$current_date = date('Y-m-d');
		$nonCat = array('domestic'=>0, 'imported'=>0);

		$query = "SELECT user_id, product_id, SUM(weight * quantity) AS total FROM ti_order_detail WHERE status NOT IN (".ORDER_DECLINED.",". ORDER_FAILED.") AND user_id=".$this->db->escape($user_id)." AND date(`creation_date`) = '".$current_date."' GROUP BY product_id";
		$gettodaysorder= $this->db->query($query)->result();

		//For old order
		if(sizeof($gettodaysorder)>0){

			foreach ($gettodaysorder as $row){

				$product_id = $row->product_id;
				$get_cat = $this->get_cat_by_product($product_id);
				$cat_id = $get_cat->category_id;
				$sub_cat_id = $get_cat->sub_category_id;

				if($cat_id=='7' || $cat_id=='9'){ //For Wine(7) & Beer(9)
					if(sizeof($cats)>0){
						if(!in_array($cat_id, $cats)){
							array_push($cats, $cat_id);
							$catIds[$cat_id] = $row->total;
						}else{
							$catIds[$cat_id] = $catIds[$cat_id]+$row->total;
						}
					}else{
						array_push($cats, $cat_id);
						$catIds[$cat_id] = $row->total;
					}
				}else{ //For other products which belongs to Imported/Domestic category
					//1=>Domestic
					//2=>Imported
					$prod_type = $this->db->select('type')->where(array('id'=>$sub_cat_id))->get('category')->row()->type;
					if($prod_type=='1'){
						$nonCat['domestic'] = intval($nonCat['domestic'])+intval($row->total);
					}else{
						$nonCat['imported'] = intval($nonCat['imported'])+intval($row->total);
					}
				}

			}

			$response['wine'] = $catIds['7'];
			$response['beer'] = $catIds['9'];
			$response['domestic'] = $nonCat['domestic'];
			$response['imported'] = $nonCat['imported'];
		}
		return $response;
	}

	/*
	 * @method: get_current_order_quantity
	 */
	private function get_current_order_quantity($products){
		$response = array('beer'=>0, 'wine'=>0, 'domestic'=>0, 'imported'=>0);
		$domestic_status = true;
		$imported_status = true;
		$catIds = array('7'=>0, '9'=>0);
		$cats = array();
		$current_date = date('Y-m-d');
		$nonCat = array('domestic'=>0, 'imported'=>0);

		foreach ($products as $val){

			$product_id = $val['product_id'];
			$get_cat = $this->get_cat_by_product($product_id);
			$cat_id = $get_cat->category_id;
			$sub_cat_id = $get_cat->sub_category_id;

			if($cat_id=='7' || $cat_id=='9'){ //For Wine(7) & Beer(9)
				if(sizeof($cats)>0){
					if(!in_array($cat_id, $cats)){
						array_push($cats, $cat_id);
						$catIds[$cat_id] = $val['weight']*$val['quantity'];
					}else{
						$catIds[$cat_id] = $catIds[$cat_id]+($val['weight']*$val['quantity']);
					}
				}else{
					array_push($cats, $cat_id);
					$catIds[$cat_id] = $val['weight']*$val['quantity'];
				}
			}else{ //For other products which belongs to Imported/Domestic category
				//1=>Domestic
				//2=>Imported
				$prod_type = $this->db->select('type')->where(array('id'=>$sub_cat_id))->get('category')->row()->type;
				if($prod_type=='1'){
					$nonCat['domestic'] = intval($nonCat['domestic'])+intval($val['weight']*$val['quantity']);
				}else{
					$nonCat['imported'] = intval($nonCat['imported'])+intval($val['weight']*$val['quantity']);
				}
			}
		}
		$response['wine'] = $catIds['7'];
		$response['beer'] = $catIds['9'];
		$response['domestic'] = $nonCat['domestic'];
		$response['imported'] = $nonCat['imported'];
		return $response;
	}

	/*
	 * @method: get_total_quantity
	 */
	private function get_total_quantity($prv_q, $current_q,$cart_q = null){
		$sums = array();

		//In Case Update Cart Api - 20 MARCH 2018
		$cart_beer     = !empty($cart_q['beer']) ? $cart_q['beer']:'0';
		$cart_wine     = !empty($cart_q['wine']) ? $cart_q['wine']:'0';
		$cart_domestic = !empty($cart_q['domestic']) ? $cart_q['domestic']:'0';
		$cart_imported = !empty($cart_q['imported']) ? $cart_q['imported']:'0';

		$sums['beer']     = $prv_q['beer']+$current_q['beer']+$cart_beer;
		$sums['wine']     = $prv_q['wine']+$current_q['wine']+$cart_wine;
		$sums['domestic'] = $prv_q['domestic']+$current_q['domestic']+$cart_domestic;
		$sums['imported'] = $prv_q['imported']+$current_q['imported']+$cart_imported;
		return $sums;
	}


	/* get cart quantity --- Add This LOC 0n 20 MARCH 2018 */
	private function get_prev_cart_quantity($user_id,$product_id)
	{
		$response = array('beer'=>0, 'wine'=>0, 'domestic'=>0, 'imported'=>0);

		$catIds = array('7'=>0, '9'=>0);
		$cats = array();
		$current_date = date('Y-m-d');
		$nonCat = array('domestic'=>0, 'imported'=>0);

		$query = "SELECT user_id, product_id, SUM(weight * quantity) AS total FROM ti_cart WHERE user_id=".$this->db->escape($user_id)."  AND product_id != ".$product_id." GROUP BY product_id";
		$getcartdetails= $this->db->query($query)->result();

		//For old order
		if(sizeof($getcartdetails)>0){

			foreach ($getcartdetails as $row){

				$product_id = $row->product_id;
				$get_cat = $this->get_cat_by_product($product_id);
				$cat_id = $get_cat->category_id;
				$sub_cat_id = $get_cat->sub_category_id;

				if($cat_id=='7' || $cat_id=='9'){ //For Wine(7) & Beer(9)
					if(sizeof($cats)>0){
						if(!in_array($cat_id, $cats)){
							array_push($cats, $cat_id);
							$catIds[$cat_id] = $row->total;
						}else{
							$catIds[$cat_id] = $catIds[$cat_id]+$row->total;
						}
					}else{
						array_push($cats, $cat_id);
						$catIds[$cat_id] = $row->total;
					}
				}else{ //For other products which belongs to Imported/Domestic category
					//1=>Domestic
					//2=>Imported
					$prod_type = $this->db->select('type')->where(array('id'=>$sub_cat_id))->get('category')->row()->type;
					if($prod_type=='1'){
						$nonCat['domestic'] = intval($nonCat['domestic'])+intval($row->total);
					}else{
						$nonCat['imported'] = intval($nonCat['imported'])+intval($row->total);
					}
				}

			}

			$response['wine'] = $catIds['7'];
			$response['beer'] = $catIds['9'];
			$response['domestic'] = $nonCat['domestic'];
			$response['imported'] = $nonCat['imported'];
		}
		return $response;
	}



	/******************************************************************************* Promotion *********************************************/

	// All Brands/Prodcts Name List Data
	public function get_brandName($brand_name){

	 $query = $this->db->select('id,promotion_id,brand_name,promotion_title,promotion_img,prmototion_banner_url,city,released_date,expiry_date')->where(array('promo_status' => '0' ,'deleted' => '0'))->like('brand_name' ,$brand_name,'both')->get('ti_promotion')->result();
		// echo $this->db->last_query(); die;
		$finalPath = 	base_url().'assets/images/promotion_image/';
		$newArray = array();
		$i =0;
		foreach($query as $val):
				$newArray[$i]['id'] 									= $val->id;
				$newArray[$i]['promotion_id'] 				= $val->promotion_id;
				$newArray[$i]['brand_name'] 					= $val->brand_name;
				$newArray[$i]['promotion_title'] 		= $val->promotion_title;
				$newArray[$i]['promotion_img'] 			= $finalPath.$val->promotion_img;
				$newArray[$i]['city']				 				= $val->city;
				$newArray[$i]['released_date']				= site_date($val->released_date);
				$newArray[$i]['expiry_date']				 	= site_date($val->expiry_date);
				$i++;
			endforeach;
			return $newArray ? $newArray : "";
	}
	// All History for participated User Data
	public function get_participatedUserHistory ($user_id){
	 $query = $this->db->select('participated_users.user_id,participated_users.promotion_id,participated_users.qty,participated_users.barcode,participated_users.participated_on,participated_users.user_address,promotion.brand_name')->where(array(`participated_users`.'user_id' => $user_id))->join('promotion', 'promotion.promotion_id = participated_users.promotion_id','left')->order_by("participated_users.participated_on", "DESC")->get('participated_users')->result();
		// echo $this->db->last_query(); die;
		return $query ? $query : "";
	}
	// Promotion List Data
	public function get_promotionList($userid){
	 // Helper function to get how to participated rules
	 $res = get_participateRules();
	 if(!empty($userid)):
			 $query2 = $this->db->select('id')->where(array('id' => $userid,'deleted' => '0','status' => '1'))->get('ti_users')->num_rows();
			 if( $query2 > 0  ):
					 $query = $this->db->select('promotion.id,promotion.promotion_id,promotion.brand_name,promotion.promotion_desc,promotion.promotion_title,city,promotion.released_date,promotion.released_time,promotion.expiry_date,promotion.expiry_time,promotion.promotion_img,promotion.prmototion_banner_url,promotions_terms_condtions.terms_conditions')->join('promotions_terms_condtions' , 'promotions_terms_condtions.id = promotion.terms_id' , 'left')->where(array('promotion.promo_status' => '0','promotion.deleted' => '0'))->order_by('promotion.released_date','DESC')->get('promotion')->result();
			 else:
					 return	'inactive_user';
			 endif;
	 else:
			 $query = $this->db->select('promotion.id,promotion.promotion_id,promotion.brand_name,promotion.promotion_desc,promotion.promotion_title,city,promotion.released_date,promotion.released_time,promotion.expiry_date,promotion.expiry_time,promotion.promotion_img,promotion.prmototion_banner_url,promotions_terms_condtions.terms_conditions')->join('promotions_terms_condtions' , 'promotions_terms_condtions.id = promotion.terms_id' , 'left')->where(array('promotion.promo_status' => '0','promotion.deleted' => '0'))->order_by('promotion.released_date','DESC')->get('promotion')->result();
	 endif;
			 $finalPath = 	base_url().'assets/images/promotion_image/';
			 $newArray = array();
			 $i =0;
			 foreach($query as $val):

				 $newArray[$i]['id'] 									= $val->id;
				 $newArray[$i]['promotion_id'] 				= $val->promotion_id;
				 $newArray[$i]['brand_name'] 					= $val->brand_name;
				 $newArray[$i]['promotion_title'] 			= $val->promotion_title;
			 //	$newArray[$i]['promotion_desc'] 			= $val->promotion_desc;
				 $newArray[$i]['how_to_part'] 					= $res->rules;;
				 $newArray[$i]['promotion_desc'] 			= $val->terms_conditions;
				 $newArray[$i]['promotion_img'] 			  = (image_extension($val->promotion_img) == 'jpg' || image_extension($val->promotion_img) == 'jpeg' || image_extension($val->promotion_img) == 'png') ?  $finalPath.$val->promotion_img : '';
				 $newArray[$i]['prmototion_banner_url']= (image_extension($val->prmototion_banner_url ) == 'jpg' || image_extension($val->prmototion_banner_url) == 'jpeg' || image_extension($val->prmototion_banner_url) == 'png') ?  $finalPath.$val->prmototion_banner_url : '';
				 //$newArray[$i]['prmototion_banner_url']= $finalPath.$val->prmototion_banner_url;
				 $newArray[$i]['city']				 				  = $val->city;
				 $newArray[$i]['released_date']				= site_date($val->released_date);
				 $newArray[$i]['expiry_date']				 	= site_date($val->expiry_date);
				 $i++;
			 endforeach;
			 return $newArray ? $newArray : "";
	}
	// Promotion List Data Final Updated API FUnction
	public function get_promotionDetails($id,$promotion_id,$user_id,$is_participated,$barcode,$qty,$address,$reward_points){
			 $curr_date = mdate("%Y-%m-%d");
				 // $curr_date = date("Y-m-d");
				 $fromdate = $curr_date.' '.'00:00:00';
				 $todate = $curr_date.' '.'23:59:59';
				 $get_city = explode(" ",$address);
				 $user_city	=	$get_city[0];
			$qry ="SELECT COUNT(promotion_id) as part_limit FROM `ti_participated_users` WHERE `promotion_id`='$promotion_id' AND user_id='$user_id' AND  `participated_on` BETWEEN '$fromdate' AND '$todate' ";
			$count = $this->db->query($qry)->row();
	 // 	 echo $this->db->last_query();
				 if( ($count->part_limit == 2 ) ){
					 return $count->part_limit;
			 }else{
						 $finalPath = 	base_url().'assets/images/promotion_image/';
						 $qry ="SELECT u.*,pu.*,p.* FROM `ti_users` as u LEFT JOIN ti_participated_users as pu ON pu.user_id = u.id LEFT JOIN ti_promotion as p ON p.promotion_id = pu.promotion_id WHERE ( u.id ='$user_id' ) ";
						 $row = $this->db->query($qry)->row();

			 $promotion_data = get_promotionDetails($promotion_id);
						 $useremail 			  	= $row->email;
						 // $role_id	 		= $row->role_id;
						 $promo_id 					= $promotion_data->promotion_id;
						 $promotion_title 		= $promotion_data->promotion_title;
						 $brand_name 				= $promotion_data->brand_name;

						 $promo_banner = trim($finalPath.$row->prmototion_banner_url);
						 $display_name = $row->display_name;
						 $name 				= explode(" ",$display_name);
						 $fist_name 		= $name[0];

						 $subject = "Tiplur | Scan & Play";
							$msg = '<table style="margin: 0 auto; background: #f5f5f5;color: #444;font-size: 14px;border: 1px solid #ffde16;border-collapse: collapse;" width="600">
							 <tr><!-- Header -->
							 <td style="margin:10px auto;background:#fff">
								 <img style="width:440px;margin:0 auto;display:block" src='.$promo_banner.'>
							 </td>
							 </tr>
							 <tr><!-- Body -->
							 <td>
								 <table style="width:100%;border-collapse: collapse;">
									 <tr>
										 <td style="font-size:16px;color:#000;font-weight:600;padding:8px;text-align:left">
											 Dear '.$fist_name.',
										 </td>
									 </tr>
									 <tr>

											<td style="padding:8px;text-align:left;margin-bottom:10px">
												<p>Welcome to Tiplur.<br>Thanks for participating in Scan & Play contest. The intent is to make the entire process smarter & simpler.
													 Gifts to be won through draw of lots, you will be intimated directly by the brand in case you win any reward. 	</p>

													 <p>You may continue to participate by submitting your entry each time you buy the product, and not only win gifts, but also increase your chances of winning the mega prize.</p>

														<p>Lastly, Consume alcoholic beverages responsibly, excessive consumption of alcohol is harmful to your health, & never Drink & Drive.</p>
													 <p>Sincerely,<br>Tiplur Team</p>
												 </td>

									 </tr>
								 </table>
									 </td>
							 </tr>
							 <tr><!-- Footer -->
								<td style="text-align:center;font-size:10px;background:#ffde16;color:#000;padding: 5px;">&#x24B8; Tiplur 2019</td>
							 </tr>
							 </table>';

					 if((isset($user_id) && $user_id != '') && $is_participated == '1'):

								 $today = date('Y-m-d H:i:s');
								 $part_array =array(  'user_id' => $user_id ,'promotion_id' => $promotion_id ,'promo_id' =>$id ,'barcode' => $barcode ,'qty' => $qty ,'user_city' => $user_city,'user_address' => $address, 'participated_on' => $today,'reward_points' => $reward_points);
									 $query = $this->db->insert('ti_participated_users', $part_array);
								 // Set Data For push notification on android/ios

										 $participate_data = array(
												 'promo_id' => $id, 			// Promotion-id
												 'order_id' => $promotion_id, 			// Promotion-id
												 'notified_to' => $user_id,		// user-id
												 'requested_by' => $user_id,
												 'type' => PROMOTION_NOTIFICATION,
												 // 'title' => $promotion_title,
												 'title' => 'Thanks for participating and we will get back to you.',
												 'message' => 'Thanks for participation for promotion',
												 'status' => 'promotion_participate',
										 );

										 // $notify = new Push_notification();
										 //   	$notify->send_promotion_notification($participate_data);

										 return $query ?  (smtp_mail($useremail, $msg ,$subject, $attachment = null) ? 'Participated Successfully' :false) : false;
									 //	return $query ?  ('Participated Successfully') : false;
					 else:
								 if((isset($user_id) && $user_id != '') || $is_participated =='0'):
													 $finalPath = 	base_url().'assets/images/promotion_image/';
													 $query2 = $this->db->select('id,promotion_id,brand_name,promotion_title,promotion_desc,promotion_img,prmototion_banner_url')->where(array('promotion_id' => $promotion_id,'promo_status' => '0','deleted' => '0' ))->get('ti_promotion')->result();
													 $newArray = array();
														foreach($query2 as $val):
															 $newArray['id']						 				= $val->id;
															 $newArray['promotion_id'] 				= $val->promotion_id;
															 $newArray['brand_name'] 					= $val->brand_name;
															 $newArray['promotion_title'] 			= $val->promotion_title;
															 $newArray['promotion_desc'] 			= $val->promotion_desc;
															 $newArray['prmototion_banner_url'] = $finalPath.$val->prmototion_banner_url;
														endforeach;
											 return array($newArray) ? array($newArray) : "";
								 endif;

								 $finalPath = 	base_url().'assets/images/promotion_image/';
										$query2 = $this->db->select('id,promotion_id,brand_name,promotion_title,promotion_desc,promotion_img,prmototion_banner_url')->where(array('promotion_id' => $promotion_id,'promo_status' => '0','deleted' => '0' ))->get('ti_promotion')->result();
										$newArray = array();
										 foreach($query2 as $val):
											 $newArray['id'] 									= $val->id;
											 $newArray['promotion_id'] 				= $val->promotion_id;
											 $newArray['brand_name'] 					= $val->brand_name;
											 $newArray['promotion_title'] 			= $val->promotion_title;
											 $newArray['promotion_desc'] 			= $val->promotion_desc;
											 // $newArray['promotion_img'] 		   	= $finalPath.$val->promotion_img;
											 $newArray['prmototion_banner_url'] = $finalPath.$val->prmototion_banner_url;
										 endforeach;
								return array($newArray) ? array($newArray) : "";
						 endif;
					 }
	}
	//**  Brand BarCode and promotion  information  **********\\
		 public function get_barcode_detail ($user_id,$brand_barcode,$brand_name,$promotion_id)
		 {
			 $barcode_12digit = substr($brand_barcode, 0, 12);

			 //$brand_barcode;
			 $curr_date = mdate("%Y-%m-%d");
			 // $curr_date = date("Y-m-d");
			 $fromdate = $curr_date.' '.'00:00:00';
			 $todate = $curr_date.' '.'23:59:59';
				 $barcode_count = barcode_exist($user_id,$brand_barcode,$promotion_id);
					if( $barcode_count > 0 )
					{
						 return $barcode_count;
					}else{
				 $finalPath =     base_url().'assets/images/promotion_image/';
				 $array = array('promo_status' => '0','deleted' => '0','promotion_id' =>$promotion_id);
					$query = $this->db->select('promotion_id,promotion_img,prmototion_banner_url,brand_name,brand_barcode_180ml,brand_barcode_375ml,brand_barcode_750ml,reward_points_750ml,reward_points_375ml,reward_points_180ml');
					$this->db->group_start();
					$this->db->like('brand_barcode_180ml', $barcode_12digit,'after');
					$this->db->or_like('brand_barcode_375ml', $barcode_12digit,'after');
					$this->db->or_like('brand_barcode_750ml', $barcode_12digit,'after');
					$this->db->group_end();

					$this->db->like('brand_name', $brand_name,'both');
					$this->db->from('ti_promotion');
					$this->db->where($array);
					$res = $this->db->get();
					// echo     $this->db->last_query(); die;
					 if ($res->num_rows() > 0) {

								$barArray = array();
								$newArray = array();
									 foreach($res->result_array() as $val):
												 // Validate right fields for verify bar-code
											if(strpos($val['brand_barcode_180ml'], $barcode_12digit) !== false){
													$newArray['brand_barcode_180ml'] = $val['brand_barcode_180ml'];
													$newArray['reward_points'] = $val['reward_points_180ml'];
											}elseif( strpos($val['brand_barcode_375ml'], $barcode_12digit  ) !== false){
													 $newArray['brand_barcode_375ml'] = $val['brand_barcode_375ml'];
													 $newArray['reward_points'] = $val['reward_points_375ml'];
											}else{
													 $newArray['brand_barcode_750ml'] = $val['brand_barcode_750ml'];
													 $newArray['reward_points'] = $val['reward_points_750ml'];
											}
					 // END Validate right fields for verify bar-code
										 $barArray['promotion_id']                 = $val['promotion_id'];
											$barArray['brand_name']                   = $val['brand_name'];
											$barArray['promotion_img']                 = (image_extension($val['prmototion_banner_url'] ) == 'jpg' || image_extension($val['prmototion_banner_url']) == 'jpeg' || image_extension($val['prmototion_banner_url']) == 'png') ?  $finalPath.$val['prmototion_banner_url'] : '';
									 endforeach;
												// Find keys from array to explode quantity of bottle
												 $keys = array_keys($newArray);
												 $qty  =substr($keys[0], strrpos($keys[0], '_') + 1);
																 $barArray['qty']  = $qty;
																 $barArray["reward_points"]  = $newArray['reward_points'];
					 // END
									 return array($barArray) ? array($barArray) : "";
							} else {
							 return $arr  = array('status' => false, 'msg' => "$brand_name");
							 // return $brand_name//  return false;
							}
				 }
			 }
	//************** End here ****************** \\


	 // Sum of Total Reward Points
	 public
	 function total_reward_points ($user_id)
	 {
		 if($user_id):
			 $res = $this->db->select('SUM(reward_points) as total_rewardPoints')->where( array('user_id' => $user_id) )->get ( 'participated_users' )->row ();
			 // $res = $this->db->select('SUM(reward_points) as total_rewardPoints')->where( array('promotion_id' =>$promotion_id , 'user_id' => $user_id) )->get ( 'participated_users' )->row ();
		 endif;
		 return ( $res ) ? $res : '0';
	 }


	 // Checkout reward point by user
	 public function redeem_checkout ($user_id,$reward_points)
	 {
		 $row = $this->db->get_where('participated_users' , array('user_id' => $user_id) )->row();
			 // Create array for Insert Redeem Data
			 $redeem_data =array(
					 'user_id' 			=> $user_id,
					 'redeem_points' 	=> $reward_points,
					 'promotion_id' 		=> $row->promotion_id,
					 'redeem_request_by' => $user_id,
					 'redeemed_on' 		=> date('Y-m-d H:i:s')
			 );

			 // Make array for Reset Redeem Points
			 $redeem_reset = array(
				 'reward_points' => '0'
			 );

		 if($row)
		 {
			 if($reward_points >= 10 ){
				 $updated = $this->db->where ( 'user_id', $user_id )->update ('participated_users', $redeem_reset);
				 if($updated)
				 {
					 $this->db->insert('redeem_points',$redeem_data);
					 $insertId = $this->db->insert_id();
				 }
			 }else{
					 $res['status'] = '0';
					 $res['error'] = 'Your redeem points must be greater than 10 Points. Thank You !!';
			 }
		 }
		 return $insertId ? $redeem_data : $res;
	 }

	/***************************************************************************************************************************************/

		/*
	 * @method: get_prev_order_weight
	 */
	private function get_prev_order_weight($user_id){
		$response = array('beer'=>0, 'wine'=>0, 'domestic'=>0, 'imported'=>0);

		$catIds = array('7'=>0, '9'=>0);
		$cats = array();
		$current_date = date('Y-m-d');
		$nonCat = array('domestic'=>0, 'imported'=>0);

		$query = "SELECT user_id, product_id, SUM(weight * quantity) AS total FROM ti_order_detail WHERE status NOT IN (".ORDER_DECLINED.",". ORDER_FAILED.") AND user_id=".$this->db->escape($user_id)." AND date(`creation_date`) = '".$current_date."' GROUP BY product_id";
		$gettodaysorder= $this->db->query($query)->result();

		//For old order
		if(sizeof($gettodaysorder)>0){

			foreach ($gettodaysorder as $row){

				$product_id = $row->product_id;
				$get_cat = $this->get_cat_by_product($product_id);
				$cat_id = $get_cat->category_id;
				$sub_cat_id = $get_cat->sub_category_id;

				if($cat_id=='7' || $cat_id=='9'){ //For Wine(7)
					if(sizeof($cats)>0){
						if(!in_array($cat_id, $cats)){
							array_push($cats, $cat_id);
							$catIds[$cat_id] = $row->total;
						}else{
							$catIds[$cat_id] = $catIds[$cat_id]+$row->total;
						}
					}else{
						array_push($cats, $cat_id);
						$catIds[$cat_id] = $row->total;
					}
				}else{ //For other products which belongs to Imported/Domestic category
					//1=>Domestic
					//2=>Imported
					$prod_type = $this->db->select('type')->where(array('id'=>$sub_cat_id))->get('category')->row()->type;
					if($prod_type=='1'){
						$nonCat['domestic'] = intval($nonCat['domestic'])+intval($row->total);
					}else{
						$nonCat['imported'] = intval($nonCat['imported'])+intval($row->total);
					}
				}

			}

			$response['wine'] = $catIds['7'];
			$response['beer'] = $catIds['9'];
			$response['domestic'] = $nonCat['domestic'];
			$response['imported'] = $nonCat['imported'];
		}
		return $response;
	}

	/*
	 * @method: get_current_order_weight
	 */
	private function get_current_order_weight($products){
		$response = array('beer'=>0, 'wine'=>0, 'domestic'=>0, 'imported'=>0);
		$domestic_status = true;
		$imported_status = true;
		$catIds = array('7'=>0, '9'=>0);
		$cats = array();
		$current_date = date('Y-m-d');
		$nonCat = array('domestic'=>0, 'imported'=>0);

		foreach ($products as $val){

			$product_id = $val['product_id'];
			$get_cat = $this->get_cat_by_product($product_id);
			$cat_id = $get_cat->category_id;
			$sub_cat_id = $get_cat->sub_category_id;

			if($cat_id=='7' || $cat_id=='9'){ //For Wine(7)
				if(sizeof($cats)>0){
					if(!in_array($cat_id, $cats)){
						array_push($cats, $cat_id);
						$catIds[$cat_id] = $val['weight']*$val['quantity'];
					}else{
						$catIds[$cat_id] = $catIds[$cat_id]+($val['weight']*$val['quantity']);
					}
				}else{
					array_push($cats, $cat_id);
					$catIds[$cat_id] = $val['weight']*$val['quantity'];
				}
			}else{ //For other products which belongs to Imported/Domestic category
				//1=>Domestic
				//2=>Imported
				$prod_type = $this->db->select('type')->where(array('id'=>$sub_cat_id))->get('category')->row()->type;
				if($prod_type=='1'){
					$nonCat['domestic'] = intval($nonCat['domestic'])+intval($val['weight']*$val['quantity']);
				}else if($prod_type=='2'){
					$nonCat['imported'] = intval($nonCat['imported'])+intval($val['weight']*$val['quantity']);
				}
			}
		}
		$response['wine'] = $catIds['7'];
		$response['beer'] = $catIds['9'];
		$response['domestic'] = $nonCat['domestic'];
		$response['imported'] = $nonCat['imported'];
		return $response;
	}


	/* get cart weight --- Add This LOC 0n 20 MARCH 2018 */
	private function get_prev_cart_weight($user_id,$product_id)
	{
		$response = array('beer'=>0, 'wine'=>0, 'domestic'=>0, 'imported'=>0);

		$catIds = array('7'=>0, '9'=>0);
		$cats = array();
		$current_date = date('Y-m-d');
		$nonCat = array('domestic'=>0, 'imported'=>0);

		$query = "SELECT user_id, product_id, SUM(weight * quantity) AS total FROM ti_cart WHERE user_id=".$this->db->escape($user_id)."  AND product_id != ".$product_id." GROUP BY product_id";
		$getcartdetails= $this->db->query($query)->result();

		//For old order
		if(sizeof($getcartdetails)>0){

			foreach ($getcartdetails as $row){

				$product_id = $row->product_id;
				$get_cat = $this->get_cat_by_product($product_id);
				$cat_id = $get_cat->category_id;
				$sub_cat_id = $get_cat->sub_category_id;

				if($cat_id=='7' || $cat_id=='9'){ //For Wine(7)
					if(sizeof($cats)>0){
						if(!in_array($cat_id, $cats)){
							array_push($cats, $cat_id);
							$catIds[$cat_id] = $row->total;
						}else{
							$catIds[$cat_id] = $catIds[$cat_id]+$row->total;
						}
					}else{
						array_push($cats, $cat_id);
						$catIds[$cat_id] = $row->total;
					}
				}else{ //For other products which belongs to Imported/Domestic category
					//1=>Domestic
					//2=>Imported
					$prod_type = $this->db->select('type')->where(array('id'=>$sub_cat_id))->get('category')->row()->type;
					if($prod_type=='1'){
						$nonCat['domestic'] = intval($nonCat['domestic'])+intval($row->total);
					}else{
						$nonCat['imported'] = intval($nonCat['imported'])+intval($row->total);
					}
				}

			}

			$response['wine'] = $catIds['7'];
			$response['beer'] = $catIds['9'];
			$response['domestic'] = $nonCat['domestic'];
			$response['imported'] = $nonCat['imported'];
		}
		return $response;
	}

	/*
	 * @method: get_total_weight
	 */
	private function get_total_weight($prv_q, $current_q,$cart_q = null){
		$sums = array();

		//In Case Update Cart Api - 20 MARCH 2018
		$cart_beer     = !empty($cart_q['beer']) ? $cart_q['beer']:'0';
		$cart_wine     = !empty($cart_q['wine']) ? $cart_q['wine']:'0';
		$cart_domestic = !empty($cart_q['domestic']) ? $cart_q['domestic']:'0';
		$cart_imported = !empty($cart_q['imported']) ? $cart_q['imported']:'0';

		$sums['beer']     = $prv_q['beer']+$current_q['beer']+$cart_beer;
		$sums['wine']     = $prv_q['wine']+$current_q['wine']+$cart_wine;
		$sums['domestic'] = $prv_q['domestic']+$current_q['domestic']+$cart_domestic;
		$sums['imported'] = $prv_q['imported']+$current_q['imported']+$cart_imported;
		return $sums;
	}

	public function pubsubcategories_list($data){
			$res = array();
		//	$cat = $this->db->select('id category_id, title category_name')->where('id',$data['category_id'])->get('category')->result();
			$subcategory = $this->db->select('id, title name, type, description,parent_id')->where('parent_id != ','0')->where(array('is_pub'=>'1','status' => '1'))->get('category')->result();

			return $subcategory;

		}

		//update product
public function add_product()
{
  $db_data = array();
	$response = new response ();
	$this->db->trans_start();
	$insert_data = array();
	$product = json_decode($this->input->post('quality_price'),true);
	$db_data = $this->input->post(array('title', 'description', 'sub_category_id', 'retailer_id','IBU','ABV'));
	$category=$this->db->select('parent_id')->where('id',$db_data['sub_category_id'])->get('category')->row();
	$db_data['category_id'] = $category->parent_id;
	$db_data['type'] = 1;
	$db_data['status'] = '1';
    $db_data['is_cod'] = '2';
	$db_data['in_store'] = '1';
	$db_data['quantity'] = NULL;
	$db_data['created_on'] = date('Y-m-d H:i:s');

	if(isset( $_FILES ['image'] ))
	{
		$path = './assets/images/product_image/';
		$file = $this->upload_image('image',$_FILES ['image']['tmp_name'],$path);
		$db_data['image'] = $file ['upload_data'] ['file_name'];
	}

	foreach ($product as $val){
					$db_data['weight'] = $val['packQuantity'];
					$db_data['price'] = $val['packPrice'];
					array_push($insert_data, $db_data);
				}
//prd($insert_data);
	// check exist product
	     $this->db->insert_batch( 'product', $insert_data );
					$this->db->trans_complete();
		// update user details
	//	$res = $this->db->insert ('product', $db_data);
		if ($this->db->trans_status() === TRUE)
		{
			$response->setStatus ( 1 );
			$response->setMsg ( "Product Details inserted successfully." );
		//	$response->setObjArray ( $db_data );
		} else {
			$response->setStatus ( 0 );
			$response->setMsg ( "somethins going wrong" );
		//	$response->setObjArray ( $db_data );
		}

	return $response;

}

/***************************************************************************************************/

}

?>
