<?php

class Products extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('products_model');
        $this->load->library('auth');
        $this->load->library('form_validation');
        $this->load->helper('date');
        $this->set_data = array('theme' => 'admin'); //define theme name
        if (!$this->auth->_is_logged_in()) {
            redirect(base_url());
        }
    }

    /*  filter */

    function filter() {
        return $this->input->get(array('title', 'category', 'type', 'status', 'retailer_id'));
    }

    /*  Script for  product listing */

    function index() {
        $this->load->library('pagination');

        $data['js'] = array('/assets/js/product.js');
        // delete multiple profduct
        if (!empty($_GET['multi_product'])) {

            $this->delete_products();
        }


        $per_page = isset($_GET['per_page']) ? $_GET['per_page'] : false;
        unset($_GET['per_page']);
        $query_string = http_build_query($_GET);
        $site_url = !$query_string ? base_url(SITE_AREA . '/products') : base_url(SITE_AREA . '/products?' . $query_string);
        if ($per_page) {
            $_GET['per_page'] = $per_page;
        }
        $num_rows = $this->products_model->product_details($this->filter());
        $config = pagination_formatting();
        $limit = 10;
        $config['base_url'] = $site_url;
        $config['total_rows'] = $num_rows;
        $config['per_page'] = $limit;
        $config['num_links'] = 5;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $this->pagination->initialize($config);


        $offset = isset($_GET['per_page']) ? ($_GET['per_page'] - 1) * $limit : null;
        $records = $getting = $this->products_model->product_details($this->filter(), $limit, $offset, false);
        $data['result'] = $records;
        $this->theme($data);
    }

    function delete_products() {

        $checked = $this->input->get('product_id');

        if (is_array($checked) && count($checked)) {
            // If any of the deletions fail, set the result to false, so
            // failure message is set if any of the attempts fail, not just
            // the last attempt

            $result = true;
            foreach ($checked as $pid) {

                $data = array('status' => '2');

                $updated = $this->products_model->update_products($data, $pid);
                if ($updated == false) {
                    $result = false;
                }
            }
            if ($result) {
                set_flash_message($type = 'success', $message = count($checked) . ' row deleted successfully.');
                redirect('admin/products');
            } else {
                set_flash_message($type = 'error', $message = 'something wrong');
                redirect('admin/products');
            }
        } else {
            set_flash_message($type = 'error', $message = 'Please select product.');
            redirect('admin/products');
        }
    }

    /*  Script for add product */

    function create() {
        $data['js'] = array('/assets/js/product.js');
//        if (isset($_POST['save'])) {
//
//            $this->form_validation->set_rules($this->products_model->get_form_validation_rules());
//            if ($this->form_validation->run() === false) {
//                set_flash_message($type = 'error', $message = 'Please enter product details.');
//            } else {
//                if ($this->save_product('insert')) {
//                    set_flash_message($type = 'success', $message = 'Product add successfully.');
//                    redirect('admin/products');
//                }
//            }
//        }

        if (isset($_POST['title'])) {

            //prd($_POST);

            if ($this->save_product('insert')) {
                set_flash_message($type = 'success', $message = 'Product add successfully.');

                redirect('admin/products');
            }
        }

        $this->theme($data);
    }

    /*  script for delete product */

    function delete_product() {
        $id = $this->input->post('product_id');
        $res = $this->db->where('id', $id)->update('product', array('status' => '2'));
        $data['msg'] = set_flash_message($type = 'success', $message = 'Deleted successfully.');
        $data['status'] = ($res) ? true : false;
        echo json_encode($data);
    }


    /*  script for update product stock */

    function stock_update_product() {
        $id = $this->input->post('product_id');
        $in_store = $this->input->post('in_store');
        $update_data = array('in_store' => $in_store );
        $res = $this->db->where('id', $id)->update('product', $update_data);
       // prd($res);
        $data['msg'] = set_flash_message($type = 'success', $message = 'Stock Updated successfully.');
        $data['status'] = ($res) ? true : false;
        echo json_encode($data);
    }

    /*  script for update product */

    function edit() {
        $data['js'] = array('/assets/js/product.js');
        $id = $this->uri->segment(4);

        $data['result'] = $this->products_model->product_detail_by_id($id);
        if (!$data['result']) {
            show_404();
        }
//        if (isset($_POST['save'])) {
//            $this->form_validation->set_rules($this->products_model->get_form_validation_rules());
//            //
//            if ($this->form_validation->run() === false) {
//                set_flash_message($type = 'error', $message = 'Please enter product details.');
//            } else {
//                if ($this->save_product('update', $id)) {
//                    set_flash_message($type = 'success', $message = 'Product Update successfully.');
//                    redirect('admin/products');
//                }
//            }
//        }

        if (isset($_POST['title'])) {

            if ($this->save_product('update', $id)) {
                set_flash_message($type = 'success', $message = 'Product Update successfully.');
                redirect('admin/products');
            }
        }

        $this->theme($data);
    }

    /*  script for save and update  product */

    private function save_product($type = false, $id = false) {
        //prd($_POST);
        $data = $this->input->post(array('title', 'description', 'category_id', 'sub_category_id', 'type', 'quantity', 'weight', 'price', 'special_price', 'retailer_id'));
        $data['status'] = (!empty($_POST['status']) && $_POST['status'] == 1) ? $_POST['status'] : '0';
        $data['is_cod'] = (!empty($_POST['is_cod']) && $_POST['is_cod'] == 1) ? $_POST['is_cod'] : '2';
        $data['in_store'] = (isset($_POST['in_store']) && $_POST['in_store'] == 1) ? $_POST['in_store'] : '2';
        $data['food_category'] = isset($_POST['food_type']) ? $_POST['food_type'] : null;

        if ($data['type'] == 1) {
            $snack_price = $data['price'];
            $snack_weight = $data['weight'] . 'ml';
        } else {
            $snack_price = (!empty($_POST['half_price'])) ? ($_POST['half_price'] . '/' . $_POST['full_price']) : $_POST['full_price'];
            $snack_weight = (!empty($_POST['half_price'])) ? 'half/full' : 'full';
        }

        $data['weight'] = $snack_weight;
        $data['price'] = $snack_price;
        $data['quantity'] = null;
        $data['created_on'] = $data['modified_on'] = db_date_time();
        $data['image'] = $this->upload_image();
       //prd($data);
        $final_array = array();
        if ($type == 'insert') {
            $id = $this->products_model->add_products($data);
        } elseif ($type == 'update') {
            unset($data['created_on']);
            $id = $this->products_model->update_products($data, $id);
        }

        return $id;
    }

     private function upload_image() {
      $this->load->library('upload');
      $upload_path = FCPATH . "assets/images/product_image/";
      $h_file = $this->input->post('h_image');
      $input_field_name = 'image';
      $config = array(
      'upload_path' => $upload_path,
      'allowed_types' => 'gif|jpg|png|jpeg',
      'file_name' => time().$_FILES[$input_field_name]['name'],
      );
      //prd($_FILES);
      if (is_uploaded_file($_FILES[$input_field_name]['tmp_name'])) {

      $this->upload->initialize($config);
      if (!$this->upload->do_upload($input_field_name)) {
      $this->form_validation->set_message($input_field_name, $this->upload->display_errors());
      return false;
      } else {
      if ($h_file and file_exists($upload_path . '/' . $h_file)) {
      unlink($upload_path . '/' . $h_file);
      }
      $uploaded = $this->upload->data();
      return $uploaded['file_name'];
      }
      } else {
      return $h_file;
      }
      } 

    function sub_cat() {
        $cat_id = $this->input->post('cat_id');
        product_cat_subcat($cat_id);
    }

    public function upload() {
        if (!empty($_SESSION["file_name"])) {
            unlink('./assets/uploads/uploaded_xls/' . $_SESSION["file_name"]);
            unset($_SESSION["file_name"]);
        }

        $this->theme();
    }

    public function uploadfile() {

        $data['temp'] = '';
        $file = (!empty($_FILES['file']['name'])) ? $_FILES['file']['name'] : $_SESSION["file_name"];
        $ext = pathinfo($file, PATHINFO_EXTENSION);
        $imageFilePath=FCPATH . 'assets/images/product_image/';//The path where the image is stored locally
        if (! file_exists ( $imageFilePath )) {
            mkdir("$imageFilePath", 0777, true);
        }
        if ($ext == 'xls') {
            if (empty($_SESSION["file_name"])) {
                $path = FCPATH . 'assets/uploads/uploaded_xls/';
                $file_name = time() . $_FILES['file']['name'];
                $final = $path . '/' . $file_name;
                if (move_uploaded_file($_FILES['file']['tmp_name'], $final)) {
                    $_SESSION["file_name"] = $file_name;
                }
            }
            if (!empty($_SESSION["file_name"])) {
                $inputFileName = FCPATH . 'assets/uploads/uploaded_xls/' . @$_SESSION["file_name"];
                if (!empty($_POST['upload'])) {
                    $this->preview_data();
                }

                $this->load->library('PHPExcel/PHPExcel');
                // read file data
                $objReader = PHPExcel_IOFactory::createReaderForFile($inputFileName);
                $objReader->setReadDataOnly(false);
                
                $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
                //get only the Cell Collection
                $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
                //prd($objPHPExcel->getActiveSheet()->getDrawingCollection());
                //Extract to a PHP readable array format
                $arr_data=array();
                $i = 1;
                $heder_defination = array("A" => "product_name", "B" => "description", "C" => "category", "D" => "sub_category", "E" => "product_type", "F" => "price", "G" => "special_price", "H" => "quantity", "I" => "weight", "J" => "in_store", "K" => "is_cod", "L" => "retailer_id",'M'=>'image');
                foreach ($cell_collection as $k => $cell) {

                    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
                    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
                    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
                   
                    if ($row == 1) {
                        $header[$row][$column] = $data_value;
                        $arry_keys = $data_value;
                    } else {
                        $arr_data[$row][$heder_defination[$column]] = trim($data_value);
                        
                    }
                    //header will/should be in row 1 only. of course this can be modified to suit your need.
                 
                    $i++;
                }
                foreach ($objPHPExcel->getActiveSheet()->getDrawingCollection() as $img) {
                   // prd($img->getMimeType());
                    list($startColumn,$startRow)= PHPExcel_Cell::coordinateFromString($img->getCoordinates());//Get the row and column of the picture 
                     
                    if ($img instanceof PHPExcel_Worksheet_MemoryDrawing){
                        ob_start();
                        call_user_func(
                        $img->getRenderingFunction(),
                        $img->getImageResource()
                        );
                        $imageContents = ob_get_contents();
                        ob_end_clean();
                        switch ($img->getMimeType()){
                        case PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_PNG :
                        
                        $extension = 'png';
                        break;
                        case PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_GIF:
                        
                        $extension = 'gif';
                        break;
                        case PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_JPEG :
                        
                        $extension = 'jpg';
                        break;
                        }
                        } else {
                        $zipReader = fopen($img->getPath(),'r');
                        $imageContents = '';
                        while (!feof($zipReader)) {
                        $imageContents .= fread($zipReader,1024);
                        }
                        fclose($zipReader);
                        $extension = $img->getExtension();
                        }
                        $imageFileName = md5(uniqid());
                    $imageFileName = $img->getCoordinates().mt_rand(100, 999).'.'.$extension;
                var_dump(file_put_contents($imageFilePath.$imageFileName,$imageContents));
                $arr_data[$startRow][$heder_defination[$startColumn]]=$imageFileName;;//Insert the picture into the array
//prd($arr_data);
            }
               //prd($arr_data);
               
                //send the data in an array format
                $data['header'] = $header;

                $validataion = array_diff_assoc($heder_defination, $data['header'][1]);

                if (empty($validataion)) {

                    $data['values'] = $arr_data;

                    if (!empty($data['values'])) {
                        $excel_data = array();
                        foreach ($data['header'][1] as $column) {
                            $excel_data[$column] = array_column($data['values'], $column);
                        }

                        $this->preview_data($excel_data);
                    }
                } else {
                    $data['error_data'] = 'hi';
                    set_flash_message($type = 'error', $message = 'Required  excel format not same plz download correct format ' . anchor('assets/uploads/product.xls', 'click here', 'class="link-class"'));
                    redirect('admin/products/upload');
                }
            }
        } else {
            set_flash_message($type = 'error', $message = 'Required  excel format not same plz download correct format ' . anchor('assets/uploads/product.xls', 'click here', 'class="link-class"'));
            redirect('admin/products/upload');
        }

        $this->theme($data);
    }

public function ABC2decimal($abc){
    $ten = 0;
    $len = strlen($abc);
    for($i=1;$i<=$len;$i++){
                 $char = substr($abc,0-$i,1);//Get a single character in reverse
 
        $int = ord($char);
        $ten += ($int-65)*pow(26,$i-1);
    }
    return $ten;
}

    public function preview_data($set_data = false) {
        // form or excel data

        $post_data = ($set_data && empty($_POST['upload'])) ? $set_data : $this->input->post();
     //  prd($post_data);
        $this->form_validation->set_data($post_data);
        $this->form_validation->set_rules($this->products_model->custom_form_validation_rules($post_data));
        // form validation
        if ($this->form_validation->run() === false) {

            $data['data'] = $this->form_validation->error_array();
            set_flash_message($type = 'error', $message = 'Please enter product details.');
            return false;
        } else if (!empty($post_data)) {

            $final_array = array();
            foreach ($post_data['product_name'] as $k => $val) {
                /* prepare product data */
                $final_array['title'] = $post_data['product_name'][$k];
                $final_array['description'] = $post_data['description'][$k];
                $final_array['category_id'] = ($post_data['category'][$k]) ? $this->products_model->cat_check($post_data['category'][$k]) : '';
                $final_array['sub_category_id'] = (!empty($post_data['sub_category'][$k])) ? $this->products_model->sub_cat_check_upload($post_data['sub_category'][$k],$final_array['category_id']) : null;
                $final_array['type'] = $post_data['product_type'][$k];
                $final_array['price'] = $post_data['price'][$k];
                $final_array['special_price'] = $post_data['special_price'][$k];
                $final_array['quantity'] = null;
                $final_array['weight'] = ($post_data['product_type'][$k] == 1) ? $post_data['weight'][$k] . 'ml' : $post_data['weight'][$k];
                $final_array['in_store'] = $post_data['in_store'][$k];
                $final_array['is_cod'] = $post_data['is_cod'][$k];
                //$final_array['retailer_id'] = ($post_data['retailer_name'][$k]) ? $this->products_model->retailer_name_check($post_data['retailer_name'][$k]) : '';
                $final_array['retailer_id'] = $post_data['retailer_id'][$k];
                $final_array['image'] = $post_data['image'][$k];
                $final_array['created_on'] = db_date_time();

                /* insert product */
                $id = $this->products_model->add_products($final_array);

                // $final_array;
            }
            unlink('./assets/uploads/uploaded_xls/' . $_SESSION["file_name"]);
            unset($_SESSION["file_name"]);
            set_flash_message($type = 'success', $message = 'Product add successfully.');
            redirect('admin/products');
            //prd($d);
        }
    }

    public function get_cat_by_product_type() {

        echo $this->load->view('products/admin/get_cat_by_product_type', array('type' => $_POST['type']), true);
    }

    public function get_related_subcat() {

        echo $this->load->view('products/admin/get_related_subcat', array('cat_id' => $_POST['cat_id']), true);
    }

    public function validations() {

        $form_data = $this->input->get();
//prd($form_data);
        $this->form_validation->set_data($form_data);
        $this->form_validation->set_rules($this->products_model->product_validation_rules());
        $data['error'] = 0;
        if ($this->form_validation->run() === false and $form_data) {
            $data = $this->form_validation->error_array();
            $data['error'] = 1;
        }
        print json_encode($data);
    }

    public function price_log() {

        $this->load->library('pagination');

        $per_page = isset($_GET['per_page']) ? $_GET['per_page'] : false;
        unset($_GET['per_page']);
        $query_string = http_build_query($_GET);
        $site_url = !$query_string ? base_url(SITE_AREA . '/products/price_log') : base_url(SITE_AREA . '/products/price_log?' . $query_string);
        if ($per_page) {
            $_GET['per_page'] = $per_page;
        }
        $num_rows = $this->products_model->get_retailers_price_log($this->price_log_filter());
        $config = pagination_formatting();
        $limit = 10;
        $config['base_url'] = $site_url;
        $config['total_rows'] = $num_rows;
        $config['per_page'] = $limit;
        $config['num_links'] = 5;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $this->pagination->initialize($config);


        $offset = isset($_GET['per_page']) ? ($_GET['per_page'] - 1) * $limit : null;
        $records = $this->products_model->get_retailers_price_log($this->price_log_filter(), $limit, $offset, false);
        $data['result'] = $records;
        //prd($records);
        $this->theme($data);
    }

    function price_log_filter() {
        return $this->input->get(array('product_id', 'retailer_id'));
    }

    public function export_orders() {

        $filter = isset($_GET['filter']) ? $this->filter() : false;

        $reports = $this->products_model->product_details($filter, false, false, false);
        //prd($reports);
        $type = product_type();
        //echo "<pre>"; print_r($reports);die;

        /* Start Logic for excel */
        $exceldata = array();
        $table = '<table><tr><th>product_name</th><th>description</th><th>category</th><th>sub_category</th><th>product_type</th><th>price</th><th>special_price</th><th>quantity</th><th>weight</th><th>in_store</th><th>is_cod</th><th>retailer_id</th></tr>';
        foreach ($reports as $record) {
            //print_r($reports);

            //$retailer = user_data($record->retailer_id);
            //$retailer_name = $retailer ? ucfirst($retailer->display_name) : '';
            //$status = ($record->status == 1) ? 'Active' : 'In-Active';

            $weight = ($record->weight) ? $record->weight : '';

            if($record->type == 1){
                $quantity = ($record->quantity) ? $record->quantity : '';
            }else{
                $quantity = null;
            }

            $pro_type = ($record->type) ? $type[$record->type] : 'NA';

            $table .= '<tr>';
            $table .='<td>' . ucfirst($record->title) . '</td>';
            $table .= '<td>' . $record->description . '</td>';
            $table .= '<td>' . $record->category_name . '</td>';
            $table .= '<td>' . $record->sub_category_name . '</td>';
            $table .= '<td>' . $pro_type . '</td>';
            $table .= '<td>' . $record->price . '</td>';
            $table .= '<td>' . $record->special_price . '</td>';
            $table .='<td>' . $quantity . '</td>';
            $table .= '<td>' . $weight . '</td>';
            $table .='<td>' . $record->in_store . '</td>';
            $table .='<td>' . $record->is_cod . '</td>';
            $table .='<td>' . $record->retailer_id . '</td>';
            $table .= '</tr>';
        }
        $table .= '</table>';
        $date = date('d-m-Y');

        header("Content-Type: application/vnd.ms-excel");
        header("Content-disposition: attachment; filename=Products Details(" . $date . ").xls");
        echo $table;
        die();
        /* end Logic for excel */
    }

    function retailer_products_delete() {

        if (empty($_POST)) {
            show_404();
        }

        $retailer_id = $this->input->post('ret_id');

        $result = $this->db->where('retailer_id',$retailer_id)->update('product', array('deleted'=>'1', 'status'=>'2'));

        if ($result) {
            set_flash_message($type = 'success', $message = 'Retailer Products deleted successfully.');
            redirect('admin/products');
        } else {
            set_flash_message($type = 'error', $message = 'something wrong.');
            redirect('admin/products');
        }
    }

}
