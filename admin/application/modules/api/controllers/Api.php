<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
require APPPATH . '/libraries/REST_Controller.php';


class Api extends REST_Controller {

	public function __construct() {
		parent::__construct ();
		error_reporting ( 0 );
		$this->load->model ( 'api/Api_service' );
		$this->load->model ( 'api/Api_dao' );
		include_once './application/objects/Response.php';
		$this->load->helper ( 'string' );
		$this->load->library ( array (
				'form_validation'
		) );

		/*
		 * Check user is logged in or not?
		 */
		// if(!isset($_REQUEST['check_login']) && !isset($_REQUEST['user_id']) && !isset($_REQUEST['device_id'])){
		// 	//do nothing
		// }else{
		// 	$flag = isset($_REQUEST['check_login'])?$_REQUEST['check_login']:"1";

		// 	if($flag!='0'){

		// 		$apiService = new Api_service();
		// 		$check_login = $apiService->is_user_logged_in();
		// 		$this->form_validation->set_rules ( 'user_id', 'User Id', 'trim|required' );
		// 		$this->form_validation->set_rules ( 'device_id', 'Device Id', 'trim|required' );

		// 		if ($this->form_validation->run () == TRUE)
		// 		{
		// 			if($_REQUEST['user_id']!='0'){
		// 				if($check_login->getStatus()==4){
		// 					echo json_encode( array (
		// 							'status' => $check_login->getStatus(),
		// 							'message' => $check_login->getMsg(),
		// 							'jsonData' => NULL
		// 					));
		// 					exit();
		// 				}
		// 			}
		// 		}else{
		// 			echo json_encode( array (
		// 					'status' => 0,
		// 					'message' => "Requested data not found",
		// 					'jsonData' => "abcd"
		// 			));
		// 			exit();
		// 		}
		// 	}
		// }

		error_reporting ( 0 );
	}


	public function index_post() {

		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			$this->form_validation->set_rules ( 'fcm_id', 'FCM Id', 'trim|required' );
				if ($this->form_validation->run () == TRUE)
				{
					$apiService = new Api_service();
					$response = $apiService->test();
					$this->set_response ( array (
					'status' => '1',
					'message' => 'test',
					'jsonData' => $response
					), REST_Controller::HTTP_OK );
				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Requested data not found'
					), REST_Controller::HTTP_OK );
				}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}

	}


	//All States
	public function states_post()
	{
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
				$apiService = new Api_service();
				$response = $apiService->State_list();

					if ($response->getStatus () == 1)
					{
				        $this->set_response ( array (
								'status' => $response->getStatus (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray ()
						), REST_Controller::HTTP_OK );

					} else {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray ()
						), REST_Controller::HTTP_OK );
					}

			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_BAD_REQUEST );

			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}



	// permit Available state
	public function permit_post()
	{
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
				$apiService = new Api_service();
				$response = $apiService->is_available_permit_in_state();

					if ($response->getStatus () == 1)
					{
				        $this->set_response ( array (
								'status' => $response->getStatus (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray ()
						), REST_Controller::HTTP_OK );

					} else {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray ()
						), REST_Controller::HTTP_OK );
					}

			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_BAD_REQUEST );

			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}


	// Shipping Charge Available state
	public function shipping_charge_post()
	{
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
				$apiService = new Api_service();
				$response = $apiService->shipping_charge_in_state_extra();

					if ($response->getStatus () == 1)
					{
								$this->set_response ( array (
								'status' => $response->getStatus (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray ()
						), REST_Controller::HTTP_OK );

					} else {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray ()
						), REST_Controller::HTTP_OK );
					}

			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_BAD_REQUEST );

			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}

	// Shipping Charge Available state
	public function shipping_charge_extra_post()
	{
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
				$apiService = new Api_service();
				$response = $apiService->shipping_charge_in_state();

					if ($response->getStatus () == 1)
					{
								$this->set_response ( array (
								'status' => $response->getStatus (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray ()
						), REST_Controller::HTTP_OK );

					} else {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray ()
						), REST_Controller::HTTP_OK );
					}

			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_BAD_REQUEST );

			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}

// Implement Face OCR truthscreen app

public function face_ocr_post()
{
	if ($_SERVER ['REQUEST_METHOD'] == "POST")
	{
		try
		{
		//	$this->form_validation->set_rules ( 'user', 'User Id', 'trim|required' );
			// if ($this->form_validation->run () == TRUE &&isset($_FILES['Image']) && (is_uploaded_file($_FILES['Image']['tmp_name']))&&isset($_FILES['document']) && (is_uploaded_file($_FILES['document']['tmp_name'])))
			// {
			$apiService = new Api_service();
			$response = $apiService->face_ocr();

				if ($response->getStatus () == 1)
				{
							$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );

				} else {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );
				}
// }
// else {
// 	$this->set_response ( array (
// 			'status' => 0,
// 			'message' => 'Requested data not found'
// 	), REST_Controller::HTTP_OK );
// }

		} catch ( Exception $e ) {

			$this->set_response ( array (
					'status' => 0,
					'message' => $e->getMessage ()
			), REST_Controller::HTTP_BAD_REQUEST );

		}
	} else {
		$this->set_response ( array (
				'status' => 0,
				'message' => 'This HTTP method is not allowed'
		), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
	}
}


public function idSearch_post()
{
	if ($_SERVER ['REQUEST_METHOD'] == "POST")
	{
		try
		{
		//	$this->form_validation->set_rules ( 'user', 'User Id', 'trim|required' );
			// if ($this->form_validation->run () == TRUE &&isset($_FILES['Image']) && (is_uploaded_file($_FILES['Image']['tmp_name']))&&isset($_FILES['document']) && (is_uploaded_file($_FILES['document']['tmp_name'])))
			// {
			$apiService = new Api_service();
			$response = $apiService->idSearch();

				if ($response->getStatus () == 1)
				{
							$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );

				} else {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );
				}
// }
// else {
// 	$this->set_response ( array (
// 			'status' => 0,
// 			'message' => 'Requested data not found'
// 	), REST_Controller::HTTP_OK );
// }

		} catch ( Exception $e ) {

			$this->set_response ( array (
					'status' => 0,
					'message' => $e->getMessage ()
			), REST_Controller::HTTP_BAD_REQUEST );

		}
	} else {
		$this->set_response ( array (
				'status' => 0,
				'message' => 'This HTTP method is not allowed'
		), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
	}
}




	/*
	 * @author: Aakriti
	 * @date: 23/10/2017
	 * @method: register
	 * @desc: User registration process (Cutomer/Retailer)
	 * @params: username, password, email, mobile, dob, state_id, address, device_type, device_id, fcm_reg_id
	 */
	public function register_post()
	{
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
				$this->form_validation->set_rules ( 'username', 'User Id', 'trim|required' );
				$this->form_validation->set_rules ( 'password', 'Password', 'trim|required' );
				$this->form_validation->set_rules ( 'email', 'email', 'trim|required' );
				$this->form_validation->set_rules ( 'mobile', 'mobile', 'trim|required' );
				$this->form_validation->set_rules ( 'state_id', 'state', 'trim|required' );
				$this->form_validation->set_rules ( 'city_id', 'City Id', 'trim|required' );

				//$this->form_validation->set_rules ( 'pincode', 'pincode', 'trim|required' );
				$this->form_validation->set_rules ( 'device_type', 'Device Type', 'trim|required' );
				$this->form_validation->set_rules ( 'device_id', 'Device Id', 'trim|required' );
				$this->form_validation->set_rules ( 'fcm_reg_id', 'Fcm Id', 'trim' );  //required remove on 12-march-2018



				//For Retailer
				if($this->post('is_retailer')){
					$this->form_validation->set_rules ( 'is_retailer', 'Is retailer', 'trim|required' );
					$this->form_validation->set_rules ( 'store_name', 'Store name', 'trim|required' );
					$this->form_validation->set_rules ( 'licence', 'Licence number', 'trim|required' );
					$this->form_validation->set_rules ( 'cp_name', 'Contact person name', 'trim|required' );
					$this->form_validation->set_rules ( 'cp_mobile', 'Contact person mobile', 'trim|required' );
					$this->form_validation->set_rules ( 'is_provide_snacks', 'Is provide snacks', 'trim|required' );
					$this->form_validation->set_rules ( 'latitude', 'latitude', 'trim|required' );
					$this->form_validation->set_rules ( 'longitude', 'longitude', 'trim|required' );
					$this->form_validation->set_rules ( 'min_order', 'Min order value', 'trim|required' );
					$this->form_validation->set_rules ( 'start_time', 'Opening time', 'trim|required' );
					$this->form_validation->set_rules ( 'end_time', 'Closing time', 'trim|required' );
					$this->form_validation->set_rules ( 'payment_mode', 'Payment Mode', 'trim|required' );
					$this->form_validation->set_rules ( 'address', 'address', 'trim|required' );
					$this->form_validation->set_rules ( 'state', 'state', 'trim|required' );
					$this->form_validation->set_rules ( 'city', 'city', 'trim|required' );
				}else{
					$this->form_validation->set_rules ( 'dob', 'dob', 'trim|required' );
				}
				if ($this->form_validation->run () == TRUE)
				{

					$apiService = new Api_service();
					$response = $apiService->registration_process ();

					if ($response->getStatus () == 1)
					{
				            //prd($response);

				        $this->set_response ( array (
								'status' => $response->getStatus (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray ()
						), REST_Controller::HTTP_OK );

					} else {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray ()
						), REST_Controller::HTTP_OK );
					}

				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Requested data not found'
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_BAD_REQUEST );

			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}


	/*
	 * @author: Aakriti
	 * @date: 23/10/2017
	 * @method: login
	 * @desc: App login process
	 * @params: username, password(conditional), device_id, device_type, fcm_reg_id, login_type
	 */

	public function login_post()
	{
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
           try
			{
				$this->form_validation->set_rules ( 'username', 'username', 'trim|required' );
				$this->form_validation->set_rules ( 'device_type', 'Device Type', 'trim|required' );
				$this->form_validation->set_rules ( 'device_id', 'Device Id', 'trim|required' );
				$this->form_validation->set_rules ( 'fcm_reg_id', 'Fcm Id', 'trim' ); //required remove on 12-march-2018
				$this->form_validation->set_rules ( 'login_type', 'Login type', 'trim|required' );

				if($this->post('login_type')==LOGIN_BY_PASSWORD){
					$this->form_validation->set_rules ( 'password', 'password', 'trim|required' );
				}

				if($this->post('is_retailer')){
					$this->form_validation->set_rules ( 'is_retailer', 'Is retailer', 'trim|required' );
				}

				if ($this->form_validation->run () == TRUE)
				{

					$apiService = new Api_service();
					$response = $apiService->login();

					if ($response->getStatus () == 1)
					{
				            //prd($response);

				        $this->set_response ( array (
								'status' => $response->getStatus (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray ()
						), REST_Controller::HTTP_OK );

					} else {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray ()
						), REST_Controller::HTTP_OK );
					}
				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Requested data not found'
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_OK);

			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_OK);
		}
	}



	/**** update profile ******/
	public function update_profile_post()
	{
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			$apiService = new Api_service();
			try {

				$this->form_validation->set_rules ( 'id', 'User Id', 'trim|required' );
				$this->form_validation->set_rules ( 'username', 'User name', 'trim|required' );
				$this->form_validation->set_rules ( 'email', 'Email', 'trim|required' );

				if($this->post('is_retailer')){
					$this->form_validation->set_rules ( 'is_retailer', 'Is retailer', 'trim|required' );
					$this->form_validation->set_rules ( 'store_id', 'Store id', 'trim|required' );
					$this->form_validation->set_rules ( 'store_name', 'Store name', 'trim|required' );
					//$this->form_validation->set_rules ( 'licence', 'Licence number', 'trim|required' );
					$this->form_validation->set_rules ( 'cp_name', 'Contact person name', 'trim|required' );
					$this->form_validation->set_rules ( 'cp_mobile', 'Contact person mobile', 'trim|required' );
					$this->form_validation->set_rules ( 'is_provide_snacks', 'Is provide snacks', 'trim|required' );
					//$this->form_validation->set_rules ( 'latitude', 'latitude', 'trim|required' );
					//$this->form_validation->set_rules ( 'longitude', 'longitude', 'trim|required' );
					$this->form_validation->set_rules ( 'min_order', 'Min order value', 'trim|required' );
				}

				if ($this->form_validation->run () == TRUE)
				{
					$response = $apiService->update_user_profile();

					if ($response->getStatus ()) {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray ()
						), REST_Controller::HTTP_OK );
					} else {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray ()
						), REST_Controller::HTTP_OK );
					}
				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Requested data not found'
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}

/*************************************************************************************************************************************/
/**** update product details ******/
public function update_product_post()
{
	if ($_SERVER ['REQUEST_METHOD'] == "POST")
	{
		$apiService = new Api_service();
		try {
      $this->form_validation->set_rules ( 'title', 'Title', 'trim|required' );
			$this->form_validation->set_rules ( 'description', 'Description', 'trim|required' );
			$this->form_validation->set_rules ( 'price', 'Price', 'trim|required' );


			if ($this->form_validation->run () == TRUE)
			{
				$response = $apiService->update_product();

				if ($response->getStatus ()) {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );
				} else {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );
				}
			} else {
				$this->set_response ( array (
						'status' => 0,
						'message' => 'Requested data not found'
				), REST_Controller::HTTP_OK );
			}
		} catch ( Exception $e ) {

			$this->set_response ( array (
					'status' => 0,
					'message' => $e->getMessage ()
			), REST_Controller::HTTP_BAD_REQUEST );
		}
	} else {
		$this->set_response ( array (
				'status' => 0,
				'message' => 'This HTTP method is not allowed'
		), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
	}
}

/*************************************************************************************************************************************/


/**** update product details ******/
public function update_product_post()
{
	if ($_SERVER ['REQUEST_METHOD'] == "POST")
	{
		$apiService = new Api_service();
		try {

			$this->form_validation->set_rules ( 'description', 'User Id', 'trim|required' );
			$this->form_validation->set_rules ( 'price', 'User name', 'trim|required' );


			if ($this->form_validation->run () == TRUE)
			{
				$response = $apiService->update_product();

				if ($response->getStatus ()) {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );
				} else {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );
				}
			} else {
				$this->set_response ( array (
						'status' => 0,
						'message' => 'Requested data not found'
				), REST_Controller::HTTP_OK );
			}
		} catch ( Exception $e ) {

			$this->set_response ( array (
					'status' => 0,
					'message' => $e->getMessage ()
			), REST_Controller::HTTP_BAD_REQUEST );
		}
	} else {
		$this->set_response ( array (
				'status' => 0,
				'message' => 'This HTTP method is not allowed'
		), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
	}
}

/*************************************************************************************************************************************/

/**** update product details ******/
public function add_product_post()
{
	if ($_SERVER ['REQUEST_METHOD'] == "POST")
	{
		$apiService = new Api_service();
		try {

			$this->form_validation->set_rules ( 'description', 'User Id', 'trim|required' );
			$this->form_validation->set_rules ( 'price', 'User name', 'trim|required' );


			if ($this->form_validation->run () == TRUE)
			{
				$response = $apiService->add_product();

				if ($response->getStatus ()) {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );
				} else {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );
				}
			} else {
				$this->set_response ( array (
						'status' => 0,
						'message' => 'Requested data not found'
				), REST_Controller::HTTP_OK );
			}
		} catch ( Exception $e ) {

			$this->set_response ( array (
					'status' => 0,
					'message' => $e->getMessage ()
			), REST_Controller::HTTP_BAD_REQUEST );
		}
	} else {
		$this->set_response ( array (
				'status' => 0,
				'message' => 'This HTTP method is not allowed'
		), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
	}
}

/*************************************************************************************************************************************/




public function order_permit_post()
{
	if ($_SERVER ['REQUEST_METHOD'] == "POST")
	{
		$apiService = new Api_service();
		try {

			$this->form_validation->set_rules ( 'id', 'User Id', 'trim|required' );
			$this->form_validation->set_rules ( 'permit_avail', 'Permit Available', 'trim|required' );

			if ($this->form_validation->run () == TRUE)
			{
				$response = $apiService->order_permit();

				if ($response->getStatus ()) {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );
				} else {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );
				}
			} else {
				$this->set_response ( array (
						'status' => 0,
						'message' => 'Requested data not found'
				), REST_Controller::HTTP_OK );
			}
		} catch ( Exception $e ) {

			$this->set_response ( array (
					'status' => 0,
					'message' => $e->getMessage ()
			), REST_Controller::HTTP_BAD_REQUEST );
		}
	} else {
		$this->set_response ( array (
				'status' => 0,
				'message' => 'This HTTP method is not allowed'
		), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
	}
}

/*************************************************************************************************************************************/
	//search retailers
	public function retailers_list_post()
	{
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			$apiService = new Api_service();
			try {

				$this->form_validation->set_rules ( 'latitude', 'latitude', 'trim|required' );
				$this->form_validation->set_rules ( 'longitude', 'longitude', 'trim|required' );
				$this->form_validation->set_rules ( 'state_id', 'state id', 'trim|required' );
				$this->form_validation->set_rules ( 'user_id', 'user id', 'trim|required' );
				$this->form_validation->set_rules ( 'fcm_reg_id', 'Fcm Reg id', 'trim' );
				//fcm_reg_id added on 12-March-2018
				$this->form_validation->set_rules ( 'current_city_id', 'Current City Id', 'trim' );

				if ($this->form_validation->run () == TRUE)
				{
					$response = $apiService->search_retailers();

					if ($response->getStatus ()) {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray (),
								'notification_count' => $apiService->get_notification_count(),
								'cart_count' => $apiService->removePreviousCart($this->input->post('user_id'),true)
						), REST_Controller::HTTP_OK );
					} else {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray ()
						), REST_Controller::HTTP_OK );
					}
				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Requested data not found'
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}


	//category List
	public function categories_post()
	{
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			$apiService = new Api_service();
			try {

					$response = $apiService->all_categories();

					if ($response->getStatus ()) {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray (),
                                'banner_images' => $apiService->get_banner_images()
						), REST_Controller::HTTP_OK );
					} else {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray ()
						), REST_Controller::HTTP_OK );
					}

			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_BAD_REQUEST );

			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}


	//Sub categories List
	public function subcategories_post()
	{
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			$apiService = new Api_service();
			try {

				$this->form_validation->set_rules ('category_id', 'Category Id', 'trim|required');

				if ($this->form_validation->run () == TRUE)
				{
					$response = $apiService->subcategories_list();

					if ($response->getStatus ()) {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray ()
						), REST_Controller::HTTP_OK );
					} else {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray ()
						), REST_Controller::HTTP_OK );
					}
				}else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Requested data not found'
					), REST_Controller::HTTP_OK );
				}

			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}


//Pub Sub categories List
public function pubsubcategories_post()
{
	if ($_SERVER ['REQUEST_METHOD'] == "POST")
	{
		$apiService = new Api_service();
		try {

			//$this->form_validation->set_rules ('category_id', 'Category Id', 'trim|required');

			
				$response = $apiService->pubsubcategories_list();

				if ($response->getStatus ()) {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );
				} else {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );
				}
			

		} catch ( Exception $e ) {

			$this->set_response ( array (
					'status' => 0,
					'message' => $e->getMessage ()
			), REST_Controller::HTTP_BAD_REQUEST );
		}
	} else {
		$this->set_response ( array (
				'status' => 0,
				'message' => 'This HTTP method is not allowed'
		), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
	}
}


	/****SEND OTP ******/
	public function send_otp_post()
	{
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			$apiService = new Api_service();

			try {

				$this->form_validation->set_rules ( 'mobile', 'Mobile', 'trim|required' );
				$this->form_validation->set_rules ( 'is_login', 'Is login check', 'trim|required' );

				if ($this->form_validation->run () == TRUE)
				{

					$response = $apiService->validate_mobile();

					if ($response->getStatus ()) {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray ()
						), REST_Controller::HTTP_OK );
					} else {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray ()
						), REST_Controller::HTTP_OK );
					}
				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Requested data not found'
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}

	/*** End Here ******/

	/**** Product details******/
	public function products_post()
	{
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{

			$apiService = new Api_service();

			try {

				$this->form_validation->set_rules ( 'retailer_id', 'Retailer id', 'trim|required' );

				if($this->post('is_retailer')){ //Is retailer?
					$this->form_validation->set_rules ( 'is_retailer', 'Is retailer', 'trim|required' );
				}else{
					//to get already added quantity in cart
					$this->form_validation->set_rules ( 'user_id', 'User id', 'trim|required' );
				}

				if($this->post('type')==1&&$this->post('is_retailer')==0){ //liquor
					$this->form_validation->set_rules ( 'subcategory_id', 'Sub Category', 'trim|required' );
				}

				$this->form_validation->set_rules ( 'type', 'Product Type', 'trim|required' );

				if ($this->form_validation->run () == TRUE)
				{

					$response = $apiService->products_listing();

					if ($response->getStatus ()) {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray ()
						), REST_Controller::HTTP_OK );
					} else {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray ()
						), REST_Controller::HTTP_OK );
					}
				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Requested data not found'
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}

	/*** End Here ******/

	/**** Product details******/
	public function products_pub_post()
	{
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{

			$apiService = new Api_service();

			try {

				$this->form_validation->set_rules ( 'retailer_id', 'Retailer id', 'trim|required' );

				if($this->post('is_retailer')){ //Is retailer?
					$this->form_validation->set_rules ( 'is_retailer', 'Is retailer', 'trim|required' );
				}else{
					//to get already added quantity in cart
					$this->form_validation->set_rules ( 'user_id', 'User id', 'trim|required' );
				}

				if($this->post('type')==1&&$this->post('is_retailer')==0){ //liquor
					$this->form_validation->set_rules ( 'subcategory_id', 'Sub Category', 'trim|required' );
				}

				$this->form_validation->set_rules ( 'type', 'Product Type', 'trim|required' );

				if ($this->form_validation->run () == TRUE)
				{

					$response = $apiService->products_pub_listing();

					if ($response->getStatus ()) {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray ()
						), REST_Controller::HTTP_OK );
					} else {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray ()
						), REST_Controller::HTTP_OK );
					}
				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Requested data not found'
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}

	/*** End Here ******/

	public function app_info_post()
	{
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			$apiService = new Api_service();
			try
			{
				$response = $apiService->app_info();

					if ($response->getStatus ()) {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray ()
						), REST_Controller::HTTP_OK );
					} else {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray ()
						), REST_Controller::HTTP_OK );
					}

			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}

	/*
	 * @author: Anjani Gupta
	 * @date: 24/10/2017
	 * @method: update_cart
	 * @desc: add/delete cart products
	 * @params: action, product_id, retailer_id, user_id, quantity, price
	 */

	public function update_cart_post(){
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
				$this->form_validation->set_rules ( 'action', 'action', 'trim|required' );
				$this->form_validation->set_rules ( 'product_id', 'Product Id', 'trim|required' );
				$this->form_validation->set_rules ( 'retailer_id', 'Retailer Id', 'trim|required' );
				$this->form_validation->set_rules ( 'user_id', 'User Id', 'trim|required' );
				$this->form_validation->set_rules ( 'quantity', 'Quantity', 'trim|required' );
				$this->form_validation->set_rules ( 'price', 'Product Price', 'trim|required' );
				$this->form_validation->set_rules ( 'city_id', 'City Id', 'trim' );

				if ($this->form_validation->run () == TRUE)
				{

					$apiService = new Api_service();
					$response = $apiService->update_cart();

					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );

				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Missing parameters.'
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_OK);
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_OK);
		}
	}

	/*
	 * @author: Anjani Gupta
	 * @date: 24/10/2017
	 * @method: user_cart
	 * @desc: get cart detail
	 * @params: user_id
	 */
	public function user_cart_post(){
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
				$this->form_validation->set_rules ( 'user_id', 'User Id', 'trim|required' );

				if ($this->form_validation->run () == TRUE)
				{

					$apiService = new Api_service();
					$response = $apiService->get_cart();

					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );

				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Missing parameters.'
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_OK);
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_OK);
		}
	}


	/*
	 * @author: Anjani Gupta
	 * @date: 24/10/2017
	 * @method: add_address
	 * @desc: add new address of user
	 * @params: user_id, address, state, city, pincode
	 */
	public function add_address_post(){
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
				$this->form_validation->set_rules ( 'user_id', 'User Id', 'trim|required' );
				$this->form_validation->set_rules ( 'state_id', 'State Id', 'trim|required' );
				$this->form_validation->set_rules ( 'name', 'User Name', 'trim|required' );
				$this->form_validation->set_rules ( 'mobile', 'Mobile Number', 'trim|required' );
				$this->form_validation->set_rules ( 'address', 'Address', 'trim|required' );
				$this->form_validation->set_rules ( 'state', 'State', 'trim|required' );
				$this->form_validation->set_rules ( 'city', 'City', 'trim|required' );
				$this->form_validation->set_rules ( 'is_default', 'is default', 'trim|required' );

				$this->form_validation->set_rules ( 'city_id', 'City Id', 'trim|required' );
				$this->form_validation->set_rules ( 'latitude', 'latitude', 'trim|required' );
				$this->form_validation->set_rules ( 'longitude', 'longitude', 'trim|required' );

				if ($this->form_validation->run () == TRUE)
				{

					$apiService = new Api_service();
					$response = $apiService->add_address();
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );

				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Missing parameters.'
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_OK);

			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_OK);
		}
	}

	/*
	 * @author: Anjani Gupta
	 * @date: 25/10/2017
	 * @method: address
	 * @desc: get list of user address
	 * @params: user_id
	 */
	public function address_post(){
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
				$this->form_validation->set_rules ( 'user_id', 'User Id', 'trim|required' );

				if ($this->form_validation->run () == TRUE)
				{

					$apiService = new Api_service();
					$response = $apiService->get_address();
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );

				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Missing parameters.'
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_OK);
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_OK);
		}
	}

	/*
	 * @author: Anjani Gupta
	 * @date: 25/10/2017
	 * @method: terms
	 * @desc: get terms & conditions by state id
	 * @params: state_id
	 */
	public function terms_post(){
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
				$this->form_validation->set_rules ( 'state_id', 'State Id', 'trim|required' );

				if($this->post('is_retailer')){
					$this->form_validation->set_rules ( 'is_retailer', 'Is retailer', 'trim|required' );
				}

				if ($this->form_validation->run () == TRUE)
				{

					$apiService = new Api_service();
					$response = $apiService->get_terms();
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );

				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Missing parameters.'
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_OK);

			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_OK);
		}
	}

	/*
	 * @author: Anjani Gupta
	 * @date: 25/10/2017
	 * @method: empty_cart
	 * @desc: empty cart data
	 * @params: user_id, retailer_id
	 */
	public function empty_cart_post(){
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
				$this->form_validation->set_rules ( 'user_id', 'User Id', 'trim|required' );
				$this->form_validation->set_rules ( 'retailer_id', 'Retailer Id', 'trim|required' );

				if ($this->form_validation->run () == TRUE)
				{

					$apiService = new Api_service();
					$response = $apiService->empty_cart();
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );

				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Missing parameters.'
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_OK);
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_OK);
		}
	}


	/*
	 * @author: Anjani Gupta
	 * @date: 25/10/2017
	 * @method: request_order
	 * @desc: order request by user
	 * @params: user_id, retailer_id, address_id, array of product object (product_id, quantity, price)
	 */
	public function request_order_post(){

		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
				$this->form_validation->set_rules ( 'user_id', 'User Id', 'trim|required' );
				$this->form_validation->set_rules ( 'retailer_id', 'Retailer Id', 'trim|required' );
				$this->form_validation->set_rules ( 'address_id', 'Address Id', 'trim|required' );
				$this->form_validation->set_rules ( 'product', 'Product Object', 'trim|required' );
				$this->form_validation->set_rules ( 'instruction', 'Instruction', 'trim' );

				if ($this->form_validation->run () == TRUE)
				{

					$apiService = new Api_service();
					$response = $apiService->request_order();
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );

				} else {

					$this->set_response ( array (
							'status' => 0,
							'message' => 'Missing parameters.'
					), REST_Controller::HTTP_OK );
				}

			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_OK);
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_OK);
		}
	}

	/*
	 * @author: Anjani Gupta
	 * @date: 26/10/2017
	 * @method: order_history
	 * @desc: order request by user
	 * @params: order_id(optional), retailer_id, user_id
	 */
	public function order_history_post(){
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
				$this->form_validation->set_rules ( 'order_id', 'Order Id', 'trim|required' );
				$this->form_validation->set_rules ( 'user_id', 'User Id', 'trim|required' );

				if ($this->form_validation->run () == TRUE)
				{

					$apiService = new Api_service();
					$response = $apiService->order_history();
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );

				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Missing parameters.'
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_OK);
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_OK);
		}
	}


	/*
	 * @author: Anjani Gupta
	 * @date: 26/10/2017
	 * @method: rating
	 * @desc: mark rating
	 * @params: order_id, rating
	 */
	public function rating_post(){
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
				$this->form_validation->set_rules ( 'order_id', 'Order Id', 'trim|required' );
				$this->form_validation->set_rules ( 'rating', 'Rating', 'trim|required' );
				$this->form_validation->set_rules ( 'user_id', 'User Id', 'trim|required' );
				//$this->form_validation->set_rules ( 'retailer_id', 'Retailer Id', 'trim|required' );

				if ($this->form_validation->run () == TRUE)
				{

					$apiService = new Api_service();
					$response = $apiService->update_rating();
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );

				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Missing parameters.'
					), REST_Controller::HTTP_OK );
				}

			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_OK);
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_OK);
		}
	}

	/*
	 * @author: Anjani Gupta
	 * @date: 27/10/2017
	 * @method: all_ratings
	 * @desc: fetch all product ratings
	 * @params: order_id(optional), user_id
	 */
	public function all_ratings_post(){
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{

				$this->form_validation->set_rules ( 'order_id', 'Order Id', 'trim|required' );
				$this->form_validation->set_rules ( 'user_id', 'User Id', 'trim|required' );

				if ($this->form_validation->run () == TRUE)
				{

					$apiService = new Api_service();
					$response = $apiService->get_ratings();
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );

				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Missing parameters.'
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_OK);
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_OK);
		}
	}

	/*
	 * @author: Anjani Gupta
	 * @date: 28/10/2017
	 * @method: update_price
	 * @desc: Update product price
	 * @params: product_id, user_id, price
	 */
	public function update_price_post(){
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
				$this->form_validation->set_rules ( 'product_id', 'Product Id', 'trim|required' );
				$this->form_validation->set_rules ( 'user_id', 'User Id', 'trim|required' );
				$this->form_validation->set_rules ( 'price', 'Price', 'trim|required' );

				if ($this->form_validation->run () == TRUE)
				{

					$apiService = new Api_service();
					$response = $apiService->update_price();
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );

				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Missing parameters.'
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_OK);
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_OK);
		}
	}

	/*
	 * @author: Anjani Gupta
	 * @date: 28/10/2017
	 * @method: remove_product
	 * @desc: Update product price
	 * @params: product_id, user_id
	 */
	public function remove_product_post(){
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
				$this->form_validation->set_rules ( 'product_id', 'Product Id', 'trim|required' );
				$this->form_validation->set_rules ( 'user_id', 'User Id', 'trim|required' );

				if ($this->form_validation->run () == TRUE)
				{

					$apiService = new Api_service();
					$response = $apiService->remove_product();
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );

				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Missing parameters.'
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_OK);

			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_OK);
		}
	}

	/*
	 * @author: Anjani Gupta
	 * @date: 28/10/2020
	 * @method: in_out_stock_product
	 * @desc: Update stock status
	 * @params: product_id, user_id
	 */
	public function in_out_stock_product_post(){
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
				$this->form_validation->set_rules ( 'product_id', 'Product Id', 'trim|required' );
				$this->form_validation->set_rules ( 'retailer_id', 'User Id', 'trim|required' );

				if ($this->form_validation->run () == TRUE)
				{

					$apiService = new Api_service();
					$response = $apiService->in_out_stock_product();
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );

				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Missing parameters.'
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_OK);

			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_OK);
		}
	}


	/*
	 * @author: Anjani Gupta
	 * @date: 28/10/2017
	 * @method: requested_orders
	 * @desc: list of requested orders
	 * @params: retailer_id
	 */
	public function requested_orders_post(){
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
				$this->form_validation->set_rules ( 'retailer_id', 'Retailer Id', 'trim|required' );
				$this->form_validation->set_rules ( 'order_id', 'Order Id', 'trim|required' );

				if ($this->form_validation->run () == TRUE)
				{

					$apiService = new Api_service();
					$response = $apiService->requested_orders();
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );

				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Missing parameters.'
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_OK);

			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_OK);
		}
	}

	/*
	 * @author: Anjani Gupta
	 * @date: 28/10/2017
	 * @method: payment_details_submit
	 * @desc: submit the data after completeting payment through payment gateway(payU)
	 * @params:
	 */
	public function payment_details_submit_post(){
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
				// $this->form_validation->set_rules ( 'retailer_id', 'Retailer Id', 'trim|required' );
				// $this->form_validation->set_rules ( 'order_id', 'Order Id', 'trim|required' );
				//
				// if ($this->form_validation->run () == TRUE)
				// {

					$apiService = new Api_service();
					$response = $apiService->payment_details_submit();
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );

				//}
				// else {
				// 	$this->set_response ( array (
				// 			'status' => 0,
				// 			'message' => 'Missing parameters.'
				// 	), REST_Controller::HTTP_OK );
				// }
			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_OK);

			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_OK);
		}
	}




	/*
	 * @author: Anjani Gupta
	 * @date: 31/10/2017
	 * @method: respond_order_request
	 * @desc: Confirm/decline/ongoing/delivered/failed
	 * @params: order_id
	 */
	public function respond_order_request_post(){
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
				$this->form_validation->set_rules ( 'order_id', 'Order Id', 'trim|required' );
				$this->form_validation->set_rules ( 'status', 'Order status(Confirm/decline/ongoing/delivered/failed)', 'trim|required' );

				if($this->post('status')==ORDER_DELIVERED){
					$this->form_validation->set_rules ( 'delivered_at', 'Order Id', 'trim|required' );
				}

				if($this->post('status')==ORDER_DECLINED){
					$this->form_validation->set_rules ( 'reason', 'Declined reason', 'trim|required' );
				}

				if ($this->form_validation->run () == TRUE)
				{

					$apiService = new Api_service();
					$response = $apiService->respond_order_request();
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );

				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Missing parameters.'
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_OK);

			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_OK);
		}
	}

	/*
	 * @author: Anjani Gupta
	 * @date: 02/11/2017
	 * @method: retailer_orders
	 * @desc: Get retailer orders
	 * @params: retailer_id
	 */
	public function retailer_orders_post(){
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
				$this->form_validation->set_rules ( 'retailer_id', 'Retailer Id', 'trim|required' );

				if ($this->form_validation->run () == TRUE)
				{

					$apiService = new Api_service();
					$response = $apiService->get_retailer_orders();
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );

				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Missing parameters.'
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_OK);

			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_OK);
		}
	}

	/*
	 * @author: Anjani Gupta
	 * @date: 02/11/2017
	 * @method: retailer_order_detail
	 * @desc: Get retailer order detail
	 * @params: retailer_id, order_id
	 */
	public function retailer_order_detail_post(){
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
				$this->form_validation->set_rules ( 'retailer_id', 'Retailer Id', 'trim|required' );
				$this->form_validation->set_rules ( 'order_id', 'Retailer Id', 'trim|required' );

				if ($this->form_validation->run () == TRUE)
				{

					$apiService = new Api_service();
					$response = $apiService->retailer_order_detail();
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );

				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Missing parameters.'
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_OK);

			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_OK);
		}
	}

	/*
	 * @author: Anjani Gupta
	 * @date: 02/11/2017
	 * @method: update_address
	 * @desc: Update user address
	 * @params: id
	 */
	public function update_address_post(){
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
				$this->form_validation->set_rules ( 'user_id', 'User Id', 'trim|required' );
				$this->form_validation->set_rules ( 'id', 'Address Id', 'trim|required' );
				$this->form_validation->set_rules ( 'name', 'name', 'trim|required' );
				$this->form_validation->set_rules ( 'address', 'address', 'trim|required' );
				$this->form_validation->set_rules ( 'city', 'city', 'trim|required' );
				$this->form_validation->set_rules ( 'state', 'state', 'trim|required' );
				$this->form_validation->set_rules ( 'city_id', 'city id', 'trim|required' );
				$this->form_validation->set_rules ( 'state_id', 'state id', 'trim|required' );
				$this->form_validation->set_rules ( 'is_default', 'Is default', 'trim|required' );
				//$this->form_validation->set_rules ( 'mobile', 'mobile', 'trim|required' );
				//$this->form_validation->set_rules ( 'pincode', 'pincode', 'trim|required' );
				if($this->post('is_retailer')){
					$this->form_validation->set_rules ( 'is_retailer', 'Is retailer', 'trim|required' );
					$this->form_validation->set_rules ( 'latitude', 'latitude', 'trim|required' );
					$this->form_validation->set_rules ( 'longitude', 'longitude', 'trim|required' );
				}

				if ($this->form_validation->run () == TRUE)
				{

					$apiService = new Api_service();
					$response = $apiService->update_address();
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );

				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Missing parameters.'
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_OK);

			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_OK);
		}
	}

	/*
	 * @author: Anjani Gupta
	 * @date: 06/11/2017
	 * @method: delete_address
	 * @desc: Delete user address
	 * @params: id
	 */
	public function delete_address_post(){
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
				$this->form_validation->set_rules ( 'id', 'Address Id', 'trim|required' );
				$this->form_validation->set_rules ( 'user_id', 'User Id', 'trim|required' );

				if ($this->form_validation->run () == TRUE)
				{

					$apiService = new Api_service();
					$response = $apiService->delete_address();
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );

				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Missing parameters.'
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_OK);

			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_OK);
		}
	}


	/*
	 * @method: update_terms
	 * @date: 09-11-2017
	 * @params: state_id, is_accepted, user_id
 	 */

	public function update_terms_post(){
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
				$this->form_validation->set_rules ( 'state_id', 'State Id', 'trim|required' );
				$this->form_validation->set_rules ( 'user_id', 'User Id', 'trim|required' );
				$this->form_validation->set_rules ( 'is_accepted', 'Is accepetd', 'trim|required' );

				if ($this->form_validation->run () == TRUE)
				{

					$apiService = new Api_service();
					$response = $apiService->update_terms();
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );

				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Missing parameters.'
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_OK);

			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_OK);
		}
	}

	/*
	 * @method: holidays
	 * @date: 14-11-2017
	 * @params: state_id
	 */

	public function holidays_post(){
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
				$this->form_validation->set_rules ( 'state_id', 'State Id', 'trim|required' );

				if ($this->form_validation->run () == TRUE)
				{

					$apiService = new Api_service();
					$response = $apiService->get_holidays();
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );

				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Missing parameters.'
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_OK);

			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_OK);
		}
	}


	public function retailer_rating_post(){
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
				$this->form_validation->set_rules ( 'retailer_id', 'Retailer Id', 'trim|required' );

				if ($this->form_validation->run () == TRUE)
				{

					$apiService = new Api_service();
					$response = $apiService->retailer_rating();
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );

				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Missing parameters.'
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_OK);

			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_OK);
		}
	}

	public function request_count_post(){
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
				$this->form_validation->set_rules ( 'retailer_id', 'Retailer Id', 'trim|required' );

				if ($this->form_validation->run () == TRUE)
				{

					$apiService = new Api_service();
					$response = $apiService->request_count();
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );

				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Missing parameters.'
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_OK);

			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_OK);
		}
	}


	/* user last order rating check  30-11-2017 6:00 PM */
	public function check_order_rating_post(){
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
				$this->form_validation->set_rules ( 'user_id', 'User Id', 'trim|required' );

				if ($this->form_validation->run () == TRUE)
				{

					$apiService = new Api_service();
					$response = $apiService->check_rating_existence();
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );

				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Missing parameters.'
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_OK);

			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_OK);
		}
	}

	/**
	 * @method: forgot password
	 */

	public function forgot_password_post(){
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
				$this->form_validation->set_rules ( 'email', 'Email Id', 'trim|required' );

				if ($this->form_validation->run () == TRUE)
				{

					$apiService = new Api_service();
					$response = $apiService->is_email_exist();
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );

				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Missing parameters.'
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_OK);

			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_OK);
		}
	}

	public function auto_reject_orders_get(){
		$apiService = new Api_service();

		$response = $apiService->auto_reject_orders();
		$this->set_response ( array (
				'status' => 1,
				'message' => "Auto rejected orders",
				'jsonData' => $response
		), REST_Controller::HTTP_OK );
	}

	/**
	 * @method: user_detail
	 * @param $user_id
	 * @return user data
	 */
	public function user_detail_post(){
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
				$this->form_validation->set_rules ( 'user_id', 'User Id', 'trim|required' );

				if ($this->form_validation->run () == TRUE)
				{

					$apiService = new Api_service();
					$response = $apiService->get_user_data();

					$this->set_response ( array (
							'status' => 1,
							'message' => 'User data found',
							'jsonData' => $response
					), REST_Controller::HTTP_OK );

				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Missing parameters.'
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_OK);
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_OK);
		}
	}

	/**
	 * @method: logout
	 * @param  $user_id, $device_id
	 * @return user data
	 */
	public function logout_post(){
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
				$this->form_validation->set_rules ( 'user_id', 'User Id', 'trim|required' );
				$this->form_validation->set_rules ( 'device_id', 'Device Id', 'trim|required' );

				if ($this->form_validation->run () == TRUE)
				{

					$apiService = new Api_service();
					$response = $apiService->logout();

					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );

				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Missing parameters.'
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_OK);

			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_OK);
		}
	}

	/**
	 * @method: city_list
	 * @param  $state_id
	 * @return city list
	 */
	public function city_list_post(){
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
				$this->form_validation->set_rules ( 'state_id', 'State Id', 'trim|required' );

				if ($this->form_validation->run () == TRUE)
				{

					$apiService = new Api_service();
					$response = $apiService->city_list();

					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );

				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Missing parameters.'
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_OK);

			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_OK);
		}
	}


	public function get_city_state_id_by_name_post(){
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
				$this->form_validation->set_rules ( 'state', 'State name', 'trim|required' );
				$this->form_validation->set_rules ( 'city', 'City name array', 'trim|required' );

				if ($this->form_validation->run () == TRUE)
				{

					$apiService = new Api_service();
					$response = $apiService->get_city_state_id_by_name();

					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );

				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Missing parameters.'
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_OK);

			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_OK);
		}
	}


	/**
	 * @method: notification_list
	 * @param  $user_id
	 * @return notification list of last two days
	 */
	public function notification_list_post(){
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
				$this->form_validation->set_rules ( 'user_id', 'User Id', 'trim|required' );

				if ($this->form_validation->run () == TRUE)
				{

					$apiService = new Api_service();
					$response = $apiService->notification_list();

					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );

				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Missing parameters.'
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_OK);

			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_OK);
		}
	}


	/*
	 * @author: Aakriti
	 * @date: 10/03/2018
	 * @method: Mapped Retailer
	 * @desc: Mapped Retailer Process (Retailer)
	 * @params: username, password, email, mobile, dob, state_id, address, device_type, device_id, fcm_reg_id
	 */
	public function mapped_retailer_post()
	{
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
				$this->form_validation->set_rules ( 'check_login', 'Check Login', 'trim|required' );
				$this->form_validation->set_rules ( 'latitude', 'latitude', 'trim|required' );
				$this->form_validation->set_rules ( 'longitude', 'longitude', 'trim|required' );
				$this->form_validation->set_rules ( 'licence', 'Licence number', 'trim|required' );
				$this->form_validation->set_rules ( 'device_type', 'Device Type', 'trim|required' );
				$this->form_validation->set_rules ( 'device_id', 'Device Id', 'trim|required' );
				$this->form_validation->set_rules ( 'store_name', 'Store name', 'trim|required' );
				$this->form_validation->set_rules ( 'is_retailer', 'Is retailer', 'trim|required' );
				$this->form_validation->set_rules ( 'username', 'User Id', 'trim|required' );
				//$this->form_validation->set_rules ( 'mobile', 'mobile', 'trim|required' );
				$this->form_validation->set_rules ( 'fcm_reg_id', 'Fcm Id', 'trim|required' );
				$this->form_validation->set_rules ( 'city', 'city', 'trim|required' );
				$this->form_validation->set_rules ( 'pincode', 'pincode', 'trim|required' );
				$this->form_validation->set_rules ( 'state_id', 'state', 'trim|required' );
				$this->form_validation->set_rules ( 'city_id', 'City Id', 'trim|required' );
				$this->form_validation->set_rules ( 'address', 'address', 'trim|required' );
				$this->form_validation->set_rules ( 'state', 'state', 'trim|required' );


				if ($this->form_validation->run () == TRUE)
				{
					$apiService = new Api_service();
					$response = $apiService->registration_process(true);

					if ($response->getStatus () == 1)
					{
				            //prd($response);

				        $this->set_response ( array (
								'status' => $response->getStatus (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray ()
						), REST_Controller::HTTP_OK );

					} else {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray ()
						), REST_Controller::HTTP_OK );
					}

				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Requested data not found'
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_BAD_REQUEST );

			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}


	/*
	 * @author: Aakriti
	 * @date: 12/03/2018
	 * @method: Mapped Retailer Registration
	 * @desc: Mapped Retailer Registration Process (Retailer)
	 * @params: username, password, email, mobile, dob, state_id, address, device_type, device_id, fcm_reg_id
	 */
	public function mapped_retailer_registration_post()
	{
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
				$this->form_validation->set_rules ( 'mapped_id', 'Mapped Id', 'trim|required' );
				$this->form_validation->set_rules ( 'username', 'User Id', 'trim|required' );
				$this->form_validation->set_rules ( 'password', 'Password', 'trim|required' );
				$this->form_validation->set_rules ( 'email', 'email', 'trim|required' );
				$this->form_validation->set_rules ( 'mobile', 'mobile', 'trim|required' );
				$this->form_validation->set_rules ( 'state_id', 'state', 'trim|required' );
				$this->form_validation->set_rules ( 'city_id', 'City Id', 'trim|required' );
				$this->form_validation->set_rules ( 'device_type', 'Device Type', 'trim|required' );
				$this->form_validation->set_rules ( 'device_id', 'Device Id', 'trim|required' );
				$this->form_validation->set_rules ( 'fcm_reg_id', 'Fcm Id', 'trim|required' );
				$this->form_validation->set_rules ( 'is_retailer', 'Is retailer', 'trim|required' );
				$this->form_validation->set_rules ( 'store_name', 'Store name', 'trim|required' );
				$this->form_validation->set_rules ( 'licence', 'Licence number', 'trim|required' );
				$this->form_validation->set_rules ( 'cp_name', 'Contact person name', 'trim|required' );
				$this->form_validation->set_rules ( 'cp_mobile', 'Contact person mobile', 'trim|required' );
				$this->form_validation->set_rules ( 'is_provide_snacks', 'Is provide snacks', 'trim|required' );
				$this->form_validation->set_rules ( 'latitude', 'latitude', 'trim|required' );
				$this->form_validation->set_rules ( 'longitude', 'longitude', 'trim|required' );
				$this->form_validation->set_rules ( 'min_order', 'Min order value', 'trim|required' );
				$this->form_validation->set_rules ( 'start_time', 'Opening time', 'trim|required' );
				$this->form_validation->set_rules ( 'end_time', 'Closing time', 'trim|required' );
				$this->form_validation->set_rules ( 'payment_mode', 'Payment Mode', 'trim|required' );
				$this->form_validation->set_rules ( 'address', 'address', 'trim|required' );
				$this->form_validation->set_rules ( 'state', 'state', 'trim|required' );
				$this->form_validation->set_rules ( 'city', 'city', 'trim|required' );


				if ($this->form_validation->run () == TRUE)
				{

					$apiService = new Api_service();
					$response = $apiService->update_mapped_retailer();

					if ($response->getStatus () == 1)
					{
				            //prd($response);

				        $this->set_response ( array (
								'status' => $response->getStatus (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray ()
						), REST_Controller::HTTP_OK );

					} else {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray ()
						), REST_Controller::HTTP_OK );
					}

				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Requested data not found'
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_BAD_REQUEST );

			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}



	/******************************************promotion ************************************************/

	//All Products/Brands Name
	public function brands_post()
	{
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
					$brand_name = trim($this->post('brand_name'));
						$apiService = new Api_service();
						$response = $apiService->brands_list ($brand_name);
									$this->set_response ( array (
									'status' => $response->getStatus (),
									'message' => $response->getMsg (),
									'jsonData' => $response->getObjArray ()
							), REST_Controller::HTTP_OK );
				} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_BAD_REQUEST );

			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}


	/*
	 * @author: Tanuj
	 * @date: 21/06/2019
	 * @method: promotion
	 * @desc: User check available promotion
	 * @params: Open for Pan-india
	 [ json params:
	 	{
			"userId" :"1989"
		}
	]
	 */
	//All Promotion pincode-wise
	public function promotions_post()
	{
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
					$userid = trim($this->post('userId'));

						$apiService = new Api_service();
						$response = $apiService->promotions_list ($userid);
									$this->set_response ( array (
									'status' => $response->getStatus (),
									'message' => $response->getMsg (),
									'jsonData' => $response->getObjArray ()
							), REST_Controller::HTTP_OK );
				} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}

	// All Promotion and Brands Details
	public function participatedUserHistory_post()
	{
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
		 		  $user_id 	= trim($this->post('userId'));
				 		$apiService = new Api_service();
						$response = $apiService->participatedUserHistory ($user_id);
									$this->set_response ( array (
									'status' => $response->getStatus (),
									'message' => $response->getMsg (),
									'jsonData' => $response->getObjArray ()
							), REST_Controller::HTTP_OK );
				} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_BAD_REQUEST );

			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}




/*
	 * @author: Tanuj
	 * @date: 21/06/2019
	 * @method: barcode_detail
	 * @desc: Barcode fetch and get validate first then return details
	 * @params: brand_barcode
	    json params:
		{
			"promotion_id" :"RG004587690",
			"userId": "1978",
			"qty":"375ml",
			"address":"Gurugram 122018",
			"barcode":"21040703310719071900679001018769",
			"reward_points":"0",
			"is_participated": "1"
		}

	 */

	// All Promotion and Brands Details
	public function promotion_details_post()
	{
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
		 		  $id 								= trim($this->post('id'));
		 		  $promotion_id 					= trim($this->post('promotion_id'));
				  $user_id 							= trim($this->post('userId'));
				  $is_participated 					= trim($this->post('is_participated'));
				  $qty						 		= trim($this->post('qty'));
				  $address						 	= trim($this->post('address'));
				  // $barcode						= substr($this->post('barcode'),0,12);
				  $barcode							= $this->post('barcode');
				  $reward_points					= $this->post('reward_points');

						$apiService = new Api_service();
						$response = $apiService->promotions_details ($id,$promotion_id,$user_id,$is_participated,$barcode,$qty,$address,$reward_points);
									$this->set_response ( array (
									'status' => $response->getStatus (),
									'message' => $response->getMsg (),
									'jsonData' => $response->getObjArray ()
							), REST_Controller::HTTP_OK );
				} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_BAD_REQUEST );

			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}

	/*
	 * @author: Tanuj
	 * @date: 21/06/2019
	 * @method: barcode_detail
	 * @desc: Barcode fetch and get validate first then return details
	 * @params: brand_barcode
	 [ json params:
		{
			"promotion_id" :"122018"
		}
	]
	 */
	// All Barcode fetch and get validate first then return details
	public function barcode_detail_post()
	{
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
					// $brand_barcode 			= substr($this->post('brand_barcode'),0,12);
					$brand_barcode 			= $this->post('brand_barcode');
					$brand_name 				= trim($this->post('brand_name'));
					$promotion_id 			= trim($this->post('promotion_id'));
					$user_id 						= trim($this->post('userId'));

					$apiService = new Api_service();
						$response = $apiService->barcode_detail ($user_id,$brand_barcode,$brand_name,$promotion_id);
									$this->set_response ( array (
									'status' => $response->getStatus (),
									'message' => $response->getMsg (),
									'jsonData' => $response->getObjArray ()
							), REST_Controller::HTTP_OK );
				} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_BAD_REQUEST );

			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}


	/*
	 * @author: Tanuj
	 * @date: 21/06/2019
	 * @method: barcode_detail
	 * @desc: Barcode fetch and get validate first then return details
	 * @params: brand_barcode
	 [ json params:
		{
			"promotion_id" :"122018"
		}
	]
	 */
	// Sum of All Reward points api
	public function total_reward_points_post()
	{
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
					$user_id 			= trim($this->post('userId'));

					$apiService = new Api_service();
						$response = $apiService->total_reward_points ($user_id);
									$this->set_response ( array (
									'status' => $response->getStatus (),
									'message' => $response->getMsg (),
									'jsonData' => $response->getObjArray ()
							), REST_Controller::HTTP_OK );
				} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_BAD_REQUEST );

			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}

	/*
	 * @author: Tanuj
	 * @date: 21/06/2019
	 * @method: redeed
	 * @desc: redeem checkout out
	 * @params: Open for Pan-india
	 [ json params:
	 	{
			"userId" :"1989"
			"reward_points" :"10"
		}
	]
	 */
	public
	function redeem_checkout_post()
	{
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			try
			{
					$user_id 				= trim($this->post('userId'));
					$reward_points 			= trim($this->post('reward_points'));

					$apiService = new Api_service();
						$response = $apiService->redeem_checkout ($user_id,$reward_points);
									$this->set_response ( array (
									'status' => $response->getStatus (),
									'message' => $response->getMsg (),
									'jsonData' => $response->getObjArray ()
							), REST_Controller::HTTP_OK );
				} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_BAD_REQUEST );

			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}





	/*********************************************************************************************************/


	//Pub Sub categories List
	public function pubsubcategories_post()
	{
		if ($_SERVER ['REQUEST_METHOD'] == "POST")
		{
			$apiService = new Api_service();
			try {

				//$this->form_validation->set_rules ('category_id', 'Category Id', 'trim|required');


					$response = $apiService->pubsubcategories_list();

					if ($response->getStatus ()) {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray ()
						), REST_Controller::HTTP_OK );
					} else {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray ()
						), REST_Controller::HTTP_OK );
					}


			} catch ( Exception $e ) {

				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage ()
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed'
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}

	/**** update product details ******/
public function add_product_post()
{
	if ($_SERVER ['REQUEST_METHOD'] == "POST")
	{
		$apiService = new Api_service();
		try {

			$this->form_validation->set_rules ( 'description', 'Description', 'trim|required' );
			$this->form_validation->set_rules ( 'title', 'Title', 'trim|required' );


			if ($this->form_validation->run () == TRUE)
			{
				$response = $apiService->add_product();

				if ($response->getStatus ()) {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );
				} else {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray ()
					), REST_Controller::HTTP_OK );
				}
			} else {
				$this->set_response ( array (
						'status' => 0,
						'message' => 'Requested data not found'
				), REST_Controller::HTTP_OK );
			}
		} catch ( Exception $e ) {

			$this->set_response ( array (
					'status' => 0,
					'message' => $e->getMessage ()
			), REST_Controller::HTTP_BAD_REQUEST );
		}
	} else {
		$this->set_response ( array (
				'status' => 0,
				'message' => 'This HTTP method is not allowed'
		), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
	}
}

/*************************************************************************************************************************************/




}
