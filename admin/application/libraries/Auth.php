<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth {
	
   private $ci; 
   function __construct() { 
       $this->ci =& get_instance();
       $this->ci->load->database();
       $this->ci->load->library('session');
       if($this->ci->session->userdata('role_id') =='1' && !isset($_SESSION['is_login_otp']))
       {
  	 $this->logout_no_redirect();
  	 redirect('login');
       }
       
       
   }

	function _is_logged_in() { 
        $logged_in = (isset($_SESSION['logged_in']) and isset($_SESSION['site']) and $_SESSION['site'] == base_url('')) ? true : false;
        if (!$logged_in and isset($_COOKIE['logged_in'])) {
            $this->remember_login();
        } else { 
            return $logged_in;
        }
    }
	
	

    function check_login() {
        if ($this->_is_logged_in()) {
            $role_id = $this->ci->session->userdata('role_id');
            $this->redirect_role($role_id);
        } else {
            return false;
        }
    }
	
	function remember_login() {
        $user_cookie = ($_COOKIE['logged_in'] and isset($_COOKIE['userid']) and isset($_COOKIE['site']) and $_COOKIE['site'] == base_url()) ? true : false;
        if ($user_cookie) {
            $userdata = $this->ci->db->where('id', $_COOKIE['userid'])->get('users')->first_row(); 
            $this->set_user_data_session($userdata); 
			$this->redirect_role($userdata->role_id);
        } else {
            return false;
        }
    }
	
	function set_user_data_session($userdata){   
		$userdata->site   = base_url();
		$userdata->logged_in  = TRUE; 
		$userdata->is_login_otp = TRUE;  
		$this->ci->session->set_userdata((array)$userdata); 
	}

	function redirect_role($role_id){ 
		switch ($role_id) {
			case 1:
				redirect('admin/dashboard');
				break;
			case 2:
				redirect('admin/dashboard');
                break;
                case 4:
                    redirect('admin/dashboard');
                    break;
			default:
				redirect('');
		}
	}
	
	
	function auth_login($login = null, $password = null, $remember = false) {  
        //is empty
        if (empty($login) || empty($password)) {
            $this->ci->session->set_flashdata('message', array('status' => 'error', 'message' => 'Username and Password fields must be filled out.'));
            return false;
        }
	
        // getting user data by username, pass, status
        $userdata = $this->ci->db->where(array('email' => $login, 'password' => md5($password), 'status' => '1','deleted' => '0'))->get('users')->first_row(); 
        // is username pass exist	   
        if ($userdata) {
            //The login was successfully validated, so setup the session		 
           $this->set_user_data_session($userdata); 

            // if rememebe then set cooki for 1m
            if ($remember) {
                foreach ($login_data as $name => $val) {
                    setcookie($name, $val, time() + (86400 * 30));
                }

                $e_password = base64_encode($password);
                setcookie('password', $e_password, time() + (86400 * 30));
            } else {
                foreach ($login_data as $name => $val) {
                    setcookie($name, $val, time() - 3600);
                }
                setcookie("password", false, time() - 3600);
            }




            //$this->ci->session->set_flashdata('message', array('status' => 'success', 'message' => 'User successfully logined!')); 
			$this->redirect_role($userdata->role_id);
        } else {
			set_flash_message($type = 'error', $message = 'Sorry! Username and Password not matching');
            //$this->ci->session->set_flashdata('message', array('status' => 'error', 'message' => 'Sorry! Username and Password not matching'));
            redirect('login');
        }
    }
	 
	 
    function logout_no_redirect() {
        setcookie("logged_in", false, time() - 3600);
        $this->ci->session->unset_userdata(array('userid', 'email', 'user_name', 'role_id', 'site', 'logged_in'));
        $this->ci->session->unset_userdata('auth_otp');
        return true;
    }
	
	function logout($change_pass = false) {
        if ($this->logout_no_redirect()) {
            if ($_SESSION['flash_message']) {
                set_flash_message($_SESSION['flash_status'], $_SESSION['flash_message']);
            }
            redirect(base_url('login'));
        }
    }
    
	
	function send_pass_hash(){
		$this->form_validation->set_rules($this->user_model->forgot_password_validation_rules()); 
		if ($this->form_validation->run() === false) {
			set_flash_message('error','There is something wrong, Please check below error(s).');
			return false;
		}
		
		// User exists, create a hash to confirm the reset request.
		$this->load->helper('string');
		$hash = sha1(random_string('alnum', 40) . $this->input->post('email'));

		// Save the hash to the db for later retrieval.
		$email = $this->input->post('email');
		$this->ci->db->where('email',$email)->update('users',array('reset_hash' => $hash, 'reset_by' => strtotime("+24 hours"))
		);

		// Create the link to reset the password.
		$pass_link = site_url('users/reset_password/' . str_replace('@', ':', $this->input->post('email')) . "/{$hash}");

		// Now send the email 
		 
		$this->load->library('email');

		$this->email->from('admin@peach.com', 'Peach');
		$this->email->to($this->input->post('email')); 

		$this->email->subject('Peach: Your Temporary Password');
		$message = $this->load->view(
				'forgot_password_email',
				array('link' => $pass_link),
				true
			);
			 
		$this->email->message($message);  
		$this->email->set_mailtype("html"); 
		if ($this->email->send()) {
			return true;
		} else {
			set_flash_message('error',"There is some technical issue. email can't send");
			return false;
		}
		
	}
	
	
	public function reset_password($email = '', $code = '')
    { 
		// If the user is logged in, go home.
		if ($this->_is_logged_in()) {
            $this->check_login();
        }  

        // If there is no code/email, then it's not a valid request.
        if (empty($code) || empty($email)) {
			set_flash_message('error','That did not appear to be a valid password reset request.');
			redirect(base_url('users/forgot_password'));
        }else{
			$email = str_replace(':', '@', $email);
		}
		
		
        // Check the code against the database or time out
        $where =array(
                'email'       => $email,
                'reset_hash'  => $code,
                // 'reset_by >=' => time(),
            );
        $user = $this->ci->db->where($where)->get('users')->row(); 
        // $user will be an Object if a single result was returned.
        if (!is_object($user)) {
			set_flash_message('error','That did not appear to be a valid password reset request.');
			redirect(base_url('users/forgot_password'));
        }

		
        // Handle the form
        if (isset($_POST['set_password'])) {  
			$this->form_validation->set_rules('password', 'password', 'required|max_length[120]|min_length[6]');
			$this->form_validation->set_rules('pass_confirm', 'confirm password', 'required|matches[password]');
            if ($this->form_validation->run() !== false) { 
                $data = array(
                    'password'   => md5($this->input->post('password')),
					'reset_by'    => 0,
					'reset_hash'  => '', 
                );
				$this->ci->db->where('email',$email)->update('users',$data);
				set_flash_message('success','Please login using your new password.');
				redirect(base_url('login')); 
            }
        }
		
		// open view if validate 
        $includeCss = array( 
            'assets/css/developer.css',
        ); 
        $data['includes_for_layout_css'] = add_includes('css', $includeCss);
 
        $data['meta_title'] = "Reset Password";
        $data['meta_keyword'] = "Peach";
        $data['meta_description'] = "Peach"; 
		
        $this->setData($data);
		
 
    }
	


}
