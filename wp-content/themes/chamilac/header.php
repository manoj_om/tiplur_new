<?php

/**

 * The template for displaying the header

 *

 * Displays all of the head element and everything up until the "site-content" div.

 *

 * @package WordPress

 * @subpackage Twenty_Sixteen

 * @since Twenty Sixteen 1.0

 */



?><!DOCTYPE html>

<html <?php language_attributes(); ?> class="no-js">

<head>
    <meta name="google-site-verification" content="g7WWubPtWoXWRsireLKl_cuiumdfjXfupNEYZWZihvk" />
	<meta charset="<?php bloginfo( 'charset' ); ?>">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>

	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php endif; ?>





    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900" rel="stylesheet">



    <link href="<?php bloginfo('template_directory');?>/assets/css/bootstrap.min.css" rel="stylesheet">



    <link href="<?php bloginfo('template_directory');?>/assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">



    <link href="<?php bloginfo('template_directory');?>/assets/vendors/linearicons/css/linearicons.css" rel="stylesheet">



    <link href="<?php bloginfo('template_directory');?>/assets/fonts/flaticon/flaticon.css" rel="stylesheet">



    <link href="<?php bloginfo('template_directory');?>/assets/vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">

    <link href="<?php bloginfo('template_directory');?>/assets/vendors/owl-carousel/owl.theme.min.css" rel="stylesheet">



    <link href="<?php bloginfo('template_directory');?>/assets/vendors/magnific-popup/css/magnific-popup.css" rel="stylesheet">



    <link href="<?php bloginfo('template_directory');?>/assets/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.css" rel="stylesheet">



    <link href="<?php bloginfo('template_directory');?>/assets/vendors/vegas/vegas.min.css" rel="stylesheet">



    <link href="<?php bloginfo('template_directory');?>/assets/vendors/YTPlayer/css/jquery.mb.YTPlayer.min.css" rel="stylesheet">



    <link href="<?php bloginfo('template_directory');?>/assets/css/base.css" rel="stylesheet">

    <link href="<?php bloginfo('template_directory');?>/assets/css/style.css" rel="stylesheet">
    <link href="<?php bloginfo('template_directory');?>/assets/css/responsive.css" rel="stylesheet">


	<?php wp_head(); ?>

  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-170892151-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-170892151-1');
</script>

</head>



<body id="body" class="wide-layout preloader-active">



    <div id="preloader" class="preloader"></div>



    <div id="pageWrapper" class="page-wrapper">



		<header id="mainHeader" class="main-header ">

			<div class="header-content">

                <div class="container">

                    <div class="row">

                        <div class="col-md-3  col-xs-12 t-center t-md-left header-logo-sec">
                            <a class="header-brand ptb-10" href="<?php echo home_url();?>">
                                <img src="<?php bloginfo('template_directory');?>/images/logo.png"  alt="">
                            </a>
                        </div>
						<div class="col-md-9 pt-30 ">
                            <div class="header-menu header-menu-1">
                                <nav class="nav-bar bg-blue-dark navbar">
                                    <div class="">
                                        <div class="navbar-header">
                                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#header-navbar" aria-expanded="false">
                                                <span class="sr-only">Toggle navigation</span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>
                                            <!-- <a class="navbar-brand" href="#">Brand</a> -->
                                        </div>
                                        <div id="header-navbar" class="sticky-nav-tab collapse navbar-collapse"><?php echo wp_nav_menu( array( 'menu_class'      => 'nav-menu' ) ); ?></div>
                                    </div>
                                </nav>
                            </div>
						</div>
                    </div>
                </div>
            </div>
            <!-- end: Header Content -->
        </header>



		<?php

		 $key_1_value = get_post_meta( get_the_ID(), 'banner', true );

		 $key_1_value2 = get_post_meta( get_the_ID(), 'banner-content', true );

		  if(!is_home() && ($key_1_value)){ ?>

			 <div class="img-banner top-banner-info-sec" id = "banner">

				<div class="banner-info-sec container">

					<div class="info-content">
						<?php if(get_the_ID()!=862 && get_the_ID()!=887 && get_the_ID()!=889){ ?>
								<span class="top-banner-title"><?php the_title();?></span>
						<?php  } ?>


						<div class="top-banner-info"><?php echo $key_1_value2 = get_post_meta( get_the_ID(), 'banner-content', true ); ?></div>

					</div>

				</div>

			</div>

		<?php }

		?>


		<script>

				if(window.innerWidth>768){

				var key_1_value = "<?php echo get_post_meta( get_the_ID(), 'banner', true ); ?>";

			}else{

				var key_1_value = "<?php echo get_post_meta( get_the_ID(), 'mobile-banner', true ); ?>";

			}

			jQuery(function(){
			jQuery(window).resize(function(){
				if(window.innerWidth>768){
					var key_1_value = "<?php echo get_post_meta( get_the_ID(), 'banner', true ); ?>";
				}else{
					var key_1_value = "<?php echo get_post_meta( get_the_ID(), 'mobile-banner', true ); ?>";
				}
				jQuery('#banner').css({'background-image':'url(' + key_1_value + ')'});
			});





						jQuery('#banner').css({'background-image':'url(' + key_1_value + ')'});


			})

		</script>
