<?php

class Users extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('orders/orders_model');
        $this->load->library('auth');
        $this->load->library('form_validation');
        $this->set_data = array('theme' => 'admin'); //define theme name
    }

    function filter() {
        return $this->input->get(array('name', 'contact', 'email', 'role', 'status','state_id','type'));
    }

    function index() {

        if (!$this->auth->_is_logged_in()) {
            redirect(base_url());
        }
        $includeJs = array('/assets/js/users.js','/theme/admin/js/moment.js','/theme/admin/js/bootstrap-datetimepicker.js');
        //for adding js in footer
        $data['js'] = $includeJs;

        if (!empty($_GET['multi_activate'])) {
            $this->users_status('activate');
        }

        if (!empty($_GET['multi_deactivate'])) {
            $this->users_status('de-activate');
        }

        $this->load->library('pagination');
        $per_page = isset($_GET['per_page']) ? $_GET['per_page'] : false;
        unset($_GET['per_page']);
        $query_string = http_build_query($_GET);
        $site_url = !$query_string ? base_url(SITE_AREA . '/users') : base_url(SITE_AREA . '/users?' . $query_string);
        if ($per_page) {
            $_GET['per_page'] = $per_page;
        }
        $num_rows = $this->user_model->users_details($this->filter(), false, false, true, '1');
        $config = pagination_formatting();
        $limit = 50;
        $config['base_url'] = $site_url;
        $config['total_rows'] = $num_rows;
        $config['per_page'] = $limit;
        $config['num_links'] = 5;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $this->pagination->initialize($config);
        $offset = isset($_GET['per_page']) ? ($_GET['per_page'] - 1) * $limit : null;
        $records = $getting = $this->user_model->users_details($this->filter(), $limit, $offset, false, '1');
        $data['result'] = $records;
        //print_r($records);die;
        $this->theme($data);
    }


    function pending_request() {

        if (!$this->auth->_is_logged_in()) {
            redirect(base_url());
        }
        $includeJs = array('/assets/js/users.js','/theme/admin/js/moment.js','/theme/admin/js/bootstrap-datetimepicker.js');
        //for adding js in footer
        $data['js'] = $includeJs;

        if (!empty($_GET['multi_activate'])) {
            $this->users_request_status('approve');
        }

        if (!empty($_GET['multi_deactivate'])) {
            $this->users_request_status('de-activate');
        }

        $this->load->library('pagination');
        $per_page = isset($_GET['per_page']) ? $_GET['per_page'] : false;
        unset($_GET['per_page']);
        $query_string = http_build_query($_GET);
        $site_url = !$query_string ? base_url(SITE_AREA . '/users') : base_url(SITE_AREA . '/users?' . $query_string);
        if ($per_page) {
            $_GET['per_page'] = $per_page;
        }
        $num_rows = $this->user_model->users_pending_details($this->filter(), false, false, true, '1');
        $config = pagination_formatting();
        $limit = 50;
        $config['base_url'] = $site_url;
        $config['total_rows'] = $num_rows;
        $config['per_page'] = $limit;
        $config['num_links'] = 5;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $this->pagination->initialize($config);
        $offset = isset($_GET['per_page']) ? ($_GET['per_page'] - 1) * $limit : null;
        $records = $getting = $this->user_model->users_pending_details($this->filter(), $limit, $offset, false, '1');
        $data['result'] = $records;
        //print_r($records);die;
        $this->theme($data);
    }


    // Customers List
    function customers() {

        if (!$this->auth->_is_logged_in()) {
            redirect(base_url());
        }


        $includeJs = array('/assets/js/users.js');
        //for adding js in footer
        $data['js'] = $includeJs;

        if (!empty($_GET['multi_activate'])) {

            $this->users_status('activate', 'customer');
        }

        if (!empty($_GET['multi_deactivate'])) {
            $this->users_status('de-activate', 'customer');
        }

        $this->load->library('pagination');
        $per_page = isset($_GET['per_page']) ? $_GET['per_page'] : false;
        unset($_GET['per_page']);
        $query_string = http_build_query($_GET);
        $site_url = !$query_string ? base_url(SITE_AREA . '/users/customers') : base_url(SITE_AREA . '/users/customers?' . $query_string);
        if ($per_page) {
            $_GET['per_page'] = $per_page;
        }
        $num_rows = $this->user_model->users_details($this->filter(), false, false, true, '2');
        $config = pagination_formatting();
        $limit = 100;
        $config['base_url'] = $site_url;
        $config['total_rows'] = $num_rows;
        $config['per_page'] = $limit;
        $config['num_links'] = 5;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $this->pagination->initialize($config);
        $offset = isset($_GET['per_page']) ? ($_GET['per_page'] - 1) * $limit : null;
        $records = $getting = $this->user_model->users_details($this->filter(), $limit, $offset, false, '2');
        $data['result'] = $records;
        $data['view'] = 'index';
        $this->theme($data);
    }

    function reject_doc() {

        if (empty($_POST)) {
            show_404();
        }

        $user_id = $this->input->post('user_id');

        $data = array('status' => '8');

        $result = true;
        $updated = $this->user_model->update_users_details($data, $user_id);
        //echo $updated;exit;
        if ($updated == false) {
            $result = false;
        }

        if ($result) {

            $notify_data = array(
                'order_id' => 'Document Rejected',
                'notified_to' => $user_id,
                'requested_by' => $this->session->userdata('id'),
                'type' => DOCUMENT_REJECTED,
                'title' => 'Document Rejected',
                'message' => 'Your Document is rejected by admin',
                'reason' => 'Invalid Document',
            );

            $notify = new Push_notification();
            $notify->send_notification($notify_data);

            $email = user_data($user_id)->email;

            $this->send_welcome_mail($email); //send mail

            set_flash_message($type = 'success', $message = 'Document rejected successfully.');
            redirect('admin/users/customers');
        } else {
            set_flash_message($type = 'error', $message = 'something wrong.');
            redirect('admin/users/customers');
        }
    }

    function users_status($status, $role_type = 'retailer') {

        $checked = $this->input->get('users_id');
        if (is_array($checked) && count($checked)) {
            // If any of the deletions fail, set the result to false, so
            // failure message is set if any of the attempts fail, not just
            // the last attempt
            $result = true;
            foreach ($checked as $pid) {

                if ($status == 'activate') {
                    $data = array('status' => '1');
                } else {
                    $data = array('status' => '0');

                    $email = user_data($pid)->email;

                    // $this->user_inactive_mail($email); //send mail
                }

                $updated = $this->user_model->update_users_details($data, $pid);
                //echo $updated;exit;
                if ($updated == false) {
                    $result = false;
                }
            }
            if ($result) {

                set_flash_message($type = 'success', $message = count($checked) . ' row updated successfully.');
                ($role_type == 'customer') ? redirect('admin/users/customers') : redirect('admin/users');
            } else {
                set_flash_message($type = 'error', $message = 'something wrong.');
                ($role_type == 'customer') ? redirect('admin/users/customers') : redirect('admin/users');
            }
        } else {
            set_flash_message($type = 'error', $message = 'Please select users.');
            ($role_type == 'customer') ? redirect('admin/users/customers') : redirect('admin/users');
        }
    }


    function users_request_status($status, $role_type = 'retailer') {

        $checked = $this->input->get('users_id');
        if (is_array($checked) && count($checked)) {
            // If any of the deletions fail, set the result to false, so
            // failure message is set if any of the attempts fail, not just
            // the last attempt
            $result = true;
            foreach ($checked as $pid) {

                if ($status == 'approve') {
                    $data = array('status' => '1');
                } else {
                    $data = array('status' => '4');

                    $email = user_data($pid)->email;

                    // $this->user_inactive_mail($email); //send mail
                }

                $updated = $this->user_model->update_users_details($data, $pid);
                //echo $updated;exit;
                if ($updated == false) {
                    $result = false;
                }
            }
            if ($result) {

                set_flash_message($type = 'success', $message = count($checked) . ' row updated successfully.');
                ($role_type == 'retailer') ? redirect('admin/users/pending_request') : redirect('admin/users');
            } else {
                set_flash_message($type = 'error', $message = 'something wrong.');
                ($role_type == 'retailer') ? redirect('admin/users/pending_request') : redirect('admin/users');
            }
        } else {
            set_flash_message($type = 'error', $message = 'Please select users.');
            ($role_type == 'retailer') ? redirect('admin/users/pending_request') : redirect('admin/users');
        }
    }

    public function send_welcome_mail($email) {

        $subject = "Tiplur | Valid Document Request";
        $msg = "<table style='margin: 0 auto; background: #f5f5f5;color: #444;font-size: 14px;border: 1px solid #ffde16;border-collapse: collapse;' width='600'>
				     <tr><!-- Header -->
				      <td style='margin:10px auto;background:#fff'>
				       <img style='width:70px;margin:0 auto;display:block' src='http://tiplur.in/theme/site/img/assets/logo.png'>
				      </td>
				     </tr>
				     <tr><!-- Body -->
				      <td>
				       <table style='width:100%;border-collapse: collapse;'>
				        <tr>
				         <td style='font-size:16px;color:#000;font-weight:600;padding:8px;text-align:left'>
				          Dear Patron,
				         </td>
				        </tr>
				        <tr>
				          <td style='padding:8px;text-align:left;margin-bottom:10px'>
				           <p>
								On the onset let me thank you for downloading tiplur on your phone.
								We at tiplur ensure that all our users, are above the age as mandated by the respective state in which they reside.
								We have observed that you have not uploaded a valid govt issued photo ID, this is required at our end to validate the age credentials, adhering to the state laws.
							</p>
				            <p style='margin-bottom:10px;'>
				          		Would request you to kindly go on your profile, where we have an option for you to upload a valid ID, as to activate your account.
				            </p>
				            <p style='margin-bottom:5px;'>
				           		In case this has caused any inconvenience to you, we are extremely sorry for that, but you would agree, that we don't want any user below the age mandated by state, accessing the platform.
				            </p>
				             <p>Sincerely yours,<br>Rakshat Chopra</p>
				            </td>
				        </tr>
				       </table>
				        </td>
				      </tr>
				      <tr><!-- Footer -->
				       <td style='text-align:center;font-size:10px;background:#ffde16;color:#000;padding: 5px;'>&#x24B8; Tiplur 2018</td>
				      </tr>
     			 </table>";
         smtp_mail($email, $msg, $subject);

    }

    public function user_inactive_mail($email) {

        $subject = "Tiplur | Invalid/Incomplete Profile Details";
        $msg = '<table style="margin: 0 auto; background: #f5f5f5;color: #444;font-size: 14px;border: 1px solid #ffde16;border-collapse: collapse;" width="600">
     <tr><!-- Header -->
      <td style="margin:10px auto;background:#fff">
       <img style="width:70px;margin:0 auto;display:block" src="http://tiplur.in/theme/site/img/assets/logo.png">
      </td>
     </tr>
     <tr><!-- Body -->
      <td>
       <table style="width:100%;border-collapse: collapse;">
        <tr>
         <td style="font-size:16px;color:#000;font-weight:600;padding:8px;text-align:left">
          Dear Patron,
         </td>
        </tr>
        <tr>
          <td style="padding:8px;text-align:left;margin-bottom:10px">
           <p>We at tiplur ensure that all our users, have a safe, simple and smart experience while exploring the platform. We have observed that the personal details as entered for registration are incomplete/ incorrect. Would request you to kindly go to your profile on the application, and update the same.</p>
           <p style="margin-bottom:10px;">In case this has caused any inconvenience to you, we are extremely sorry for that, but you would agree, non-availability of correct and proper address, phone number and email Id may cause last minute inconvenience to you when you place an order with merchant, since the merchant needs to have correct coordinates to make delivery.</p>
           <p>Sincerely yours,<br>Rakshat Chopra</p>
            </td>
        </tr>
       </table>
        </td>
      </tr>
      <tr><!-- Footer -->
       <td style="text-align:center;font-size:10px;background:#ffde16;color:#000;padding: 5px;">&#x24B8; Tiplur 2018</td>
      </tr>
      </table>';
         smtp_mail($email, $msg, $subject);

    }

    function create() {
        if (!$this->auth->_is_logged_in()) {
            redirect(base_url());
        }
        $includeJs = array('/assets/js/users.js');
        //for adding js in footer
        $data['js'] = $includeJs;
        if (isset($_POST['save'])) {
            $this->form_validation->set_rules($this->user_model->form_validation());
            if ($this->form_validation->run() === false) {
                set_flash_message($type = 'error', $message = 'Please enter user details.');
            } else if ($this->user_model->users_detail_by_id('', trim($_POST['email']))) {
                set_flash_message($type = 'error', $message = 'User already exist.');
            } else {
                if ($this->save_users('insert')) {
                    set_flash_message($type = 'success', $message = 'Add successfully.');
                    redirect('admin/users');
                }
            }
        }
        $data['temp'] = '';
        $this->theme($data);
    }

    /*  script for delete user */

    function user_delete() {
        $id = $this->input->post('user_id');
        $res = $this->db->where('id', $id)->update('users', array('deleted' => '1'));
        $data['msg'] = set_flash_message($type = 'success', $message = 'Deleted successfully.');
        $data['status'] = ($res) ? true : false;
        echo json_encode($data);
    }

    /*  script for update users details */

    function edit() {
        $id = $this->uri->segment(4);
        if (!isset($id)) {
            $id = current_user()['id'];
        }
        //prd(current_user()[id]);
        $data['result'] = $this->user_model->users_detail_by_id($id);
        //prd($data['result']);
        if (!$data['result']) {
            show_404();
        }

        $includeJs = array('/assets/js/users.js');
        //for adding js in footer
        $data['js'] = $includeJs;
        if (isset($_POST['save'])) {
            //prd($_POST);
            $this->form_validation->set_rules($this->user_model->form_validation());
            if ($this->form_validation->run() === false) {
                set_flash_message($type = 'error', $message = 'Please enter user details.');
            } else {
                if ($this->save_users('update', $id)) {
                    set_flash_message($type = 'success', $message = 'Updated successfully.');
                    if (empty($this->uri->segment(4))) {
                        redirect('admin/users/edit');
                    } else {
                        redirect('admin/users');
                    }
                }
            }
        }

        $this->theme($data);
    }

     /*******************************update payment mode*************************** */
     /*  script for update users details */

    function payment_mode() {
        $id = current_user()['id'];
       $data['store_detail']= store_detail_by_retailer_id($id);
       if (isset($_POST['save'])) {
        //prd($_POST);
        // $this->form_validation->set_rules($this->user_model->form_validation());
        // if ($this->form_validation->run() === false) {
        //     set_flash_message($type = 'error', $message = 'Please enter user details.');
        // } else {
            if ($this->save_payment_mode('update', $id)) {
                set_flash_message($type = 'success', $message = 'Updated successfully.');

                    redirect('admin/users/payment_mode');

            }
      //  }
    }
        $this->theme($data);
    }
    function save_payment_mode() {

        $data = $this->input->post();
       // prd($data);
    if($data['check']=='online')
        {
           $data['payment_mode']=9;
        }
        else if($data['check']=='offline')
        {
            $data['payment_mode']=implode(',',$data['ad_pack_id2']);
        }
       // unset($data['check'],$data['ad_pack_id1']);
    //   prd($data);
        $data['estimated_time']=($data['estimated_day']=='0')?$data['estimated_time']:'';
		$updata = array('payment_mode'=>$data['payment_mode'],'estimated_day'=>$data['estimated_day'],'estimated_time'=>($data['estimated_time'])?$data['estimated_time']:'');
		$this->db->where('id',$data['store_id']);
		$result = $this->db->update('store',$updata);
        return $result;
    }
     /********************************************************************************* */


    /*  script for save and update  categorie */

    private function save_users($type = false, $id = false) {

        $data = $this->input->post(array('display_name', 'email', 'mobile', 'email', 'address', 'role_id'));
        $data['status'] = ($_POST['status'] == 2) ? '0' : $_POST['status'];
        (isset($_POST['password']) && $_POST['password']) ? $data['password'] = md5($_POST['password']) : '';
        $data['status'] = (isset($_POST['status']) && $_POST['status'] == 1) ? $_POST['status'] : '0';
        $data['created_on'] = $data['modified_on'] = db_date_time();
        $data['image'] = $this->upload_image();

        if ($type == 'insert') {
            $id = $this->user_model->add_users($data);
            if ($id) {
                $return = $id;
            }
        } elseif ($type == 'update') {
            unset($data['created_on']);
            unset($data['email']);
            $return = $this->user_model->update_users_details($data, $id);
        }
        return $return;
    }

    private function upload_image() {
        $this->load->library('upload');
        $upload_path = FCPATH . "assets/images/profile_image/";
        $h_file = $this->input->post('h_image');
        $input_field_name = 'image';
        $config = array(
            'upload_path' => $upload_path,
            'allowed_types' => 'gif|jpg|png|jpeg',
            'file_name' => time() . $_FILES[$input_field_name]['name'],
        );
        if (is_uploaded_file($_FILES[$input_field_name]['tmp_name'])) {
            $this->upload->initialize($config);
            if (!$this->upload->do_upload($input_field_name)) {
                $this->form_validation->set_message($input_field_name, $this->upload->display_errors());
                return false;
            } else {
                if ($h_file and file_exists($upload_path . '/' . $h_file)) {
                    unlink($upload_path . '/' . $h_file);
                }
                $uploaded = $this->upload->data();
                return $uploaded['file_name'];
            }
        } else {
            return $h_file;
        }
    }

    function view() {
        if (!$this->auth->_is_logged_in()) {
            redirect(base_url());
        }

        //add js
        $includeJs = array('');


        $data['temp'] = '';
        $data['js'] = $includeJs;

        $this->theme($data);
    }

    function resetpassword() {
        if (!$this->auth->_is_logged_in()) {
            redirect(base_url());
        }

        if (isset($_POST) and $this->user_model->get_resetpassword_validation_rules()) {
            $result = $this->user_model->ResetPassword();
            if ($result == 1) {
                $this->session->set_flashdata('message', 'Your password has been changed successfully');
                redirect(base_url('admin/users/resetpassword'));
            }
        }


        $data['temp'] = '';
        $this->theme($data);
    }

    function dashboard() {
        if (!$this->auth->_is_logged_in()) {
            redirect(base_url());
        }

        $limit = 5;

        $data['recent_order_count'] = $this->orders_model->orders_listing(array('status' => '7'), false, false, true);

        $data['total_order_count'] = $this->orders_model->orders_listing(false, false, false, true);

        $data['recent_orders'] = $this->orders_model->orders_listing(false, $limit, false, false);
        //prd($data['recent_orders']);

        $data['product_count'] = $this->user_model->product_active_count();
        $data['customers_count'] = $this->user_model->users_details(false, false, false, true, '2');
        $data['retailers_count'] = $this->user_model->users_details(false, false, false, true, '1'); //$this->user_model->customers_count();
        // Logic for order process
        $data['process_order_count'] = ''; //$this->user_model->process_order_count('1');
        $data['process_order_list'] = $this->orders_model->orders_listing(array('status' => '7'), $limit, false, false);
       // prd($data);
        $this->theme($data);
    }

    function user_address() {

        $user_id = $this->input->get('id');
        $user_type = $this->input->get('type');
        $addresses = $this->user_model->users_address_detail($user_id, true);

        $store_detail = array();

        if ($user_type == 'retailer') {
            $store_detail = store_detail_by_retailer_id($user_id);
        }

        $ajax_data['html'] = $this->load->view('users/admin/user-address', array('addresses' => $addresses, 'store_detail' => $store_detail), true);
        echo json_encode($ajax_data);
    }

    public function export_users() {

        $filter = isset($_GET['filter']) ? $this->filter() : false;

        if ($_GET['type'] == 'customer') {
            $reports = $this->user_model->users_details($filter, false, false, false, '2');
        }elseif($_GET['type'] == 'mapped')
		{
			$reports = $this->user_model->mapped_retailer_details($filter, false, false, false);
		} else {
            $reports = $this->user_model->users_details($filter, false, false, false, '1');
        }

        //echo "<pre>"; print_r($reports);die;

        /* Start Logic for excel */
        $exceldata = array();
        if($_GET['type'] == 'customer'){
        	$table = '<table><tr><th>S.No</th><th>ID</th><th>Name</th><th>Phone Number</th><th>Email</th><th>Role</th>';
        	$table.= ($_GET['type'] == 'customer') ? '<th>DOB</th>' : '';
        	$table.= '<th>Document</th><th>Address</th><th>State</th><th>Registered On</th><th>Status</th></tr>';
        	$i = 1;
        	foreach ($reports as $record) {

        		$user_addr = '';
        		$addresses = $this->user_model->users_address_detail($record->id, true);
        		if ($addresses) {
        			foreach ($addresses as $address) {
        				$user_addr = ($address->default == 1) ? $address->address . "," . $address->city . "," . $address->state . "," . $address->country . "," . $address->pincode : '';
        			}
        		}

        		$table .= '<tr>';
        		$table .='<td>' . $i . '</td>';
        		$table .= '<td>' . $record->id . '</td>';
        		$table .= '<td>' . $record->display_name . '</td>';
        		$table .= '<td>' . $record->mobile . '</td>';
        		$table .= '<td>' . $record->email . '</td>';
        		$table .= '<td>' . ucfirst($record->role) . '</td>';
        		$table.= ($_GET['type'] == 'customer') ? '<td>' . site_date($record->dob) . '</td>' : '';
        		$table .= '<td>' . base_url() . 'assets/uploads/document_file/' . $record->doc_file. '</td>';
        		$table .= '<td>' . $user_addr . '</td>';
        		$table .= '<td>' . $record->state . '</td>';
        		$table .= '<td>' . site_date($record->created_on) . '</td>';
        		$table .= '<td>' . get_status($record->status) . '</td>';
        		$table .= '</tr>';
        		$i++;

        	}
        }else{
        	$table = '<table><tr><th>S.No</th><th>ID</th><th>Name</th><th>Phone Number</th><th>Email</th><th>Role</th>';
        	$table.= ($_GET['type'] == 'customer') ? '<th>DOB</th>' : '';
        	$table.= '<th>Document</th><th>licence</th><th>contact_person</th><th>contact_person_mobile</th><th>min_order</th><th>opening_time</th><th>closing_time</th><th>payment_mode</th><th>Address</th><th>State</th><th>Registered On</th><th>Status</th></tr>';
        	$i = 1;
        	foreach ($reports as $record) {
        		//print_r($reports);

        		$user_addr = '';
        		$addresses = $this->user_model->users_address_detail($record->id, true);
        		if ($addresses) {
        			foreach ($addresses as $address) {
        				$user_addr = ($address->default == 1) ? $address->address . "," . $address->city . "," . $address->state . "," . $address->country . "," . $address->pincode : '';
        			}
        		}

        		$store = $this->user_model->users_store($record->id);
        		//prd($store);
        		$pm = explode(",",$store[0]->payment_mode);

        		$pm_str = '';
        		foreach ($pm as $val){
        			if($val=='4'){
        				$pm_str .= 'BY CASH, ';
        			}elseif ($val=='5'){
        				$pm_str .= 'BY CARD, ';
        			}elseif ($val=='6'){
        				$pm_str .= 'BY PAYTM, ';
        			}elseif ($val=='7'){
        				$pm_str .= 'CARD MASTER, ';
        			}elseif ($val=='8'){
        				$pm_str .= 'CARD AMEX, ';
        			}
        		}

        		$table .= '<tr>';
        		$table .='<td>' . $i . '</td>';
        		$table .= '<td>' . $record->id . '</td>';
        		$table .= '<td>' . $record->display_name . '</td>';
        		$table .= '<td>' . $record->mobile . '</td>';
        		$table .= '<td>' . $record->email . '</td>';
        		$table .= '<td>' . ucfirst($record->role) . '</td>';
        		$table.= ($_GET['type'] == 'customer') ? '<td>' . site_date($record->dob) . '</td>' : '';
        		$table .= '<td>' . base_url() . 'assets/uploads/document_file/' . $record->doc_file. '</td>';
        		$table .= '<td>' . $store[0]->licence. '</td>';
        		$table .= '<td>' . $store[0]->contact_person. '</td>';
        		$table .= '<td>' . $store[0]->contact_person_mobile. '</td>';

        		$table .= '<td>' . $store[0]->min_order. '</td>';
        		$table .= '<td>' . $store[0]->opening_time. '</td>';
        		$table .= '<td>' . $store[0]->closing_time. '</td>';
        		$table .= '<td>' . $pm_str. '</td>';
        		$table .= '<td>' . $user_addr . '</td>';


        		$table .= '<td>' . $record->state . '</td>';
        		$table .= '<td>' . site_date($record->created_on) . '</td>';
        		if($record->status == 9 || $record->status == 10)
				{
				   $table .=($record->status == 9)?'<td> Active</td>':'<td>Inactive</td>';
				}else
				{
        		   $table .='<td>' .get_status($record->status) . '</td>';
				}
        		$table .= '</tr>';
        		$i++;

        	}
        }



        $table .= '</table>';
        $date = date('d-m-Y');

        header("Content-Type: application/vnd.ms-excel");
        header("Content-disposition: attachment; filename=Tiplur User Details(" . $date . ").xls");
        echo $table;
        die();
        /* end Logic for excel */
    }

   /*update retailer store timings*/
   public function update_retailer_store_time()
	{
		$data = $this->input->post();
    if($data['check']=='online')
        {
           $data['payment_mode']=9;
        }
        else if($data['check']=='offline')
        {
            $data['payment_mode']=implode(',',$data['ad_pack_id1']);
        }
       // unset($data['check'],$data['ad_pack_id1']);
       // prd($data);
		$updata = array('opening_time'=>$data['opening_time'],'closing_time'=>$data['closing_time'],'payment_mode'=>$data['payment_mode']);
		$this->db->where('id',$data['store_id']);
		$result = $this->db->update('store',$updata);
		if($result)
		{
			$addresses = $this->user_model->users_address_detail($data['retailer_id'], true);
            $store_detail = store_detail_by_retailer_id($data['retailer_id']);
             $ajax_data['html'] = $this->load->view('users/admin/user-address', array('addresses' => $addresses, 'store_detail' => $store_detail), true);
			 $ajax_data['status'] = true;
        }else{ $ajax_data['status'] = false; }
		echo json_encode($ajax_data);
	}

  /*update_customer_rotate_iamge*/
   public function update_customer_rotate_image()
   { //pr($_SERVER['DOCUMENT_ROOT']);

       $data = $this->input->post();

       // File and rotation
       $imageName= basename(strtok($data['image_src'], '?'));
            $rotateFilename = $_SERVER['DOCUMENT_ROOT'].'/admin/assets/uploads/document_file/'.$imageName; // PATH
           // pr($rotateFilename);
            $degrees = $data['rotation'];
            if($degrees == -90 || $degrees == 270){
                $degrees = 90;
            }elseif($degrees == -180 || $degrees == 180){
                $degrees = 180;
            }elseif($degrees == -270 || $degrees == 90){
                $degrees = 270;
            }

            $fileType = strtolower(substr($imageName, strrpos($imageName, '.') + 1));
            $source = imagecreatefromjpeg($rotateFilename);
            $rotate = imagerotate($source, $degrees, 0);
       //pr($fileType); var_dump($source); prd($rotate);
            if($fileType == 'png'){
            header('Content-type: image/png');
            $source = imagecreatefrompng($rotateFilename);
            $bgColor = imagecolorallocatealpha($source, 255, 255, 255, 127);
            // Rotate
            $rotate = imagerotate($source, $degrees, $bgColor);
            imagesavealpha($rotate, true);
            imagepng($rotate,$rotateFilename);
            $ajax_data['status'] = true;

            }

            if($fileType == 'jpg' || $fileType == 'jpeg'){
            header('Content-type: image/jpeg');
            $source = imagecreatefromjpeg($rotateFilename);
            // Rotate
            $rotate = imagerotate($source, $degrees, 0);
            imagejpeg($rotate,$rotateFilename);
             $ajax_data['status'] = true;
            // $ajax_data['source'] = $source;
            // $ajax_data['rotate'] = $rotate;
            }

            // Free the memory
            imagedestroy($source);
            imagedestroy($rotate);
           echo json_encode($ajax_data);
   }

	function mapped_retailer() {

        if (!$this->auth->_is_logged_in()) {
            redirect(base_url());
        }

		/* activate and deactivate */
		if (!empty($_GET['multi_activate'])) {
            $this->mapped_users_status('activate');
        }

        if (!empty($_GET['multi_deactivate'])) {
            $this->mapped_users_status('de-activate');
        }


        $includeJs = array('/assets/js/users.js','/theme/admin/js/moment.js','/theme/admin/js/bootstrap-datetimepicker.js');
        //for adding js in footer
        $data['js'] = $includeJs;
        $this->load->library('pagination');
        $per_page = isset($_GET['per_page']) ? $_GET['per_page'] : false;
        unset($_GET['per_page']);
        $query_string = http_build_query($_GET);
        $site_url = !$query_string ? base_url(SITE_AREA . '/users/mapped_retailer') : base_url(SITE_AREA . '/users/mapped_retailer?' . $query_string);
        if ($per_page) {
            $_GET['per_page'] = $per_page;
        }
        $num_rows = $this->user_model->mapped_retailer_details($this->filter(), false, false, true);
        $config = pagination_formatting();
        $limit = 100;
        $config['base_url'] = $site_url;
        $config['total_rows'] = $num_rows;
        $config['per_page'] = $limit;
        $config['num_links'] = 5;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $this->pagination->initialize($config);
        $offset = isset($_GET['per_page']) ? ($_GET['per_page'] - 1) * $limit : null;
        $records = $getting = $this->user_model->mapped_retailer_details($this->filter(), $limit, $offset, false);
        $data['result'] = $records;
		$this->theme($data);
    }


	function mapped_users_status($status) {

        $checked = $this->input->get('users_id');
        if (is_array($checked) && count($checked)) {
            $result = true;
            foreach ($checked as $pid) {

                if ($status == 'activate') {
                    $data = array('status' => '9');
                } else {
                    $data = array('status' => '10');
                }

                $updated = $this->user_model->update_users_details($data, $pid);
                //echo $updated;exit;
                if ($updated == false) {
                    $result = false;
                }
            }
            if ($result) {

                set_flash_message($type = 'success', $message = count($checked) . ' row updated successfully.');
                redirect('admin/users/mapped_retailer');
            } else {
                set_flash_message($type = 'error', $message = 'something wrong.');
                redirect('admin/users/mapped_retailer');
            }
        } else {
            set_flash_message($type = 'error', $message = 'Please select users.');
            redirect('admin/users/mapped_retailer');
        }
    }


	function retailer_shops() {

        if (!$this->auth->_is_logged_in()) {
            redirect(base_url());
        }

        if (!empty($_GET['multi_activate'])) {
            $this->changeStoreStatus('activate');
        }

        if (!empty($_GET['multi_deactivate'])) {
            $this->changeStoreStatus('de-activate');
        }

		if (!empty($_GET['multi_closed'])) {
            $this->changeStoreStatus('closed');
        }

        $this->load->library('pagination');
        $per_page = isset($_GET['per_page']) ? $_GET['per_page'] : false;
        unset($_GET['per_page']);
        $query_string = http_build_query($_GET);
        $site_url = !$query_string ? base_url(SITE_AREA . '/users/retailer-shops') : base_url(SITE_AREA . '/users/retailer-shops?' . $query_string);
        if ($per_page) {
            $_GET['per_page'] = $per_page;
        }
        $num_rows = $this->user_model->retailerStoreDetails($this->filter(), false, false, true);
        $config = pagination_formatting();
        $limit = 25;
        $config['base_url'] = $site_url;
        $config['total_rows'] = $num_rows;
        $config['per_page'] = $limit;
        $config['num_links'] = 5;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $this->pagination->initialize($config);
        $offset = isset($_GET['per_page']) ? ($_GET['per_page'] - 1) * $limit : null;
        $records = $getting = $this->user_model->retailerStoreDetails($this->filter(), $limit, $offset, false);
        $data['result'] = $records;
        $this->theme($data);
    }

	function changeStoreStatus($status)
	{
        $checked = $this->input->get('store_id'); pr($checked);
        if (is_array($checked) && count($checked))
		{
            $result = true;
            foreach ($checked as $sid)
			{

				$data['status'] = ($status == 'activate') ?'1':($status == 'de-activate' ? '2':'3');
				$updated = $this->user_model->updateStoreStatus($data, $sid);

                if ($updated == false) {
                    $result = false;
                }
            }
            if ($result)
			{
                set_flash_message($type = 'success', $message = count($checked) . ' row updated successfully.');
                redirect('admin/users/retailer-shops');
            } else {
                set_flash_message($type = 'error', $message = 'something wrong.');
                redirect('admin/users/retailer-shops');
            }
        } else {
            set_flash_message($type = 'error', $message = 'Please select store.');
            redirect('admin/users/retailer-shops');
        }
    }


    /*public function send_holi_mail()
    {

     $all_users = $this->db->select('id,email')->where('email !=','')->get('users')->result_array();
     $emailids = array_column($all_users,'email');
     //prd($emailids);
     foreach($emailids as $res):
                // $msg = "<img style='margin:auto; display:block;' width='50%' src=".base_url('assets/images/ipl.png').">";
		//$res = array('gautam.akriti@orangemantra.in');

		$msg= 'Dear Patron,';
		$msg.= '<p>The technical issue has been resolved, and the app is live and active to use.</p>';
		$msg.= '<p>Keep Exploring..</p>';
		$msg.= '<p>Team Tiplur</p>';
                $msg.= "<img style='display:block;' width='10%' src=".base_url('assets/images/tiplur.png').">";

               $subject = 'Live & Active';
		smtp_mail($res,$msg,$subject);
     endforeach;

    }*/




}
