/*Manage Nofications */

//window.setInterval(event, 2000);
function event() {
    jQuery.ajax({
        url: base_url + "/admin/notification/get_notification",
        success: function (data) {
            //console.log(data);
            $resl = jQuery.parseJSON(data);

            $(".notification-outer").html($resl.html);

            if ($resl.badge_count > 0) {
                $("span.label-primary").text($resl.badge_count);
                $("span.label-primary").show();
            }

        }
    });
}

function update_notification() {

    $count = $(".label-primary").html();
    if ($count) {
        jQuery.ajax({
            url: base_url + "admin/notification/update_notification",
            success: function (data) {
                $(".label-primary").hide(); //red count
                //$(".label-primary").show();//whats new count count
            }
        });
    }
}

/* Nofication Close Buttons */
$('.notification a.close').click(function (e) {
    e.preventDefault();

    $(this).parent('.notification').fadeOut();
});

/*
 Check All Feature
 */
$(".check-all").click(function (e) {
    e.stopPropagation();
    if ($(this).is(':checked')) {
        $('input.child').prop('checked', true);
    }
    else {
        $('input.child').removeAttr('checked');
    }
});
// script for upload image
$('.user_pic').change(function (e) {
    $user_pic = e.target.files[0].name;
    $(".old_user_pic").val($user_pic);
});
/* Alert*/
jQuery(document).on('click', '.delete-btn', function () {
    console.log($('.child:checked').val());
    if (typeof $('.child:checked').val() != 'undefined') {
        var test = confirm("Are you sure you want to delete this Record?");
        if (test == true) {
            return true;
        } else {
            return false;
        }
    }
});
/* Alert*/
jQuery(document).on('click', '.delete-btn-single', function () {
    var test = confirm("Are you sure you want to delete this Record?");
    if (test == true) {
        return true;
    } else {
        return false;
    }

});
// var selected = [];
// function weight_valid(invoker,name, brand_name, state_id){
//     if(state_id == 26){
//         if(brand_name == 'DOMESTIC' || brand_name == 'IMPORTED' || brand_name == 'WINE'){
//                selected.push(invoker.value);
//             var sum = selected.reduce(function(a, b){
//         return parseInt(a) + parseInt(b);
//     }, 0);

//      if(sum > 2500){
//         alert('DOMESTIC, IMPORTED and WINE should be all sum <= 2500ml');
//         return false;
//      }


//     }
//     }
// }

        setTimeout(function() {
            $('.alert').hide('fast');
        }, 10000);


        function ShowTimes(IDS,flag) {
            var status = 'none';
            if (flag) { status = 'block'; }
            else{
                $("input[name='ad_pack_id2']:checkbox").prop('checked',false);
            }
            document.getElementById('paymentModeHide').style.display = status;
          }

          $(document).ready(function() {
            $('#Rank').bind('change', function() {
                var elements = $('div.estimated_time').children().hide(); // hide all the elements
                var value = $(this).val();

                if (value.length) {
                    if(value==0)
                    {// if somethings' selected
                    elements.filter('.airman').show(); // show the ones we want
                }
                }
            }).trigger('change');

            $('.second-level-select').bind('change', function() {
                var elements = $('div.second-level-container').children().hide(); // hide all the elements
                var value = $(this).val();

                if (value.length) { // if somethings' selected
                elements.filter('.airman').show(); // show the ones we want
                }
            }).trigger('change');
        });
